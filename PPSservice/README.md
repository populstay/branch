# PPSservice

PPSservice is the backend application for Populstay. It is written in Node.js.

- 两套系统，去中心化 中心化
- 视频交流，扣除PPS
- 智能锁


## Setting

### Database Connection / DB链接建立 ：
Path / 路径:
- /PPSService/config/connections.js;
- /PPSService/config/models.js;

### 区域码配置：
1. 路径:   
/PPSService/config/configuration.js
2. 字段:  
districtCode

### 主合约配置：##
1. 路径:  
/PPSService/config/configuration.js
2. 字段:  
houseInfoList_contract_address;

### 主合约ABI地址配置：
1. 路径:  
/PPSService/config/configuration.js
2. 字段:  
houseInfoList_contract_path;

### PPS合约地址配置：
1. 地址:  
/PPSService/config/configuration.js
2. 字段:  
ppstoken_contract_address;

### PPS的ABI地址配置：
1. 地址:  
/PPSService/config/configuration.js
2. 字段:  
ppstoken_contract_path;

### 存币取币合约地址配置：
1. 地址:  
/PPSService/config/configuration.js
2. 字段:  
exchange_contract_address;

### 存币取币ABI配置：
1. 地址:  
/PPSService/config/configuration.js
2. 字段:  
exchange_contract_path;

### 预定合约的ABI配置：
1. 地址:  
/PPSService/config/configuration.js
2. 字段:  
order_contract_path;

### 通信服务器地址配置：
1. 地址:  
/PPSService/config/configuration.js
2. 字段:  
Message_Server;

### 通信计费配置：
1. 地址:  
/PPSService/config/configuration.js
2. 字段:  
Communication_Price;


## Installation

PPSservice requires [Node.js](https://nodejs.org/) v6+, [npm](https://www.npmjs.com/), and [sails](https://sailsjs.com/) installed in your machine to run.

PPSservice is using MongoDB for the database. In order to connect to the db, you need to download [Studio 3T](https://studio3t.com/), and
connect it to our server. Ask the administrator for db's configuration.

First, make sure to download the correct version of Sails. This project only support Sails 0.12. If higher version is install, please uninstall it.

```sh
    npm install sails@0.12 -g
```

Next, run the following command in your terminal to download all the node dependencies:

```sh
    npm install
```

To start the server:

```sh
    saits lift
```

PPSservice will be running on port 1337



## DB Modules （数据模块）

### Billing (通信计费)

计费唯一标识  
id: { type: 'string', required: true },

是否付费  
charged:{ type:'boolean', defaultsTo:false }

创建时间  
createdAt: { type: 'date', required: true }

通信截止时间  
endtime: { type: 'string' },

用于识别用户是直接关闭浏览器切断连接还是通过按钮切断连接  
flag: { type: 'string' },

用户的以太账户地址  
guestAddress: { type: 'string',  required: true },

通信的socketid的连接时间  
socketid:{ type: 'string', required: true },

通信开始时间  
starttime: { type: 'string' },

通信需要消耗的token数量  
tokens:{ type: 'string' },

最后更改时间  
updatedAt: { type: 'date', required: true }


### Book （预定）

预定信息的唯一标识  
id: { type: 'number', autoIncrement: true, primaryKey: true }

对房源的评价星级  
accuracyStar:{ type:"int" },

对房源的评价星级  
checkinStar:{ type:"int" },

checkin以太提交事物  
checkinTransaction:{ type: 'string' },

checkin数字签名后的方法  
checkinTransactionData:{ type:'string' },

checkin以太提交事物  
checkinTxhash:{ type:'string' },

对房源的评价星级  
cleanlinessStar:{ type:"int" },

对房源的评价星级  
communicationStar:{ type:"int" },

创建时间  
createdAt: { type: 'date', required: true }

入住天数  
days: { type: 'int' },

以太坊价格  
ethprice: { type: 'string' },

入住日期  
from: { type: 'string', required: true },

房客以太地址  
guestaddress: { type: 'string', required: true },

房东以太地址  
hostaddress: { type: 'string', required: true },

房源标识  
houseinfoid: { type: 'string', required: true },

对房源的评价星级  
locationStar:{ type:"int" },

支付码，用于标示法币支付  
paymentcode:{ type: 'string' },

PPS价格  
price: { type: 'int' },

对房源的差评原因  
reasonforbadcomment:{ type:"string" }

订单状态 2表示提交成功 4表示check in 6标示cancel  
state: {
     type: 'int' //0 submit  1 mined  2 pay by usd or mining is over
},

退房日期  
to: { type: 'string', required: true },
 
Book以太提交事物  
transaction: { type: 'string' },

Book数字签名后的方法  
transactionData: { type: 'string' },

???  
transferTransactionData: { type: 'string' }

???  
transferTxState: { type:"int" }

Book以太提交事物  
txhash: { type: 'string' },

最后更改时间  
updatedAt: { type: 'date', required: true }

美元价格  
usdprice: { type: 'string' },

对房源的评价星级  
valueStar:{ type:"int" },


### EmailSender (发送邮件，该功能服务于www.populstay.com的contact功能)

发送邮件的唯一标识别符  
id: { type: 'number', autoIncrement: true, primaryKey: true },

创建时间  
createdAt: { type: 'date', required: true }

发送邮件的描述信息  
emailinfo:{ type:'json' }

发送邮件所显示的地址FROM栏目  
from: { type: 'string' },

存入DB的客户名称  
name:{ type: 'string' },

发送邮件所显示的标题  
subject: { type: 'string' },

存入DB的客户电话  
telephone: { type: 'string' },

发送邮件所显示信息  
text: { type: 'string' },
 
发送邮件所显示的地址TO栏目  
to: { type: 'string' },

最后更改时间  
updatedAt: { type: 'date', required: true }


### EmailVerify ()

数据库字段的唯一表识符  
id: { type: 'number', autoIncrement: true, primaryKey: true },

需要验证的code  
code:{ type: 'string' }

创建时间  
createdAt: { type: 'date', required: true }

最后更改时间  
updatedAt: { type: 'date', required: true }

需要验证的邮箱  
verifyEmail: { type: 'email', required: true },


### HouseInformation （房源）

房源数据唯一定位标识：  
id: { type: 'string', required: true, primaryKey: true },

该房源的已经预定日期  
bookedDate: { type:'json' },

创建时间  
createdAt: { type: 'date', required: true }

描述
description: { type:'json'}.

区域码  
districeCode: { type:'string' },

房源的以太坊价格  
ethprice:{ type:'string' },

是否生成智能合约  
generateSmartContract: { 
  type:'int'  //0 no smart contract   1 smart contract
 },

客人数量  
guests: { type:'int' },

房东的以太地址  
hostAddress: { type:'string', required:true },

房源的byt32标示，与智能合约标识一致，可以转化为IPFS的ID  
houseinfo: { type: 'json' },

房源地址  
place:{  type:'string' },

房源的PPS价格  
price: { type:'int' },

房源的首页描述  
profile: {  type:'json' },
 
房源信息的简单描述  
roominfo: { type:'json' }

状态  
state： { type:'int' },

以太坊提交事物的Tx  
transaction: { type: 'string' },

发布该房源的智能合约调用方法，经过PK加密  
transactionData:{ type:'string' },

以太坊提交事物的Tx  
txhash: { type:'string' },

最后更改时间  
updatedAt: { type: 'date', required: true }

房源的USD价格  
usdprice:{ type:'string' },


### Message (该表用于记录数据通信的message，按照设计，该表数据是保存与redis模块之中，有一定的过期时间)

字段的唯一标识符号  
id: { type: 'string', primaryKey: true, defaultsTo: uuidv1() },

创建时间  
createdAt: { type: 'date', required: true }

单条message的内容  
text: { type: 'string' }

最后更改时间  
updatedAt: { type: 'date', required: true }


### Register （注册表）

注册表的唯一标志id:  
id: { type: 'string', required: true, primaryKey: true },

以太账户  
account: { type: 'string', required:true },

创建时间  
createdAt: { type: 'date', required: true }

用户邮件  
email: { type: 'email', required: true, unique: true },

加密密码  
encryptedPassword: { type: 'string', required: true }

加密主键  
encryptedPK: { type: 'string', required: true }

用户电话  
phone: { type: 'string' }

最后更改时间  
updatedAt: { type: 'date', required: true }

用户名称，目前主要用email登录，但后续需求中也需要用用户名完成登录  
user: { type: 'string', required: true },


### Withdraw (提币)

提币一条记录的唯一标识符
id: { type: 'string', required: true, primaryKey: true },

申请提币的账号
account: { type: 'string', required: true },

申请提币的目标地址
applyAddress: { type:'string', required:true },

申请提币的以太坊交易TX
applyTransaction:{ type: 'string' },

申请提币的经过数字签名的以太坊交易数据
applyTransactionData:{ type:'string' },

申请提币的以太坊交易TX
applyTxhash:{ type:'string' },

创建时间  
createdAt: { type: 'date', required: true }

申请提币的数量
size: { type:'int', required: true },

申请提币的状态
state: { type:'int', defaultsTo: 0 },

批准提币的TX
transaction:{ type: 'string' },

最后更改时间  
updatedAt: { type: 'date', required: true }

批准提币的TX ##
txhash:{ type:'string' },

批准提币的经过数字签名的以太坊交易数据 ##
transactionData:{ type:'string' }


## Other Record

test private key:
94dd52158675184d833e5b0aec1b6de7fdba193313c8650814324f648b259c6f


source:f7087ae10d350501be73b94705d001ff04d10317


test private key:
94dd52158675184d833e5b0aec1b6de7fdba193313c8650814324f648b259c6f
