
module.exports = {

  attributes: {
       id: {
         type: 'number',
         autoIncrement: true,
         primaryKey: true
     },
     price: {
         type: 'int'
     },
     ethprice: {
         type: 'string'
     },
     usdprice: {
         type: 'string'
     },
     paymentcode:{
        type: 'string'
     },
     orderContractAddress:{
        type: 'string'
     },
     state: {
         type: 'int' //0 submit  1 mined  2 pay by usd or mining is over
     },
     from: {
         type: 'string',
         required: true
     },
     to: {
         type: 'string',
         required: true
     },
     days: {
         type: 'int'
     },
     houseinfoid: {
         type: 'string',
         required: true
     },
     guestaddress: {
         type: 'string',
         required: true
     },
     hostaddress: {
         type: 'string',
         required: true
     },
     transaction: {
         type: 'string'
     },
    txhash: {
        type: 'string'
    },
    transactionData: {
        type: 'string'
    },
    
    checkinTransaction:{
        type: 'string'
    },
    checkinTxhash:{
        type:'string'
    },        
    checkinTransactionData:{
        type:'string'
    },
    comment:{
        type:"string"
    },
    accuracyStar:{
         type:"int"
    },
    locationStar:{
         type:"int"
    },
    communicationStar:{
         type:"int"
    },
    checkinStar:{
         type:"int"
    },
    cleanlinessStar:{
         type:"int"
    },
    valueStar:{
         type:"int"
    },
    reasonforbadcomment:{
        type:"string"
    }

  }
};

