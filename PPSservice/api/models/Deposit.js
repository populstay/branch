/**
 * Deposit.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	  	id: 
  	{
            type: 'string',
            required: true,
            primaryKey: true
    },
    balance:
    {
    		type:'int',
    		required: true
    },
    history:
    {     
	    	type:'json',
	        required : true
    },
    txhash:{
          type:'string',
          //required:true
    },
    state:{
         type:'int',
         defaultsTo: 0
    }

  }
};

