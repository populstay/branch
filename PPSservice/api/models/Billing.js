/**
 * Billing.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	 id: {
         type: 'string',
         required: true
     },
     flag: {
         type: 'string'
     },
     guestAddress: {
         type: 'string', 
         required: true
     },
     starttime: {
         type: 'string'
     },
     endtime: {
         type: 'string'
     },
     socketid:{
     	type: 'string',
        required: true
     },
     tokens:{
     	type: 'string'
     },
     charged:{
        type:'boolean',
        defaultsTo:false
     }


  }
};
