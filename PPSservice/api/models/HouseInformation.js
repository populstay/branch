/**
 * HouseInformation.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


module.exports = {

    attributes: {
        id:
            {
                type: 'string',
                required: true,
                primaryKey: true
            },
        price:
            {
                type:'int'
            },
        ethprice:{
            type:'string'
        },
        usdprice:{
            type:'string'
        },
        houseinfo:
            {
                type: 'json'
            },
        districeCode:{
            type:'string'
        },
        bookedDate:
            {
                type:'json'
            },
        guests:
            {
                type:'int'
            },
        place:{
            type:'string'
        },
        hostAddress:
            {
                type:'string',
                required:true
            },
        profile:{
            type:'json'
        },
        transaction:{
            type: 'string'
        },
        txhash:{
            type:'string'
        },
        transactionData:{
            type:'string'
        },
        generateSmartContract:{
            type:'int'  //0 no smart contract   1 smart contract
        },
        roominfo:{
            type:'json'
        }

    }
};

