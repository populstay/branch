/**
 * GuestRegister.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        id: {
            type: 'string',
            required: true,
            primaryKey: true
        },
        user:{
            type: 'string',
            required: true
        },
        account:{
            type: 'string',
            required: true
        },
        email: {
            type: 'email',
            required: true,
            unique: true
        },
        phone:{
            type: 'string',
            required: true
        },
        profile: {

        }
    }
};

