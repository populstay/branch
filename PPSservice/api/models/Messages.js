/**
 * Messages.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 const uuidv1 = require('uuid/v1');

module.exports = {

  attributes: {

	id: {
		type: 'string',
		primaryKey: true,
		defaultsTo: uuidv1()

	},
	text: {
		type: 'string'
	}


  }
};

