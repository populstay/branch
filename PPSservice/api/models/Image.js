/**
 * Image.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        id:{
            type: 'number',
            autoIncrement: true,
            primaryKey: true
        },
        data:{
            type: 'string', //'base64',
            required: true
        },
        postDate:{
            type: 'string',
            defaultTo: (new Date().getTime()).toString()
        }
    }
};

