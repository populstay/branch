/**
 * Withdraw.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
  	id: 
  	{
		type: 'string',
		required: true,
		primaryKey: true
    },
    applyAddress:
    {
    	type:'string',
        required:true
    },
    size:
    {
		type:'int',
		required: true
    },
    account: 
  	{
		type: 'string',
		required: true
    },
    state:{
		type:'int',
		defaultsTo: 0
    },

    applyTransaction:{
		type: 'string'
	},

	applyTxhash:{
		type:'string'
	},
	        
	applyTransactionData:{
		type:'string'
	},

	transaction:{
		type: 'string'
	},
	txhash:{
		type:'string'
	},        
	transactionData:{
		type:'string'
	}
  }
};

