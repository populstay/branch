/**
 * Apply.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	id: 
  	{
        type: 'string',
        required: true,
        primaryKey: true
    },
    account:
    {
	    type:'string',
	    required: true
    },
    size:
    {
		type:'int',
		required:true
    },
    applyAddress:
    {
        type:'string',
        required: true
    },
    withdrawId:
    {
        type:'string'
    },
    transaction:
    {
    	type:'string'

    },
    state:
    {
    	type:'int',
    	defaultsTo: 0
    }

  }
};

