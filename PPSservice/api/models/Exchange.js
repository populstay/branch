/**
 * Exchange.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
            // params.approveTransactionData = approveTransactionData;
            // params.depositTransactionData = '0x' + serializedTx;
            // params.id                     = id;
            // params.balance                = size;
            // params.account                = window.address;
module.exports = {

  attributes: {
  	id: 
  	{
            type: 'string',
            required: true,
            primaryKey: true
    },
    account:
    {
    	    type:'string',
    	    required: true
    },
    balance:
    {
    		type:'int'
    },
    approveTransactionData:
    {     
	    	type:'string',
	        required : true
    },
    approveHash:
    {     
	    	type:'string'
    },
    approveTransactionLog:
    {     
	    	type:'json'
    },
    depositTransactionData:{
          type:'string',
          required:true
    },
    depositHash:{
          type:'string'
    },
    depositTransactionLog:{
          type:'json'
    },
    state:{
         type:'int',
         defaultsTo: 0
    },
    recordInfo:{
    	type:'json'
    }

  }
};

