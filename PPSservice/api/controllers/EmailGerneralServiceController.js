/**
 * EmailGerneralServiceController
 *
 * @description :: Server-side logic for managing emailgerneralservices
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res){
	   var mailData = req.body;


	    MailService
	    .generateMailHtml(
	    					mailData.metadataCode,
	    	                mailData.value1,
	    	                mailData.value2,
	    	                mailData.value3,
	    	                mailData.value4
	    	              )
	    .then((html)=>{

	   	 var mailOptions = {
								from: mailData.from,
								  to: mailData.to,
							 subject: mailData.subject,
								html: html
						    };
        console.log(mailOptions);
        MailService
        .sendMail(mailOptions)
        .then((info)=>{
	        res.json({emailinfo:info});
        });

	   }).catch((err)=>{
	   	 console.log(err);
	   })
	}
	
};

