module.exports = {
    create: function(req, res) {
        let houseinfo = req.body;

        if(houseinfo.generateSmartContract === "0" || houseinfo.generateSmartContract === 0)
        {
            houseinfo.state  = -1;

            HouseInformation.create(houseinfo).exec(function(err, data) {
                if (err || !data) {
                    sails.log.error(err);
                } else {
                    res.json(data);
                }
            })
        }
        else if( houseinfo.generateSmartContract === "1" || houseinfo.generateSmartContract === 1)
        {
            Web3Service.ranTransaction('0x' + req.body.transactionData)
                .then((result)=>{
                    houseinfo.txhash = result;
                    houseinfo.state  = 0;
                    HouseInformation.create(houseinfo).exec(function(err, data) {
                        if (err || !data) {
                            sails.log.error(err);
                        } else {
                            res.json(data);
                        }
                    });
                })
                .catch((err)=>{

                });
        }
    },

    findByHostID: (req, res) => {

        let searchCondition ={
            id: req.query.id
        };

        HouseInformation
            .find(searchCondition)
            .exec(function (err, record) {
                let result = [];

                if (record && record.length > 0)
                {
                    res.json(record[0]);
                } else {
                    res.json(result);
                }
            });

    },

    removeDisabledDate: function(req, res) {
        let dateObj ={};

        if( !req.body.houseinfoid )
        {
            res.json({error:"houseinfoid_not_existed"});
            return;
        }
        else
        {
            dateObj.houseinfoid = req.body.houseinfoid;
        }

        if( !req.body.from )
        {
            res.json({error:"date_from_not_existed"});
            return;
        }
        else
        {
            dateObj.from = req.body.from;
        }

        if( !req.body.to )
        {
            res.json({error:"date_from_not_existed"});
            return;
        }
        else
        {
            dateObj.to = req.body.to;
        }

        let query = HouseInformation.find({id:dateObj.houseinfoid});
        query.exec( (err, records) =>{
            if(records && records[0])
            {
                let bookedDateObj = records[0];
                let dateArray = bookedDateObj.bookedDate.data;
                let result =[];

                for (let i = 0; i < dateArray.length; i++) {
                    if(!(dateArray[i].start === dateObj.from && dateArray[i].end === dateObj.to))
                    {
                        result.push(dateArray[i]);
                    }
                }

                bookedDateObj.bookedDate.data = result;
                HouseInformation
                    .update({id:dateObj.houseinfoid},{bookedDate:bookedDateObj.bookedDate})
                    .exec((err, updatedRecord) =>
                    {
                        if(updatedRecord)
                        {
                            console.log("orginal bookedDate",dateArray);
                            console.log("updated bookedDate",updatedRecord.bookedDate);
                            res.json({BookedDate:result});
                        }

                    });
            }
        });

    },

    recommand: function(req, res) {
        let recommandCondition = {};
        recommandCondition.districeCode = req.query.districeCode;

        let query = HouseInformation.find(  { where: recommandCondition, limit: 6 });
        query.exec(function (err, records) {
            let result = [];
            if(records || records.length > 0)
            {
                for( let i=0;i< records.length; i++ )
                {
                    delete records[i].profile;
                    result.push(records[i]);
                }

            }

            res.json(result);

        });
    },

    search: function(req, res) {

        let searchCondition = {
            place: (req.query.place || { $ne: "" }),
            guests: { '>=': (req.query.guests || '0') },
            price:  {'<=': (req.query.price || '99999') },
            "description.roomdescription_homeorhotel": (req.query.roomdescription_homeorhotel || { $ne: "" }),
            "description.roomdescription_type": (req.query.roomdescription_type || { $ne: "" }),
            "description.roomdescription_guests_have": req.query.roomdescription_guests_have || { $ne: "" },
            "description.roomdescription_forguestorhost": req.query.roomdescription_forguestorhost || { $ne: "" },
            "description.roombasics_guestsnumber": { '>=': (req.query.queryroombasics_guestsnumber || 0) },
            "description.roombasics_guestbedrooms": { '>=': (req.query.roombasics_guestbedrooms || 0) },
            "description.roombasics_guestbeds": { '>=': (req.query.roombasics_guestbeds || 0) },
            // "description.roombasics_guestbathroom": { '>=': (req.query.roombasics_guestbathroom || 0) }, // ?
            // "description.bathroom_private": (req.query.bathroom_private || { "$regex": "" }), // ?
            "description.roombasics_Add_beds": { '>=': (req.query.roombasics_Add_beds || 0) },
            // "description.roombasics_Double_bed": (req.query.roombasics_Double_bed || { "$regex": "" }), // ?
            // "description.roombasics_Childrens_bed": (req.query.roombasics_Childrens_bed || { "$regex": "" }), // ?
            // "description.roombasics_Sofa_bed": (req.query.roombasics_Sofa_bed || { "$regex": "" }), // ？
            // "description.roombasics_Air_mattress": (req.query.roombasics_Air_mattress || { "$regex": "" }), // ？
            // "description.roombasics_Baby_cot": (req.query.roombasics_Baby_cot || { $ne: "" }), // ?
            // "description.roombasics_Hammock": (req.query.roombasics_Hammock || { $ne: "" }), // ?
            // "description.roombasics_Water_bed": (req.query.roombasics_Water_bed || { $ne: "" }), // ?
            "description.roomstuff_Country": (req.query.roomstuff_Country || { $ne: "" }),
            "description.roomstuff_Street": (req.query.roomstuff_Street || { $ne: "" }),
            "description.roomstuff_Apt": (req.query.roomstuff_Apt || { $ne: "" }),
            "description.roomstuff_City": (req.query.roomstuff_City || { $ne: "" }),
            "description.roomstuff_ZIPCode": (req.query.roomstuff_ZIPCode || { $ne: "" }),
            // "description.position": (req.query.position || { "$regex": "" }), // ？
            "description.roomstuff_Essentials": { '>=': (req.query.roomstuff_Essentials || 0) },
            "description.roomstuff_Shampoo": { '>=': (req.query.roomstuff_Shampoo || 0) },
            "description.roomstuff_Closet_drwers": { '>=': (req.query.roomstuff_Closet_drwers || 0) },
            "description.roomstuff_TV": { '>=': (req.query.roomstuff_TV || 0) },
            "description.roomstuff_Heat": { '>=': (req.query.roomstuff_Heat || 0) },
            "description.roomstuff_aircondition": { '>=': (req.query.roomstuff_aircondition || 0) },
            "description.roomstuff_breakfastcoffetea": { '>=': (req.query.roomstuff_breakfastcoffetea || 0) },
            "description.roomstuff_desk_workspace": { '>=': (req.query.roomstuff_desk_workspace || 0) },
            "description.roomstuff_fireplace": { '>=': (req.query.roomstuff_fireplace || 0) },
            "description.roomstuff_iron": { '>=': (req.query.roomstuff_iron || 0) },
            "description.roomstuff_hairdryer": { '>=': (req.query.roomstuff_hairdryer || 0) },
            "description.roomstuff_petsinhouse": { '>=': (req.query.roomstuff_petsinhouse || 0) },
            "description.roomstuff_private_entrance": { '>=': (req.query.roomstuff_private_entrance || 0) },
            // "description.roomstuff_wifi": (req.query.roomstuff_wifi || { "$regex": "" }), // ?
            // "description.roomstuff_smoke_detector": (req.query.roomstuff_smoke_detector || { $ne: "" }),
            "description.roomstuff_Pool": { '>=': (req.query.roomstuff_Pool || 0) },
            "description.roomstuff_kitchen": { '>=': (req.query.roomstuff_kitchen || 0) },
            "description.roomstuff_washer": { '>=': (req.query.roomstuff_washer || 0) },
            "description.roomstuff_dryer": { '>=': (req.query.roomstuff_dryer || 0) },
            "description.roomstuff_Park": { '>=': (req.query.roomstuff_Park || 0) },
            "description.roomstuff_Lift": { '>=': (req.query.roomstuff_Lift || 0) },
            "description.roomstuff_HotTub": { '>=': (req.query.roomstuff_HotTub || 0) },
            "description.roomstuff_Gym": { '>=': (req.query.roomstuff_Gym || 0) }

            //skip: req.query.skip,
            //limit: req.query.limit
        };

        if(
            ( req.query.limit !== null && req.query.limit !== undefined && req.query.limit !== "" &&  req.query.limit !== "null" )
            &&
            ( req.query.skip !== null && req.query.skip !== undefined && req.query.skip !== "" && req.query.skip !== "null" )
        ) {
            searchCondition.skip = req.query.skip;
            searchCondition.limit = req.query.limit;
        }

        console.log(searchCondition);

        HouseInformation.find(searchCondition).exec(function (err, records) {
            let result = [];

            if (records && records.length > 0)
            {
                for (let i = 0; i < records.length; i++)
                {
                    if (!BookService.checkBooked(records[i].bookedDate, req.query.from, req.query.to) )
                    {
                        delete records[i].profile;
                        result.push(records[i]);
                    }
                }
            }

            res.json(result);
        });
    }
};

