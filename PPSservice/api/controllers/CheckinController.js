/**
 * CheckinController
 *
 * @description :: Server-side logic for managing checkins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res) {
		var book = req.body;
		console.log(book);
		Web3Service
		.ranTransaction( book.transactionData )
	    .then((hash)=>{
					book.checkinTxhash = hash;
					book.state  = 3; 
					console.log(book);
					Book
					.update(
				        	 {
				        	   from : book.from, 
				        	 	to  : book.to, 
				       houseinfoid  : book.houseinfoid
				        	 }
				        	,{ 
				        		checkinTransaction : book.transactionData,
				        		checkinTxhash      : book.checkinTxhash,
				        		state              : book.state,
				        	orderContractAddress   : book.address
				        	}
		        	)
		        	.exec(function(err, data) 
		        	{
						res.json(data);
					});
		});
	}
	
};

