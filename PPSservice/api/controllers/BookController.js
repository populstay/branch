module.exports = {

    create: (req, res) => {
        let book = req.body;

        HouseInformation
            .find({id:book.houseinfoid})
            .exec(function (err, record) {
                if(err || !record)
                {
                    sails.log.error(err);
                }
                else
                {
                    let houseinfo = record[0];

                    if( houseinfo && BookService.checkBooked(houseinfo.bookedDate , book.from, book.to))
                    {
                        return res.json({error:"booked"});
                    }

                    Web3Service.ranTransaction(book.transactionData)
                        .then((result)=>{
                            book.txhash = result;
                            book.state  = 1;
                            Book.create(book).exec(function(err, data) {
                                if (err || !data) {
                                    sails.log.error(err);
                                } else {
                                    res.json(data);
                                }
                            });

                            if(!houseinfo.bookedDate)
                            {
                                houseinfo.bookedDate      = {};
                            }
                            if (!houseinfo.bookedDate.data)
                            {
                                houseinfo.bookedDate.data = [];
                            }

                            houseinfo.bookedDate.data.push({ start: book.from,end :book.to});

                            HouseInformation.update(
                                {id:houseinfo.id}
                                ,{bookedDate:houseinfo.bookedDate}
                            )
                                .exec(function afterwards(err, updated){
                                    if(err)
                                    {
                                        sails.log.error(err);
                                    }else
                                    {
                                        sails.log.info("updated HouseINFO:"+updated)
                                    }
                                });

                        }).catch((err)=>{
                    });
                }
            });
    },

	findByHostID: (req, res) => {
    	let searchCondition = {
            houseinfoid: req.query.houseinfoid
		};

        if(
            ( req.query.limit !== null && req.query.limit !== undefined && req.query.limit !== "" &&  req.query.limit !== "null" )
            &&
            ( req.query.skip !== null && req.query.skip !== undefined && req.query.skip !== "" && req.query.skip !== "null" )
        ) {
            searchCondition.skip = req.query.skip;
            searchCondition.limit = req.query.limit;
        }

		Book.find(searchCondition).exec( (err, records) => {
			if(err) {res.json(err);}
			else {res.json(records);}
        });
	}
};

