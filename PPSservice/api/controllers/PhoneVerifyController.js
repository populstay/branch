// A zero-dependency module that loads environment variables from a .env file into process.env
require('dotenv').config();

// Google API Secret Key
const twilioApiKeyAccountSID = process.env.TWILIO_ACCOUNT_SID;
const twilioUSAuthToken = process.env.TWILIO_US_AUTH_TOKEN;
const twilioUSPhoneNumber = process.env.TWILIO_US_PHONE_NUMBER;

// The Twilio NodeJS helper library
const twilio = require('twilio');
const client = new twilio(twilioApiKeyAccountSID, twilioUSAuthToken);

/**
 * PhoneVerifyController
 *
 * @description :: Server-side logic for managing Phoneverifies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    sendVerificationCode: (req, res) =>
    {
        
        client.messages.create({
            body: 'Hello from Node!',
            to: '+16174801331',  // Text this number
            from: twilioUSPhoneNumber // From a valid Twilio number
        }).then((message) =>
        {
            console.log(message.sid);
            res(messahe);
        });
    }
};

