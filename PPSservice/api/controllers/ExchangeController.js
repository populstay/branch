/**
 * ExchangeController
 *
 * @description :: Server-side logic for managing Exchanges
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	deposit: function(req, res) {
	      var deposit = req.body; 
	      Exchange.create( deposit ).exec((err, data)=> {
			console.log(data);
			res.json(data);
		  });
	},

	approveWithdraw:function(req, res) {
	    var withdrawObj = req.body; 
	    Deposit.findOne({id:withdrawObj.account})
	    .exec(function(err, record) {

	      if(record)
	      {
	        
	        if( parseInt( record.balance ) < parseInt( withdrawObj.size ) )
	        {
	        		res.json({error:"balance_not_enough"});
	        }
	        else
	        {
				Web3Service.ranTransaction( withdrawObj.applyTransactionData )
				.then((data)=>{
					withdrawObj.applyTxhash = data;
					withdrawObj.state       = 0; 
					Withdraw.create( withdrawObj ).exec(function(err, data) {
					res.json(data);
					});

					
				});

	        }
	      }
	      else
	      { 
	       res.json({error:"account_not_existed"});   
	      }

	    });





	}
	
};

