/**
 * BillingController
 *
 * @description :: Server-side logic for managing billings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
module.exports = {

        checkBalance: function(req, res) {
            
            Deposit
            .findOne({
                        id: req.body.account
                    })
            .exec(function(err, data) {
                if(data)
                {
                    res.json(data);
                }
                else
                {
                    res.json({balance:0});
                }
                
            });
        },
        create: function(req, res) {

            var bill = req.body;
            bill.id = req.body.socketid;
            Billing.create(bill).exec(function(err, data) {

                if (err || !data) {
                    sails.log.error(err);
                } else {
                    res.json(data);
                }

            });

        },

        cut: function(req, res) {

            console.log("id: req.query.socketid",req.body.socketid);

                Billing
                    .findOne({
                        id: req.body.socketid
                    })
                    .exec(function(err, data) {
                           if(err || !data)
                           {
                            console.log("error happened ");
                            console.log("error : ",err);
                            console.log("data : ",data);

                            return;
                           }

                           if(data && data.charged)
                           {
                            return;
                           }
                          console.log("update00");

                            Billing.update({
                                    id: req.body.socketid
                                }, {
                                    endtime: new Date().getTime()
                                })
                                .exec(

                                    
                                    function afterwards(err, updated) {

                                        console.log("update11");

                                        if (err) {
                                            sails.log.error(err);
                                            return;
                                        } else {

                                            if (updated && updated.length > 0) {

                                                var long = parseInt(updated[0].endtime) - parseInt(updated[0].starttime);

                                                console.log("updated[0].guestAddress", updated[0].guestAddress);
                                                Deposit.findOne({
                                                        id: updated[0].guestAddress
                                                    })
                                                    .exec(function(err, record) {
                                                        console.log("update22");
                                                        if (record) {
                                                            console.log("record", record);

                                                            var item = {
                                                                size: parseInt(long / 1000),
                                                                oper: "communication"
                                                            };

                                                            record.history.data.push(item);
                                                            record.balance = parseInt(record.balance) - sails.config.configuaration.Communication_Price * parseInt(long / 1000);
                           

                                                            Deposit.update({
                                                                    id: record.id
                                                                }, record)
                                                                .exec(function afterwards(err, updated) {
                                                                    console.log("update33");
                                                                    Billing
                                                                        .update({
                                                                            id: req.body.socketid
                                                                        }, {
                                                                            charged: true
                                                                        })
                                                                        .exec((err, updated) => {
                                                                            sails.log.info(updated);
                                                                        });
                                                                });
                                                        }
                                                    });
                                            }
                                        }
                                    }
                                );


                        });

                    }

};







