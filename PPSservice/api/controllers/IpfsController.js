/**
 * IpfsController
 *
 * @description :: Server-side logic for managing ipfs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var fs = require("fs");
var mongo = require('mongodb');
var Grid = require('gridfs-stream');
var str = require('string-to-stream');
var toString = require('stream-to-string');
const MongoClient = require('mongodb').MongoClient;
const bs58 = require('bs58');


const url = 'mongodb://eric_dwj@localhost:27017/admin';
 
// Database Name
const dbName = 'admin';


MongoClient.connect(url, function(err, client) {
 
  const db = client.db(dbName);
 
   gfs = Grid(db,mongo);

});
 


module.exports = {
	create: function(req, res) {
  
    //console.log("###create#",req.params.id);
	gfs.exist({filename: req.body.id}, function (err, found) {
  		if( !found )
  		{
			
			console.log(req.body.id);

			var writestream = gfs.createWriteStream({
				filename: req.body.id
			});

			str(req.body.content).pipe(writestream);
  		}
	});
	},

	findone: function(req, res) {
    var ipfsfilename;
    if( (""+req.params.id).startsWith("0x") )
    {

	    const hashHex = "1220" + (""+req.params.id).slice(2);
	    const hashBytes = Buffer.from(hashHex, 'hex');
	    const hashStr = bs58.encode(hashBytes);
	    //console.log("###### 0x to IPFS",hashStr);
	    ipfsfilename = hashStr;
    }else
    {
    	ipfsfilename = req.params.id;
    }
	
	gfs.exist({filename: ipfsfilename }, function (err, found) {
		if( !found )
  		{
  			 res.notFound();

  		}else
  		{
			gfs.createReadStream({
				filename: ipfsfilename
			}).pipe(res);
		}

	});




	}	
	
	
};

