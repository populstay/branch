/**
 * EmailsenderController
 *
 * @description :: Server-side logic for managing emailsenders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res) {

		var mailData = req.body;

		var mailOptions = {
							from: mailData.from,
							  to: mailData.to,
						 subject: mailData.subject,
							text: mailData.text
							};

		// var mailOptions = {
		// 					from: "admin@populstay.com",
		// 					  to: 'eric_dwj@icloud.com',
		// 				 subject: "test",
		// 					text: "test"
		// 					};
		MailService.sendMail(mailOptions).then((info)=>{
			var mailObj ={};
			mailObj.from 	  = mailData.from;
			mailObj.to   	  = mailData.to;
			mailObj.subject   = mailData.subject;
			mailObj.text 	  = mailData.text;
			mailObj.telephone = mailData.telephone;
			mailObj.name	  = mailData.name;
			mailObj.emailinfo = info;
            
            Emailsender.create(mailObj).exec(function(err, data) {
             res.json(data);

			});
    	 });

	}
	
};



