module.exports = {

    reasonforbadcomment: function(req, res) {
    	var comment = req.body;

        console.log(req.body);
		if( !comment.id )
		{
			return  res.json({error:"id_not_existed"});
		}

		Book.update(
					{
						id : comment.id
					}
					,
					{
						reasonforbadcomment:comment.reason
					}
    	)
    	.exec(function(err, data) 
    	{
			res.json(data);
		});

    },
	create: function(req, res) {

		var comment = req.body;

		if( !comment.id )
		{
			return  res.json({error:"id_not_existed"});
		}

		if( comment.accuracyStar && (parseInt(comment.accuracyStar)>5 || parseInt(comment.accuracyStar)<0) )
		{
			return res.json({error:"accuracyStar_outof_range"});
		}

		if( comment.locationStar &&  ( parseInt(comment.locationStar)>5 || parseInt(comment.locationStar)<0 ))
		{
			return res.json({error:"locationStar_outof_range"});
		}

		if( comment.communicationStar && ( parseInt(comment.communicationStar)>5 || parseInt(comment.communicationStar)<0 ))
		{
			return res.json({error:"communicationStar_outof_range"});
		}

		if(  comment.checkinStar && ( parseInt(comment.checkinStar)>5 || parseInt(comment.checkinStar)<0))
		{
			return res.json({error:"checkinStar_outof_range"});
		}

		if(  comment.cleanlinessStar && ( parseInt(comment.cleanlinessStar)>5 || parseInt(comment.cleanlinessStar)<0))
		{
			return res.json({error:"cleanlinessStar_outof_range"});
		}

		if(  comment.valueStar && ( parseInt(comment.valueStar)>5 || parseInt(comment.valueStar)<0))
		{
			return res.json({error:"valueStar_outof_range"});
		}

		var data ={ 
					comment 			: comment.comment, 
					state   			: 5,
					accuracyStar		: comment.accuracyStar,
					locationStar       	: comment.locationStar,
					communicationStar  	: comment.communicationStar,
					checkinStar        	: comment.checkinStar,
					cleanlinessStar    	: comment.cleanlinessStar,
					valueStar          	: comment.valueStar
				  };
		


		Book.update(
				        	 {
				        	   id : comment.id,
				        	   state: '4'
				        	 }
				        	,
				        	data
    	)
    	.exec(function(err, data) 
    	{
			res.json(data);
		});

	}
	
};

