/**
 * MessagesController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var request = require('request');

module.exports = {
    connection: function(req, res) {
        if (!req.isSocket) {
            return res.badRequest();
        }

       

    var headers = {
        "content-type": "application/json"
    };

    var options = {
        url: sails.config.configuaration.Message_Server+"billing/checkBalance",
        method: 'POST',
        headers: headers,
        json: { account:req.query.account}
    };

    //console.log( options );

     request.post( options , function(error, response, body) {
            console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.

         if(parseInt(body.balance)<=0)
         {
            


            res.json({
                  connection: req.query.account,error:"balance_not_enough"
            });

         }else
         {
            sails.sockets.broadcast(
                                    req.query.host,
                                    'connection' + req.query.host, {
                                        connection: req.query.account,
                                        type: req.query.type,
                                        guestAddress: req.query.account
                                    }
            );

              res.json({
                  connection: req.query.account
            });

         }
        });



       

    },
    answerconnection: function(req, res) {

        if (!req.isSocket) {
            return res.badRequest();
        }

        sails.sockets.broadcast(
            req.query.host,
            'answerconnection' + req.query.account, {
                connection: req.query.account,
                type: req.query.type,
                guestAddress: req.query.account,
                socketid: sails.sockets.getId(req)
            }
        );

    var headers = {
        "content-type": "application/json"
    };

    var options = {
        url: sails.config.configuaration.Message_Server+"billing",
        method: 'POST',
        headers: headers,
        json: { flag:"start", guestAddress: req.query.account , starttime: new Date().getTime(),socketid: sails.sockets.getId(req)}
    };

    //console.log( options );

     request.post( options , function(error, response, body) {
            console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.

        });


        res.json({
            connection: req.query.account,socketid: sails.sockets.getId(req)
        });

    },

    leave: function(req, res) {

        if (!req.isSocket) {
            return res.badRequest();
        }

            var headers = {
        "content-type": "application/json"
    };

    var options = {
        url: sails.config.configuaration.Message_Server+"billing/cut",
        method: 'POST',
        headers: headers,
        json: { socketid:req.query.socketid }
    };

    //console.log( options );

     request.post( options , function(error, response, body) {
            console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.

        });


        res.json({
            cutconnection: "cut_connection"
        });


    },
    join: function(req, res) {

        if (!req.isSocket) {
            return res.badRequest();
        }

        //假如host的房间
        sails.sockets.join(req, req.query.host);

        //将当前用户加入在线列表
        Users.create({
                id: sails.sockets.getId(req),
                account: req.query.account,
                roomname: req.query.host
            })
            .exec((err, data) => {
                if (err || !data) {
                    sails.log.error(err);
                } else {
                    res.json(data);
                }
            });

        //查询当前Host是否在线
        //host上线
        if (req.query.account == req.query.host) {
            //console.log("current user is  host");
            sails.sockets.broadcast(
                req.query.host,
                'online' + req.query.host, {
                    online: req.query.host
                }
            );
        } else {

            //console.log("current user is not host");
            //查询host是否在线
            Users.find({
                    account: req.query.host
                })
                .exec(function(err, data) {
                    if (data == null || data == undefined || data.length == 0) {
                        //console.log("host is not on line");
                        sails.sockets.broadcast(
                            req.query.host,
                            'offline' + req.query.host, {
                                offline: req.query.host
                            });
                        return;
                    } else {
                        //console.log("host is on line");
                        sails.sockets.broadcast(
                            data[0].roomname,
                            'online' + data[0].roomname, {
                                online: data[0].account
                            }
                        );

                    }
                });
        }




    },

    tellhost: function(req, res) {


        if (!req.isSocket) {
            return res.badRequest();
        }

        //console.log("tellhost *",'hostlisten'+req.query.host);

        sails.sockets.broadcast(req.query.host, 'hostlisten' + req.query.host, {
            message: req.query.text,
            guest: req.query.guest
        }, req);
        return res.json({
            online: true
        });

    },
    tellguest: function(req, res) {

        if (!req.isSocket) {
            return res.badRequest();
        }

        //console.log("tellguest *",'guestlisten'+req.query.guest);

        sails.sockets.broadcast(req.query.host, 'guestlisten' + req.query.guest, {
            message: req.query.text
        }, req);
        return res.json({
            online: true
        });

    }

}