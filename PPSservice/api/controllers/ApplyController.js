/**
 * ApplyController
 *
 * @description :: Server-side logic for managing applies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    compressBase64Picture: function(req, res) {
        let base64Image = req.body.base64;

        ImageCompressService.compressBase64Picture(base64Image, res);
    }

};

