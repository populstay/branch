/**
 * PaymentController
 *
 * @description :: Server-side logic for managing payments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res) {
	      var book = req.body;

	      HouseInformation
		  .find({id:book.houseinfoid})
		  .exec(function (err, record) {
	        if(err || !record) 
	        {
	            sails.log.error(err);
	        }
	        else
	        {
	           var houseinfo = record[0];

               if( houseinfo && BookService.checkBooked(houseinfo.bookedDate , book.from, book.to))
               {
               	return res.json({error:"booked"});
               }

                    console.log("booked",book);
					book.state =-2;
					Book.create(book).exec(function(err, data) {
					if (err || !data) {
						sails.log.error(err);
					} else {
						res.json(data);
					}
					});

					if(!houseinfo.bookedDate) 
					{
                       houseinfo.bookedDate ={};
					}
					if (!houseinfo.bookedDate.data) 
					{
                       houseinfo.bookedDate.data =[];
					}

					houseinfo.bookedDate.data.push({ start: book.from,end :book.to});

			        HouseInformation.update(
								        	 {id:houseinfo.id}
								        	,{bookedDate:houseinfo.bookedDate}
		        							)
					.exec(function afterwards(err, updated){
					if(err)
					{
					sails.log.error(err);
					}else
					{
					sails.log.info("updated HouseINFO:"+updated)
					}
					});
	        }
            });
		}	
	
};

