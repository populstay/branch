/**
 * ConfigurationController
 *
 * @description :: Server-side logic for managing configurations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	update: function(req, res) {
		var updated ={};
		if(!req.body.id)
		{
			res.json({err:"id_not_existed"});
			return;
		}

		if(req.body.key)
		{
          updated.key = req.body.key;
		}

		if(req.body.value)
		{
          updated.value = req.body.value;
		}

		if(req.body.description)
		{
          updated.description = req.body.description;
		}


		Configuration.update(
		        	 {
		        	   id:req.body.id
		        	 }
		        	,updated
		        	)
		        	.exec(function(err, data) 
		        	{
						res.json(data);
					});
	}
};

