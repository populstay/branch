/**
 * WithdrawController
 *
 * @description :: Server-side logic for managing withdraws
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res) {
		var withdrawObj = req.body;
	    Deposit.findOne({id:withdrawObj.account})
	    .exec(function(err, record) {

	      if(record)
	      {
	        //console.log("wasCreated",record);
	        if( parseInt( record.balance ) < parseInt( withdrawObj.size ) )
	        {
	        		res.json({error:"balance_not_enough"});
	        }
	        else
	        {
				Web3Service.ranTransaction( withdrawObj.applyTransactionData )
				.then((data)=>{
					withdrawObj.applyTxhash = data;
					withdrawObj.state       = 0; 
					Withdraw.create( withdrawObj ).exec(function(err, data) {
						res.json(data);
					});
				});

	        }
	      }
	      else
	      { 
	       res.json({error:"account_not_existed"});   
	      }

	    });

	}
	
};

