const base64Img = require('base64-img');
const Jimp = require('jimp');

function ImageCompressionClass(originalPicture, response) {

    this.picture = originalPicture;
    this.response = response;
    this.compressedBase64Image = "";

    //compress originalPicture to smallSizePicture start
    this.compressBase64Picture = function() {
        let tempFileName = (new Date().getTime()).toString();

        let filepath = base64Img.imgSync('data:image/png;base64,' + this.picture, './static/tempImgs', tempFileName);

        Jimp.read(filepath)
            .then(img => {
                let oldFileName = (filepath.split("\\"))[2];
                let tempName = (oldFileName.split("."))[0];

                img
                    .scale(0.5) // resize to half
                    .quality(80) // set JPEG quality
                    .write('./static/tempImgs/small-'+tempName+'.jpg') // save
                    .getBase64(Jimp.MIME_JPEG, (error, data) => {
                        this.compressedBase64Image = data;
                        this.response.json(this.compressedBase64Image);
                    });
            });
    }

}

module.exports = {

    compressBase64Picture:function(originalPicture, response) {
        let ImageCompressionobJ = new ImageCompressionClass( originalPicture, response );
        ImageCompressionobJ.compressBase64Picture();
    }

};