var Web3 = require('web3');
var web3 =  new Web3( new Web3.providers.HttpProvider(sails.config.configuaration.web3_http_provider));
var fs = require('fs');
var hostInfoListPath = require('path').resolve(sails.config.appPath, sails.config.configuaration.houseInfoList_contract_path )
var hostInfoABI = JSON.parse(fs.readFileSync(hostInfoListPath, 'utf8')).abi;

module.exports = {

    ranTransaction: function(transactionData) {
        return new Promise((resolve, reject) => {
            web3.eth.sendSignedTransaction(transactionData, function(err, hash) {
              if (!err) {
                resolve(hash);
              }else
              {
                console.log(err);
                reject(err);
              }
          });

       })
    }
   


}