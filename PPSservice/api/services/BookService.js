let Web3      = require( 'web3' );
let web3      = new Web3( new Web3.providers.HttpProvider( sails.config.configuaration.web3_http_provider ));
let scheduler = require( 'node-schedule' );
let fs = require('fs');
let OrderPath = require('path').resolve(sails.config.appPath, sails.config.configuaration.order_contract_path )
let OrderABI = JSON.parse(fs.readFileSync(OrderPath, 'utf8')).abi;
let hostInfoListPath = require('path').resolve(sails.config.appPath, sails.config.configuaration.houseInfoList_contract_path )
let hostInfoABI = JSON.parse(fs.readFileSync(hostInfoListPath, 'utf8')).abi;

function CheckBookClass(bookdate,start,end)
{
	this.bookdate = bookdate;
	this.start    = start;
	this.end      = end;

	this.checkBooked = function()
	{

		let flag= false;
	  	if(!this.bookdate)
	  	{
	  	   return flag;
	  	}

	  	if(!this.bookdate.data)
	  	{
	  	   return flag;
	  	}
	  	
	  	for(let i=0;i< this.bookdate.data.length; i++ )
	  	{
	  		if(this.start < this.bookdate.data[i].end && this.start > this.bookdate.data[i].start)
	  		{
                flag = true;
                break;
	  		}

	  		if(this.end < this.bookdate.data[i].end && this.end > this.bookdate.data[i].start)
	  		{
                flag = true;
                break;
	  		}

	  		if( this.end > this.bookdate.data[i].end && this.start< this.bookdate.data[i].start)
	  		{
                flag = true;
                break;
	  		}
	  	}

	  	return flag;

	}


}


module.exports = {

	checkBooked:function(bookdate,start,end) {
        let CheckBookobJ = new CheckBookClass( bookdate , start, end );
		return CheckBookobJ.checkBooked();
    }
};