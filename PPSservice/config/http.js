module.exports.http = {

  middleware: {},
  bodyParser: (function () {
    var opts = {limit:'50mb'};
    var fn;

    // Default to built-in bodyParser:
    fn = require('skipper');
    return fn(opts);

  })
};