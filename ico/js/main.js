window.onload = function () {
  $(".header .navbar-nav>li").on('click', function() {
    $(this).addClass('active').siblings().removeClass("active");
  })

  var b = new Date();

  var date = b.getFullYear() + "/" + (b.getMonth() + 1) + "/" + b.getDate() + " " + b.getHours() + ":" + b.getMinutes() + ":" + b.getSeconds();

  var datetype = -b.getTimezoneOffset() / 60;

  var Countdowndate = "2018/09/16 00:00:00";

  var datejson = {
    timeText:Countdowndate,
    timeZone: datetype,
    width: $(".Countdowndiv ul").width(), //倒计时宽度
    dayTextNumber: 2, //倒计时天数数字个数
    textGroupSpace: 15, //天、时、分、秒之间间距
    reflection: 0, //是否显示倒影
    displayDay: !0, //是否显示天数
    displayHour: !0, //是否显示小时数
    displayMinute: !0, //是否显示分钟数
    displaySecond: !0, //是否显示秒数
    displayLabel: 0, //是否显示倒计时底部label
    onFinish: function() {}
  }
  $(".countdown").jCountdown(datejson);



  var userLanguage = getCookie("userLanguage");
  $('.loading').addClass('hidden');
  if(userLanguage == 'zh-CN'){
    $(".lock-design li.lock-design__avatar:odd").append("<p>房东</p>");
    $(".lock-design li.lock-design__avatar:even").append("<p>房客</p>");
    $(".lock-design__locker1").append('<img src="img/img_zh/lock-design-smarter-zh.png" />');
    $(".lock-design__locker2").append('<img src="img/img_zh/lock-zh.png" />');
    $(".road-Milestone img.milestone").attr('src','./img/img_zh/milestone_cn.png');
    $(".lock-design__process:even").css('background-image','url("../img/img_zh/lock-design-process1-zh.png")'); 
    $(".lock-design__process:odd").css('background-image','url("../img/img_zh/lock-design-process2-zh.png")'); 
    if(document.body.clientWidth >= 768){
      $(".road-map__content").addClass("road-map__content_zh");
    }else{
      $(".road-map__content").addClass("road-map__content_zh_s");
    }
  }else if(userLanguage == 'en'){
   
    $(".lock-design li.lock-design__avatar:odd").append("<p>Host</p>");
    $(".lock-design li.lock-design__avatar:even").append("<p>Guest</p>");
    $(".lock-design__locker1").append('<img src="img/img_en/lock-design-locker.png" />');
    $(".lock-design__locker2").append('<img src="img/img_en/lock.png" />');
    $(".lock-design__process:even").css('background-image','url("../img/img_en/lock-design-process1.png")'); 
    $(".lock-design__process:odd").css('background-image','url("../img/img_en/lock-design-process2.png")'); 

    if(document.body.clientWidth >= 768){
      $(".road-map__content").removeClass("road-map__content_zh");
    }else{
      $(".road-map__content").removeClass("road-map__content_zh_s");
    }
  
  }
};

function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
  return pattern.test(emailAddress);
}

function isValidPhoneNumber(phoneNumber) {
  var pattern = new RegExp("^[0-9]*$");
  return pattern.test(phoneNumber);
}

$(function() {
  var userLanguage = getCookie("userLanguage");
  // Hide the loading if time exceeds 20 seconds
  var loading = setTimeout(function(){
    if (!$('.loading').hasClass('hidden')) {
      $('.loading').addClass('hidden');
      clearTimeout(loading);
    }
  }, 3000);

  $('#subscribeSubmit').on('click', function() {
    var inputValue = $('#subscribeEmail').val();

    $('.subscribe__email-validate-error').remove();

    if (!inputValue || inputValue === '') {
      if(userLanguage == 'zh-CN'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">邮箱为空，请输入！ </span>');
      }else if(userLanguage == 'en'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">Mailbox is empty, please enter! </span>');
      }else if(userLanguage == 'fr'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">La boîte aux lettres est vide, veuillez entrer!</span>');
      }else if(userLanguage == 'ja'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">メールボックスが空です。入力してください！</span>');
      }else if(userLanguage == 'ru'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">Почтовый ящик пуст. Пожалуйста, введите!</span>');
      }
    } else if (!isValidEmailAddress(inputValue)) {
      $('#subscribeEmail').focus();

      if(userLanguage == 'zh-CN'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">邮箱格式错误，请重新输入！</span>');
      }else if(userLanguage == 'en'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">Mailbox format error, please re-enter! </span>');
      }else if(userLanguage == 'fr'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">Le format de la boîte aux lettres est incorrect, veuillez ré-entrer!</span>');
      }else if(userLanguage == 'ja'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">メールボックスの形式が間違っています、再入力してください！</span>');
      }else if(userLanguage == 'ru'){
        $('.SICNUP div').after('<span class="subscribe__email-validate-error">Формат почтового ящика неверен, повторите ввод!</span>');
      }

    }else{
      $.post("https://server.populstay.com/generaldata",
      {
        code:'001',
        generalData:$('#subscribeEmail').val()
      },function(data){
        console.log(data)
        $("#prompt").show();
        $("#subscribeEmail").val("");
      });
    }
  });

  $("#prompt button").click(function(){
    $("#prompt").hide()
  })

  $("#DownloadSubmit").click(function () {
      var $parent = $(this).parent();
      var FirstName = $("#FirstName").val();
      var LastName = $("#LastName").val();
      var Emailaddress = $("#Emailaddress").val();
      var Message = $("#Message").val();


      var errorFirstName = "";
      var errorLastName = "";
      var errorEmailaddress = "";
      var errorMessage = "";



      $parent.find(".formtips").remove();


      if (FirstName == "" || FirstName == null || FirstName == undefined) {
        if(userLanguage == 'zh-CN'){
          errorFirstName = '请输入名.';
        }else if(userLanguage == 'en'){
          errorFirstName = 'Please enter the first Name.';
        }
        $("input[name='FirstName']").parent().append('<span class="formtips onError">'+errorFirstName+'</span>');

      }

      if (LastName == "" || LastName == null || LastName == undefined) {
        if(userLanguage == 'zh-CN'){
          errorLastName = '请输入姓.';
        }else if(userLanguage == 'en'){
          errorLastName = 'Please enter the last Name.';
        }

        $("input[name='LastName']").parent().append('<span class="formtips onError">'+errorLastName+'</span>');
      }

      if (Emailaddress == "" || Emailaddress == null || Emailaddress == undefined) {
        if(userLanguage == 'zh-CN'){
          errorEmailaddress = '请输入联系电子邮件.';
        }else if(userLanguage == 'en'){
          errorEmailaddress = 'Please enter the contact email.';
        }
      }else if (!isValidEmailAddress(Emailaddress)) {
        if(userLanguage == 'zh-CN'){
          errorEmailaddress = '请输入正确的邮箱.';
        }else if(userLanguage == 'en'){
          errorEmailaddress = 'Please enter correct mailbox.';
        }
      }
      $("input[name='Emailaddress']").parent().append('<span class="formtips onError">'+errorEmailaddress+'</span>');

      
      if (Message == "" || Message == null || Message == undefined) {
        if(userLanguage == 'zh-CN'){
          errorMessage = '请输入联系信息.';
        }else if(userLanguage == 'en'){
          errorMessage = 'Please enter the contact message.';
        }

        $("#Message").parent().append('<span class="formtips onError">'+errorMessage+'</span>');
      }

      if(!errorFirstName&&!errorLastName&&!errorEmailaddress&&!errorMessage) {
        $("#DownloadSubmit").attr("disabled",true);

        if(userLanguage == 'zh-CN'){
          $("#DownloadSubmit").html("请等待...")
        }else if(userLanguage == 'en'){
          $("#DownloadSubmit").html("Please wait...")
        }

          $.post("https://server.populstay.com/emailsender",
          {
            from:$("#Emailaddress").val(),
            to:'walter@populstay.com',
            subject:" ",
            text:$("#Message").val(),
            telephone:$("#Phone").val(),
            name:$("#FirstName").val() + " " +$("#LastName").val()
          },function(data){
          
             if(userLanguage == 'zh-CN'){
                $("#DownloadSubmit").html("下载白皮书")
              }else if(userLanguage == 'en'){
                $("#DownloadSubmit").html("Download the White paper")
            }

              $('.download .downloadform').hide();
              $("#FirstName").val("");
              $("#LastName").val("");
              $("#Emailaddress").val("");
              $("#Phone").val("")
              $("#Message").val("")
              $('.download .successfully').show();
              $("#DownloadSubmit").attr("disabled",false);

           });
      }
  })
});