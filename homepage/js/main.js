
window.onload = function () {
  var userLanguage = getCookie("userLanguage");
  $('.loading').addClass('hidden');
  if(userLanguage == 'zh-CN'){
    $(".lock-design__process:even").css('background-image','url("../img/img_zh/lock-design-process1.png")'); 
    $(".lock-design__process:odd").css('background-image','url("../img/img_zh/lock-design-process2.png")');
    $(".lock-design li.lock-design__avatar:odd").append("<p>房东</p>");
    $(".lock-design li.lock-design__avatar:even").append("<p>房客</p>");
    $(".lock-design__locker1").append('<img class="img2" src="img/img_zh/lock-design-smarter.png" />');
    $(".lock-design__locker2").append('<img class="img2" src="img/img_zh/lock.png" />');
    $(".communication .col-5:first-child img").attr("src", "../img/img_zh/communication-pic.png");
    
    if(document.body.clientWidth >= 768){
      $(".road-map__content").css('background-image','url("../img/img_zh/Roadmap.svg")'); 
    }else{
      $(".road-map__content").css('background-image','url("../img/img_zh/road-map-mobile.png")'); 
    }
  }else if(userLanguage == 'en'){
    $(".lock-design__process:even").css('background-image','url("../img/img_en/lock-design-process1.png")'); 
    $(".lock-design__process:odd").css('background-image','url("../img/img_en/lock-design-process2.png")');
    $(".lock-design li.lock-design__avatar:odd").append("<p>Host</p>");
    $(".lock-design li.lock-design__avatar:even").append("<p>Guest</p>");
    $(".lock-design__locker1").append('<img class="img2" src="img/img_en/lock-design-locker.png" />');
    $(".lock-design__locker2").append('<img class="img2" src="img/img_en/lock.png" />');
    $(".communication .col-5:first-child img").attr("src", "../img/img_en/communication-pic.png");

    if(document.body.clientWidth >= 768){
      $(".road-map__content").css('background-image','url("../img/img_en/Roadmap.svg")'); 
    }else{
      $(".road-map__content").css('background-image','url("../img/img_en/road-map-mobile.png")'); 
    }
  }else if(userLanguage == 'ja'){
    $(".lock-design__process:even").css('background-image','url("../img/img_ja/lock-design-process1.png")'); 
    $(".lock-design__process:odd").css('background-image','url("../img/img_ja/lock-design-process2.png")');
    $(".lock-design li.lock-design__avatar:odd").append("<p>ホスト</p>");
    $(".lock-design li.lock-design__avatar:even").append("<p>ゲスト</p>");
    $(".lock-design__locker1").append('<img class="img2" src="img/img_ja/lock-design-smarter.png" />');
    $(".lock-design__locker2").append('<img class="img2" src="img/img_ja/lock.png" />');
    $(".communication .col-5:first-child img").attr("src", "../img/img_ja/communication-pic.png");

    if(document.body.clientWidth >= 768){
      $(".road-map__content").css('background-image','url("../img/img_ja/Roadmap.svg")'); 
    }else{
      $(".road-map__content").css('background-image','url("../img/img_ja/road-map-mobile.png")'); 
    }
  }
 
};

function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
  return pattern.test(emailAddress);
}

function isValidPhoneNumber(phoneNumber) {
  var pattern = new RegExp("^[0-9]*$");
  return pattern.test(phoneNumber);
}

$(function() {
  var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      spaceBetween: 30,
      loop: true,
      navigation: {
        nextEl: '.blog__next-btn',
        prevEl: '.blog__prev-btn',
      },
    });
  
  var userLanguage = getCookie("userLanguage");
  // Hide the loading if time exceeds 20 seconds
  var loading = setTimeout(function(){
    if (!$('.loading').hasClass('hidden')) {
      $('.loading').addClass('hidden');
      clearTimeout(loading);
    }
  }, 3000);

  $('#subscribeSubmit').on('click', function() {
    var inputValue = $('#subscribeEmail').val();

    $('.subscribe__email-validate-error').remove();

    if (!inputValue || inputValue === '') {
      if(userLanguage == 'zh-CN'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">邮箱为空，请输入！ </span>');
      }else if(userLanguage == 'en'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">Mailbox is empty, please enter! </span>');
      }else if(userLanguage == 'fr'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">La boîte aux lettres est vide, veuillez entrer!</span>');
      }else if(userLanguage == 'ja'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">メールボックスが空です。入力してください！</span>');
      }else if(userLanguage == 'ru'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">Почтовый ящик пуст. Пожалуйста, введите!</span>');
      }
    } else if (!isValidEmailAddress(inputValue)) {
      $('#subscribeEmail').focus();

      if(userLanguage == 'zh-CN'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">邮箱格式错误，请重新输入！</span>');
      }else if(userLanguage == 'en'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">Mailbox format error, please re-enter! </span>');
      }else if(userLanguage == 'fr'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">Le format de la boîte aux lettres est incorrect, veuillez ré-entrer!</span>');
      }else if(userLanguage == 'ja'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">メールボックスの形式が間違っています、再入力してください！</span>');
      }else if(userLanguage == 'ru'){
        $('#subscribeEmail').after('<span class="subscribe__email-validate-error">Формат почтового ящика неверен, повторите ввод!</span>');
      }

    }else{
      $.post("https://server.populstay.com/generaldata",
      {
        code:'001',
        generalData:$('#subscribeEmail').val()
      });
      $("#prompt").show();
      $("#subscribeEmail").val("");
    }
  });

  $('#contactSubmit').on('click', function() {
    var $parent = $(this).parent();
    var _inputs = $('.contact-us :input');

    var errorMsgName = '';
    var errorMsgPhone = '';
    var errorMsgEmail = '';
    var errorMsgAddress = '';
    var errorMsgMessage = '';

    $parent.find(".formtips").remove();


    // Validate contact name
    if( _inputs.is('#contactName')){
      var _this = $('#contactName');

      if(!_this.val() || _this.val() === ''){
        if(userLanguage == 'zh-CN'){
          errorMsgName = '请输入名称.';
        }else if(userLanguage == 'en'){
          errorMsgName = 'Please enter the name.';
        }else if(userLanguage == 'fr'){
          errorMsgName = 'Veuillez entrer un nom';
        }else if(userLanguage == 'ja'){
          errorMsgName = '名前を入力してください。';
        }else if(userLanguage == 'ru'){
          errorMsgName = 'Введите имя.';
        }
        $('#contactName').parent().append('<span class="formtips onError">'+errorMsgName+'</span>');
      }
    }

    // Validate contact phone
    // if(_inputs.is('#contactPhone')){
    //   var _this = $('#contactPhone');

    //   if(!_this.val() || _this.val() === ''){
    //     if(userLanguage == 'zh-CN'){
    //       errorMsgPhone = '请输入联系电话.';
    //     }else{
    //       errorMsgPhone = 'Please enter the contact phone.';
    //     }
    //   } else if (!isValidPhoneNumber(_this.val())) {
    //     if(userLanguage == 'zh-CN'){
    //       errorMsgPhone = '联系电话应该是号码.';
    //     }else{
    //       errorMsgPhone = 'The contact phone should be number.';
    //     }
    //   }

    //   $('#contactPhone').parent().append('<span class="formtips onError">'+errorMsgPhone+'</span>');
    // }

    // Validate contact email
    if(_inputs.is('#contactEmail')){
      var _this = $('#contactEmail');

      if(!_this.val() || _this.val() === ''){
        if(userLanguage == 'zh-CN'){
          errorMsgEmail = '请输入联系电子邮件.';
        }else if(userLanguage == 'en'){
          errorMsgEmail = 'Please enter the contact email.';
        }else if(userLanguage == 'fr'){
          errorMsgEmail = "S'il vous plaît entrer un email de contact.";
        }else if(userLanguage == 'ja'){
          errorMsgEmail = '連絡先メールを入力してください。';
        }else if(userLanguage == 'ru'){
          errorMsgEmail = 'Введите контактное письмо.';
        }

      } else if (!isValidEmailAddress(_this.val())) {
        if(userLanguage == 'zh-CN'){
          errorMsgEmail = '请输入正确的邮箱.';
        }else if(userLanguage == 'en'){
          errorMsgEmail = 'Please enter correct mailbox.';
        }else if(userLanguage == 'fr'){
          errorMsgEmail = "Veuillez entrer l'adresse email correcte.";
        }else if(userLanguage == 'ja'){
          errorMsgEmail = '正しいメールアドレスを入力してください。';
        }else if(userLanguage == 'ru'){
          errorMsgEmail = 'Введите правильный адрес электронной почты.';
        }

      }

      $('#contactEmail').parent().append('<span class="formtips onError">'+errorMsgEmail+'</span>');
    }

     // Validate contact address
    //  if( _inputs.is('#contactAddress')){
    //   var _this = $('#contactAddress');

    //   if(!_this.val() || _this.val() === ''){
    //     if(userLanguage == 'zh-CN'){
    //       errorMsgAddress = '请输入联系地址.';
    //     }else{
    //       errorMsgAddress = 'Please enter the contact address.';
    //     }
    //     $('#contactAddress').parent().append('<span class="formtips onError">'+errorMsgAddress+'</span>');
    //   }
    // }

    // Validate contact message
    var message = $('#contactMessage');
    if(!message.val() || message.val() === ''){
      if(userLanguage == 'zh-CN'){
        errorMsgMessage = '请输入联系信息.';
      }else if(userLanguage == 'en'){
        errorMsgMessage = 'Please enter the contact message.';
      }else if(userLanguage == 'fr'){
        errorMsgMessage = "S'il vous plaît entrer des informations de contact.";
      }else if(userLanguage == 'ja'){
        errorMsgMessage = '連絡先情報を入力してください。';
      }else if(userLanguage == 'ru'){
        errorMsgMessage = 'Введите контактную информацию.';
      }

      $('#contactMessage').parent().append('<span class="formtips onError">'+errorMsgMessage+'</span>');
    }

    if (errorMsgName&&errorMsgPhone&&errorMsgEmail&&errorMsgAddress&&errorMsgMessage) {
      return false;
    }
    if(!errorMsgName&&!errorMsgEmail&&!errorMsgMessage){
      if($("#contactAddress").val() == ''){
          if(userLanguage == 'zh-CN'){
            var contactAddress = "潮箱"
          }else{
            var contactAddress = "PopulStay"
          }
      }else{
        var contactAddress = $("#contactAddress").val()
      }
      $.post("https://server.populstay.com/emailsender",
      {
        from:$("#contactEmail").val(),
        to:'walter@populstay.com',
        subject:contactAddress,
        text:$("#contactMessage").val(),
        telephone:$("#contactPhone").val(),
        name:$("#contactName").val()
      });
      $("#prompt").show();
      $("#contactEmail").val("")
      $("#contactAddress").val("")
      $("#contactMessage").val("")
      $("#contactPhone").val("")
      $("#contactName").val("")
    }
  });
  $("#prompt button").click(function(){
    $("#prompt").hide()
  })
});