pragma solidity ^0.4.18;

import './PreOrder.sol';
import './Token.sol';

contract HouseInfoListing{
    
    using SafeMath for uint;
    
    event orderid(address preOrder);
     
    address   public  tokenAddress;//tokenAddress used to pay 
    bytes32[] private districtcode;//district code
    address   private contractowner;
    uint      public  servicefeepercent = 30;////sevice fee by host which will send to Populstay,for example 15%,you should send 15
    uint      private gwei2wei=1000000000;
    
   function HouseInfoListing(address _tokenAddress)
   payable
   public{
       tokenAddress   = _tokenAddress;
       contractowner  = msg.sender; 
       districtcode.push("Tokyo");
   }
   
   
  struct HouseInfo {
    string  roominfo;
    uint    price;//PPS price
    uint    contractdatetime;
    uint    state;//0 close , 1 open
    address owner;
    uint    ethPrice;//Gwei
    uint    deadline;
    uint    forfeit;
  }
  
  mapping ( address => bytes32[] ) private hostRoomList;//every house info has one uuid,find house info by host address
  mapping ( bytes32 => HouseInfo ) private houseInfo;   //describ the house information
  mapping ( bytes32 => bytes32[] ) private uuids;       //every house info has one uuid,find house info by districtcode
                                                        //should add find house info by city street 
  //通过房屋信息uuid确定预定合约信息                                                        
  mapping ( bytes32 => address[] ) private PreOrders;   
                                                        //find preorders lists by house info uuid 
                                                        //find preOrder or order infomation from this connection 
  //通过房客address找到合约信息
  mapping (address => address[]) private GuestOrders;   //find guest orders by guest address
  //通过房东address找到合约信息
  mapping (address => address[]) private HouseOwnerOrders;//find house owner orders by house owner address
  
  modifier onlyOwner() {
    require (msg.sender == contractowner);
    _;
  }
  
  function getServiceFee(uint _size)
  public
  payable
  onlyOwner
  {
      contractowner.transfer(_size);
  }
  
  function setDistrictCode(bytes32 _districtcode) 
   public
   onlyOwner
  {
   
    districtcode.push(_districtcode);
   
  }
  
   function getDistrictCode() 
   public 
   view
   returns(bytes32[] _districtcode)
  {
    return districtcode;
  }
   
   function setServicefeePercent(uint _servicefeepercent) 
   public 
   onlyOwner
  {
    servicefeepercent = _servicefeepercent;
  }
  
  
  
  //preOrder by PPS
  //
  function preOrder( address _guestaddress,address _hostaddress, bytes32 _houseinfo, uint _from, uint _to, uint _days)
  payable
  public
  returns (address _contractaddress)
  {
        uint transferPrice = _days * houseInfo[_houseinfo].price;
        uint _deadline          = houseInfo[_houseinfo].deadline;
        uint _forfeit           = houseInfo[_houseinfo].forfeit;
        
        
        PreOrder preorder = new PreOrder( 
                                            tokenAddress , 
                                            _hostaddress , 
                                            _guestaddress , 
                                            _houseinfo , 
                                            _from , 
                                            _to,
                                            _days  , 
                                            0 , 
                                            transferPrice , 
                                            0 ,
                                            contractowner,
                                            servicefeepercent,
                                            _deadline,
                                            _forfeit
                                            );
                                            
        if(Token(tokenAddress).transferFrom(_guestaddress,preorder,transferPrice))//transfer token to contract address
        {
            PreOrders[_houseinfo].push(preorder); 
            GuestOrders[_guestaddress].push(preorder);
            HouseOwnerOrders[_hostaddress].push(preorder);
            orderid(address(preorder));
            return address(preorder);
        }
        return ;
  }
  
  modifier hasValueToPurchase( bytes32 _houseinfo, uint _days ) {
    require (msg.value >= ( _days.mul( houseInfo[_houseinfo].ethPrice ).mul(gwei2wei)  ));
    _;
  }

  
  //"0xafc28904fc9ffba207181e60a183716af4e5bce2","0xafc28904fc9ffba207181e60a183716af4e5bce2","0x7465737431000000000000000000000000000000000000000000000000000001",1034213789000,2035213789000,1
  function preOrderByEth( address _guestaddress,address _hostaddress, bytes32 _houseinfo, uint _from, uint _to, uint _days)
  public
  payable
  returns (address _contractaddress)
  {
    uint transferPrice     = _days .mul(houseInfo[_houseinfo].ethPrice.mul(103).div(100)) ;
    uint _deadline          = houseInfo[_houseinfo].deadline;
    uint _forfeit           = houseInfo[_houseinfo].forfeit;

    PreOrder preorder = new PreOrder(  
                                        tokenAddress, 
                                        _hostaddress , 
                                        _guestaddress , 
                                        _houseinfo , 
                                        _from ,
                                        _to , 
                                        _days , 
                                        0, 
                                        0, 
                                        transferPrice, 
                                        contractowner,
                                        servicefeepercent,
                                        _deadline,
                                        _forfeit
                                         );
    if(  address( preorder ).send( transferPrice.mul(gwei2wei) ) )
    {
        PreOrders[_houseinfo].push(preorder); 
        GuestOrders[_guestaddress].push(preorder);
        HouseOwnerOrders[_hostaddress].push(preorder);
        orderid(address(preorder));
        return address(preorder);
    }
    return;
 
  }
  
  //"0x7465737431000000000000000000000000000000000000000000000000000001",999,100000000,"roominfo","0x546f6b796f000000000000000000000000000000000000000000000000000000"
   function setHouseInfo(bytes32 _uuid,uint _price,uint _ethPrice,string _roominfo,bytes32 _districtcode) 
   public 
   returns(bool success)
  {
      uint _deadline = 864000000;
      uint _forfeit  = 100;
    houseInfo[_uuid] = HouseInfo(
      {
        roominfo: _roominfo,
        price   : _price,
        ethPrice: _ethPrice,
        contractdatetime:block.timestamp,
        owner   : msg.sender,
        state   : 1,
        deadline :_deadline,
        forfeit : _forfeit
      });
              
    uuids[_districtcode].push(_uuid);
    hostRoomList[msg.sender].push(_uuid);
    return true;
  }
  
  function getHostRoomLists(address _hostaddress)
    view
    public
    returns(bytes32[] _hostRoomList)
  {
    return hostRoomList[_hostaddress];
  }
    
    
  function getGuestOrders(address _guestaddress)
  view
  public
  returns (address[] _guestOrders)
  {
      return GuestOrders[_guestaddress];
  }
  
  function getHostOrders(address _hostaddress)
  view
  public
  returns (address[] _hostOrders)
  {
      return HouseOwnerOrders[_hostaddress];
  }
  
  function getPreorders(bytes32 _houseinfo)
  view
  public
  returns (address[] _preorders)
  {
      return PreOrders[_houseinfo];
  }
  
  function getUUIDS(bytes32 _districtcode)
    view
    public
    returns(bytes32[] _uuid)
  {
    return uuids[_districtcode];
  }
    
  function getHouseInfo(bytes32 _uuid)
    view
    public
    returns (uint _price,uint _ethPrice, uint _contractdatetime, address _owner,uint _state,string _roominfo,uint _deadline,uint _forfeit)
  {
    //check the contract list, the most important thing is that if state is 0, that means this house had been rented.
    HouseInfo storage hoseinfo = houseInfo[_uuid];
    
    return (
      hoseinfo.price,
      hoseinfo.ethPrice,
      hoseinfo.contractdatetime,
      hoseinfo.owner,
      hoseinfo.state,
      hoseinfo.roominfo,
      hoseinfo.deadline,
      hoseinfo.forfeit
    );
  }
 
}






    
