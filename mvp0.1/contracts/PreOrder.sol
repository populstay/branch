pragma solidity ^0.4.18;

import './SafeMath.sol';
import './Token.sol';


contract PreOrder{
    
    using SafeMath for uint;
    
    address public tokenAddress;
    address public hostaddress;
    address public guestaddress;
    address public populstayaddress;
    bytes32 public houseinfo;
    uint public from;
    uint public to;
    uint public rentDays;
    uint public status;//0:init preorder 1: confirm  2: cancel Order Without Pay 3:cancel order with pay  4:comment
    uint public price;
    uint public ethPrice;
    uint public oldprice;
    uint public oldethPrice;
    uint public servicefeepercent;
    uint public servicefeedenominator=1000;
    uint public gwei2wei=1000000000;
    uint public timestampformat = 1000;
    uint public onedaytimestamp = 864000000;
    uint public deadline;
    uint public forfeit;
    uint public forfeitdenominator=100;
    string public comment ="";
    string public star =""; 
    string public commentbyhost ="";
    string public starbyhost =""; 
    
    

   
    function () 
    public 
    payable {
    }
   
    function PreOrder (
                        address _tokenAddress, 
                        address _hostaddress,
                        address _guestaddress,
                        bytes32 _houseinfo,
                        uint _from,
                        uint _to,
                        uint _days,
                        uint _status,
                        uint _price,
                        uint _ethPrice,
                        address _populstayaddress,
                        uint _servicefeepercent,
                        uint _deadline,
                        uint _forfeit
                    ) 
    public                
    payable 
    {
        tokenAddress = _tokenAddress;
        hostaddress  = _hostaddress;
        guestaddress = _guestaddress;
        houseinfo    = _houseinfo;
        from         = _from;
        to           = _to;
        rentDays     = _days;
        status       = _status;
        price        = _price;
        ethPrice     = _ethPrice;
        populstayaddress     = _populstayaddress;
        servicefeepercent    = _servicefeepercent;
        deadline             = _deadline;
        forfeit              = _forfeit;
    }
    
        
    modifier onlyHost{
        require(msg.sender == hostaddress);
        _;
    }
    
    modifier onlyGuest{
        require(msg.sender == guestaddress);
        _;
    }
    
     modifier checkStatus{
        require(status == 0);
        _;
    }
    
    modifier checkApplytStatus{
        require(applyStatus == 1);
        _;
    }
    
    modifier checkComment{
        require(  status != 4 );
        _;
    }
    
    modifier checkCommentByHost{
        require(  status != 5 );
        _;
    }
    
    modifier checkCrossDeadline{
        require(deadline > from.sub(block.timestamp.mul(timestampformat)) );
        _;
    }
    
    modifier checkInDeadline{
        require(deadline <= from.sub(block.timestamp.mul(timestampformat)) );
        _;
    }
    
   function setComment( string _comment,string _star )
   onlyGuest
   checkComment
   public 
   {
    comment = _comment;
    star    = _star;
    status  = 4;
   }
   
   function setCommentByHost( string _comment,string _star )
   onlyHost
   checkCommentByHost
   public 
   {
    commentbyhost = _comment;
    starbyhost    = _star;
    status  = 5;
   }
    
    function getPreorderInfo()
    view
    public
    returns (
                address _tokenAddress, 
                address _owneraddress,
                address _guestaddress,
                bytes32 _houseinfo,
                uint _from,
                uint _to,
                uint _days,
                uint _status,
                uint _price,
                uint _ethPrice
            ) 
    {
    return (
        tokenAddress ,
        hostaddress ,
        guestaddress ,
        houseinfo    ,
        from         ,
        to           ,
        rentDays     ,
        status       ,
        price        ,
        ethPrice
    );
    }
    
    function cancelOrderWithoutPay()
    public
    payable
    onlyGuest
    checkStatus
    checkInDeadline
    {
         if( Token(tokenAddress).transfer(guestaddress,price) )
         {
            status = 2; 
         }
         
    }
    
    function cancelOrderWithPay()
    public
    payable
    onlyGuest
    checkStatus
    checkCrossDeadline
    {
        if( Token(tokenAddress).transfer(guestaddress,price.mul(forfeitdenominator-forfeit).div(forfeitdenominator)) 
         && Token(tokenAddress).transfer(hostaddress,price.mul(forfeit).div(forfeitdenominator)) )
        {
            status = 3;
        }
   }
    

    function cancelOrderByEthWithoutPay()
    public
    onlyGuest
    checkStatus
    checkInDeadline
    payable
    {
       
        if(guestaddress.send(ethPrice.mul(gwei2wei)))
        {
            status = 2;
        }
    }
    
    
    function cancelOrderByEthWithPay()
    public
    onlyGuest
    checkStatus
    checkCrossDeadline
    payable
    {
      
         
         if(guestaddress.send(ethPrice.mul(gwei2wei).mul(forfeitdenominator-forfeit).div(forfeitdenominator)) 
         && hostaddress.send(ethPrice.mul(gwei2wei).mul(forfeit).div(forfeitdenominator)) ) //transfer token to contract address
         {
            status = 3;
         }
    }
    
//     function confirmOrder()
//     public
//     payable
//     onlyGuest
//     checkStatus
//     {
//         uint toHostPrice      = price.mul( servicefeedenominator-servicefeepercent).div(servicefeedenominator);
//         uint toPopulstayPrice = price.mul( servicefeepercent ).div(servicefeedenominator);
        
//         if( 
//             Token(tokenAddress).transfer(hostaddress, toHostPrice) 
//         &&  Token(tokenAddress).transfer(populstayaddress, toPopulstayPrice)
//         )
//         {
//             status = 1;
//         }
         
//   }
   
//     function confirmOrderByEth()
//     public
//     onlyGuest
//     checkStatus
//     payable
//     {
//         uint totalPrice       = ethPrice.mul(gwei2wei);
//         uint toHostPrice      = totalPrice.mul( servicefeedenominator-servicefeepercent).div(servicefeedenominator);
//         uint toPopulstayPrice = totalPrice.mul( servicefeepercent ).div(servicefeedenominator);
        
//         if( hostaddress.send( toHostPrice ) && populstayaddress.send( toPopulstayPrice ) )//transfer token to contract address
//         {
//                 status = 1;
//         }
      
//     }



    
    
    function confirmOrderByEthByHost()
    public
    onlyHost
    payable
    {
        uint totalPrice;
        uint toHostPrice;
        uint toPopulstayPrice;
        
         if( block.timestamp.mul(timestampformat) < to )
         {
             totalPrice       = ethPrice.mul(gwei2wei) .mul ( block.timestamp.mul(timestampformat).sub(from)  ).div( to.sub(from) )  ;
             toHostPrice      = totalPrice.mul( servicefeedenominator-servicefeepercent).div(servicefeedenominator);
             toPopulstayPrice = totalPrice.mul( servicefeepercent ).div(servicefeedenominator);
    
            if( hostaddress.send( toHostPrice ) && populstayaddress.send( toPopulstayPrice ) )
            {
              
                    from = block.timestamp.mul(timestampformat);//set from to current date to prevent host to get ETH repeatly.
                
            }
         }
         else
         {
             totalPrice       = ethPrice.mul( gwei2wei )  ;
             toHostPrice      = totalPrice.mul( servicefeedenominator-servicefeepercent).div(servicefeedenominator);
             toPopulstayPrice = totalPrice.mul( servicefeepercent ).div(servicefeedenominator); 
             
            if( hostaddress.send( toHostPrice ) && populstayaddress.send( toPopulstayPrice ) )
            {
                
                    from = block.timestamp.mul(timestampformat);//set from to current date to prevent host to get ETH repeatly.
               
            }
         }
    }
    

    
    function confirmOrderbyHost()
    public
    payable
    onlyHost
    {
        uint totalPrice;
        uint toHostPrice;
        uint toPopulstayPrice;
        
         if( block.timestamp.mul(timestampformat) < to )
         {
             totalPrice = price.mul( block.timestamp.mul(timestampformat).sub(from) ).div( to.sub (from) );
             toHostPrice      = totalPrice.mul( servicefeedenominator-servicefeepercent).div(servicefeedenominator);
             toPopulstayPrice = totalPrice.mul( servicefeepercent ).div(servicefeedenominator);
             

             if( 
                    Token(tokenAddress).transfer(hostaddress, toHostPrice) 
                &&  Token(tokenAddress).transfer(populstayaddress, toPopulstayPrice)
                )
            {
                from = block.timestamp.mul(timestampformat);//set from to current date to prevent host to get PPS repeatly.
            }
            
         }
         else
         {
            toHostPrice      = price.mul( servicefeedenominator-servicefeepercent).div(servicefeedenominator);
            toPopulstayPrice = price.mul( servicefeepercent ).div(servicefeedenominator);
            
            
            if( 
                    Token(tokenAddress).transfer(hostaddress, toHostPrice) 
                &&  Token(tokenAddress).transfer(populstayaddress, toPopulstayPrice)
            )
            {
                from = block.timestamp.mul(timestampformat);//set from to current date to prevent host to get PPS repeatly.
            }
         }
    }
    
    
    uint public testtotalPrice;
    
    function cancelOrderbyGuest()
    public
    payable
    onlyGuest
    {
        uint totalPrice;
      
         if( block.timestamp.mul(timestampformat) < to )
         {
             totalPrice  = price.mul( to.sub(block.timestamp.mul(timestampformat).add(onedaytimestamp) )).div( to.sub (from) );
           
             testtotalPrice = totalPrice;
           
             if( 
                    Token(tokenAddress).transfer( guestaddress, totalPrice ) 
                )
            {
                status = 2;
            }
            
         }

    }
    
    
    
    function cancelOrderbyETHbyGuest()
    public
    payable
    onlyGuest
    {
        uint totalPrice;
      
         if( block.timestamp.mul(timestampformat) < to )
         {
             totalPrice  = ethPrice.mul(gwei2wei).mul( to.sub( block.timestamp.mul(timestampformat).add(onedaytimestamp) ) ).div( to.sub (from) );
             
          
             
             if(  guestaddress.send( totalPrice ) )
            {
                status = 2;
            }
            
         }

    }
    
    struct Change {
        bytes32 houseinfo;
        uint  price;
        uint  ethPrice;
        uint  from;
        uint  to;
        uint  rentDays;
   } 
   
    mapping ( bytes32 => Change ) private change;
    bytes32 public changeID;
    uint public applyStatus = 0;
    
    //"0x7465737431000000000000000000000000000000000000000000000000000002","0x7465737431000000000000000000000000000000000000000000000000000003",200,50000000,2,9,1
    function applyChange( bytes32 _id,bytes32 _houseinfo,uint _price,uint _ethPrice,uint _from, uint _to, uint  _rentDays)
    public
    onlyGuest
    checkStatus
    {
           change[_id] = Change(
      {
         houseinfo : _houseinfo,
          price    : _price,
          ethPrice : _ethPrice,
          from     : _from,
          to       : _to,
          rentDays : _rentDays
      });
      
      changeID    = _id;
      applyStatus = 1;
    }
    
    
    //type 1 PPS  type 2 eth
    function ajustBalance( uint ppsoreth)
    public
    payable
    onlyGuest
    returns (bool)
    {
      if( ppsoreth == 1 )
      {
          if( oldprice > price )
          {
              if(Token(tokenAddress).transfer( guestaddress, oldprice.sub(price) ) )
              {
                  return true;
              }
              
          }
          
      }else if ( ppsoreth == 2 )
      {
          if( oldethPrice > ethPrice )
          {
              if(guestaddress.send( (oldethPrice.sub(ethPrice)).mul(gwei2wei) ))
              {
                  return true;
              }
          }
          
      }
    }
    
    
    function approveChange( )
    public
    onlyHost
    checkApplytStatus
    {
        
        Change storage applyobj = change[changeID];
        oldprice     = price;
        oldethPrice  = ethPrice;
        houseinfo    = applyobj.houseinfo;
        from         = applyobj.from;
        to           = applyobj.to;
        rentDays     = applyobj.rentDays;
        price        = applyobj.price;
        ethPrice     = applyobj.ethPrice;
        
        applyStatus = 0;
    }
    
    function getApplyChange()
    public
    view
    returns(bytes32 _houseinfo,uint _from,uint _to,uint _rentDays,uint _price,uint _ethPrice)
    {
        Change storage applyobj = change[changeID];
        
        return
        ( 
                applyobj.houseinfo,
                applyobj.from,
                applyobj.to,
                applyobj.rentDays,
                applyobj.price,
                applyobj.ethPrice
        );
        
    }
    
    
    
    
    
    
}