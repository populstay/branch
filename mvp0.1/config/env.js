function getClientEnvironment(publicUrl) {
  var processEnv =
    {
      'NODE_ENV': JSON.stringify('production'),
      'PUBLIC_URL': JSON.stringify(publicUrl),
      'IPFS_API_PORT': JSON.stringify("5001"),
      'IPFS_DOMAIN': JSON.stringify("ipfs.infura.io"),
      'RentHouseListingAddress':JSON.stringify("0x09Ef8e65498d24eA3A2143612853CaA3A2A7B8bc"),
      'PPSAddress':JSON.stringify("0x901c5be5768798217fd4ceefecc0c4e6c38ec684"),
      'Server_Address':JSON.stringify("https://server.populstay.com/"),
      'Socket_Server':JSON.stringify("https://server.populstay.com/"),
      'Populstay_Wallet':JSON.stringify("0xB421ca5420dC2D6F0bf868c52ad1ff8614E68788"),
      'WEB3_PROVIDER':JSON.stringify("https://kovan.infura.io/FrDFhx3FbezOwQJjQv9T"),
      'Exchange_Contract':JSON.stringify("0xa88dd1ce8c1ffb87bf4a0dd097a674bf2b2530ef"),
      'ETHERSCAN_URL':JSON.stringify("https://kovan.etherscan.io/")
    }

  return {'process.env': processEnv};

}

module.exports = getClientEnvironment;



