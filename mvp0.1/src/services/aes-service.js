var CryptoJS = require("crypto-js");

class AesService {
	constructor() {}

	//CryptoJS 加密
	encrypt(privatekey, password) {
		var ciphertext = CryptoJS.AES.encrypt(privatekey, password).toString();
		return ciphertext
	}

	//CryptoJS 解密
	decrypt(privatekey, password) {
		var bytes = CryptoJS.AES.decrypt(privatekey, password).toString(CryptoJS.enc.Utf8);
		return bytes
	}
}

const aesService = new AesService()
export default aesService