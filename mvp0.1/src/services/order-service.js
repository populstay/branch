import axios from 'axios';
import PreOrder from '../../build/contracts/PreOrder.json';
import Web3 from 'web3';
import aesService from '../services/aes-service';
const Buf = require('buffer/').Buffer;
const EthereumTx = require('ethereumjs-tx');
const web_provider = process.env.WEB3_PROVIDER;

class PreOrderService {
	constructor() {
		this.contract = require('truffle-contract');
		this.PreOrderContract = this.contract(PreOrder);
	}

	//记住一定只能是-2的
	//orderService.confirmByUSD("5b30a218e13af37acb1e872a");
	confirmByUSD(id) {
		return new Promise((resolve, reject) => {
			var params = {};
			params.id = id;
			params.state = 4;

			axios
				.post(process.env.Server_Address + 'currencycheckin/', params)
				.then((response) => {
					//state:0准备提交。1 已经提交 2 已经写入以太链
					resolve(response);
				})
				.catch(function(error) {
					reject(error);
				});
		});
	}
	//大于入住7天以前cancel这个order
	cancel(address, ethOrPPS, id, password) {

		return new Promise((resolve, reject) => {
			var contract = new window.web3.eth.Contract(PreOrder.abi, address);

			var dataobj;
			//PPS会完全转回账户
			if(ethOrPPS == 'PPS')
				dataobj = contract.methods.cancelOrderWithoutPay().encodeABI();

			//ETH会完全转回账户
			if(ethOrPPS == 'ETH')
				dataobj = contract.methods.cancelOrderByEthWithoutPay().encodeABI();

			window.web3.eth.getTransactionCount(window.address).then(function(txCount) {
				var params = {};

				var txData = {
					nonce: window.web3.utils.toHex(txCount),
					gasLimit: window.web3.utils.toHex(4476768),
					gasPrice: window.web3.utils.toHex(window.gas * 1000000), // 10 Gwei
					to: address,
					from: window.address,
					data: dataobj
				};

				var pk = new Buf(aesService.decrypt(window.aesprivateKey, password).substr(2, aesService.decrypt(window.aesprivateKey, password).length), 'hex')
				var transaction = new EthereumTx(txData);
				transaction.sign(pk);

				var serializedTx = transaction.serialize().toString('hex');

				params.id = id;
				params.transactionData = '0x' + serializedTx;

				axios.post(process.env.Server_Address + 'checkin/cancel/', params)
					.then((response) => {

						resolve(response);
					})
					.catch(function(error) {
						reject(error);
					});

			});
		});
	}

	//setComment
	//setCommentByHost
	//getPreorderInfo
	//cancelOrderWithoutPay
	//cancelOrderByEthWithoutPay
	//confirmOrderByEthByHost
	//confirmOrderbyHost
	//cancelOrderbyGuest
	//cancelOrderbyETHbyGuest
	//applyChange
	//ajustBalance

	callContract(address, password, method, ethOrPPS) {

		return new Promise((resolve, reject) => {

			var contract = new window.web3.eth.Contract(PreOrder.abi, address);

			var dataobj;
			//入住24小时之前房客可以调用的API
			if(method == 'cancelOrderWithoutPay' && ethOrPPS == "PPS")
				dataobj = contract.methods.cancelOrderWithoutPay().encodeABI();

			//入住24小时之前房客可以调用的API
			if(method == 'cancelOrderByEthWithoutPay' && ethOrPPS == "ETH")
				dataobj = contract.methods.cancelOrderByEthWithoutPay().encodeABI();

			//入住之后房东，比如说房客入住10天，住了5天，房东可以取走这部分eth或者token，可以调用的API
			if(method == 'confirmOrderByEthByHost' && ethOrPPS == "ETH")
				dataobj = contract.methods.confirmOrderByEthByHost().encodeABI();
			//入住之后房东可以调用的API，比如说房客入住10天，住了5天，房东可以取走这部分eth或者token，可以调用的API
			if(method == 'confirmOrderbyHost' && ethOrPPS == "PPS")
				dataobj = contract.methods.confirmOrderbyHost().encodeABI();

			//入住之后房客可以调用的API，比如说房客入住10天，住了5天，房客想走，可以取走部分eth或者token，但未来24小时的token不可以取走
			if(method == 'cancelOrderbyGuest' && ethOrPPS == "PPS")
				dataobj = contract.methods.cancelOrderbyGuest().encodeABI();

			//入住之后房客可以调用的API，比如说房客入住10天，住了5天，房客想走，可以取走部分eth或者token，但未来24小时的token不可以取走
			if(method == 'cancelOrderbyETHbyGuest' && ethOrPPS == "ETH")
				dataobj = contract.methods.cancelOrderbyETHbyGuest().encodeABI();

			window.web3.eth.getTransactionCount(window.address).then(function(txCount) {
				var params = {};

				var txData = {
					nonce: window.web3.utils.toHex(txCount),
					gasLimit: window.web3.utils.toHex(4476768),
					gasPrice: window.web3.utils.toHex(window.gas * 1000000), // 10 Gwei
					to: address,
					from: window.address,
					data: dataobj
				};

				var pk = new Buf(aesService.decrypt(window.aesprivateKey, password).substr(2, aesService.decrypt(window.aesprivateKey, password).length), 'hex')
				var transaction = new EthereumTx(txData);
				transaction.sign(pk);

				var serializedTx = transaction.serialize().toString('hex');

				window.web3.eth.sendSignedTransaction('0x' + serializedTx, function(err, hash) {
					if(!err) {
						resolve(hash);
					} else {
						console.log(err);
						reject(err);
					}
				});
			});
		});
	}

	//小于入住7天以前cancel这个order
	cancelWithPay(address, ethOrPPS, id, password) {

		return new Promise((resolve, reject) => {
			var contract = new window.web3.eth.Contract(PreOrder.abi, address);

			var dataobj;
			//PPS会完全转回账户
			if(ethOrPPS == 'PPS')
				dataobj = contract.methods.cancelOrderWithPay().encodeABI();

			//ETH会完全转回账户
			if(ethOrPPS == 'ETH')
				dataobj = contract.methods.cancelOrderByEthWithPay().encodeABI();

			window.web3.eth.getTransactionCount(window.address).then(function(txCount) {
				var params = {};

				var txData = {
					nonce: window.web3.utils.toHex(txCount),
					gasLimit: window.web3.utils.toHex(4476768),
					gasPrice: window.web3.utils.toHex(window.gas * 1000000), // 10 Gwei
					to: address,
					from: window.address,
					data: dataobj
				};

				var pk = new Buf(aesService.decrypt(window.aesprivateKey, password).substr(2, aesService.decrypt(window.aesprivateKey, password).length), 'hex')
				var transaction = new EthereumTx(txData);
				transaction.sign(pk);

				var serializedTx = transaction.serialize().toString('hex');

				params.id = id;
				params.transactionData = '0x' + serializedTx;

				axios.post(process.env.Server_Address + 'checkin/cancelWithPay/', params)
					.then((response) => {

						resolve(response);
					})
					.catch(function(error) {
						reject(error);
					});

			});
		});
	}

	confirm(address, ethOrPPS, from, to, houseinfoid, password) {

		return new Promise((resolve, reject) => {
			var contract = new window.web3.eth.Contract(PreOrder.abi, address);

			var dataobj;
			if(ethOrPPS == 'PPS')
				dataobj = contract.methods.confirmOrder().encodeABI();

			if(ethOrPPS == 'ETH')
				dataobj = contract.methods.confirmOrderByEth().encodeABI();

			window.web3.eth.getTransactionCount(window.address).then(function(txCount) {
				var params = {};

				var txData = {
					nonce: window.web3.utils.toHex(txCount),
					gasLimit: window.web3.utils.toHex(4476768),
					gasPrice: window.web3.utils.toHex(window.gas * 1000000), // 10 Gwei
					to: address,
					from: window.address,
					data: dataobj
				};

				var pk = new Buf(aesService.decrypt(window.aesprivateKey, password).substr(2, aesService.decrypt(window.aesprivateKey, password).length), 'hex')
				var transaction = new EthereumTx(txData);
				transaction.sign(pk);
				var serializedTx = transaction.serialize().toString('hex');
				var fromLen;
				var toLen;
				if(from.length < 13) {
					fromLen = from + "000";
					toLen = to + "000";
				} else {
					fromLen = from;
					toLen = to;
				}
				params.from = fromLen; //+"000" in order to match data in DB
				params.to = toLen;
				params.houseinfoid = houseinfoid;
				params.address = address;
				params.transactionData = '0x' + serializedTx;

				axios.post(process.env.Server_Address + 'checkin/', params)
					.then((response) => {
						//state:0准备提交。1 已经提交 2 已经写入以太链
						resolve(response);
					})
					.catch(function(error) {
						reject(error);
					});

			});
		});
	}

	getPreOrderInfo(address) {
		var contract = new window.web3.eth.Contract(PreOrder.abi, address);
		return contract.methods.getPreorderInfo().call();
	}

	waitTransactionFinished(transactionReceipt, pollIntervalMilliseconds = 1000) {
		return new Promise((resolve, reject) => {
			let txCheckTimer = setInterval(txCheckTimerCallback, pollIntervalMilliseconds);

			function txCheckTimerCallback() {
				window.web3.eth.getTransaction(transactionReceipt, (error, transaction) => {
					if(transaction.blockNumber != null) {
						//console.log(`Transaction mined at block ${transaction.blockNumber}`)
						//console.log(transaction)
						clearInterval(txCheckTimer)
						setTimeout(() => resolve(transaction.blockNumber), 2000)
					}
				})
			}
		})
	}

}

const preOrderService = new PreOrderService()
export default preOrderService