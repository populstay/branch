import HouseInfoListing from '../../build/contracts/HouseInfoListing.json';
import houselistingService from './houseinfolist-service';
import orderService from '../services/order-service';
import axios from 'axios';
import { reactLocalStorage } from 'reactjs-localstorage';

class GuestService {
	static instance

	constructor() {
		if(GuestService.instance) {
			return GuestService.instance
		}

		GuestService.instance = this;

		this.contract = require('truffle-contract');
		this.houseInfoListingContract = this.contract(HouseInfoListing);
	}

	getOrderState() {
		return new Promise((resolve, reject) => {
			if(window.localStorage.getItem('webtoken')) {
				axios.defaults.headers.common['Authorization'] = window.localStorage.getItem('webtoken');
			}

			axios.get(process.env.Server_Address + 'book?guestaddress=' + window.address)
				.then((response) => {
					//state:0准备提交。2 已经提交 4 已经checkin
					resolve(response.data);
				})
				.catch(function(error) {
					reject(error);
				});
		});
	}

	setWebToken(token) {
		if(token) {
			window.localStorage.setItem('webtoken', 'Bearer ' + token);
		}
		//localStorage.setItem('jwt',window.webtoken);
	}

	login(email, password) {
		var params = {};
		params.email = email;
		params.password = password;
		return new Promise((resolve, reject) => {

			if(window.localStorage.getItem('webtoken')) {
				axios.defaults.headers.common['Authorization'] = window.localStorage.getItem('webtoken');
			}
			axios.post(process.env.Server_Address + 'auth/index', params)
				.then(function(response) {
					resolve(response.data);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		});

	}

	addComment(id, comment, accuracyStar, locationStar, communicationStar, checkinStar, cleanlinessStar, valueStar) {
		var params = {};

		params.id = id;
		params.comment = comment;
		params.accuracyStar = accuracyStar;
		params.locationStar = locationStar;
		params.communicationStar = communicationStar;
		params.checkinStar = checkinStar;
		params.cleanlinessStar = cleanlinessStar;
		params.valueStar = valueStar;

		return new Promise((resolve, reject) => {
			axios.post(process.env.Server_Address + 'comment', params)
				.then(function(response) {
					resolve(response);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		});

	}
	checkemail(emailAddress) {
		return new Promise((resolve, reject) => {
			axios.get(process.env.Server_Address + 'register?email=' + emailAddress)
				.then((response) => {
					resolve(response);
				})
				.catch(function(error) {
					reject(error);
				});
		})
	}

	reasonForBadComment(id, reason) {
		return new Promise((resolve, reject) => {
			var params = {};
			params.id = id;
			params.reason = reason;
			axios.post(process.env.Server_Address + 'comment/reasonforbadcomment', params)
				.then(function(response) {
					resolve(response);
				}).catch(function(error) {
					console.error(error)
					reject(error)
				});
		});
	}

	sendEmail(from, to, subject, text) {
		return new Promise((resolve, reject) => {
			var params = {};
			params.from = from;
			params.to = to;
			params.subject = subject;
			params.text = text;

			axios.post(process.env.Server_Address + 'emailsender', params)
				.then(function(response) {
					resolve(response);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		});

	}

	guestRegister(registerData) {
		return new Promise((resolve, reject) => {
			axios.post(process.env.Server_Address + 'Register', registerData)
				.then(function(response) {
					resolve(response);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		})
	}

	getGuesterInfo(id) {
		return new Promise((resolve, reject) => {
			if(id) {
				axios.get(process.env.Server_Address + 'Register/' + id)
					.then((response) => {
						resolve(response.data);
					})
					.catch(function(error) {
						reject(error);
					});
			}
		})
	}

	// getPreorderList(account){
	//   return houselistingService.getGuestPreorderList(account);
	// }

	getGuesterCode(email, subject) {
		return new Promise((resolve, reject) => {
			var emailArr = {};
			emailArr.to = email;
			emailArr.subject = subject;
			emailArr.from = "PopulStay潮箱@populstay.com";
			axios.post(process.env.Server_Address + 'emailverify/sendVerificationEmail', emailArr)
				.then(function(response) {
					//console.log(response)
					resolve(response);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		})
	}

	VerificationCode(email, Code) {
		return new Promise((resolve, reject) => {
			var Verification = {};
			Verification.to = email;
			Verification.code = Code;
			axios.post(process.env.Server_Address + 'emailverify/verifyEmail', Verification)
				.then(function(response) {
					resolve(response);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		})
	}

	getVerificationCode(email) {
		return new Promise((resolve, reject) => {
			var Code = {};
			Code.id = email;
			axios.post(process.env.Server_Address + 'Captchapng', Code)
				.then(function(response) {
					resolve(response);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		})
	}

	loginVerificationCode(email, Code) {
		return new Promise((resolve, reject) => {
			var loginVerification = {};
			loginVerification.id = email;
			loginVerification.code = Code;
			axios.post(process.env.Server_Address + 'Captchapng/verifyImg', loginVerification)
				.then(function(response) {
					resolve(response);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		})
	}

	importGuest(pirvatekey) {
		return new Promise((resolve, reject) => {
			axios.get(process.env.Server_Address + 'register/checkAccount?account=' + pirvatekey)
				.then((response) => {
					resolve(response.data);
				})
				.catch(function(error) {
					reject(error);
				});
		})
	}

	profile(email, userimg) {
		return new Promise((resolve, reject) => {
			var loginVerification = {};
			loginVerification.email = email;
			loginVerification.profile = userimg;
			axios.post(process.env.Server_Address + 'register/uploadProfile', loginVerification)
				.then(function(response) {
					resolve(response);
				})
				.catch(function(error) {
					console.error(error)
					reject(error)
				});
		})
	}

	checkname(name) {
		return new Promise((resolve, reject) => {
			axios.get(process.env.Server_Address + 'register/checkuser?user=' + name)
				.then((response) => {
					resolve(response.data);
				})
				.catch(function(error) {
					reject(error);
				});
		})
	}
}

const guestService = new GuestService()

export default guestService