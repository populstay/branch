const localeList = {
	"en_US": require('../locale/en_US.js'),
	"zh_CN": require('../locale/zh_CN.js'),
};

class LanguageService {

	static instance

	constructor() {
		if(LanguageService.instance) {
			return LanguageService.instance
		}

		if(!localStorage.getItem('webtoken')) {
			localStorage.removeItem('wallet')
		}

		LanguageService.instance = this;
	}

	language(language) {
		if(language) {
			window.languagelist = localeList[language];
		} else {
			window.localeList = localeList;
			if(!localStorage.getItem('language')) {
				window.languagelist = localeList["en_US"];
			} else {
				window.languagelist = localeList[localStorage.getItem('language')];
			}
		}

	}

}

const languageService = new LanguageService()
export default languageService