import React from 'react'
import {BrowserRouter as Router,Route} from 'react-router-dom'

// Components
import NavBar from './NavFoot/navbar'
import ScrollToTop from './scroll-to-top.js'
import Listings from './Search/listings-grid.js'
import ListingDetail from './Detail/detail.js'
import ListingCreate from './House/listing-create.js'
import GuestInfo from './Info/guestInfo';
import Footer from './NavFoot/footer'
import Overlay from './overlay'
import Search from './Search/search.js'
import Listingexperience from './Experience/listing-experience.js'
import Listingall from './ALL/listing-all.js'
import Experienceintro from './Experience/experience-intro.js'
import Itrolist from './Intro/intro-list.js'
import Registerlist from './RegisterList/register-list.js'
import Verifyid from './VerifyID/verify-id.js'
import ProfileReceipt from './Order/profile_receipt.js'
import ProfileConfrim from './Order/profile_confrim.js'
import Book from './Book/Book.js'
import Cancellations from './Cancellations/Cancellations.js'

// CSS
import '../css/becomehost.css'
import '../css/search.css'
import '../css/payments.css'
import '../css/detail.css'
import '../css/homepage.css'
import '../css/Modal.css'
import '../css/experience.css'
import '../css/help.css'
import '../css/main.css'
import '../css/media.css'

const SearchPage = (props) => (
	<Layout {...props} hideTagHeader={true}>
      <Search />
  </Layout>
)

const HomePage = (props) => (
	<Layout {...props}>
    <div className="container">
      <Listings />
    </div>
  </Layout>
)

const ListingDetailPage = (props) => (
	<Layout {...props}  hideTagHeader={true}>
    <ListingDetail listingId={props.match.params.listingId} />
  </Layout>
)

const CreateListingPage = (props) => (
	<Layout {...props} hideTagHeader={true}>
    <div className="container">
      <ListingCreate />
    </div>
  </Layout>
)

const Info = (props) => (
	<Layout {...props}  hideTagHeader={true} userimg={true}  >
    <div className="container">
      <GuestInfo />
    </div>
  </Layout>
)

const Layout = ({
	children,
	hideTagHeader,
	hideTagFooter,
	userimg
}) => (
	<div>
    
      <NavBar hideTagHeader={hideTagHeader} userimg={userimg} />
      {children}
    
    <Footer hideTagFooter={hideTagFooter} />
  </div>
)

const experiencePage = (props) => (
	<Layout {...props}>
    <div className="container">
      <Listingexperience />
    </div>
  </Layout>
)

const all = (props) => (
	<Layout {...props}>
    <div className="container">
      <Listingall />
    </div>
  </Layout>
)

const Intro = (props) => (
	<Layout {...props}  hideTagHeader="NO">
      <Experienceintro />
  </Layout>
)

const experiencelist = (props) => (
	<Layout {...props}  hideTagHeader="NO" hideTagFooter="NO">
      <Itrolist />
  </Layout>
)

const Register = (props) => (
	<Layout {...props}  hideTagHeader="NO" hideTagFooter="NO">
      <Registerlist />
  </Layout>
)

const VerifyID = (props) => (
	<Layout {...props}  hideTagHeader="NO" hideTagFooter="NO">
      <Verifyid />
  </Layout>
)

const Receipt = (props) => (
	<Layout {...props}  >
      <ProfileReceipt />
  </Layout>
)

const Confrim = (props) => (
	<Layout {...props}  >
      <ProfileConfrim />
  </Layout>
)

const Booking = (props) => (
	<Layout {...props}  hideTagHeader="NO" >
      <Book />
  </Layout>
)

const CANCELLATIONS = (props) => (
	<Layout {...props}  >
      <Cancellations />
  </Layout>
)

// Top level component
const App = () => (
	<Router>
    <ScrollToTop>
        <div>
          <Route exact path="/" component={SearchPage}/>
          <Route exact path="/home/:search" component={HomePage}/>
          <Route path="/page/:activePage" component={HomePage}/>
          <Route path="/listing/:listingId" component={ListingDetailPage}/>
          <Route path="/create" component={CreateListingPage}/>
          <Route path="/Info" component={Info}/>
          <Route exact path="/experience" component={experiencePage}/>
          <Route exact path="/all" component={all}/>
          <Route exact path="/Intro" component={Intro}/>
          <Route exact path="/experiencelist" component={experiencelist}/>
          <Route exact path="/Register" component={Register}/>
          <Route exact path="/VerifyID" component={VerifyID}/>
          <Route exact path="/Receipt" component={Receipt}/>
          <Route exact path="/Confrim" component={Confrim}/>
          <Route exact path="/Book" component={Booking}/>
          <Route exact path="/Cancellations" component={CANCELLATIONS}/>
        </div>
    </ScrollToTop>
  </Router>
)

export default App