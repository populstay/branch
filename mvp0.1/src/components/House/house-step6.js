import React, { Component } from 'react';
import languageService from '../../services/language-service';
import AvatarEditor from 'react-avatar-editor';
import Overlay from './../overlay';
import FileBase64 from 'react-file-base64';
import dragula from 'react-dragula';


class House_step6 extends Component {

  constructor(props) {
    super(props)

    this.STEP = {
      Step:6,
    }

    this.state={
      selectedPictures:[]
    };
    languageService.language();
  }

 
  componentWillMount(){
      this.setState({
        selectedPictures:this.props.Pictures, 
      })
  }

  fileChangedHandler(files){
        var Picturesfiles = this.state.selectedPictures;

        for(var i=0; i<files.length; i++){

          var imgtype = files[i].type.split("/")[1];  

          if ((imgtype == 'jpg' || imgtype == 'jpeg' || imgtype == 'png' || imgtype == 'PNG' || imgtype == 'JPG') && parseInt(files[i].size) < 600) {  
              Picturesfiles.push({
                file: i,
                imagePreviewUrl: files[i].base64
              });
              this.setState({selectedPictures:Picturesfiles});
          }else{
            this.setState({imgprompt:true});
          } 


        }

    }

    deletePictures(index,e){
      this.setState({
            selectedPictures: this.state.selectedPictures.filter((elem, i) => index != i)
      });

    }

    modalPictures(index,e){
      var modalBody=document.getElementById("modalBody");
      this.setState({
            state:this.state.modalimg = this.state.selectedPictures[index].imagePreviewUrl,
            canvasW:modalBody.width,
            canvasH:modalBody.height,
            photosindex:index
      });
    }

    onClickSave = () => {
      if (this.state.editor) {
        var photosindex = this.state.photosindex;
        const canvas = this.state.editor.getImage()
        const canvasScaled = this.state.editor.getImageScaledToCanvas();
        this.setState({state:this.state.selectedPictures[photosindex].imagePreviewUrl = canvasScaled.toDataURL("image/png")})
      }
    }

  Step=(obj)=>{
      this.props.house_Step(obj);
      this.props.selectedPictures(this.state.selectedPictures)
      sessionStorage.setItem('step', obj);
  }

  dragulaDecorator = (componentBackingInstance) => {
    console.log(componentBackingInstance)
    if (componentBackingInstance) {
      let options = { };
      dragula([componentBackingInstance], options);
    }
  }

  render() {

        const language = this.state.languagelist;

    return (
        <div className="row Step6">

        {this.state.imgprompt &&
          <Overlay>
            <div className="imgprompt">
              <p className="hostbook">{window.languagelist.The_picture_must_not_exceed}600KB</p>
              <p className="hostbook">{window.languagelist.Image_format_is} jpg、jpeg、png</p>
              <button className="balance" onClick={(e)=>this.setState({imgprompt:false})}>{window.languagelist.Close}</button>
            </div>  
          </Overlay>
        }
            <div className="col-md-12 col-lg-12 col-sm-12">
            
            <div className="STEPhead">
              <span className="bjpink"></span>
              <span></span>
              <span></span>
              <p>{window.languagelist.Step2}: {window.languagelist.Set_the_scene}</p>
            </div>

               <h1>{window.languagelist.Show_travellers_what_your_space_looks_like}
                <img src="../images/photoi.png" onClick={(e) => {if(this.state.Rapair == 0 )this.setState({Rapair:1});else this.setState({Rapair:0});}}/>
              </h1>
              <p></p>

              <div  className={this.state.Rapair == 0 ? 'show rightbox' : 'hide rightbox'}>
                  <span onClick={(e) => {if(this.state.Rapair == 0 )this.setState({Rapair:1});else this.setState({Rapair:0});}}>×</span>
                  <ul>
                      <img src="../images/step2_2.png" />
                      <li>{window.languagelist.Many_hosts_have_at_least_8photos}</li>
                      <img src="../images/step2_1.png" />
                      <li>{window.languagelist.Make_sure_the_room_is_well_lit}</li>
                      <li>{window.languagelist.Sometimes_shooting_from_a_corner}</li>
                  </ul>
              </div>
              
              <div className={this.state.selectedPictures.length != 0  ? "hide photo" : "show photo"}>
                 <div className="photosipt">
                    <h3>{window.languagelist.Drag_and_Drop}<p>{window.languagelist.OR}</p><button>{window.languagelist.Browse}</button></h3>
                    <img src="../images/addphoto.png" alt=""/>
                    <FileBase64  className="Fileipt" multiple={ true } onDone={ this.fileChangedHandler.bind(this) } />
                 </div>
              </div>

          

              <div className={this.state.selectedPictures.length == 0  ? "hide" : "show"}>
                   <div className="photos"  ref={this.dragulaDecorator} >
                      {this.state.selectedPictures.map((file,index) => (
                        <div className="photosimg" >
                          <img className="img-thumbnail"  data-toggle="modal" onClick={this.modalPictures.bind(this,index)} src={file.imagePreviewUrl} />
                          <span  className="glyphicon glyphicon-trash" onClick={this.deletePictures.bind(this,index)} ></span>
                        </div>
                        ))
                       }
                     <div className="photosipt">
                        <img src="../images/addphoto1.png" />
                        <FileBase64  className="Fileipt" multiple={ true } onDone={ this.fileChangedHandler.bind(this) } />
                     </div>
                  </div>
                  <p className="textpink">{this.state.PicturesSize}</p>

                  <div className="modal fade hide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                      <div className="modal-content">
                          <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div className="modal-body" id="modalBody" ref='modalBody'>
                        <AvatarEditor
                            ref = {this.setEditorRef}
                            image={this.state.modalimg}
                            width={700}
                            height={500}
                            border={[50,0,50,0]}
                            color={[0, 0, 0, 0.6]}
                            scale={this.state.canvasScale}
                            rotate={this.state.canvasRotate}
                          />
                        </div>
                        <div className="modal-footer" >
                          <ul className={this.state.modalset == 0 ? "Set modalshow" : "Set hide"}>
                              <li onClick={(e) => this.setState({modalset:1})}><img src="../images/crop.png" />{window.languagelist.Crop}</li>
                              <li onClick={(e) => this.setState({modalset:2})}><img src="../images/Brightness.png" />{window.languagelist.Adjust_Brightness}</li>
                              <li onClick={(e) => this.setState({canvasRotate:this.state.canvasRotate+90})}><img src="../images/Rotate.png" />{window.languagelist.Rotate}</li>
                          </ul>
                          <ul className={this.state.modalset != 0 ? "Brightness show" : "Brightness hide"}>
                              <li  className={this.state.modalset == 1 ? "show" : "hide"}>
                                  <p>{window.languagelist.Zoom}</p>
                                  <input type="range" onChange={(e)=>this.setState({canvasScale:e.target.value})} name="points"  step="0.01" min="0.5" max="2" />
                              </li>
                              <li  className={this.state.modalset == 2 ? "show" : "hide"}>
                                  <p>{window.languagelist.Brightness}</p>
                                  <input type="range" onChange={(e)=>this.BrightnessPictures(e.target.value)} name="points" step="0.01" min="-1" max="1" />
                              </li>
                              <li  className={this.state.modalset == 2 ? "show" : "hide"}>
                                  <p>{window.languagelist.Contrast_Ratio}</p>
                                  <input type="range" name="points" step="0.02" min="1" max="3" />
                              </li>
                          </ul>
                          <button onClick={(e) => this.setState({modalset:0})} className={this.state.modalset != 0 ? "btn Cancel show" : "btn Cancel hide"} type="button">{window.languagelist.Cancel}</button>
                          <button onClick={(e) => this.setState({modalset:0})} className={this.state.modalset != 0 ? "btn Complete show" : "btn Complete hide"} type="button" >{window.languagelist.Complete}</button>
                          <button  className={this.state.modalset == 0 ? "btn Replace show" : "btn Replace hide"} data-dismiss="modal" aria-hidden="true" type="button" onClick={(e)=>this.onClickSave(e)}>{window.languagelist.Save_and_Replace}</button>
                        </div>
                      </div>
                    </div>
                  <div className="modal-backdrop fade in"></div>
                  </div>
              </div>
             
            <div className="STEPBTN">
              <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
              <button className="btn btn-default btn-lg bg-pink color-white Right" disabled={this.state.selectedPictures.length < 1 ? "true" : ""} onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
            </div>
             
             </div>
             
             </div>
    )
  }
}

export default House_step6