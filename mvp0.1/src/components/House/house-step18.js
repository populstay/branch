import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step18 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 18,
		}

		this.state = {
			discount_Weekly: "",
			discount_Monthly: "",
		}

		languageService.language();
	}

	componentWillMount() {
		var listStorage = JSON.parse(sessionStorage.getItem('step18'));
		if(listStorage) {
			this.setState({
				discount_Weekly: listStorage.discount_Weekly,
				discount_Monthly: listStorage.discount_Monthly,
			})
		}
	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
		sessionStorage.setItem('step18', JSON.stringify(this.state));
	}

	render() {

		return(
			<div className="row Step18">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>

              <h1>{window.languagelist.Length_of_stay_prices}</h1>

              <div className="box boxdiv col-md-12">
                <p className="p1">{window.languagelist.Encourage_travellers_to_book_longer_stays_by_offering_a_discount}</p>

                <h3>{window.languagelist.Weekly_discount}</h3>
                <div className="Base">
                  <div className="btn-group col-lg-12 boxdiv">
                    <input type="number" placeholder="0 % off" onChange={(e) => {this.setState({discount_Weekly:e.target.value})}} className="form-control" value={this.state.discount_Weekly} />
                  </div>
                </div>

                <h2 className="demand" onClick={(e) => {this.setState({discount_Weekly:21})}}>{window.languagelist.Tipprice}: 21%</h2>
                <p className="textpink">{window.languagelist.Travellers_searching_for_stays_longer}</p>

                <h3>{window.languagelist.Monthly_discount}</h3>

                <div className="Base">
                  <div className="btn-group col-lg-12 boxdiv">
                    <input type="number" placeholder="0 % off" onChange={(e) => {this.setState({discount_Monthly:e.target.value})}} className="form-control" value={this.state.discount_Monthly} />
                  </div>
                </div>

                <h2 className="demand" onClick={(e) => {this.setState({discount_Monthly:49})}}>{window.languagelist.Tipprice}: 49%</h2>
                <p className="textpink">{window.languagelist.of_travellers_staying_longer_than}</p>

              </div>

              
              <div className="STEPBTN">
                <button className="Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className={ this.state.discount_Weekly > 100 || this.state.discount_Monthly > 100 ? "buttonActive Right" : "Right"} disabled={this.state.discount_Weekly > 100 || this.state.discount_Monthly > 100 ? "true" : ""} onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
               
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                <div>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <h6>{window.languagelist.Discount_for_longer_stays}</h6>
                    <p>{window.languagelist.To_encourage_longer_stays}</p>
                    <p>{window.languagelist.Weekly_discounts_will_apply}</p>
                </div>
             </div>


             
             </div>
		)
	}
}

export default House_step18