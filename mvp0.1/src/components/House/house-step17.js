import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step17 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 17,
		}

		this.state = {
			StepActibve: 1,
			first_guests_20: 0,
		}

		languageService.language();
	}

	componentWillMount() {
		var listStorage = JSON.parse(sessionStorage.getItem('step17'));
		if(listStorage) {
			this.setState({
				first_guests_20: listStorage.first_guests_20,
			})
		}
	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
		sessionStorage.setItem('step17', JSON.stringify(this.state));
	}

	render() {

		return(
			<div className="row Step17 Step3_14">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>

              <h1>{window.languagelist.Something_special_for_your_first_guests}</h1>

              <div className="box col-md-12">
                <div className="boxdiv" onClick={(e) => {this.setState({first_guests_20:1})}} >
                  <div className="col-lg-9 content">
                    <h3>{window.languagelist.Offer_off_to_your_first_guests}</h3>
                    <p>{window.languagelist.The_first_3_guests_who_book_your_place}</p>
                    <span>{window.languagelist.RECOMMENDED}</span>
                  </div>
                  <div className="col-lg-2 col-lg-push-1 radio">
                    <p><span className={this.state.first_guests_20 == 1 ?"show":"hide"}></span></p>
                  </div>
                </div>
                <div className="boxdiv" onClick={(e) => {this.setState({first_guests_20:0})}} >
                  <div className="col-lg-9 content">
                    <h3>{window.languagelist.Dont_add_a_special_offer}</h3>
                    <p>{window.languagelist.Once_you_publish_your_listing}</p>
                  </div>
                  <div className="col-lg-2 col-lg-push-1 radio">
                    <p><span className={this.state.first_guests_20 == 0 ?"show":"hide"}></span></p>
                  </div>
                </div>
              </div>

              
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
               
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
              <div className="rightdiv">
                 <div className={this.state.StepActibve ==1 ? 'show' : 'hide'}>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <h6>{window.languagelist.Why_add_a_special_offer}</h6>
                    <p className="step3_13p">{window.languagelist.This_will_help_attract_your_first_guests}</p>
                </div>

                <div className={this.state.StepActibve == 2 ? 'show' : 'hide'}>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <h6>{window.languagelist.Well_share_your_offer}</h6>
                    <p>{window.languagelist.Well_let_guests_who_are_searching}</p>
                </div>

                <div className={this.state.StepActibve == 3 ? 'show' : 'hide'}>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <h6>{window.languagelist.First_guests_can_claim_your_offer}</h6>
                    <p>{window.languagelist.The_offer_will_be_available_to_3_guests}</p>
                </div>

                <div className="step17span">
                  <span className={this.state.StepActibve == 1 ? 'bjpink' : ''} onClick={(e) => this.setState({StepActibve:1})} ></span>
                  <span className={this.state.StepActibve == 2 ? 'bjpink' : ''} onClick={(e) => this.setState({StepActibve:2})}></span>
                  <span className={this.state.StepActibve == 3 ? 'bjpink' : ''} onClick={(e) => this.setState({StepActibve:3})}></span>
               </div>
              </div>

             </div>


             
             </div>
		)
	}
}

export default House_step17