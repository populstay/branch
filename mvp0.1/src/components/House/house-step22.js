import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step22 extends Component {

	constructor(props) {
		super(props)

		this.state = {
			Step: 22,
			languagelist: {},
			homeId: "",
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
		});

		this.setState({
			homeId: sessionStorage.getItem('homeId')
		})

	}
	render() {
		const language = this.state.languagelist;

		return(
			<div className="row">
            <div className="col-md-12 col-lg-12 col-sm-12 success">
          <div>
            <img src="./images/SUCCESS.png" />
            <h1>{language.Submission_of_success}</h1>
            <a className="btn" href={`/listing/${this.state.homeId}`} >
            {language.View_house}
            </a>
          </div>  
          </div>
        </div>
		)
	}
}

export default House_step22