import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step16 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 16,
		}

		this.state = {
			generate_smart_contract: 1,
			price_perday: "",
			ETHprice_perday: "",
			USDprice_perday: 0,
		}

		languageService.language();
	}

	componentWillMount() {
		var listStorage = JSON.parse(sessionStorage.getItem('step16'));
		if(listStorage) {
			this.setState({
				price_perday: listStorage.price_perday,
				ETHprice_perday: listStorage.ETHprice_perday,
			})
		}
	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
		sessionStorage.setItem('step16', JSON.stringify(this.state));
	}

	render() {

		return(
			<div className="row Step16">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>

              <h1>{window.languagelist.Price_your_space}</h1>

              <div className="box col-md-12">
                <h3>{window.languagelist.Increase_your_chances_of_getting_booked}</h3>
                
                <div className="Base">
                  <h1>{window.languagelist.Set_up_the_same_base_price_for_each_night}</h1>
                  <h3>{window.languagelist.Base_Price}</h3>

                  <div className="btn-group col-lg-12 boxdiv">

                    <div className="check hide" onClick={(e) => {if(this.state.generate_smart_contract ==0 )this.setState({generate_smart_contract:1});else this.setState({generate_smart_contract:0,price_perday:0,ETHprice_perday:0});}}>
                      <p  className="Pinput">
                          <img className={this.state.generate_smart_contract ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                      </p>
                      <p className="divinput">{window.languagelist.Generate_Smart_Contract}</p>
                    </div>
                    <div className={this.state.generate_smart_contract == 0 ? "hide" : "show"}>
                      <h4>PPS</h4>
                      <input type="number"  onChange={(e)=>this.setState({price_perday:e.target.value})} placeholder={window.languagelist.Please_use_at_least + "10PPS" + window.languagelist.reserve_price} value={this.state.price_perday}/>
                      <p className={this.state.price_perday < 10 && this.state.price_perday != "" ? "textpink show" : "hide"}>{window.languagelist.Please_use_at_least} 10PPS {window.languagelist.reserve_price}</p>

                      <h4>ETH</h4>
                      <input type="number" onChange={(e)=>this.setState({ETHprice_perday:e.target.value})} placeholder={window.languagelist.Please_use_at_least + "0.00001ETH" + window.languagelist.reserve_price} value={this.state.ETHprice_perday} />
                      <p className={this.state.ETHprice_perday < 0.00001 && this.state.ETHprice_perday != "" ? "textpink show" : "hide"}>{window.languagelist.Please_use_at_least} 0.00001ETH {window.languagelist.reserve_price}</p>
                    </div>
                    <div className="hide" >
                      <h4>USD</h4>
                      <input type="number" onChange={(e)=>this.setState({USDprice_perday:e.target.value})} placeholder={window.languagelist.Please_use_at_least + "10USD" + window.languagelist.reserve_price} value={this.state.USDprice_perday} />
                      <p className={this.state.USDprice_perday < 10 && this.state.USDprice_perday != "" ? "textpink show" : "hide"}>{window.languagelist.Please_use_at_least} 10USD {window.languagelist.reserve_price}</p>
                    </div>
                    <h3 className="hide">{window.languagelist.Please_select_at_least_one_payment_method}</h3>
                  </div>
                </div>
              </div>

              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                    {this.state.generate_smart_contract == 1 &&
                        <button className={this.state.price_perday < 10 || this.state.ETHprice_perday <= 0 ? "buttonActive Right" : "Right"} disabled={ this.state.price_perday < 10 || this.state.ETHprice_perday <= 0 ? "disabled" : ""} onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
                    }

                    {this.state.generate_smart_contract == 0 &&
                        <button className={this.state.USDprice_perday < 10 ? "buttonActive Right" : "Right"} disabled={this.state.USDprice_perday < 10 ? "disabled" : ""} onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
                    }
              </div>
               
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                 <div>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <h6>{window.languagelist.Start_with_a_lower_price_to_attract_bookings}</h6>
                    <p>{window.languagelist.New_hosts_start_with_a_lower_price_to_attract}</p>
                </div>
             </div>
    
             </div>
		)
	}
}

export default House_step16