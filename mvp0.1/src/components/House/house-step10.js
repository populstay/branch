import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step10 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 10,
		}

		this.state = {
			anytime_Checkin: 0,
			rules_children: 0,
			rules_infants: 0,
			rules_pets: 0,
			rules_Smoking: 0,
			rules_parties: 0,
			AdditionalRules: [],
			guests_know: 0,
			climb_stairs: 0,
			Potential_noise: 0,
			property_Pet: 0,
			property_parking: 0,
			shared_spaces: 0,
			Amenity_limitations: 0,
			property_recording: 0,
			property_Weapons: 0,
			property_animals: 0,
			RulesIpt: ""
		}
		languageService.language();
	}

	componentWillMount() {
		var listStorage = JSON.parse(sessionStorage.getItem('step10'));
		if(listStorage) {
			this.setState({
				anytime_Checkin: listStorage.anytime_Checkin,
				rules_children: listStorage.rules_children,
				rules_infants: listStorage.rules_infants,
				rules_pets: listStorage.rules_pets,
				rules_Smoking: listStorage.rules_Smoking,
				rules_parties: listStorage.rules_parties,
				AdditionalRules: listStorage.AdditionalRules,
				guests_know: listStorage.guests_know,
				climb_stairs: listStorage.climb_stairs,
				Potential_noise: listStorage.Potential_noise,
				property_Pet: listStorage.property_Pet,
				property_parking: listStorage.property_parking,
				shared_spaces: listStorage.shared_spaces,
				Amenity_limitations: listStorage.Amenity_limitations,
				property_recording: listStorage.property_recording,
				property_Weapons: listStorage.property_Weapons,
				property_animals: listStorage.property_animals,
			})
		}

	}

	AdditionalRules(e) {
		if(this.state.RulesIpt != '') {
			this.setState({
				state: this.state.AdditionalRules.push(this.state.RulesIpt)
			});
			this.setState({
				state: this.state.RulesIpt = ""
			});
		}
	}

	deleteRules(index, e) {
		this.setState({
			AdditionalRules: this.state.AdditionalRules.filter((elem, i) => index != i)
		});
	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
		sessionStorage.setItem('step10', JSON.stringify(this.state));
	}

	keykeypress(e) {
		if(e.which !== 13) return
		this.AdditionalRules();
	}

	Trim(str) {
		return str.replace(/(^\s*)|(\s*$)/g, "");
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="row Step10">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span></span>
                <span></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>

              <h1>{window.languagelist.Review_PopulStay_guest_requirements}</h1>


              <div className="box col-md-12">
                <h3>{window.languagelist.All_PopulStay_guests_must_provide}:</h3>
                <div className="radio"  >
                  <label className="text-muted"><img src="../images/dashang.png" />{window.languagelist.Email_address}</label>
                </div>
                <div className="radio"  >
                  <label className="text-muted"><img src="../images/dashang.png" />{window.languagelist.Confirmed_phone_number}</label>
                </div>
                <div className="radio"  >
                  <label className="text-muted"><img src="../images/dashang.png" />{window.languagelist.Payment_information}</label>
                </div>
                <div className="radio"  >
                  <label className="text-muted"><img src="../images/dashang.png" />{window.languagelist.A_message_about_the_guests_trip}</label>
                </div>
                <div className="radio"  >
                  <label className="text-muted"><img src="../images/dashang.png" />{window.languagelist.Checkin_time_for_last_minute_trips}</label>
                </div>

                <h3>{window.languagelist.Your_additional_requirements}<span className="textpink hide"  onClick={(e) => this.setState({step:this.STEP.Step3_2})}>{window.languagelist.Edit}</span></h3>
                <div className="radio"  >
                  <label className="text-muted"><img src="../images/dashang.png" />{window.languagelist.Submit_a_government_issued_ID_to_PopulStay}</label>
                </div>

                <br/>
                <h1>{window.languagelist.Your_House_Rules}<span className="textpink hide"  onClick={(e) => this.setState({step:this.STEP.Step3_2})}>{window.languagelist.Edit}</span></h1>

                <div className="radio"  onClick={(e) => {if(this.state.anytime_Checkin ==0 )this.setState({anytime_Checkin:1});else this.setState({anytime_Checkin:0});}} >
                  <label className="text-muted"><p><span className={this.state.anytime_Checkin == 1 ?"show":"hide"}></span></p>{window.languagelist.Check_in_is_anytime_after_12PM}</label>
                </div>

                <div className="radio"  onClick={(e) => {if(this.state.rules_children ==0 )this.setState({rules_children:1});else this.setState({rules_children:0});}} >
                  <label className="text-muted"><p><span className={this.state.rules_children == 1 ?"show":"hide"}></span></p>{window.languagelist.Safe_or_suitable_for_children}</label>
                </div>

                <div className="radio"  onClick={(e) => {if(this.state.rules_infants ==0 )this.setState({rules_infants:1});else this.setState({rules_infants:0});}} >
                  <label className="text-muted"><p><span className={this.state.rules_infants == 1 ?"show":"hide"}></span></p>{window.languagelist.Safe_or_suitable_for_infants}</label>
                </div>

                <div className="radio"  onClick={(e) => {if(this.state.rules_pets ==0 )this.setState({rules_pets:1});else this.setState({rules_pets:0});}} >
                  <label className="text-muted"><p><span className={this.state.rules_pets == 1 ?"show":"hide"}></span></p>{window.languagelist.Suitable_for_pets}</label>
                </div>

                <div className="radio"  onClick={(e) => {if(this.state.rules_Smoking ==0 )this.setState({rules_Smoking:1});else this.setState({rules_Smoking:0});}} >
                  <label className="text-muted"><p><span className={this.state.rules_Smoking == 1 ?"show":"hide"}></span></p>{window.languagelist.Smoking_allowed}</label>
                </div>

                <div className="radio"  onClick={(e) => {if(this.state.rules_parties ==0 )this.setState({rules_parties:1});else this.setState({rules_parties:0});}} >
                  <label className="text-muted"><p><span className={this.state.rules_parties == 1 ?"show":"hide"}></span></p>{window.languagelist.Parties_or_events}</label>
                </div>

                <h3>{window.languagelist.Additional_rules}</h3>
                <div className="Additional_rules">
                  {this.state.AdditionalRules.map((Rules,index) => (
                    <p>{Rules}<img src="./images/registerlist_cuo.png" data-index={index} onClick={this.deleteRules.bind(this,index)} /></p>
                    ))
                  }
                </div>

                <div className="add">
                  <input type="text" onChange={(e)=>this.setState({RulesIpt:this.Trim(e.target.value)})} placeholder={window.languagelist.Quiet_hours_No_shoes_in_the_house} value={this.state.RulesIpt}  onKeyPress={this.keykeypress.bind(this)} />
                  <button type="submit" onClick={(e)=>this.AdditionalRules(e)} >{window.languagelist.Add}</button>
                </div>

                <h3 className={this.state.guests_know == 0 ? "textpink":""} onClick={(e)=>this.setState({guests_know:1})}>{window.languagelist.Details_guests_must_know_about_your_home}</h3>
                <br/>
                <div className={this.state.guests_know == 0 ? "hide":"show"}>
                  <div className="check1" onClick={(e) => {if(this.state.climb_stairs ==0 )this.setState({climb_stairs:1});else this.setState({climb_stairs:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.climb_stairs ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.Must_climb_stairs}</p>
                  </div>
                  <div className="check1" onClick={(e) => {if(this.state.Potential_noise ==0 )this.setState({Potential_noise:1});else this.setState({Potential_noise:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.Potential_noise ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.Potential_for_noise}</p>
                  </div>
                  <div className="check1" onClick={(e) => {if(this.state.property_Pet ==0 )this.setState({property_Pet:1});else this.setState({property_Pet:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.property_Pet ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.Pets_live_on_property}</p>
                  </div>
                  <div className="check1" onClick={(e) => {if(this.state.property_parking ==0 )this.setState({property_parking:1});else this.setState({property_parking:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.property_parking ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.No_parking_on_property}</p>
                  </div>
                  <div className="check1" onClick={(e) => {if(this.state.shared_spaces ==0 )this.setState({shared_spaces:1});else this.setState({shared_spaces:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.shared_spaces ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.Some_spaces_are_shared}</p>
                  </div>
                  <div className="check1" onClick={(e) => {if(this.state.Amenity_limitations ==0 )this.setState({Amenity_limitations:1});else this.setState({Amenity_limitations:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.Amenity_limitations ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.Amenity_limitations}</p>
                  </div>
                  <div className="check1" onClick={(e) => {if(this.state.property_recording ==0 )this.setState({property_recording:1});else this.setState({property_recording:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.property_recording ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.D_Survellance_or_recording_devices_on_property}</p>
                  </div>
                  <div className="check1" onClick={(e) => {if(this.state.property_Weapons ==0 )this.setState({property_Weapons:1});else this.setState({property_Weapons:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.property_Weapons ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.Weapons_on_property}</p>
                  </div>
                  <div className="check1" onClick={(e) => {if(this.state.property_animals ==0 )this.setState({property_animals:1});else this.setState({property_animals:0});}}>
                    <p  className="Pinput">
                        <img className={this.state.property_animals ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                    </p>
                    <p className="divinput">{window.languagelist.Dangerous_animals_on_property}</p>
                  </div>
                </div>

              </div>


              

             
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                    <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
               
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                 <div>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <p>{window.languagelist.Guests_will_only_be_able_to_book}</p>
                </div>
             </div>
    

             
             </div>
		)
	}
}

export default House_step10