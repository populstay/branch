import React, {Component} from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import { Map, Marker } from 'react-amap';

class House_step3 extends Component {

	constructor(props) {
		super(props)
		const _this = this;

		this.STEP = {
			Step: 3,
		}

		this.state = {
			Countrys: [],
			roomstuff_Country: "",
			roomstuff_Street: "",
			roomstuff_Apt: "",
			roomstuff_City: "",
			roomstuff_ZIPCode: "",
			position: {
				lng: 113.867164,
				lat: 22.575645
			},
		};

		languageService.language();

		this.mapEvents = {
			created(map) {
				_this.map = map;

			},
			click(e) {
				const lnglat = e.lnglat;
				_this.setState({
					position: lnglat,
				});
			}
		};
	}

	componentWillMount() {
		this.setState({
			Countrys: window.languagelist.Countrys,
			roomstuff_Country: window.languagelist.Please_choose,
		});

		var listStorage = JSON.parse(sessionStorage.getItem('step3'));
		if(listStorage) {
			this.setState({
				Countrys: listStorage.Countrys,
				roomstuff_Country: listStorage.roomstuff_Country,
				roomstuff_Street: listStorage.roomstuff_Street,
				roomstuff_Apt: listStorage.roomstuff_Apt,
				roomstuff_City: listStorage.roomstuff_City,
				roomstuff_ZIPCode: listStorage.roomstuff_ZIPCode,

			})
		}

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step3', JSON.stringify(this.state));
		sessionStorage.setItem('step', obj);
	}

	Trim(str) {
		return str.replace(/(^\s*)|(\s*$)/g, "");
	}

	ZIPCodeTrim(str) {
		return str.replace(/[\sa-zA-Z\u4e00-\u9fa5_']/g, "");
	}

	render() {

		return(
			<div className="row Step3">
            <div className="col-md-7 col-lg-7 col-sm-12">
            <div className="STEPhead">
              <span className="bjpink"></span>
              <span className="bjpink"></span>
              <span className="bjpink"></span>
              <span></span>
              <p>{window.languagelist.Step1}: {window.languagelist.Start_with_the_basics}</p>
            </div>

              <h1>{window.languagelist.Wheres_your_place_located}</h1>
              
              <div className="Stepbox">
                <div className="col-md-12 col-lg-12 Step3box">
                  <h2>{window.languagelist.Country} / {window.languagelist.Region}</h2>
                  <div className="btn-group col-md-12">
                    <button type="button" data-toggle="dropdown">{this.state.roomstuff_Country}<span>▼</span></button>
                    <ul className="dropdown-menu" role="menu">
                      {this.state.Countrys.map((item,index) => (
                          <li><a onClick={(e)=>this.setState({roomstuff_Country:item})} >{item}</a></li>
                        ))
                      }
                    </ul>
                  </div>
                </div>

                <div className="col-md-12 col-lg-12 Step3box">
                  <h2>{window.languagelist.Street_Address}<span>{window.languagelist.eg_Blk_35_Mandalay_Road}</span></h2>
                  <input onChange={(e) => this.setState({roomstuff_Street: this.Trim(e.target.value)})} value={this.state.roomstuff_Street}  type="text" />
                </div>

                <div className="col-md-12 col-lg-12 Step3box">
                  <h2>{window.languagelist.Apt_Suite_optional}<span>{window.languagelist.eg1337_Mandalay_Towers} </span></h2>
                  <input onChange={(e) => this.setState({roomstuff_Apt: this.Trim(e.target.value)})} value={this.state.roomstuff_Apt}   type="text" />
                </div>

                <div className="col-md-12 col-lg-12 Step3box">
                  <div className="col-md-12 col-lg-6 Step3box">
                    <h2>{window.languagelist.City}<span>{window.languagelist.eg_Singapore}</span></h2>
                    <input  onChange={(e) => this.setState({roomstuff_City: this.Trim(e.target.value)})} value={this.state.roomstuff_City}   type="text" />
                  </div>
                  <div className=" col-md-12 col-lg-6 Step3box right">
                    <h2>{window.languagelist.ZIP_Code}<span>{window.languagelist.eg_308215}</span></h2>
                    <input  onChange={(e) => this.setState({roomstuff_ZIPCode: this.ZIPCodeTrim(e.target.value)})} value={this.state.roomstuff_ZIPCode}  type="text" />
                    <br/>
                    <small className={this.state.roomstuff_ZIPCode.length != 6 ?  "hide color-pink" : "hide color-pink" } >{window.languagelist.Zip_code_format_is_incorrect}</small>
                  </div>
                </div>

              </div>
              
              <h3>{window.languagelist.Is_the_pin_in_the_right_place}</h3>
              <p>{window.languagelist.If_needed_you_can_drag_the_pin_to_adjust_its_location}</p>




            <div className="STEPBTN">
              <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
              <button  className={ this.state.roomstuff_Country == window.languagelist.Please_choose || this.state.roomstuff_Street == "" || this.state.roomstuff_Apt == "" || this.state.roomstuff_City == "" || this.state.roomstuff_ZIPCode == ""   ? "buttonActive Right" : "Right"} disabled={ this.state.roomstuff_Country == window.languagelist.Please_choose || this.state.roomstuff_Street == "" || this.state.roomstuff_Apt == "" || this.state.roomstuff_City == "" || this.state.roomstuff_ZIPCode == "" ? "disabled" : ""} onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
            </div>
             
             </div>
             
             <div className="col-md-5 col-lg-4 col-sm-12 paddingNone">
                 <div>
                    <div className="Map">
                      <Map 
                        center={this.state.position} 
                        zoom={15}
                        events={this.mapEvents}
                      >
                          <Marker 
                              position={this.state.position}
                          >
                              <div className="Mapicon" >
                                  <img src="./images/Map.png" />
                              </div> 
                          </Marker>
                      </Map>
                    </div>
                    <p className="text1">{this.state.position.lng}&nbsp;&nbsp;&nbsp;{this.state.position.lat}</p>
                </div>
             </div>
             </div>

		)
	}
}

export default House_step3