import React, { Component } from 'react';
import languageService from '../../services/language-service';

class House_step8 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 8,
			National_name: ["0244", "93", "335", "213", "376", "1254", "1268", "54", "374", "247", "61", "43", "994", "1242", "973", "880", "1246", "375", "32", "501", "229", "1441", "591", "267", "55", "673", "359", "226", "95", "257", "237", "1", "1345", "236", "235", "56", "86", "57", "242", "682", "506", "53", "357", "420", "45", "253", "1890", "593", "20", "503", "372", "251", "679", "358", "33", "594", "689", "241", "220", "995", "49", "233", "350", "30", "1809", "1671", "502", "224", "592", "509", "504", "852", "36", "354", "91", "62", "98", "964", "353", "972", "39", "225", "1876", "81", "962", "855", "327", "254", "82", "965", "331", "856", "371", "961", "266", "231", "218", "423", "370", "352", "853", "261", "265", "60", "960", "223", "356", "1670", "596", "230", "52", "373", "377", "976", "1664", "212", "258", "264", "674", "977", "599", "31", "64", "505", "227", "234", "850", "47", "968", "92", "507", "675", "595", "51", "63", "48", "351", "1787", "974", "262", "40", "7", "1758", "1784", "684", "685", "378", "239", "966", "221", "248", "232", "65", "421", "386", "677", "252", "27", "34", "94", "1758", "1784", "249", "597", "268", "46", "41", "963", "886", "992", "255", "66", "228", "676", "1809", "216", "90", "993", "256", "380", "971", "44", "1", "598", "233", "58", "84", "967", "381", "263", "243", "260"],
		}

		this.state = {
			roomdescription_phone: "",
			roomstuff_AreaCode: 86,
			phoneactive: 0,
		};

		languageService.language();

	}

	phonenumber(e) {
		this.setState({
			state: this.state.roomdescription_phone = e
		});
		var rephone = /^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/;
		if(e.length != "" && rephone.test(e)) {
			this.setState({
				state: this.state.phoneactive = 1
			});
		} else {
			this.setState({
				state: this.state.phoneactive = 0
			});
		}
	}

	componentWillMount() {
		var listStorage = JSON.parse(sessionStorage.getItem('step8'));
		if(listStorage) {
			this.setState({
				roomdescription_phone: listStorage.roomdescription_phone,
				roomstuff_AreaCode: listStorage.roomstuff_AreaCode,
				phoneactive: listStorage.phoneactive,
			})
		}

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
		sessionStorage.setItem('step8', JSON.stringify(this.state));
	}

	render() {
		return(
			<div className="row Step8">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <p>{window.languagelist.Step2}: {window.languagelist.Set_the_scene}</p>
              </div>

              <h1>{window.languagelist.Add_your_mobile_number}</h1>


              <div className="box col-md-10">
                <div className="stepbox">
                <div className="phoneimg"><img className="becomehost__info" src="./images/phoneimg.png" alt=""/></div>

                <div className="btn-group col-md-12 phonecode">
                  <span data-toggle="dropdown">+{this.state.roomstuff_AreaCode}</span>
                  <ul className="dropdown-menu" role="menu">
                    {this.STEP.National_name.map((item,index) => (
                        <li><a onClick={(e)=>this.setState({roomstuff_AreaCode:item})} >+{item}</a></li>
                      ))
                    }
                  </ul>
                </div>
                
                <input onChange={(e) => this.phonenumber(e.target.value)} value={this.state.roomdescription_phone}  type="number" />

                <img className={this.state.phoneactive == 1 ? "show" : "hide"} src="./images/landloard_page-30.png" alt=""/>
              </div>
              </div>


              

             
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className={ this.state.phoneactive == 0 ? "buttonActive Right" : "Right"} disabled={ this.state.phoneactive == 0 ? "disabled" : ""} onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
               
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                 <div>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <p>{window.languagelist.Only_confirmed_guests_get_your_phone_number}</p>
                </div>
             </div>
    

             
             </div>
		)
	}
}

export default House_step8