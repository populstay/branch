import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step12 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 12,
		}

		this.state = {
			Cancellations: 0,
		}

		languageService.language();
	}

	componentWillMount() {
		var listStorage = JSON.parse(sessionStorage.getItem('step12'));
		if(listStorage) {
			this.setState({
				roomstuff_Closet_drwers: listStorage.roomstuff_Closet_drwers,
			})
		}
	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step12', JSON.stringify(this.state));
		sessionStorage.setItem('step', obj);
	}

	render() {
		return(
			<div className="row Step12">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>
              <h1>{window.languagelist.Choose_Your_Cancellation_Method}</h1>

              <div className="box col-md-12">
                  <div className="col-md-12 form-group">
                      <div className="radio" onClick={(e) => this.setState({Cancellations: 0})}>
                        <label className="text-muted"><p><span className={this.state.Cancellations == 0 ?"show":"hide"}></span></p>{window.languagelist.Flexible}</label>
                      </div>
                      <div className="radio" onClick={(e) => this.setState({Cancellations: 1})}>
                        <label className="text-muted"><p><span className={this.state.Cancellations == 1 ?"show":"hide"}></span></p>{window.languagelist.Moderate}</label>
                      </div>
                      <div className="radio" onClick={(e) => this.setState({Cancellations: 2})}>
                        <label className="text-muted"><p><span className={this.state.Cancellations == 2 ?"show":"hide"}></span></p>{window.languagelist.Strict}</label>
                      </div>
                  </div>
                  <a target="_blank" href="/Cancellations"><p className="color-pink" >{window.languagelist.Cancellations}</p></a>
              </div>

              <h1 className="hide">{window.languagelist.Successful_hosting_starts}</h1>
              <div className="box col-md-12 hide">
                <p>{window.languagelist.Guests_will_book_available_days_instantly}</p>
                <p>{window.languagelist.Cancelling_disrupts_guests_plans}</p>

                <div className="check"  onClick={(e) => {if(this.state.roomstuff_Closet_drwers ==0 )this.setState({roomstuff_Closet_drwers:1});else this.setState({roomstuff_Closet_drwers:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_Closet_drwers ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Got_it_Ill_keep_my_calendar_up_to_date}</p> 
                </div>
              </div>
             
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button  className={ this.state.roomstuff_Closet_drwers ==0 ? "buttonActive Right" : "Right"} disabled={ this.state.roomstuff_Closet_drwers ==0 ? "disabled" : ""} onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
             </div>

              <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                   <div>
                      <img className="becomehost__info" src="./images/step3_4img4.png" alt=""/>
                      <p>{window.languagelist.Based_on_your_responses}</p>
                  </div>
               </div>
             </div>
		)
	}
}

export default House_step12