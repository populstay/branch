import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step14 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 14,
		}

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,

		});

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="row Step11">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <p>{language.Step3}: {language.Get_ready_for_guests}</p>
              </div>

              <h1>{language.Lets_talk_pricing_essentials}</h1>


              <div className="box col-md-12">
                <div className="boxdiv">
                  <div className="col-lg-3 pull-left">
                    <img  src="../images/step3_10img3.png" />
                  </div>
                  <div className="col-lg-9 pull-right">
                    <h3>{language.Start_by_choosing_a_price_range}</h3>
                    <p>{language.Factor_in_things_like_your_location}</p>
                  </div>
                </div>
                <div className="boxdiv">
                  <div className="col-lg-3 pull-left">
                    <img  src="../images/step3_10img2.png" />
                  </div>
                  <div className="col-lg-9 pull-right">
                    <h3>{language.Demand_changes_your_price_should_too}</h3>
                    <p>{language.Take_advantage_of_high_demand}</p>
                  </div>
                </div>
                <div className="boxdiv">
                  <div className="col-lg-3 pull-left">
                    <img  src="../images/step3_10img.png" />
                  </div>
                  <div className="col-lg-9 pull-right">
                    <h3>{language.Were_here_to_help}</h3>
                    <p>{language.We_offer_tools_to_help_you}</p>
                  </div>
                </div>
              </div>


              
             
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{language.Back}</button>
                <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{language.Next}</button>
              </div>
               
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                 <div>
                    <h6>39%</h6>
                    <p>{language.Hosts_who_use_Smart_Pricing_earn_an_average}</p>
                </div>
             </div>
    

             
             </div>
		)
	}
}

export default House_step14