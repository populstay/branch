import React, { Component } from 'react';
import languageService from '../../services/language-service';

class House_step4 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 4,
		}

		this.state = {
			roomstuff_Essentials: 0,
			roomstuff_Shampoo: 0,
			roomstuff_Closet_drwers: 0,
			roomstuff_TV: 0,
			roomstuff_Heat: 0,
			roomstuff_aircondition: 0,
			roomstuff_breakfastcoffetea: 0,
			roomstuff_desk_workspace: 0,
			roomstuff_fireplace: 0,
			roomstuff_iron: 0,
			roomstuff_hairdryer: 0,
			roomstuff_petsinhouse: 0,
			roomstuff_private_entrance: 0,
			roomstuff_smartpincode: 0,
			roomstuff_smoke_detector: 0,
			roomstuff_smartpincode_password: '',
			roomstuff_smartpincode_confirmpassword: '',
			roomstuff_Pool: 0,
			roomstuff_kitchen: 0,
			roomstuff_washer: 0,
			roomstuff_dryer: 0,
			roomstuff_Park: 0,
			roomstuff_Lift: 0,
			roomstuff_HotTub: 0,
			roomstuff_Gym: 0,
			roomstuff_wifi: 0,
		};

		languageService.language();

	}

	componentWillMount() {
		var listStorage = JSON.parse(sessionStorage.getItem('step4'));
		if(listStorage) {
			this.setState({
				roomstuff_Essentials: listStorage.roomstuff_Essentials,
				roomstuff_Shampoo: listStorage.roomstuff_Shampoo,
				roomstuff_Closet_drwers: listStorage.roomstuff_Closet_drwers,
				roomstuff_TV: listStorage.roomstuff_TV,
				roomstuff_Heat: listStorage.roomstuff_Heat,
				roomstuff_aircondition: listStorage.roomstuff_aircondition,
				roomstuff_breakfastcoffetea: listStorage.roomstuff_breakfastcoffetea,
				roomstuff_desk_workspace: listStorage.roomstuff_desk_workspace,
				roomstuff_fireplace: listStorage.roomstuff_fireplace,
				roomstuff_iron: listStorage.roomstuff_iron,
				roomstuff_hairdryer: listStorage.roomstuff_hairdryer,
				roomstuff_petsinhouse: listStorage.roomstuff_petsinhouse,
				roomstuff_private_entrance: listStorage.roomstuff_private_entrance,
				roomstuff_smartpincode: listStorage.roomstuff_smartpincode,
				roomstuff_smoke_detector: listStorage.roomstuff_smoke_detector,
				roomstuff_smartpincode_password: listStorage.roomstuff_smartpincode_password,
				roomstuff_smartpincode_confirmpassword: listStorage.roomstuff_smartpincode_confirmpassword,
				roomstuff_Pool: listStorage.roomstuff_Pool,
				roomstuff_kitchen: listStorage.roomstuff_kitchen,
				roomstuff_washer: listStorage.roomstuff_washer,
				roomstuff_dryer: listStorage.roomstuff_dryer,
				roomstuff_Park: listStorage.roomstuff_Park,
				roomstuff_Lift: listStorage.roomstuff_Lift,
				roomstuff_HotTub: listStorage.roomstuff_HotTub,
				roomstuff_Gym: listStorage.roomstuff_Gym,
				roomstuff_wifi: listStorage.roomstuff_wifi,
			})
		}

	}

	Safety_amenities() {
		if(this.state.roomstuff_smartpincode_confirmpassword == this.state.roomstuff_smartpincode_password) {
			return "ok"
		} else {
			return window.languagelist.Two_passwords_are_different
		}
	}

	Step = (obj) => {
		if(this.state.roomstuff_smartpincode == 1) {
			if(this.state.roomstuff_smartpincode_password != '' && this.state.roomstuff_smartpincode_confirmpassword != '' && this.state.roomstuff_smartpincode_password == this.state.roomstuff_smartpincode_confirmpassword) {
				this.setState({
					state: this.state.PasswordActibve = 1
				});
				this.props.house_Step(obj);
				sessionStorage.setItem('step4', JSON.stringify(this.state));
				sessionStorage.setItem('step', obj);
			} else {
				this.setState({
					state: this.state.PasswordActibve = 0
				});
			}
		} else {
			this.props.house_Step(obj);
			sessionStorage.setItem('step4', JSON.stringify(this.state));
			sessionStorage.setItem('step', obj);
		}
	}

	Trim(str) {
		return str.replace(/[\sa-zA-Z_+*/*&^%$#@!~-]/g, "");
	}

	render() {

		return(

			<div className="row Step4">
              <div className="col-md-7 col-lg-7 col-sm-12">
               <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <p>{window.languagelist.Step1}: {window.languagelist.Start_with_the_basics}</p>
              </div>


              <div className="Stepbox">
                <h1>{window.languagelist.What_amenities_do_you_offer}</h1>
                <div onClick={(e) => {if(this.state.roomstuff_Essentials ==0 )this.setState({roomstuff_Essentials:1});else this.setState({roomstuff_Essentials:0});}}>
                  <p className="Pinput" >
                    <img className={this.state.roomstuff_Essentials ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <div className="divinput">
                    <p>{window.languagelist.Essentials}</p>
                    <p>{window.languagelist.Towels_bed_sheets_soap_toilet_paper_and_pillows}</p>
                  </div>
                </div>

                <div  onClick={(e) => {if(this.state.roomstuff_Shampoo ==0 )this.setState({roomstuff_Shampoo:1});else this.setState({roomstuff_Shampoo:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_Shampoo ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Shampoo}</p> 
                 
                </div>

                <div  onClick={(e) => {if(this.state.roomstuff_Closet_drwers ==0 )this.setState({roomstuff_Closet_drwers:1});else this.setState({roomstuff_Closet_drwers:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_Closet_drwers ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Closet_drawers}</p> 
                </div>

                  <div  onClick={(e) => {if(this.state.roomstuff_TV ==0 )this.setState({roomstuff_TV:1});else this.setState({roomstuff_TV:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_TV ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.TV}</p>
                </div>


                  <div onClick={(e) => {if(this.state.roomstuff_Heat ==0 )this.setState({roomstuff_Heat:1});else this.setState({roomstuff_Heat:0});}}>
                  <p  className="Pinput" >
                      <img className={this.state.roomstuff_Heat ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Heat}</p>
                </div>


                  <div onClick={(e) => {if(this.state.roomstuff_aircondition ==0 )this.setState({roomstuff_aircondition:1});else this.setState({roomstuff_aircondition:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_aircondition ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Air_conditioning}</p>
                </div>

                  <div onClick={(e) => {if(this.state.roomstuff_breakfastcoffetea ==0 )this.setState({roomstuff_breakfastcoffetea:1});else this.setState({roomstuff_breakfastcoffetea:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_breakfastcoffetea ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Breakfast_coffe_tea}</p>
                  
                </div>

                  <div onClick={(e) => {if(this.state.roomstuff_desk_workspace ==0 )this.setState({roomstuff_desk_workspace:1});else this.setState({roomstuff_desk_workspace:0});}}>
                  <p  className="Pinput" >
                      <img className={this.state.roomstuff_desk_workspace ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Desk_workspace}</p>
                </div>

                  <div onClick={(e) => {if(this.state.roomstuff_fireplace ==0 )this.setState({roomstuff_fireplace:1});else this.setState({roomstuff_fireplace:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_fireplace ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Fireplace}</p>
                  </div>

                  <div  onClick={(e) => {if(this.state.roomstuff_iron ==0 )this.setState({roomstuff_iron:1});else this.setState({roomstuff_iron:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_iron ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Iron}</p>
                </div>

                  <div onClick={(e) => {if(this.state.roomstuff_hairdryer ==0 )this.setState({roomstuff_hairdryer:1});else this.setState({roomstuff_hairdryer:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_hairdryer ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Hair_dryer}</p>
                </div>

                  <div onClick={(e) => {if(this.state.roomstuff_petsinhouse ==0 )this.setState({roomstuff_petsinhouse:1});else this.setState({roomstuff_petsinhouse:0});}}>
                  <p  className="Pinput" >
                      <img className={this.state.roomstuff_petsinhouse ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Pets_in_the_house}</p>
                </div>
                  <div onClick={(e) => {if(this.state.roomstuff_private_entrance ==0 )this.setState({roomstuff_private_entrance:1});else this.setState({roomstuff_private_entrance:0});}}>
                  <p  className="Pinput" >
                      <img className={this.state.roomstuff_private_entrance ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Private_entrance}</p>
                </div>
                <div onClick={(e) => {if(this.state.roomstuff_wifi ==0 )this.setState({roomstuff_wifi:1});else this.setState({roomstuff_wifi:0});}}>
                  <p  className="Pinput" >
                      <img className={this.state.roomstuff_wifi ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.WIFI}</p>
                </div>

                <h2>{window.languagelist.Safety_amenities}</h2>
                 <div>
                  <p  className="Pinput"  onClick={(e) => {if(this.state.roomstuff_smartpincode ==0 )this.setState({roomstuff_smartpincode:1});else this.setState({roomstuff_smartpincode:0,roomstuff_smartpincode_password:'',roomstuff_smartpincode_confirmpassword :''});}}>
                      <img className={this.state.roomstuff_smartpincode ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput"  onClick={(e) => {if(this.state.roomstuff_smartpincode ==0 )this.setState({roomstuff_smartpincode:1});else this.setState({roomstuff_smartpincode:0,roomstuff_smartpincode_password:'',roomstuff_smartpincode_confirmpassword :''});}}>{window.languagelist.Smart_pin_code}</p>

                  <div className={this.state.roomstuff_smartpincode == 0 ? "hide" : ""}>
                      <div className="control-group">
                        <label className="control-label">{window.languagelist.Enter_your_password}</label>
                        <input type="password" placeholder={window.languagelist.Password_length_is_not_less_than_6_digits} className="controls" onChange={(e) => this.setState({roomstuff_smartpincode_password: this.Trim(e.target.value),PasswordActibve:1,roomstuff_smartpincode_confirmpassword:""})} value={this.state.roomstuff_smartpincode_password} />
                        <span className={this.state.PasswordActibve == 0 ? 'glyphicon glyphicon-remove-sign' : ''}></span>   
                      </div>

                      <div className="control-group control-group1">
                       <label className="control-label">{window.languagelist.ConFirm_your_password}</label>
                       <input type="password" placeholder={window.languagelist.Password_length_is_not_less_than_6_digits} className="controls" disabled={this.state.roomstuff_smartpincode_password.length < 6 ? "true" : "" } onChange={(e) => this.setState({roomstuff_smartpincode_confirmpassword: this.Trim(e.target.value),PasswordActibve:1})} value={this.state.roomstuff_smartpincode_confirmpassword} />
                        <span className={this.state.PasswordActibve == 0 ? 'glyphicon glyphicon-remove-sign' : ''}></span>  
                        <br/>
                        { this.Safety_amenities() != "ok" &&
                          <small className={this.state.roomstuff_smartpincode_confirmpassword == "" ? "hide pinkColor" : "show pinkColor"} >{this.Safety_amenities()}</small>
                        }
                     </div>
                  </div>
                </div>

                <div className="detector"  onClick={(e) => {if(this.state.roomstuff_smoke_detector ==0 )this.setState({roomstuff_smoke_detector:1});else this.setState({roomstuff_smoke_detector:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_smoke_detector ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Smoke_detector}</p>
                </div>

                <h1>{window.languagelist.What_spaces_can_guests_use}</h1>
                
                <div  onClick={(e) => {if(this.state.roomstuff_Pool ==0 )this.setState({roomstuff_Pool:1});else this.setState({roomstuff_Pool:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_Pool ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Pool}</p> 
                </div>

                <div onClick={(e) => {if(this.state.roomstuff_kitchen ==0 )this.setState({roomstuff_kitchen:1});else this.setState({roomstuff_kitchen:0});}}>
                  <p  className="Pinput" >
                      <img className={this.state.roomstuff_kitchen ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.kitchen}</p> 
                </div>

                  <div  onClick={(e) => {if(this.state.roomstuff_washer ==0 )this.setState({roomstuff_washer:1});else this.setState({roomstuff_washer:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_washer ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Laundry_washer}</p>
                </div>


                  <div onClick={(e) => {if(this.state.roomstuff_dryer ==0 )this.setState({roomstuff_dryer:1});else this.setState({roomstuff_dryer:0});}}>
                  <p  className="Pinput" >
                      <img className={this.state.roomstuff_dryer ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Laundry_dryer}</p>
                </div>


                  <div onClick={(e) => {if(this.state.roomstuff_Park ==0 )this.setState({roomstuff_Park:1});else this.setState({roomstuff_Park:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_Park ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Park}</p>
                </div>

                  <div onClick={(e) => {if(this.state.roomstuff_Lift ==0 )this.setState({roomstuff_Lift:1});else this.setState({roomstuff_Lift:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_Lift ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Lift}</p>
                  
                </div>

                  <div onClick={(e) => {if(this.state.roomstuff_HotTub ==0 )this.setState({roomstuff_HotTub:1});else this.setState({roomstuff_HotTub:0});}}>
                  <p  className="Pinput" >
                      <img className={this.state.roomstuff_HotTub ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Hot_tub}</p>
                </div>

                  <div onClick={(e) => {if(this.state.roomstuff_Gym ==0 )this.setState({roomstuff_Gym:1});else this.setState({roomstuff_Gym:0});}}>
                  <p  className="Pinput">
                      <img className={this.state.roomstuff_Gym ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                  </p>
                  <p className="divinput">{window.languagelist.Gym}</p>
                  </div>

          </div>

          

              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
              </div>
              <div className="col-md-5 col-lg-4  col-sm-12 paddingNone rightbox">
                  <div>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <p>{window.languagelist.Provide_the_essentials_helps_guests_feel_at_home_in_your_place}</p>
                    <p>{window.languagelist.Some_hosts_provide_breakfast_or_just_coffee_and_tea}</p>
                  </div>
               </div>
          </div>
		)
	}
}

export default House_step4