import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step19 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 19,
		}

		this.state = {
			Months: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
			requirements_book: 1,
			starting_host: 1,
			confirmation_booking: 1,
			Welcome_guests: 1,
		}

		languageService.language();
	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
	}

	render() {

		return(
			<div className="row Step11 Step19">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>

              <h1>{window.languagelist.Based_on_your_settings__heres_what_you_could_expect}</h1>

              <div className="box col-md-12">
                <div className="boxdiv">
                  <div className="col-lg-1 radio">
                    <p><span className={this.state.starting_host == 1 ?"show":"hide"}></span></p>
                  </div>
                  <div className="col-lg-9  content">
                    <h3> {new Date().getDate()+2} {this.state.Months[new Date().getMonth()]} {window.languagelist.Youre_available_to_host_starting}</h3>
                    <p>{window.languagelist.Lou_is_planning_her_trip_and_thinks_your_listing_is_perfect}</p>
                  </div>
                </div>

                <div className="boxdiv">
                  <div className="col-lg-1 radio">
                    <p><span className={this.state.requirements_book == 1 ?"show":"hide"}></span></p>
                  </div>
                  <div className="col-lg-9  content">
                    <h3>{window.languagelist.Guests_who_meet_PopulStay_requirements}</h3>
                    <p>{window.languagelist.In_addition_to_meeting_guest_requirements}</p>
                  </div>
                </div>

                <div className="boxdiv">
                  <div className="col-lg-1 radio">
                    <p><span className={this.state.confirmation_booking == 1 ?"show":"hide"}></span></p>
                  </div>
                  <div className="col-lg-9  content">
                    <h3>{window.languagelist.Guests_send_a_message_with_their_booking_confirmation}</h3>
                    <p>{window.languagelist.Lou_says_shell_be_in_town_for_work_and_shed_love_to_stay_with_you}</p>
                  </div>
                </div>

                <div className="boxdiv">
                  <div className="col-lg-1 radio">
                    <p><span className={this.state.Welcome_guests == 1 ?"show":"hide"}></span></p>
                  </div>
                  <div className="col-lg-9  content">
                    <h3>{window.languagelist.Welcome_guests_to_your_space}</h3>
                    <p>{window.languagelist.Before_Lou_arrives}</p>
                  </div>
                </div>


              </div>

              
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
               
             </div>



             
             </div>
		)
	}
}

export default House_step19