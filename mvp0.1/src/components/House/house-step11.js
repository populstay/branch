import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step11 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 11,
		}

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,

		});

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="row Step11">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span></span>
                <span></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>

              <h1>{window.languagelist.Heres_how_guests_will_book_with_you}</h1>


              <div className="box col-md-12">
                <div className="boxdiv">
                  <div className="col-lg-3 pull-left">
                    <img src="../images/step3_4img1.png" />
                  </div>
                  <div className="col-lg-9 pull-right">
                    <h3>{window.languagelist.Qualified_guests_find_your_listing}</h3>
                    <p>{window.languagelist.Anyone_who_wants_to_book_with}</p>
                  </div>
                </div>
                <div className="boxdiv">
                  <div className="col-lg-3 pull-left">
                    <img src="../images/step3_4img2.png" />
                  </div>
                  <div className="col-lg-9 pull-right">
                    <h3>{window.languagelist.You_set_controls_for_who_can_book}</h3>
                    <p>{window.languagelist.To_book_available_dates_without_having}</p>
                    <p className="textpink">{window.languagelist.I_want_to_review_every_request}</p>
                  </div>
                </div>
                <div className="boxdiv">
                  <div className="col-lg-3 pull-left">
                    <img src="../images/step3_4img3.png" />
                  </div>
                  <div className="col-lg-9 pull-right">
                    <h3>{window.languagelist.Once_a_guest_books_you_get_notified}</h3>
                    <p>{window.languagelist.Youll_immediately_get_a_confirmation}</p>
                  </div>
                </div>
              </div>

              
             
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
               
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                 <div>
                    <img className="becomehost__info" src="./images/step3_4img4.png" alt=""/>
                    <p>{window.languagelist.In_the_rare_case_there_are_issues}</p>
                    <h5>{window.languagelist.Set_rules_for_who_can_book_instantly}</h5>
                </div>
             </div>
    

             
             </div>
		)
	}
}

export default House_step11