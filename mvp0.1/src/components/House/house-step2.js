import React, {Component} from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';

class House_step2 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 2,
		}

		this.state = {
			roombasics_guestsnumber: 1,
			roombasics_guestbathroom: 0.5,
			roombasics_guestbedroom: 1,
			roombasics_guestbed: 1,
			bathroom_private: 0,
			roombasics_Childrens_bed: 0,
			roombasics_Add_beds: 0,
			roombasics_Double_bed: 0,
			roombasics_Sofa_bed: 0,
			roombasics_Air_mattress: 0,
			roombasics_Baby_cot: 0,
			roombasics_Hammock: 0,
			roombasics_Water_bed: 0,
			Add_beds: 999,
			bedroom: [{
				roombasics_Childrens_bed: 0,
				roombasics_Add_beds: 0,
				roombasics_Double_bed: 0,
				roombasics_Sofa_bed: 0,
				roombasics_Air_mattress: 0,
				roombasics_Baby_cot: 0,
				roombasics_Hammock: 0,
				roombasics_Water_bed: 0,
			}],
		};

		languageService.language();
	}

	componentWillMount() {

		console.log(this.state.bedroom[0].roombasics_Childrens_bed)

		var listStorage = JSON.parse(sessionStorage.getItem('step2'));
		if(listStorage) {
			this.setState({
				roombasics_guestsnumber: listStorage.roombasics_guestsnumber,
				roombasics_guestbathroom: listStorage.roombasics_guestbathroom,
				roombasics_guestbedroom: listStorage.roombasics_guestbedroom,
				roombasics_guestbed: listStorage.roombasics_guestbed,
				bathroom_private: listStorage.bathroom_private,
				roombasics_Childrens_bed: listStorage.roombasics_Childrens_bed,
				roombasics_Add_beds: listStorage.roombasics_Add_beds,
				roombasics_Double_bed: listStorage.roombasics_Double_bed,
				roombasics_Sofa_bed: listStorage.roombasics_Sofa_bed,
				roombasics_Air_mattress: listStorage.roombasics_Air_mattress,
				roombasics_Baby_cot: listStorage.roombasics_Baby_cot,
				roombasics_Hammock: listStorage.roombasics_Hammock,
				roombasics_Water_bed: listStorage.roombasics_Water_bed,
				bedroom: listStorage.bedroom,
			})
		}

	}

	bedsnumber(value) {
		var bedsnumber = this.state.bedroom[value].roombasics_Add_beds + this.state.bedroom[value].roombasics_Double_bed + this.state.bedroom[value].roombasics_Childrens_bed + this.state.bedroom[value].roombasics_Sofa_bed + this.state.bedroom[value].roombasics_Air_mattress + this.state.bedroom[value].roombasics_Baby_cot + this.state.bedroom[value].roombasics_Hammock + this.state.bedroom[value].roombasics_Water_bed
		// var bedsnumber = this.state.roombasics_Childrens_bed+this.state.roombasics_Add_beds+this.state.roombasics_Double_bed+this.state.roombasics_Sofa_bed+this.state.roombasics_Air_mattress+this.state.roombasics_Baby_cot+this.state.roombasics_Hammock+this.state.roombasics_Water_bed
		return bedsnumber;
	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step2', JSON.stringify(this.state));
		sessionStorage.setItem('step', obj);
	}

	jianbedroom() {
		if(this.state.roombasics_guestbedroom == 1) {
			this.setState({
				roombasics_guestbedroom: 1
			});
		} else {
			this.setState({
				roombasics_guestbedroom: this.state.roombasics_guestbedroom - 1
			})
		}

		if(this.state.bedroom.length >= 2) {
			this.setState({
				bedroom: this.state.bedroom.filter((elem, i) => this.state.bedroom.length - 1 != i)
			});
		}

	}

	jiabedroom() {
		if(this.state.roombasics_guestbedroom == 16) {
			this.setState({
				roombasics_guestbedroom: 16
			});
		} else {
			this.setState({
				roombasics_guestbedroom: this.state.roombasics_guestbedroom + 1
			})
		}

		var bedroom = this.state.bedroom;
		bedroom.push({
			roombasics_Childrens_bed: 0,
			roombasics_Add_beds: 0,
			roombasics_Double_bed: 0,
			roombasics_Sofa_bed: 0,
			roombasics_Air_mattress: 0,
			roombasics_Baby_cot: 0,
			roombasics_Hammock: 0,
			roombasics_Water_bed: 0,
		});
		this.setState({
			bedroom: bedroom
		});
	}

	render() {

		var list = (length) => {
			var res = [];
			for(var i = 0; i < length; i++) {
				res.push(
					<div className="col-md-12 form-group">

                  <div className="step3box">
                    <div className="divbox" >
                      <div className="divLeft">
                       <h3 className="text-muted">{window.languagelist.bedroom} {i+1} </h3>
                       <h3 className="text-muted">{this.bedsnumber(i)} {window.languagelist.Zhangbeds}</h3>
                      </div>
  
                      <div className="divRight">
                       <button className="btn btn-default btn-lg bg-pink color-white" data-index={i} onClick={(e)=>{if(this.state.Add_beds == event.target.getAttribute("data-index"))this.setState({Add_beds:999});else this.setState({Add_beds:event.target.getAttribute("data-index")})}}>{this.state.Add_beds == i ? window.languagelist.Complete : window.languagelist.Add_beds}</button>
                      </div>
                    </div>

                    <div className={this.state.Add_beds == i ? "show Add_bed" : "hide Add_bed"}>
                        <div className="col-md-12 form-group">
                              <label className="col-md-3">{window.languagelist.Single_bed}</label>
                              <div className="btn-group guestBtn col-md-5">
                                  <span className={this.state.bedroom[i].roombasics_Add_beds == 0 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Add_beds == 0 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Add_beds=0});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Add_beds=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Add_beds-1})}}  data-index={i}></span>
                                    <button type="button">
                                      {this.state.bedroom[i].roombasics_Add_beds}
                                    </button>
                                  <span className={this.state.bedroom[i].roombasics_Add_beds == 5 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Add_beds == 5 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Add_beds=5});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Add_beds=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Add_beds+1})}}  data-index={i}></span>
                              </div>
                        </div>

                        <div className="col-md-12 form-group">
                              <label className="col-md-3">{window.languagelist.Double_bed}</label>
                              <div className="btn-group guestBtn col-md-5">
                                  <span className={this.state.bedroom[i].roombasics_Double_bed == 0 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Double_bed == 0 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Double_bed=0});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Double_bed=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Double_bed-1})}}  data-index={i}></span>
                                    <button type="button">
                                      {this.state.bedroom[i].roombasics_Double_bed}
                                    </button>
                                  <span className={this.state.bedroom[i].roombasics_Double_bed == 5 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Double_bed == 5 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Double_bed=5});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Double_bed=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Double_bed+1})}}  data-index={i}></span>
                              </div>
                        </div>

                        <div className="col-md-12 form-group">
                              <label className="col-md-3">{window.languagelist.Childrens_bed}</label>
                              <div className="btn-group guestBtn col-md-5">
                                  <span className={this.state.bedroom[i].roombasics_Childrens_bed == 0 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Childrens_bed == 0 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Childrens_bed=0});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Childrens_bed=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Childrens_bed-1})}}  data-index={i}></span>
                                    <button type="button">
                                      {this.state.bedroom[i].roombasics_Childrens_bed}
                                    </button>
                                  <span className={this.state.bedroom[i].roombasics_Childrens_bed == 5 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Childrens_bed == 5 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Childrens_bed=5});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Childrens_bed=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Childrens_bed+1})}}  data-index={i}></span>
                              </div>
                        </div>

                        <div className="col-md-12 form-group">
                              <label className="col-md-3">{window.languagelist.Sofa_bed}</label>
                              <div className="btn-group guestBtn col-md-5">
                                  <span className={this.state.bedroom[i].roombasics_Sofa_bed == 0 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Sofa_bed == 0 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Sofa_bed=0});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Sofa_bed=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Sofa_bed-1})}}  data-index={i}></span>
                                    <button type="button">
                                      {this.state.bedroom[i].roombasics_Sofa_bed}
                                    </button>
                                  <span className={this.state.bedroom[i].roombasics_Sofa_bed == 5 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Sofa_bed == 5 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Sofa_bed=5});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Sofa_bed=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Sofa_bed+1})}}  data-index={i}></span>
                              </div>
                        </div>

                        <div className="col-md-12 form-group">
                              <label className="col-md-3">{window.languagelist.Air_mattress}</label>
                              <div className="btn-group guestBtn col-md-5">
                                  <span className={this.state.bedroom[i].roombasics_Air_mattress == 0 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Air_mattress == 0 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Air_mattress=0});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Air_mattress=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Air_mattress-1})}}  data-index={i}></span>
                                    <button type="button">
                                      {this.state.bedroom[i].roombasics_Air_mattress}
                                    </button>
                                  <span className={this.state.bedroom[i].roombasics_Air_mattress == 5 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Air_mattress == 5 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Air_mattress=5});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Air_mattress=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Air_mattress+1})}}  data-index={i}></span>
                              </div>
                        </div>

                        <div className="col-md-12 form-group">
                              <label className="col-md-3">{window.languagelist.Baby_cot}</label>
                              <div className="btn-group guestBtn col-md-5">
                                  <span className={this.state.bedroom[i].roombasics_Baby_cot == 0 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Baby_cot == 0 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Baby_cot=0});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Baby_cot=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Baby_cot-1})}}  data-index={i}></span>
                                    <button type="button">
                                      {this.state.bedroom[i].roombasics_Baby_cot}
                                    </button>
                                  <span className={this.state.bedroom[i].roombasics_Baby_cot == 5 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Baby_cot == 5 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Baby_cot=5});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Baby_cot=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Baby_cot+1})}}  data-index={i}></span>
                              </div>
                        </div>

                        <div className="col-md-12 form-group">
                              <label className="col-md-3">{window.languagelist.Hammock}</label>
                              <div className="btn-group guestBtn col-md-5">
                                  <span className={this.state.bedroom[i].roombasics_Hammock == 0 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Hammock == 0 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Hammock=0});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Hammock=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Hammock-1})}}  data-index={i}></span>
                                    <button type="button">
                                      {this.state.bedroom[i].roombasics_Hammock}
                                    </button>
                                  <span className={this.state.bedroom[i].roombasics_Hammock == 5 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Hammock == 5 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Hammock=5});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Hammock=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Hammock+1})}}  data-index={i}></span>
                              </div>
                        </div>

                        <div className="col-md-12 form-group">
                              <label className="col-md-3">{window.languagelist.Water_bed}</label>
                              <div className="btn-group guestBtn col-md-5">
                                  <span className={this.state.bedroom[i].roombasics_Water_bed == 0 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Water_bed == 0 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Water_bed=0});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Water_bed=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Water_bed-1})}}  data-index={i}></span>
                                    <button type="button">
                                      {this.state.bedroom[i].roombasics_Water_bed}
                                    </button>
                                  <span className={this.state.bedroom[i].roombasics_Water_bed == 5 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Water_bed == 5 )this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Water_bed=5});else this.setState({state:this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Water_bed=this.state.bedroom[event.target.getAttribute("data-index")].roombasics_Water_bed+1})}}  data-index={i}></span>
                              </div>
                        </div>


                    </div>
                  </div>
              </div>
				)
			}
			return res
		}

		return(
			<div className="row Step2">
          <div className="col-md-7 col-lg-7 col-sm-12">
          <div className="STEPhead">
              <span className="bjpink"></span>
              <span className="bjpink"></span>
              <span></span>
              <span></span>
              <p >{window.languagelist.Step1}: {window.languagelist.Start_with_the_basics}</p>
            </div>
            
              <h1>{window.languagelist.How_many_guests_can_your_place_accommodate}</h1>

                  <div className="col-md-12 form-group">
                      <label>{window.languagelist.Number_of_guests}</label>
                      <div className="btn-group guestBtn col-md-4">
                          <span className={this.state.roombasics_guestsnumber == 1 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.roombasics_guestsnumber == 1 )this.setState({roombasics_guestsnumber:1});else this.setState({roombasics_guestsnumber:this.state.roombasics_guestsnumber-1})}}></span>
                            <button type="button">
                              {this.state.roombasics_guestsnumber}
                            </button>
                          <span className={this.state.roombasics_guestsnumber == 16 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.roombasics_guestsnumber == 16 )this.setState({roombasics_guestsnumber:16});else this.setState({roombasics_guestsnumber:this.state.roombasics_guestsnumber+1})}}></span>
                      </div>
                  </div>

                   <div className="col-md-12 form-group">
                      <label>{window.languagelist.How_many_bedrooms_can_guests_use}</label>
                      <div className="btn-group guestBtn col-md-4">
                          <span className={this.state.roombasics_guestbedroom == 1 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>this.jianbedroom()}></span>
                            <button type="button">
                              {this.state.roombasics_guestbedroom}
                            </button>
                          <span className={this.state.roombasics_guestbedroom == 16 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>this.jiabedroom()}></span>
                      </div>
                  </div>

                  <div className="col-md-12 form-group">
                      <label>{window.languagelist.How_many_bathrooms_in_place}</label>
                      <div className="btn-group guestBtn col-md-4">
                          <span className={this.state.roombasics_guestbathroom == 0.5 ? "btnjian spanActive" : "btnjian"} onClick={(e)=>{if(this.state.roombasics_guestbathroom == 0.5 )this.setState({roombasics_guestbathroom:0.5});else this.setState({roombasics_guestbathroom:this.state.roombasics_guestbathroom-0.5})}}></span>
                            <button type="button">
                              {this.state.roombasics_guestbathroom}
                            </button>
                          <span className={this.state.roombasics_guestbathroom == 50 ? "btnjia spanActive" : "btnjia"} onClick={(e)=>{if(this.state.roombasics_guestbathroom == 50 )this.setState({roombasics_guestbathroom:50});else this.setState({roombasics_guestbathroom:this.state.roombasics_guestbathroom+0.5})}}></span>
                      </div>
                  </div>
                  
                  <div className="col-md-12 form-group">
                      <label>{window.languagelist.Is_the_bathroom_private}</label>
                      <div className="radio col-md-6" onClick={(e) => this.setState({bathroom_private: 0})}>
                        <label className="text-muted"><p><span className={this.state.bathroom_private == 0 ?"show":"hide"}></span></p>{window.languagelist.Yes}</label>
                      </div>
                      <div className="radio col-md-6" onClick={(e) => this.setState({bathroom_private: 1})}>
                        <label className="text-muted"><p><span className={this.state.bathroom_private == 1 ?"show":"hide"}></span></p>{window.languagelist.Noits_shared}</label>
                      </div>
                  </div>

                  <h3 className="text-muted Sleeping">{window.languagelist.Sleeping_arrangments}</h3>
                  
                  {list(this.state.roombasics_guestbedroom)}

                 <div className="STEPBTN">
                    <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                    <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
                  </div>
          </div>
          <div className="col-md-5 col-lg-4 col-sm-12 paddingNone rightbox">
              <div>
                <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                <p>{window.languagelist.The_number_and_type}</p>
                <p>{window.languagelist.Sleeping_arrangements_help_guests}</p>
              </div>
          </div>
          </div>

		)
	}
}

export default House_step2