import React, { Component } from 'react';
import dragula from 'react-dragula';

class Photo extends Component {

  constructor(props) {
    super(props)

    this.state={
      items: [1,2,3,4,5,6]
    };

  }


  dragulaDecorator = (componentBackingInstance) => {
    if (componentBackingInstance) {
      let options = { };
      dragula([componentBackingInstance], options);
    }
  }


  render() {

    return (
        <div className='container' ref={this.dragulaDecorator} > 
          <div>Swap me around</div>
          <div>Swap her around</div>
          <div>Swap him around</div>
          <div>Swap them around</div>
          <div>Swap us around</div>
          <div>Swap things around</div>
          <div>Swap everything around</div>
        </div>
    )
  }
}

export default Photo