import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step15 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 15,
		}

		this.state = {
			Price_demand: 0,
		}

		languageService.language();
	}

	componentWillMount() {
		var listStorage = JSON.parse(sessionStorage.getItem('step15'));
		if(listStorage) {
			this.setState({
				Price_demand: listStorage.Price_demand,
			})
		}

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
		sessionStorage.setItem('step15', JSON.stringify(this.state));
	}

	render() {

		return(
			<div className="row Step11">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>

              <h1>{window.languagelist.How_do_you_want_to_set_your_price}</h1>

              <div className="box col-md-12">
                <div className="boxdiv" onClick={(e) => this.setState({Price_demand:1})} >
                  <div className="col-lg-3 pull-left">
                    <img  src="../images/step3_11img1.png" />
                  </div>
                  <div className="col-lg-8 content">
                    <h3>{window.languagelist.Price_adapts_to_demand}</h3>
                    <p>{window.languagelist.You_tell_Smart_Pricing_to_automatically}</p>
                    <span>{window.languagelist.RECOMMENDED}</span>
                  </div>
                  <div className="col-lg-1 radio">
                    <p><span className={this.state.Price_demand == 1 ?"show":"hide"}></span></p>
                  </div>
                </div>
                <div className="boxdiv" onClick={(e) => this.setState({Price_demand:0})} >
                  <div className="col-lg-3 pull-left">
                    <img  src="../images/step3_11img2.png" />
                  </div>
                  <div className="col-lg-8 content">
                    <h3>{window.languagelist.Price_is_fixed}</h3>
                    <p>{window.languagelist.Set_a_base_price}</p>
                  </div>
                  <div className="col-lg-1 radio">
                    <p><span className={this.state.Price_demand == 0 ?"show":"hide"}></span></p>
                  </div>
                </div>
              </div>

              
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
               
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                <div>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <p>{window.languagelist.The_right_price_can_change}</p>
                </div>
             </div>
    

             
             </div>
		)
	}
}

export default House_step15