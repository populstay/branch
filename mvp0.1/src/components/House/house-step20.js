import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step20 extends Component {

	constructor(props) {
		super(props)

		this.state = {
			Step: 20,
			languagelist: {},
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,

		});

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
	}

	render() {
		const language = this.state.languagelist;

		return(
			<div className="row Step20">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span className="bjpink"></span> 
                <p>{language.Step3}: {language.Get_ready_for_guests}</p>
              </div>

              <h1>{language.Your_local_laws_and_taxes}</h1>

              <div className="box col-md-12">
                <h3>{language.Make_sure_you_familiarise}<span className="textpink">{language.PopulStays_Nondiscrimination_Policy}</span></h3>
                <p>{language.Please_educate_yourself_about_the_laws}</p>
                <p>{language.Most_cities_have_rules_covering_homesharing}</p>
                <p>{language.Since_you_are_responsible_for_your_own_decision}</p>
                <p>{language.By_accepting_our_Terms}</p>
              </div>

              
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.state.Step-1)}>{language.Back}</button>
                <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.state.Step+1)}>{language.Next}</button>
              </div>
               
             </div>

             <div className="col-md-6 col-lg-4 col-md-push-1 col-sm-6 paddingNone">
                <img className="stepbg" src="../images/step3_17img.png" alt=""/>
            </div>



             
             </div>
		)
	}
}

export default House_step20