import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';

class House_step5 extends Component {

	constructor(props) {
		super(props)

		this.state = {
			Step: 5,
			languagelist: {},
		};

		languageService.language();

	}

	componentWillMount() {
		guestService.getGuesterInfo(window.address).then((data) => {
			if(data) {
				this.setState({
					user: data.user
				});
			}
		});

		this.setState({
			languagelist: window.languagelist,
		});

		var listStorage10 = JSON.parse(sessionStorage.getItem('step10'));
		var listStorage12 = JSON.parse(sessionStorage.getItem('step12'));
		var listStorage13 = JSON.parse(sessionStorage.getItem('step13'));
		var listStorage15 = JSON.parse(sessionStorage.getItem('step15'));
		var listStorage16 = JSON.parse(sessionStorage.getItem('step16'));
		var listStorage17 = JSON.parse(sessionStorage.getItem('step17'));
		var listStorage18 = JSON.parse(sessionStorage.getItem('step18'));

		if(listStorage10 || listStorage12 || listStorage13 || listStorage15 || listStorage16 || listStorage17 || listStorage18) {
			this.setState({
				step3: true
			})
		}

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="row Step5">
          <div className="col-md-7 col-lg-7 col-sm-12">
          <h1>{language.Great_process} {this.state.user}!</h1>
          <h3>{language.Now_lets_get_some_details}</h3>
          <div className="change">
              <div>
                <p>{language.Bedrooms_beds_amenities_and_more}</p>
                <p className="textpink"  onClick={(e)=>this.Step(1)}>{language.change}</p>
              </div>
              <img  className="becomehost__step-1" src="../images/landloard_page-30.png" alt=""/>
          </div>

          <div className="Step2box">
            <p className="Step2">{language.Step2}</p>
            <h2>{language.Set_the_sence}</h2>
            <p className="Set">{language.photos_short_description_title}</p>
            <button className="btn btn-default btn-lg bg-pink color-white subbtn Left" onClick={(e)=>this.Step(this.state.Step+1)}>{language.Continue}</button>
          </div>

          <div className="Step2box">
            <p className={this.state.step3 ? "Step2" : "Step3"}>{language.Step3}</p>
            <h2>{language.Get_ready_for_guests}</h2>
            <p className={this.state.step3 ? "Set" : "Set1"}>{language.Booking_settings_calendar_price}</p>
            {this.state.step3 &&
              <button className="btn btn-default btn-lg bg-pink color-white subbtn Left" onClick={(e)=>this.Step(10)}>{language.Continue}</button>
            }
          </div>

          <div className="Stepbox1">
            <h2>{language.The_3rd_Party_service_provided_by_host}</h2>

            <div className="service">
              <div>
                  <h5><p>{language.Home_Rapair}<span>▲</span></p></h5>
                  <h5><p>{language.Marketing_Brand}<span>▼</span></p></h5>
                  <h5><p>{language.Photoshooting}<span>▲</span></p></h5>
                  <h5><p>{language.Interior_Design}<span>▼</span></p></h5>
                  <h5><p>{language.Cleaning_Washing}<span>▲</span></p></h5>
              </div>
            </div>

          </div>

          <div className="Rapair" onClick={(e) => {if(this.state.Rapair == 0 )this.setState({Rapair:1});else this.setState({Rapair:0});}}>
              <img  src="../images/footer_icon-19.png" alt=""/>
              <span className="left">{language.We_recommend}: </span>
              <span className="right">{language.Home_Rapair}</span>
          </div>      

          <div  className={this.state.Rapair != 0 ? 'show Shop' : 'hide Shop'}>
              <div className={this.state.Rapair == 1 ? 'show Shoplist' : 'hide Shoplist'}>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
              </div>
              <div className={this.state.Rapair == 2 ? 'show Shoplist' : 'hide Shoplist'}>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
              </div>
              <div  className={this.state.Rapair == 3 ? 'show Shoplist' : 'hide Shoplist'}>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
                  <div className="Shopitem">
                      <div>
                          <span className="left">{language.Shop1}</span>
                          <span className="right">{language.Home_Fix}</span>  
                      </div>
                      <ul>
                          <li><span>{language.Address}: </span> Selffix DIY Vivo City,1 Harbourfront Walk,#B2-20/21 VivoCity,098585</li>
                          <li><span>{language.Contact_No}:</span>  +65 84736394</li>
                          <li><span>{language.Email_Address} : </span> HFC@homefix.com</li>
                          <li><span>{language.Reference} :</span> Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do e</li>
                      </ul>
                  </div>
              </div>

              <ul className="lilist">
                  <li className={this.state.Rapair == 1 ? 'bjpink' : ''}  onClick={(e) => this.setState({Rapair:1})}></li>
                  <li className={this.state.Rapair == 2 ? 'bjpink' : ''}  onClick={(e) => this.setState({Rapair:2})}></li>
                  <li className={this.state.Rapair == 3 ? 'bjpink' : ''}  onClick={(e) => this.setState({Rapair:3})}></li>
              </ul>
          </div>


          </div>
          <div className="col-md-5 col-lg-4 col-md-push-1 col-sm-12 paddingNone">
              <img className="stepbg" src="../images/1.png" alt=""/>
          </div>
          </div>
		)
	}
}

export default House_step5