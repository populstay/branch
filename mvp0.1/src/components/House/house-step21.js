import React, {Component} from 'react';
import languageService from '../../services/language-service';
import Overlay from './../overlay';
import aesService from '../../services/aes-service';
import houselistingService from '../../services/houseinfolist-service';

class House_step21 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 21,
		}

		this.state = {
			PasswordBook: "",
		};

		languageService.language();
	}

	submit() {

		this.setState({
			EnterPassword: true
		})
	}

	Submitkeypress(e) {
		if(e.which === 13){
			this.submithome()
		}
	}

	submithome() {
		var resultJsonObject = {};
		for(var attr in JSON.parse(sessionStorage.getItem('step1'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step1'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step2'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step2'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step3'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step3'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step6'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step6'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step7'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step7'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step8'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step8'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step10'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step10'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step12'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step12'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step13'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step13'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step15'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step15'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step16'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step16'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step17'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step17'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step18'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step18'))[attr];
		}

		for(var attr in JSON.parse(sessionStorage.getItem('step4'))) {
			resultJsonObject[attr] = JSON.parse(sessionStorage.getItem('step4'))[attr];
		}

		for(var attr in this.props.Pictures) {
			resultJsonObject[attr] = this.props.Pictures[attr];
		}

		if(aesService.decrypt(window.aesprivateKey, this.state.PasswordBook)) {
			this.setState({
				EnterPassword: false
			})
			this.setState({
				SUCCESS: true
			});
			houselistingService.submitListing(resultJsonObject, this.state.PasswordBook)
				.then((data) => {
					sessionStorage.removeItem('step1');
					sessionStorage.removeItem('step2');
					sessionStorage.removeItem('step3');
					sessionStorage.removeItem('step4');
					sessionStorage.removeItem('step6');
					sessionStorage.removeItem('step7');
					sessionStorage.removeItem('step8');
					sessionStorage.removeItem('step10');
					sessionStorage.removeItem('step12');
					sessionStorage.removeItem('step13');
					sessionStorage.removeItem('step15');
					sessionStorage.removeItem('step16');
					sessionStorage.removeItem('step17');
					sessionStorage.removeItem('step18');

					if(data.txhash || data.state == -1) {
						sessionStorage.setItem('homeId', data.id);
						this.Step(this.STEP.Step + 1)
					}
				})
				.catch((error) => {
					this.setState({
						SUCCESS: false
					});
				})
		} else {
			this.setState({
				wrong_password: true
			})
		}

	}

	SUCCESS() {
		this.setState({
			SUCCESS: false
		})
		sessionStorage.removeItem('step');
	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.removeItem('step');
	}

	Trim(str) {
		return str.replace(/(^\s*)|(\s*$)/g, "");
	}

	render() {

		return(
			<div className="row Step5 Step21">
          {this.state.EnterPassword &&
          <Overlay imageUrl="/images/circular-check-button.svg">
            <div className="EnterPassword">
              <h2>{window.languagelist.Enter_your_password_Release_house}</h2>
              <input type="password" placeholder={window.languagelist.User_Password} value={this.state.PasswordBook} onChange={(e)=>this.setState({PasswordBook:this.Trim(e.target.value)})} onKeyPress={this.Submitkeypress.bind(this)} />
              <p className={this.state.wrong_password ? "show wrong_password" : "hide wrong_password"}>{window.languagelist.wrong_password}</p>
              <button className={this.state.PasswordBook == "" ? "PasswordNull Left" : "Left"} disabled={this.state.PasswordBook == "" ? "true" : ""} onClick={(e)=>this.submithome()} >{window.languagelist.OK}</button>
              <button className="Right" onClick={(e)=>this.setState({step:this.STEP.Step3_18,EnterPassword:false,wrong_password:false})}>{window.languagelist.Cancel}</button>
            </div>
          </Overlay>
        }

        {!this.state.SUCCESS &&
          <div>
            <div className="col-md-6 col-lg-7 col-sm-6">
              <h1>{window.languagelist.Youre_ready_to_publish}</h1>
              <h3>{window.languagelist.Youll_be_able_to_welcome}</h3>
              <div className="change">
                  <div>
                    <p>{window.languagelist.Bedrooms_beds_amenities_and_more}</p>
                    <p className="textpink"  onClick={(e)=>this.Step(1)}>{window.languagelist.change}</p>
                  </div>
                  <img  className="becomehost__step-1" src="../images/landloard_page-30.png" alt=""/>
              </div>

              <div className="change">
                  <div>
                    <p>{window.languagelist.photos_short_description_title}</p>
                    <p className="textpink" onClick={(e)=>this.Step(6)}>{window.languagelist.change}</p>
                  </div>
                  <img  className="becomehost__step-1" src="../images/landloard_page-30.png" alt=""/>
              </div>

              <div className="change">
                  <div>
                    <p>{window.languagelist.Booking_settings_calendar_price}</p>
                    <p className="textpink" onClick={(e)=>this.Step(10)}>{window.languagelist.change}</p>
                  </div>
                  <img  className="becomehost__step-1" src="../images/landloard_page-30.png" alt=""/>
              </div>

              <button className="btn btn-default btn-lg bg-pink color-white" onClick={(e)=>this.submit(e)}>{window.languagelist.Publish_listing}</button>
            </div>
            
            <div className="col-md-6 col-lg-5 col-sm-6 paddingNone" >
                <img className="stepbg" src="../images/step3_18img.png" alt=""/>
                <div className="Preview hide">
                  <img src="./images/becomehost-step5-preview.jpg" />
                  <p>{window.languagelist.Place_name}</p>
                  <h6>{window.languagelist.Preview}</h6>
                </div>
            </div>
          </div>
        }
        
        {this.state.SUCCESS &&
          <div className="col-md-12 col-lg-12 col-sm-12 success">
              <img src="./images/SUCCESS.png" />
              <h1>{window.languagelist.Submission_of_PROCESSING}<span className="glyphicon glyphicon-refresh"></span></h1>
              <button className="subbtn Left hide"  onClick={(e)=>this.SUCCESS()}>{window.languagelist.back}</button>
          </div>
        }  
    </div>
		)
	}
}

export default House_step21