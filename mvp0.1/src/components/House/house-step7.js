import React, { Component } from 'react';
import languageService from '../../services/language-service';

class House_step7 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 7,
		}

		this.state = {
			roomdescription_title: "",
			roomdescription_description: "",
			roomdescription_Otherthings: "",
			roomdescription_around: "",
			scene: 0,
		};

		languageService.language();
	}

	componentWillMount() {

		var listStorage = JSON.parse(sessionStorage.getItem('step7'));
		if(listStorage) {
			this.setState({
				roomdescription_title: listStorage.roomdescription_title,
				roomdescription_description: listStorage.roomdescription_description,
				roomstuff_withKids: listStorage.roomstuff_withKids,
				roomstuff_BigGroups: listStorage.roomstuff_BigGroups,
				roomstuff_pets: listStorage.roomstuff_pets,
				roomdescription_Otherthings: listStorage.roomdescription_Otherthings,
				roomdescription_around: listStorage.roomdescription_around,
				scene: listStorage.scene,
			})
		}

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step', obj);
		sessionStorage.setItem('step7', JSON.stringify(this.state));
	}

	Trim(str) {
		return str.replace(/(^\s*)|(\s*$)/g, "");
	}

	render() {
		return(
			<div className="row Step7">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <p>{window.languagelist.Step2}: {window.languagelist.Set_the_scene}</p>
              </div>

              <h1>{window.languagelist.Name_your_place}</h1>
              <div className="box1">
                <input placeholder={window.languagelist.Listing_title} onChange={(e) => this.setState({roomdescription_title: this.Trim(e.target.value)})} value={this.state.roomdescription_title}  type="text" />
              </div>

              <h1>{window.languagelist.Edit_your_description}</h1>
              <h4>{window.languagelist.Summary}</h4>
              <textarea onChange={(e) => this.setState({roomdescription_description: this.Trim(e.target.value)})} placeholder={window.languagelist.Describe_the_decor_light_whats_nearby_etc}>{this.state.roomdescription_description}</textarea>

              <h4 className="cursor"  onClick={(e) => {if(this.state.scene ==0 )this.setState({scene:1});else this.setState({scene:0});}}>{window.languagelist.Add_more_optional}</h4>
              <p className="cursor color-pink"  onClick={(e) => {if(this.state.scene ==0 )this.setState({scene:1});else this.setState({scene:0});}}>{window.languagelist.only_of_hosts_add_more_info_here}</p>

              <div className={this.state.scene != 0 ? 'show' : 'hide'}>

                <h4>{window.languagelist.Other_things_to_note}  <span className="color-pink" >{window.languagelist.Optional}</span></h4>
                <textarea onChange={(e) => this.setState({roomdescription_Otherthings: this.Trim(e.target.value)})}>{this.state.roomdescription_Otherthings}</textarea>

                <h2>{window.languagelist.The_neighbourhood}</h2>

                <h4>{window.languagelist.How_to_get_around}  <span className="color-pink">{window.languagelist.Optional}</span></h4>
                <textarea onChange={(e) => this.setState({roomdescription_around:  this.Trim(e.target.value)})}>{this.state.roomdescription_around}</textarea>

              </div>
              <div className="STEPBTN">
                <button className="Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className={ this.state.roomdescription_title == "" || this.state.roomdescription_description == "" ? "buttonActive Right" : "Right"} disabled={ this.state.roomdescription_title == "" || this.state.roomdescription_description == "" ? "disabled" : ""}  onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
             </div>

             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                 <div>
                    <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                    <p>{window.languagelist.Your_summary_description}</p>
                    <p>{window.languagelist.You_can_also_use_your_description}</p>
                </div>
             </div>
    

             
             </div>
		)
	}
}

export default House_step7