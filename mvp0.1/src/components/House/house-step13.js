import React, {Component} from 'react';
import languageService from '../../services/language-service';

class House_step13 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 13,
		}

		this.state = {
			Howoften_Froms: "",
			Howoften_Tos: "",
			advance_books: "",
			question_rented: "",
			Howoften_guests: "",
			notice_arrives: "",
			advance_book: 0,
			Howoften_From: "",
			Howoften_To: ""
		}

		languageService.language();
	}

	componentWillMount() {

		this.setState({
			Howoften_Froms: window.languagelist.Howoften_Times,
			Howoften_Tos: window.languagelist.Howoften_Times,
			advance_books: window.languagelist.advance_books,
			question_rented: window.languagelist.Please_choose,
			Howoften_guests: window.languagelist.Please_choose,
			notice_arrives: window.languagelist.Please_choose,
			Howoften_From: window.languagelist.select_a_time,
			Howoften_To: window.languagelist.select_a_time
		});

		var listStorage = JSON.parse(sessionStorage.getItem('step13'));
		if(listStorage) {
			this.setState({
				roomstuff_Closet_drwers: listStorage.roomstuff_Closet_drwers,
				question_rented: listStorage.question_rented,
				Howoften_guests: listStorage.Howoften_guests,
				notice_arrives: listStorage.notice_arrives,
				advance_book: listStorage.advance_book,
				Howoften_From: listStorage.Howoften_From,
				Howoften_To: listStorage.Howoften_To
			})
		}

		console.log(this.state.Howoften_Tos)

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step13', JSON.stringify(this.state));
		sessionStorage.setItem('step', obj);
	}

	Getcontent(event) {
		return event.target.innerHTML;
	}

	render() {
		return(
			<div className="row Step13">
            <div className="col-md-8 col-lg-7 col-sm-8 ">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span className="bjpink"></span>
                <span></span>
                <span></span>
                <p>{window.languagelist.Step3}: {window.languagelist.Get_ready_for_guests}</p>
              </div>

              <h1>{window.languagelist.Lets_get_started_with_a_couple_questions}</h1>


              <div className="box col-md-12">

                <h3>{window.languagelist.Have_you_rented_out_your_place_before}</h3>
                <div className="form-group">    
                  <div className="btn-group col-md-12">
                    <button type="button" data-toggle="dropdown">{this.state.question_rented}<span>▼</span></button>
                    <ul className="dropdown-menu" role="menu">
                      <li><a onClick={(e)=>this.setState({question_rented:this.Getcontent(e)})}>{window.languagelist.Im_a_novice_in_this_respect}</a></li>
                      <li><a onClick={(e)=>this.setState({question_rented:this.Getcontent(e)})}>{window.languagelist.I_have_a_renting_experience}</a></li>
                    </ul>
                  </div>
                </div>
                <h3>{window.languagelist.How_often_do_you_want_to_have_guests}</h3>
                <div className="form-group">    
                  <div className="btn-group col-md-12">
                    <button type="button" data-toggle="dropdown">{this.state.Howoften_guests}<span>▼</span></button>
                    <ul className="dropdown-menu" role="menu">
                      <li><a onClick={(e)=>this.setState({Howoften_guests:this.Getcontent(e)})}>{window.languagelist.Not_sure_yet}</a></li>
                      <li><a onClick={(e)=>this.setState({Howoften_guests:this.Getcontent(e)})}>{window.languagelist.Part_of_the_time}</a></li>
                      <li><a onClick={(e)=>this.setState({Howoften_guests:this.Getcontent(e)})}>{window.languagelist.As_much_as_possible}</a></li>
                    </ul>
                  </div>
                </div>
              </div>

              <h1>{window.languagelist.How_much_notice_do_you_need_before_a_guest_arrives}</h1>

              <div className="box col-md-12">

                <div className="form-group">    
                  <div className="btn-group col-md-12">
                    <button type="button" data-toggle="dropdown">{this.state.notice_arrives}<span>▼</span></button>
                    <ul className="dropdown-menu" role="menu">
                      <li><a onClick={(e)=>this.setState({notice_arrives:this.Getcontent(e)})}>{window.languagelist.Same_day}</a></li>
                      <li><a onClick={(e)=>this.setState({notice_arrives:this.Getcontent(e)})}>{window.languagelist.onedays}</a></li>
                      <li><a onClick={(e)=>this.setState({notice_arrives:this.Getcontent(e)})}>{window.languagelist.twodays}</a></li>
                      <li><a onClick={(e)=>this.setState({notice_arrives:this.Getcontent(e)})}>{window.languagelist.threedays}</a></li>
                      <li><a onClick={(e)=>this.setState({notice_arrives:this.Getcontent(e)})}>{window.languagelist.sevendays}</a></li>
                    </ul>
                  </div>
                </div>
                <h5><b className="textpink">{window.languagelist.Tip}:</b> {window.languagelist.At_least2_days_notice_can_help}</h5>
                <h3 className="textpink" onClick={(e)=>this.setState({guests_check:true})}>{window.languagelist.When_can_guests_checkin}</h3>
                <div className={this.state.guests_check == true?"form-group form-group1 show":"form-group form-group1 hide"}>    
                  <div className="btn-group col-md-6">
                    <h5>{window.languagelist.From}:</h5>
                    <button type="button" data-toggle="dropdown">{this.state.Howoften_From}<span>▼</span></button>
                    <ul className="dropdown-menu" role="menu">
                      {this.state.Howoften_Froms.map((item,index) => (
                          <li><a onClick={(e)=>this.setState({Howoften_From:item})} >{item}</a></li>
                        ))
                      }
                    </ul>
                  </div>

                  <div className="btn-group col-md-6">
                    <h5>{window.languagelist.To}:</h5>
                    <button type="button" data-toggle="dropdown" disabled={this.state.Howoften_From == "flexible" ? "disabled" : "" } > {this.state.Howoften_From == "flexible" ? "flexible": this.state.Howoften_To }<span>▼</span></button>
                    <ul className="dropdown-menu" role="menu">
                      {this.state.Howoften_Tos.map((item,index) => (
                          <li><a onClick={(e)=>this.setState({Howoften_To:item})} >{item}</a></li>
                        ))
                      }
                    </ul>
                  </div>
                </div>
              </div>

              <h1>{window.languagelist.How_far_in_advance_can_guests_book}</h1>


              <div className="box col-md-12">

                <div className="form-group">    
                  <div className="btn-group col-md-12">
                    <button type="button" data-toggle="dropdown">{window.languagelist.advance_books[this.state.advance_book]}<span>▼</span></button>
                    <ul className="dropdown-menu" role="menu">
                      {this.state.advance_books.map((item,index) => (
                          <li><a onClick={(e)=>this.setState({advance_book:index})} >{item}</a></li>
                        ))
                      }
                    </ul>
                  </div>
                </div>
                <h5><b className="textpink">{window.languagelist.Tip}:</b>{window.languagelist.Avoid_cancelling_or_declining_guests}</h5>
              </div>

             
              <div className="STEPBTN">
                <button className="btn btn-default btn-lg bg-pink color-white Left" onClick={(e)=>this.Step(this.STEP.Step-1)}>{window.languagelist.Back}</button>
                <button className="btn btn-default btn-lg bg-pink color-white Right" onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
               
             </div>


             <div className="col-md-4 col-lg-4 col-sm-4 paddingNone rightbox">
                 <div>
                    <img className="becomehost__info" src="./images/step3_4img4.png" alt=""/>
                    <p>{window.languagelist.Based_on_your_responses}</p>
                </div>
             </div>
             
             </div>
		)
	}
}

export default House_step13