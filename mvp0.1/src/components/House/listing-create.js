import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import houselistingService from '../../services/houseinfolist-service';
import Overlay from '../overlay';
import Modal from 'react-modal';
import hostService from '../../services/host-service';
import { DateRangePicker } from 'react-dates';
import AvatarEditor from 'react-avatar-editor';
import { reactLocalStorage } from 'reactjs-localstorage';
import guestService from '../../services/guest-service';
import languageService from '../../services/language-service';
import GoogleMapReact from 'google-map-react';
import { Map, Marker } from 'react-amap';
import web3service from '../../services/web3-service';
import aesService from '../../services/aes-service';
import Housestep1 from './house-step1';
import Housestep2 from './house-step2';
import Housestep3 from './house-step3';
import Housestep4 from './house-step4';
import Housestep5 from './house-step5';
import Housestep6 from './house-step6';
import Housestep7 from './house-step7';
import Housestep8 from './house-step8';
import Housestep9 from './house-step9';
import Housestep10 from './house-step10';
import Housestep11 from './house-step11';
import Housestep12 from './house-step12';
import Housestep13 from './house-step13';
import Housestep14 from './house-step14';
import Housestep15 from './house-step15';
import Housestep16 from './house-step16';
import Housestep17 from './house-step17';
import Housestep18 from './house-step18';
import Housestep19 from './house-step19';
import Housestep20 from './house-step20';
import Housestep21 from './house-step21';
import Housestep22 from './house-step22';

class ListingCreate extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step1: 1,
			Step2: 2,
			Step3: 3,
			Step4: 4,
			Step5: 5,
			Step6: 6,
			Step7: 7,
			Step8: 8,
			Step9: 9,
			Step10: 10,
			Step11: 11,
			Step12: 12,
			Step13: 13,
			Step14: 14,
			Step15: 15,
			Step16: 16,
			Step17: 17,
			Step18: 18,
			Step19: 19,
			Step20: 20,
			Step21: 21,
			Step22: 22,
			Step23: 23,
			Step24: 24,
			Step25: 25,
			Step26: 26,
			Step27: 27,
			Step28: 28,
			Step29: 29,
			Step30: 30,
			Step31: 31,
			Step32: 32,
			Step33: 33,
			Step34: 34,
			Step35: 35,
			Step36: 36,
			Step37: 37,
			PROCESSING: 404,
			SUCCESS: 200,

		}

		this.state = {
			step: 1,
			selectedPictures: []
		}

		languageService.language();
		web3service.loadWallet();

	}

	componentWillMount() {
		var step = sessionStorage.getItem('step')
		if(step) {
			if(step > 6) {
				if(this.state.selectedPictures.length > 1) {
					this.setState({
						step: step
					})
				} else {
					this.setState({
						step: 6
					})
				}
			} else {
				this.setState({
					step: step
				})
			}
		} else {
			this.setState({
				step: 1
			})
		}

	}

	house_Step = (value) => {
		this.setState({
			step: value
		})
	}

	selectedPictures = (value) => {
		this.setState({
			selectedPictures: value
		});
	}

	render() {
		const language = this.state.languagelist;
		const AnyReactComponent = ({
			text
		}) => <div className="Mapicon"><img src="/images/Map.png" /></div>;
		return(
			<div className="becomehost container">
        {this.state.EnterPassword &&
          <Overlay imageUrl="/images/circular-check-button.svg">
            <div className="EnterPassword">
              <h2>{language.Enter_your_password_Release_house}</h2>
              <input type="password" placeholder={language.User_Password} onChange={(e)=>this.setState({PasswordBook:e.target.value})} onKeyPress={this.Submitkeypress.bind(this)} />
              <p className={this.state.wrong_password ? "show wrong_password" : "hide wrong_password"}>{language.wrong_password}</p>
              <button className={this.state.PasswordBook == "" ? "PasswordNull Left" : "Left"} disabled={this.state.PasswordBook == "" ? "true" : ""} onClick={(e)=>this.submithome()} >{language.OK}</button>
              <button className="Right" onClick={(e)=>this.setState({step:this.STEP.Step3_18,EnterPassword:false,wrong_password:false})}>{language.Cancel}</button>
            </div>
          </Overlay>
        }

        { this.state.step == this.STEP.Step1 &&
           <Housestep1 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step2 &&
           <Housestep2 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step3 &&
           <Housestep3 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step4 &&
           <Housestep4 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step5 &&
           <Housestep5 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step6 &&
           <Housestep6 house_Step={this.house_Step} selectedPictures={this.selectedPictures} Pictures={this.state.selectedPictures} />
        }

        { this.state.step == this.STEP.Step7 &&
           <Housestep7 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step8 &&
           <Housestep8 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step9 &&
           <Housestep9 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step10 &&
           <Housestep10 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step11 &&
           <Housestep11 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step12 &&
           <Housestep12 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step13 &&
           <Housestep13 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step14 &&
           <Housestep14 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step15 &&
           <Housestep15 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step16 &&
           <Housestep16 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step17 &&
           <Housestep17 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step18 &&
           <Housestep18 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step19 &&
           <Housestep19 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step20 &&
           <Housestep20 house_Step={this.house_Step} />
        }

        { this.state.step == this.STEP.Step21 &&
           <Housestep21 house_Step={this.house_Step} Pictures={this.state}  />
        }

        { this.state.step == this.STEP.Step22 &&
           <Housestep22 house_Step={this.house_Step} />
        }

      </div>
		)
	}
}

export default ListingCreate