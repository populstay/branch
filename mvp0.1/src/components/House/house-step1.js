import React, {
	Component
} from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';

class House_step1 extends Component {

	constructor(props) {
		super(props)

		this.STEP = {
			Step: 1,
		}

		this.state = {
			homeorhotels: [],
			types: [],
			guestshaves: [],
			roomdescription_homeorhotel: 200,
			roomdescription_type: 200,
			roomdescription_guests_have: 200,
			roomdescription_forguestorhost: 2,
		};

		languageService.language();
	}

	componentWillMount() {

		this.setState({
			homeorhotels: window.languagelist.homeorhotels,
			types: window.languagelist.types,
			guestshaves: window.languagelist.Categorys,
		});

		var listStorage = JSON.parse(sessionStorage.getItem('step1'));
		if(listStorage) {
			this.setState({
				roomdescription_homeorhotel: listStorage.roomdescription_homeorhotel,
				roomdescription_type: listStorage.roomdescription_type,
				roomdescription_guests_have: listStorage.roomdescription_guests_have,
				roomdescription_forguestorhost: listStorage.roomdescription_forguestorhost
			})
		}

	}

	Step = (obj) => {
		this.props.house_Step(obj);
		sessionStorage.setItem('step1', JSON.stringify(this.state));
		sessionStorage.setItem('step', obj);
	}

	render() {

		return(
			<div className="row Step1">
            <div className="col-md-7 col-lg-7 col-sm-12">
              <div className="STEPhead">
                <span className="bjpink"></span>
                <span></span>
                <span></span>
                <span></span>
                <p>{window.languagelist.Step1}: {window.languagelist.Start_with_the_basics}</p>
              </div>

              <h1>{window.languagelist.What_kind_of_room_do_you_listing}</h1>

            <div className="box"> 
              <h2>{window.languagelist.Is_this_listing_a_home_hotel_or_something_else}</h2>

              <div className="form-group">    
                <div className="btn-group col-sm-12 col-md-8 col-lg-8">
                  <button type="button" data-toggle="dropdown">{this.state.roomdescription_homeorhotel == 200 ? window.languagelist.Please_choose : this.state.homeorhotels[this.state.roomdescription_homeorhotel]}<span>▼</span></button>
                  <ul className="dropdown-menu" role="menu"> 
                    {this.state.homeorhotels.map((item,index) => (
                        <li><a onClick={(e)=>this.setState({roomdescription_homeorhotel:index})} >{item}</a></li>
                      ))
                    }
                  </ul>
                </div>
              </div>
                
              <div className={this.state.roomdescription_homeorhotel == 200 ? 'hide':'show'}>  
              <h2>{window.languagelist.What_type_is_it}</h2>
              <div className="form-group">    
                <div className="btn-group col-sm-12 col-md-8 col-lg-8">
                  <button type="button" data-toggle="dropdown">{this.state.roomdescription_type == 200 ? window.languagelist.Please_choose : this.state.types[this.state.roomdescription_type]}<span>▼</span></button>
                  <ul className="dropdown-menu" role="menu">
                    {this.state.types.map((item,index) => (
                        <li><a onClick={(e)=>this.setState({roomdescription_type:index})} >{item}</a></li>
                      ))
                    }
                  </ul>
                </div>
              </div>
              </div>

              <div className={this.state.roomdescription_type == 200 ? 'hide':'show'}>
                  <h2>{window.languagelist.What_guests_will_have}</h2>
                  <div className="form-group">    
                    
                    <div className="btn-group col-sm-12 col-md-8 col-lg-8">
                      <button type="button" data-toggle="dropdown">{this.state.roomdescription_guests_have == 200 ? window.languagelist.Please_choose : this.state.guestshaves[this.state.roomdescription_guests_have]}<span>▼</span></button>
                      <ul className="dropdown-menu" role="menu">
                        {this.state.guestshaves.map((item,index) => (
                            <li><a onClick={(e)=>this.setState({roomdescription_guests_have:index})} >{item}</a></li>
                          ))
                        }
                      </ul>
                    </div>
                  </div>
              </div>

              <div className={this.state.roomdescription_guests_have == 200 ? 'hide':'show'}>
                  <h2>{window.languagelist.Is_this_setup_dedicated_a_guest_space}</h2>

                 <div className="radio" onClick={(e) => this.setState({roomdescription_forguestorhost: 0})}>
                    <label className="text-muted"><p><span className={this.state.roomdescription_forguestorhost == 0 ?"show":"hide"}></span></p>{window.languagelist.Yes_its_primarily_set_up_for_guests}</label>
                  </div>
                  <div className="radio" onClick={(e) => this.setState({roomdescription_forguestorhost: 1})}>
                    <label className="text-muted"><p><span className={this.state.roomdescription_forguestorhost == 1 ?"show":"hide"}></span></p>{window.languagelist.No_I_keep_my_personal_belongings_here}</label>
                  </div>
              </div>
                    

                </div>

             </div>
             <div className="col-md-5 col-lg-4 col-sm-12 paddingNone rightbox">
                <div className={ this.state.roomdescription_forguestorhost == 0 || this.state.roomdescription_forguestorhost == 1 ? "show" : "hide"}>
                  <img className="becomehost__info" src="./images/rightBoximg.png" alt=""/>
                  <h6>{window.languagelist.Entire_place}</h6>
                  <p>{window.languagelist.Entire_place_presentation}</p>
                  <h6>{window.languagelist.Private_room}</h6>
                  <p>{window.languagelist.Private_room_presentation}</p>
                  <h6>{window.languagelist.Shared_room}</h6>
                  <p>{window.languagelist.Shared_room_presentation}</p>
                </div>
             </div>

              <div className="STEPBTN col-md-7 col-lg-7 col-sm-12">
                <button className={ this.state.roomdescription_forguestorhost == 0 || this.state.roomdescription_forguestorhost == 1 ? "Left" : "buttonActive Left"} disabled={ this.state.roomdescription_forguestorhost == 0 || this.state.roomdescription_forguestorhost == 1 ? "" : "disabled"} onClick={(e)=>this.Step(this.STEP.Step+1)}>{window.languagelist.Next}</button>
              </div>
             </div>

		)
	}
}

export default House_step1