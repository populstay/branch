import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import languageService from '../../services/language-service';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import aesService from '../../services/aes-service';

class WalletManage extends React.Component {
	constructor() {
		super();

		this.state = {
			modalIsOpen: false,
			languagelist: {},
			password: "",
			privateKey: "",
		};

		this.openModal = this.openModal.bind(this);
		this.afterOpenModal = this.afterOpenModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.import = this.import.bind(this);

		languageService.language();
	}

	componentDidMount() {
		this.setState({
			languagelist: window.languagelist
		});
	}

	import() {}
	openModal() {
		this.setState({
			modalIsOpen: true
		});
	}

	afterOpenModal() {
		this.subtitle.style.color = '#f00';
	}

	closeModal() {
		this.setState({
			modalIsOpen: false,
			copied: false,
			copied1: false
		});
	}

	substring0x = (str) => {
		str = str + "";
		return str.substring(2, str.length);
	}

	Getkey() {
		if(aesService.decrypt(window.aesprivateKey, this.state.password)) {
			this.setState({
				privateKey: aesService.decrypt(window.aesprivateKey, this.state.password)
			})
		} else {
			this.setState({
				privateKey: ""
			})
		}
	}

	keykeypress(e) {
		if(e.which !== 13) return
		this.Getkey();
	}

	render() {
		const language = this.state.languagelist;
		return(

			<div>
        <button className="btn btn-danger" onClick={this.openModal}>{language.Export}</button>
        <Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal}  
        contentLabel="Wallet Message">
        <div className="PirvateKey">
            <img className="redlogo" src="../images/pps.png" />
            <h2 ref={subtitle => this.subtitle = subtitle}>{language.Please_Remember_Your_Pirvate_Key}</h2>
            <div>
              <h3>{language.Address}: &nbsp;&nbsp;</h3>
              <CopyToClipboard text={window.address}
                onCopy={() => this.setState({copied: true})}>
                <button className="copy">{this.state.copied ? language.Successful_copy : language.Copy_address}</button>
              </CopyToClipboard>
              <p className="text1">{window.address}</p>
              <h3>{language.Private_Key}: &nbsp;&nbsp;</h3>
              {this.state.privateKey != "" &&
                <CopyToClipboard text={this.state.privateKey}
                  onCopy={() => this.setState({copied1: true})}>
                  <button className="copy">{this.state.copied1 ? language.Successful_copy : language.Copy_Private_Key}</button>
                </CopyToClipboard>
              }
              {this.state.privateKey == "" &&
                <div className="Getkey">
                  <input type="password" placeholder={language.Enter_your_login_password_to_get_the_private_key} onChange={(e)=>this.setState({password:e.target.value})} onKeyPress={this.keykeypress.bind(this)}  />
                  <button onClick={(e)=>this.Getkey()}>{language.Get}</button>
                </div>
              }
              <p className={this.state.privateKey == "" ? "hide text1" : "show text1"}>{this.substring0x(this.state.privateKey)}</p>
            </div>  
            <button className="btn btn-primary Right" onClick={this.closeModal}>{language.Cancel}</button>
          </div>
        </Modal>
      </div>
		);
	}
}
export default WalletManage