import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Overlay from '../overlay';
import PropTypes from 'prop-types';
import GuestRegister from '../RegisterLogin/guest-register';
import { DateRangePicker } from 'react-dates';
import { withRouter } from 'react-router';
import WalletClear from '../Wallet/walletClear';
import InputRange from 'react-input-range';
import InputRangecss from 'react-input-range/lib/css/index.css';
import houselistingService from '../../services/houseinfolist-service';
import Pagination from 'react-js-pagination';
import ListingCard from '../Other/listing-card';
import languageService from '../../services/language-service';

class Listingall extends Component {

	constructor(props) {
		super(props);
		this.state = {
			Adult: 1,
			children: 0,
			Baby: 0,
			Citys: ["TOKYO", "NEW YORK", "SHANGHAI", "LONDON", "PARIS", "SINGAPORE"],
			language: ['English', 'Français', 'Deutsch', '日本語', 'Italiano', 'Русский', 'Español', '中文', 'العربية', 'Hindi', 'Português', 'Türkçe', 'Bahasa Indonesia', 'Nederlands', '한국어', 'Bengali', 'ภาษาไทย', 'Punjabi', 'Ελληνικά', 'Sign Language', 'עברית', 'Polski', 'Bahasa Malaysia', 'Tagalog', 'Dansk', 'Svenska', 'Norsk', 'Suomi', 'Čeština', 'Magyar', 'українська'],
			Facilities: ['Kitchen', 'Shampoo', 'Heating', 'Air conditioner', 'Washing machine', 'Dryer', 'Wireless network', 'breakfast', 'Indoor fireplace', 'Doorbell / building interphone', 'Guard', 'Coat hanger', 'Iron', 'Hair drier', 'Desk / work area', 'Television', 'Baby bed', 'High foot chair for children', 'Check in', 'smoke detector', 'Carbon Monoxide Alarm'],
			Facilities1: ['Free parking space', 'Gym', 'Massage bathtub', 'Swimming Pool'],
			Source_type: ['A complete set of single house', 'Apartment type residence', 'Breakfast and Breakfast', 'The Inn Boutique', 'Loft', 'Village hut', 'Villa', 'Guest Room', 'Guest suite', 'Log cabin', 'Bungalow', 'Holiday wooden house', 'Resort', 'Hostel', 'Villas', 'Hotel'],
			Characteristic: ['Agritainment', 'Primary residence acupoint', 'Cuban family hotel', 'Castle', 'Tent', 'Miniature house', 'Tree House', 'Train', 'Natural Hostel', 'Ship', 'A ship’s house', 'Thatched cottage', 'Camping area', 'Camping car / RV'],
			Rules: ['Suitable for hosting activities', 'Allowed to carry a pet', 'Allow smoking'],
			Code_house: ['Suitable for hosting activities', 'Allowed to carry a pet', 'Allow smoking'],
			Citys_type: 'City',
			Home_Type: '',
			PriceAdd: 0,
			PriceDele: 0,
			Pricemin: 0,
			Pricemax: 10000,
			Price: 'Price',
			listingRows: [],
			listingsPerPage: 20,
			districtCodes: [],
			curDistrictCodeIndex: 0,
			experienceList: 1,
			listingtype: 0,
			languagelist: {},
		};
		window.searchCondition = this.state;
		languageService.language();
	}

	componentWillMount() {

		this.setState({
			languagelist: window.languagelist
		});
		this.setState({
			Citys_type: window.languagelist.City,
			Home_Type: window.languagelist.Home_Type,
			Price: window.languagelist.Price
		});

		houselistingService.getDistrictCodes().then((codes) => {
			this.setListingRows(codes);
		});

		this.timerID = setTimeout(
			() => this.setState({
				Progresshide: 1
			}),
			1000
		);
	}

	setListingRows = (codes) => {
		this.setState({
			districtCodes: codes.data
		});
		var uuids = houselistingService.getAllLists(codes.data[0].id).then((data) => {
			if(data.length != 0) {
				this.setState({
					listingRows: data,
					alllength: false
				});
			} else {
				this.setState({
					listingRows: data,
					alllength: true
				});
			}
			window.listingRows = data;
		});
	}

	//鼠标移入地图高亮
	MouseOver = (id) => {
		this.setState({
			maphomeid: id
		})
	}

	render() {
		const showListingsRows = this.state.listingRows;
		const language = this.state.languagelist;

		return(

			<div className="headerbox">
          <div className="container experience ALL">
            <div className={this.state.Progresshide == 1 ? "Progress hide" : "Progress"}><p style={{width:this.state.Progress+"%"}}></p></div>
                <h2>{language.All_home}</h2>
                <div className={this.state.experienceList == 1 ? "show All_experiences row" : "hide All_experiences row"}>
                    {showListingsRows.map(row => (
                      <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3 listing-card">
                        <ListingCard row={row} MouseOver={this.MouseOver} />
                          
                      </div>
                    ))}
                </div>

                {
                  this.state.listingRows.length == 0 && !this.state.alllength &&
                  <div className="No_result">
                    <img src="/images/loader.gif" />
                  </div>
                }

                {
                  this.state.listingRows.length == 0 && this.state.alllength &&
                  <div className="No_result">
                    <h1 className="text-center color-pink">{language.No_result}</h1>
                  </div>
                }

                <div className="listspan">
                  <span className={this.state.experienceList == 1 ? "active hide" : ""}  onClick={(e)=>this.setState({experienceList:1})}></span>
                  <span className={this.state.experienceList == 1 ? "active hide" : ""}  onClick={(e)=>this.setState({experienceList:2})}></span>
                  <span className={this.state.experienceList == 1 ? "active hide" : ""}  onClick={(e)=>this.setState({experienceList:3})}></span>
                </div>
            </div>
      </div>

		)
	}
}

export default withRouter(Listingall)