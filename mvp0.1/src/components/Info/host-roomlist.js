import React, { Component } from 'react';
import houselistingService from '../../services/houseinfolist-service'
import { Link } from 'react-router-dom'
import languageService from '../../services/language-service';
import Timestamp from 'react-timestamp';
var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

class HostRoomListRow extends Component {

	constructor(props) {
		super(props)
		this.state = {
			category: "Loading...",
			beds: "Loading...",
			location: "Loading",
			price: "Loading",
			status: 1,
			languagelist: {},
		}

		languageService.language();
	}

	getHostRoomInfo() {
		var roominfo = this.props.row.houseinfo;
		this.setState({
			category: window.languagelist.Categorys[this.props.row.description.roomdescription_guests_have],
			location: roominfo.location,
			transaction: this.props.row.transaction,
			previewurl: this.props.row.profile.previewImage,
			bathroom: this.props.row.description.roombasics_guestbathroom
		});
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist
		});

		if(this.props.row) {
			this.getHostRoomInfo();
		}

		if(this.props.row.id) {
			houselistingService
				.getHouseInfoById(this.props.row.id)
				.then((res) => {
					if(res.description) {
						this.setState({
							title: res.description.roomdescription_title,
						});
					} else {
						this.setState({
							title: "-",
						});
					}
				});

		}

	}

	render() {
		const language = this.state.languagelist;

		return(
			<div className="RoomList">
          <a target="_blank" href={`/listing/${this.props.row.id}`}  style={this.state.previewurl == '' ? {backgroundImage:"url(/images/home.png)"}:{backgroundImage:"url("+this.state.previewurl+")"}} ></a>
            <div className="Right">
              <div className="homeTitle">
                <p className="Left">{this.state.title}</p>
                <h2 className="Right col-sm-3 col-md-3 col-lg-3">{this.state.location}</h2>
              </div>
              <div className="information">
                <div className="box1 col-sm-9 col-md-9 col-lg-9">
                  <ul>
                    <li><h4>{language.Category}</h4><p>{this.state.category}</p></li>
                    <li><h4>{language.Bathrooms}</h4><p>&nbsp;&nbsp;&nbsp;{this.state.bathroom}</p></li>
                    <li><h4>{language.Price}</h4>
                    {this.props.row.generateSmartContract == 0 &&
                      <p>{this.props.row.usdprice+"/USD"}</p>
                    }
                    {this.props.row.generateSmartContract == 1 &&
                      <p>{this.props.row.price+"/PPS"}</p>
                    }
                    </li>
                  </ul>
                </div>
                <div className="box2 col-lg-12">
                  <p>
                    <a target="_blank" href={`/listing/${this.props.row.id}`} ><button>{language.View_house}</button></a>
                    <a target="_blank" href={`https://kovan.etherscan.io/tx/${this.state.transaction}`} ><button> {language.Smart_Contract}</button></a>
                    <span>{this.props.row.state == 1 || this.props.row.state == -1 ? language.state6 : language.Writing_etheric_chain}</span>
                  </p>
                  <p className="details hide" onClick={(e) => {if(this.state.View_details)this.setState({View_details:false});else this.setState({View_details:true});}}>View details</p>
                </div>
              </div>
          </div>
          {
            this.state.View_details &&
            <div className="col-lg-12 Viewdetails">
              <div className="col-lg-4 Guests" >
                <h4>3 Guests</h4>
                <div className="GuestsList" >
                  <div>
                    <img src="../images/uesr.png" />
                  </div>
                  <p>user</p>
                </div>
                <div className="GuestsList" >
                  <div>
                    <img src="../images/uesr.png" />
                  </div>
                  <p>user</p>
                </div>
                <div className="GuestsList" >
                  <div>
                    <img src="../images/uesr.png" />
                  </div>
                  <p>user</p>
                </div>
              </div>
              <div className="col-lg-4 Time" >
                <h4>Time</h4>
                <div>
                  <p><b>{language.Check_In}</b></p>
                  <p>{language.language == "zh" ? moment(new Date()).locale("zh-cn").format('L') : moment(new Date()).format('LL')}</p>
                </div>
                <div>
                  <p><b>{language.Check_Out}</b></p>
                  <p>{language.language == "zh" ? moment(new Date()).locale("zh-cn").format('L') : moment(new Date()).format('LL')}</p>
                </div>
              </div>
              <div className="col-lg-4 Payment" >
                <h4>Payment</h4>
                <p>Processing</p>
              </div>
            </div>
          }
        </div>

		)
	}
}

export default HostRoomListRow