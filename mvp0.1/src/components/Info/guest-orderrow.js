import React, { Component } from 'react';
import orderService from '../../services/order-service';
import { Link } from 'react-router-dom';
import Timestamp from 'react-timestamp';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import Overlay from '../overlay';
import Modal from 'react-modal';
import houselistingService from '../../services/houseinfolist-service'
import web3Service from '../../services/web3-service';
import aesService from '../../services/aes-service';
import FileBase64 from 'react-file-base64';
var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

class GuestOrderRow extends Component {

	constructor(props) {
		super(props)
		this.state = {
			contractAddress: '',
			user: '',
			modalIsOpen: false,
			status: "Loading...",
			houseInformation: "Loading...",
			from: "Loading",
			to: "Loading",
			price: "Loading",
			ethPrice: "Loading",
			url: "",
			languagelist: {},
			Comment: '',
			selectedPictures: [],
			Accuracy: 0,
			Location: 0,
			Communication: 0,
			Check_in: 0,
			Cleanliness: 0,
			Value: 0,
			Bad_review: '',
			password: "",
		}

		this.checkIn = this.checkIn.bind(this);
		this.checkInUSD = this.checkInUSD.bind(this);
		this.Reviews = this.Reviews.bind(this);
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.deletePictures = this.deletePictures.bind(this);
		languageService.language();
		web3Service.loadWallet();
	}

	checkIn() {
		if(aesService.decrypt(window.aesprivateKey, this.state.password)) {
			var ethOrPPS;
			if(this.props.item.price != 0 || this.props.item.price != '0') {
				ethOrPPS = 'PPS';
			} else {
				ethOrPPS = 'ETH';
			}

			orderService.confirm(
				this.state.contractAddress,
				ethOrPPS,
				this.props.item.from,
				this.props.item.to,
				this.props.item.houseinfoid,
				this.state.password
			).then((tx) => {
				this.setState({
					state: '4',
					checkInOpen: false
				});
				return orderService.waitTransactionFinished(tx)
			}).then((blockNumber) => {
				this.setState({
					state: '1'
				});
			}).catch((error) => {
				console.error(error);
			});
		} else {
			this.setState({
				wrong_password: true
			})
		}
	}

	checkInUSD() {
		orderService.confirmByUSD(this.props.item.id).then((tx) => {
			this.setState({
				state: '4',
				USDcheckInOpen: false
			});
			this.openModal();
			return orderService.waitTransactionFinished(tx)
		}).then((blockNumber) => {
			this.setState({
				state: '1'
			})
		}).catch((error) => {
			console.error(error);
		});
	}
	componentDidMount() {
		console.log(this.props)

		if(this.props.item.orderContractAddress) {
			this.setState({
				contractAddress: this.props.item.orderContractAddress
			});
		}

		var D = new Date().getTime();
		var DateType = 0;
		if(D > this.props.item.from && D > this.props.item.to) {
			var DateType = 100;

		} else if(D < this.props.item.from && D < this.props.item.to) {
			var DateType = 200;

		} else {
			var DateType = 300;
		}

		this.setState({
			houseinfoid: this.props.item.houseinfoid,
			id: this.props.item.id,
			from: Number(this.props.item.from),
			to: Number(this.props.item.to),
			price: this.props.item.price,
			ethprice: this.props.item.ethprice,
			usdprice: this.props.item.usdprice,
			state: this.props.item.state,
			guest: this.props.item.guest,
			DateType: DateType,
		});

		if(this.props.item.houseinfoid) {
			houselistingService
				.getHouseInfoById(this.props.item.houseinfoid)
				.then((res) => {
					console.log(res)
					this.setState({
						location: res.place,
						previewurl: res.profile.previewImage,
						phone: res.description.roomdescription_phone,
						title: res.description.roomdescription_title,
						Country: res.description.roomstuff_Country,
						City: res.description.roomstuff_City,
						Street: res.description.roomstuff_Street,
						Apt: res.description.roomstuff_Apt,
						Joined_in_time: res.createdAt,

					});

					guestService.getGuesterInfo(res.hostAddress).then((data) => {
						this.setState({
							user: data.user
						});
						if(data.profile) {
							this.setState({
								userPictures: data.profile.profile
							});
						} else {
							this.setState({
								userPictures: "../images/uesr.png"
							});
						}
					});
				});

		}

		if(this.props.item.orderContractAddress) {
			this.setState({
				orderContractAddress: web3Service.etherscanAddress(this.props.item.orderContractAddress),
			})
		}

		this.setState({
			languagelist: window.languagelist
		});
		//orderService.confirmByUSD("5b30a218e13af37acb1e872a");
	}

	openModal() {
		this.setState({
			modalIsOpen: true
		});
	}

	closeModal() {
		this.setState({
			modalIsOpen: false
		});
	}

	Reviews() {
		if(this.state.Accuracy != 0 && this.state.Location != 0 && this.state.Communication != 0 && this.state.Check_in != 0 && this.state.Cleanliness != 0 && this.state.Value != 0) {
			// if(this.state.Comment != "") {
				guestService.addComment(this.props.item.id, this.state.Comment, this.state.Accuracy, this.state.Location, this.state.Communication, this.state.Check_in, this.state.Cleanliness, this.state.Value).then((data) => {
					this.setState({
						state: '5',
						modalIsOpen: false
					});
				});
				this.setState({
					Bad_review_type: false
				});
			// }
		}
	}

	fileChangedHandler(files){
        var Picturesfiles = this.state.selectedPictures;
        		console.log(files)
        for(var i=0; i<files.length; i++){

          var imgtype = files[i].type.split("/")[1];  

          if ((imgtype == 'jpg' || imgtype == 'jpeg' || imgtype == 'png' || imgtype == 'PNG' || imgtype == 'JPG') && parseInt(files[i].size) < 600) {  
              Picturesfiles.push({
                file: i,
                imagePreviewUrl: files[i].base64
              });
              this.setState({selectedPictures:Picturesfiles});
          }else{
            this.setState({imgprompt:true});
            setTimeout(
				() => this.setState({
					imgprompt: false
				}),
				2000
			);
          } 
        }
    }

	deletePictures(index, e) {
		this.setState({
			selectedPictures: this.state.selectedPictures.filter((elem, i) => index != i)
		});

	}

	onCheck() {
		var url = "/Confrim"
		// window.location.href=url;
		sessionStorage.setItem('Order', JSON.stringify(this.props.item));
		window.open(url)
	}

	onCancel() {
		if(aesService.decrypt(window.aesprivateKey, this.state.password)) {
			var ethOrPPS;
			if(this.props.item.price != 0 || this.props.item.price != '0') {
				ethOrPPS = 'PPS';
			} else {
				ethOrPPS = 'ETH';
			}

			if(this.state.Refund_time) {
				orderService.cancel(this.props.item.orderContractAddress, ethOrPPS, this.state.id, this.state.password).then((data) => {
					houselistingService.removeDisabledDate(this.state.houseinfoid, this.state.from, this.state.to)
					this.setState({
						state: '6',
						checkOutOpen: false
					});
				}).catch((error) => {
					console.error(error);
				});
			} else {
				orderService.cancelWithPay(this.props.item.orderContractAddress, ethOrPPS, this.state.id, this.state.password).then((data) => {
					houselistingService.removeDisabledDate(this.state.houseinfoid, this.state.from, this.state.to)
					this.setState({
						state: '6',
						checkOutOpen: false
					});
				}).catch((error) => {
					console.error(error);
				});
			}

		} else {
			this.setState({
				wrong_password: true
			})
		}
	}

	checkOutOpen() {
		var currentDate = new Date().getTime() + 604800000; //当前时间+7天
		var fromDate = this.state.from //入住时间
		if(fromDate >= currentDate) {
			//全部退回
			this.setState({
				Refund_time: true,
				checkOutOpen: true
			});
		} else {
			//退回50%
			this.setState({
				Refund_time: false,
				checkOutOpen: true
			});
		}
	}

	checkOutkeypress(e) {
		if(e.which !== 13) return
		this.onCancel()
	}

	checkInkeypress(e) {
		if(e.which !== 13) return
		this.checkIn()
	}

	render() {
		const language = this.state.languagelist;

		return(
			<div>
        <div className="GuestList">
          <div className="homeList">
	        <a target="_blank" href={`/listing/${this.state.houseinfoid}${this.state.state == '4' ? '?'+this.state.id : ''}`}   style={this.state.previewurl == '' ? {backgroundImage:"url(/images/home.png)"}:{backgroundImage:"url("+this.state.previewurl+")"}} >
	        </a>
			<div className="Right" >
	            <div className="homeTitle">
	              <p className="Left">{this.state.title ? this.state.title : "-"}</p>
	              <h2 className="Right">{this.state.location ? this.state.location : "-"}</h2>
	            </div>
	            <div className="information">
	              <div className="box1 col-xs-12 col-sm-9 col-md-9 col-lg-9">
	                <ul>
	                  <li><h4>{language.Smart_Contact}</h4><p>{this.state.phone ? this.state.phone : "/"}</p></li>
	                  <li><h4>{language.Guest}</h4><p>&nbsp;&nbsp;&nbsp;{this.state.guest}</p></li>
	                  <li><h4>{language.Price}</h4>
	                  {this.state.usdprice != '0' && this.state.usdprice && <p>{Number(this.state.usdprice).toFixed(5)+"/USD"}</p>}
	                  {this.state.ethprice != '0' && this.state.ethprice && <p>{this.state.ethprice+"/ETH"}</p> }
	                  {this.state.price != '0' && this.state.price && <p>{this.state.price+"/PPS"}</p> }
	                  </li>
	                </ul>
	                <p><b>{language.From}</b> &nbsp; {language.language == "zh" ? moment(this.state.from).locale("zh-cn").format('LL') : moment(this.state.from).format('LL')} &nbsp; - &nbsp; <b>{language.To}</b> &nbsp; {language.language == "zh" ? moment(this.state.to).locale("zh-cn").format('LL') : moment(this.state.to).format('LL')}</p>
	              </div>
	              <div className="col-xs-12 col-sm-3 col-sm-3 col-md-3 col-lg-3">
	                <div className="Guestphoto"  style={this.state.userPictures == '' ? {backgroundImage:"url(/images/uesrimg.png)"}:{backgroundImage:"url("+this.state.userPictures+")"}}>
	                </div>
	                <p className="GuestName">{this.state.user ? this.state.user : "-"}</p>
	              </div>
	              <div className="box2">
	                <p>
	                <button onClick={(e)=>this.onCheck()}>{language.check_order}</button>

	                <a target="_blank" href={`/listing/${this.state.houseinfoid}${this.state.state == '4' ? '?'+this.state.id : ''}`} ><button>{language.View_house}</button></a>

	                {this.state.orderContractAddress &&
	                  <a target="_blank" href={this.state.orderContractAddress} ><button>{language.Smart_Contract}</button></a>
	                }

	                { this.state.state === '1' &&  this.state.state === '3' &&<button>{language.state1}</button>}
	                { this.state.state === '2' &&<button className={this.state.DateType == 300 ? "" : "btnActive"}  disabled={this.state.DateType == 300 ? "" : "true"} onClick={(e)=>this.setState({checkInOpen:true})}>{this.state.DateType == 100 ? language.Check_In_to : ""}{this.state.DateType == 200 ? language.Check_In_from : ""}{this.state.DateType == 300 ? language.Check_In : ""}</button>}
	                { this.state.state === '-2' &&<button className={this.state.DateType == 300 ? "" : "btnActive"} disabled={this.state.DateType == 300 ? "" : "true"} onClick={(e)=>this.setState({USDcheckInOpen:true})}>{this.state.DateType == 100 ? language.Check_In_to : ""}{this.state.DateType == 200 ? language.Check_In_from : ""}{this.state.DateType == 300 ? language.Check_In : ""}</button>}
	                { this.state.state === '4' &&<button className="btn-sn btn-danger" onClick={this.openModal}>{language.Reviews}</button>}
	                { this.state.state === '5' &&<button className="btnActive" >{language.ok_Reviews}</button>}
	                { this.state.state === '6' &&<button className="btnActive" >{language.Order_cancelled}</button>}
	                { this.state.state === '7' &&<button className="btnActive" >{language.Order_cancelled}</button>}
	                { this.state.state != "6" && this.state.state != "7" && this.state.state != "4" && this.state.state != "5" && this.state.DateType != 100 &&
	                  <button onClick={(e)=>this.checkOutOpen()}>{language.Cancel_Order}</button>
	                }
	                </p>
	              </div>
	            </div>
	        </div>
          </div>
        </div>


        <Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal}  
        contentLabel="Wallet Message">
          <div className="Wallet_Reviews">
            <div className="box1">
                <div className="userimg">
                  <img src="/images/uesrimg.png"/>
                </div>
                <div className="usertext">
                  <h4>{this.state.user}</h4>
                  <p>{this.state.Country}- { this.state.City}- { this.state.Street}- { this.state.Apt} -
                  {language.language == "zh" ? moment(this.state.Joined_in_time).locale("zh-cn").format('LL') + language.Joined_in : language.Joined_in + moment(this.state.Joined_in_time).format('LL')}
                  </p>
                  <h6>{this.state.title }</h6>
                </div>
            </div>

            <ul className="box2">
                <li>
                  <p>{language.Accuracy}</p>
                  <div className="divxx">
                    <img src={this.state.Accuracy >= 1 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Accuracy:1})} />
                    <img src={this.state.Accuracy >= 2 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Accuracy:2})} />
                    <img src={this.state.Accuracy >= 3 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Accuracy:3})} />
                    <img src={this.state.Accuracy >= 4 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Accuracy:4})} />
                    <img src={this.state.Accuracy >= 5 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Accuracy:5})} />
                  </div>
                </li>
                <li>
                  <p>{language.Location}</p>
                  <div className="divxx">
                    <img src={this.state.Location >= 1 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Location:1})} />
                    <img src={this.state.Location >= 2 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Location:2})} />
                    <img src={this.state.Location >= 3 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Location:3})} />
                    <img src={this.state.Location >= 4 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Location:4})} />
                    <img src={this.state.Location >= 5 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Location:5})} />
                  </div>
                </li>
                <li>
                  <p>{language.Communication}</p>
                  <div className="divxx">
                    <img src={this.state.Communication >= 1 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Communication:1})} />
                    <img src={this.state.Communication >= 2 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Communication:2})} />
                    <img src={this.state.Communication >= 3 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Communication:3})} />
                    <img src={this.state.Communication >= 4 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Communication:4})} />
                    <img src={this.state.Communication >= 5 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Communication:5})} />
                  </div>
                </li>
                <li>
                  <p>{language.Check_in}</p>
                  <div className="divxx">
                    <img src={this.state.Check_in >= 1 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Check_in:1})} />
                    <img src={this.state.Check_in >= 2 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Check_in:2})} />
                    <img src={this.state.Check_in >= 3 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Check_in:3})} />
                    <img src={this.state.Check_in >= 4 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Check_in:4})} />
                    <img src={this.state.Check_in >= 5 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Check_in:5})} />
                  </div>
                </li>
                <li>
                  <p>{language.Cleanliness}</p>
                  <div className="divxx">
                    <img src={this.state.Cleanliness >= 1 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Cleanliness:1})} />
                    <img src={this.state.Cleanliness >= 2 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Cleanliness:2})} />
                    <img src={this.state.Cleanliness >= 3 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Cleanliness:3})} />
                    <img src={this.state.Cleanliness >= 4 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Cleanliness:4})} />
                    <img src={this.state.Cleanliness >= 5 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Cleanliness:5})} />
                  </div>
                </li>
                <li>
                  <p>{language.Value}</p>
                  <div className="divxx">
                    <img src={this.state.Value >= 1 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Value:1})} />
                    <img src={this.state.Value >= 2 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Value:2})} />
                    <img src={this.state.Value >= 3 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Value:3})} />
                    <img src={this.state.Value >= 4 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Value:4})} />
                    <img src={this.state.Value >= 5 ? "../images/reviews2.png" : "../images/reviews1.png"} alt="" onClick={(e)=>this.setState({Value:5})} />
                  </div>
                </li>
            </ul>

            <textarea placeholder={language.Describe_your_experiece_here} onChange={(e)=>this.setState({Comment:e.target.value})}></textarea>

            <div className="photos">
                {this.state.selectedPictures.map((file,index) => (
                  <div className="photosimg" >
                    <img className="img-thumbnail" src={file.imagePreviewUrl} />
                    <span  className="glyphicon glyphicon-trash" onClick={this.deletePictures.bind(this,index)} ></span>
                  </div>
                  ))
                 }
               <div className="photosipt">
                  <img src="../images/reviews3.png" />
                  <FileBase64 className="Fileipt"  multiple={ true } onDone={ this.fileChangedHandler.bind(this) } />
               </div>
            </div>
        	{this.state.imgprompt &&
            	<p className="textpink">{window.languagelist.The_picture_must_not_exceed}600KB <br/>
             	{window.languagelist.Image_format_is} jpg、jpeg、png</p>
			}
            <button className="Right Right1" onClick={this.closeModal}>{language.Cancel}</button>
            <button className="Right" onClick={this.Reviews}>{language.Reviews}</button>
          </div>
        </Modal>

        {this.state.checkInOpen &&
          <Overlay>
            <div className="checkIn">
              <h3>{language.checkIn_ok_no}</h3>
              <input type="password" placeholder={language.Please_enter_your_login_password} onChange={(e)=>this.setState({password:e.target.value,wrong_password:false})} value={this.state.password} onKeyPress={this.checkInkeypress.bind(this)} />
              <p className={this.state.wrong_password ? "show wrong_password" : "hide wrong_password"}>{language.wrong_password}</p>
              <button className="btn btn-primary Left" disabled={!this.state.password ? "true" : ""} onClick={this.checkIn}>{language.Check_in}</button>
              <button className="btn btn-primary Right" onClick={(e)=>this.setState({checkInOpen:false,password:"",wrong_password:false})}>{language.Cancel}</button>
            </div> 
          </Overlay>
        }

        {this.state.USDcheckInOpen &&
          <Overlay>
            <div className="checkIn">
              <h3>{language.checkIn_ok_no}</h3>
              <button className="btn btn-primary Left" onClick={this.checkInUSD}>{language.Check_in}</button>
              <button className="btn btn-primary Right" onClick={(e)=>this.setState({USDcheckInOpen:false})}>{language.Cancel}</button>
            </div> 
          </Overlay>
        }

        {this.state.checkOutOpen &&
          <Overlay>
            <div className="checkIn">
              <h3>{language.Are_you_sure_you_want_to_cancel}</h3>
              {!this.state.Refund_time &&
                <p>{language.refund_for_cancellations_made_less_than_one_week_after_checkin_date}</p>
              }
              <input type="password" placeholder={language.Please_enter_your_login_password} onChange={(e)=>this.setState({password:e.target.value,wrong_password:false})} value={this.state.password}  onKeyPress={this.checkOutkeypress.bind(this)}  />
              <p className={this.state.wrong_password ? "show wrong_password" : "hide wrong_password"}>{language.wrong_password}</p>
              <button className="btn btn-primary Left" disabled={!this.state.password ? "true" : ""} onClick={(e)=>this.onCancel()}>{language.OK}</button>
              <button className="btn btn-primary Right" onClick={(e)=>this.setState({checkOutOpen:false,password:"",wrong_password:false})}>{language.Cancel}</button>
            </div> 
          </Overlay>
        }
      </div>

		)
	}
}

export default GuestOrderRow