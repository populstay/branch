import React from 'react';
import ReactDOM from 'react-dom';
import guestService from '../../services/guest-service';
import ppsService from '../../services/pps-service';
import orderService from '../../services/order-service'
import GuestOrderRow from './guest-orderrow';
import HostRoomList from './host-roomlist';
import web3Service from '../../services/web3-service';
import WalletManage from '../Wallet/walletManage';
import WalletGas from '../Wallet/walletGas';
import WalletDeposit from '../Wallet/walletDeposit';
import WalletWithdraw from '../Wallet/walletWithdraw';
import { Link } from 'react-router-dom';
import Timestamp from 'react-timestamp';
import languageService from '../../services/language-service';
import EthereumQRPlugin from 'ethereum-qr-code';
import hostService from '../../services/host-service';
import Overlay from '../overlay';
import { reactLocalStorage } from 'reactjs-localstorage';
import FileBase64 from 'react-file-base64';
const qr = new EthereumQRPlugin();

class GuestInfo extends React.Component {
	constructor() {
		super();
		this.CONST = {
			weiToEther: 1000000000000000000
		}

		this.state = {
			id: "",
			user: "",
			account: "",
			phone: "",
			email: "",
			ppsBalance: 0,
			ethBalance: 0,
			ppsDeposited: 0,
			orderlist: [],
			usdOrderList: [],
			userPictures: "./images/uesrimg.png",
			languagelist: {},
			roomInfoList: [],
			qrurl: ""
		};

		languageService.language();
		web3Service.loadWallet();
	}

	componentWillMount() {
		if(!window.localStorage.getItem('webtoken') || reactLocalStorage.getObject('wallet') == null) {
			window.location.href = '/';
		}

		hostService.getHouseListing(window.address).then((data) => {
			this.setState({
				roomInfoList: data
			});
		});

	}

	componentDidMount() {

		this.setState({
			languagelist: window.languagelist
		});
		this.setState({
			account: window.address,
			id: window.address
		});

		guestService.getOrderState().then((data) => {
			this.setState({
				orderlist: data
			});
		});

		ppsService.getBalance(window.address).then((data) => {
			this.setState({
				ppsBalance: data
			});
		});

		web3Service.getETHBalance(window.address).then((data) => {
			this.setState({
				ethBalance: data
			});
		});

		ppsService.getUsdOrderList(window.address).then((data) => {
			//这个地方获取没有生成智能合约的订单数据！！
			//state等于2的是没有生成智能合约的
			//Guest Managment Panel 里面加一个没有生成智能合约的预定list。。。
			this.setState({
				usdOrderList: data.data
			});
		});

		this.onGetDepositBalance();
		this.loadQrCode();
	}

	loadQrCode = () => {
		qr.toDataUrl({
			to: window.address,
			gas: window.gas
		}).then((qrCodeDataUri) => {
			this.setState({
				qrurl: qrCodeDataUri.dataURL
			}); //'data:image/png;base64,iVBORw0KGgoA....'
		})
	}

	onGetDepositBalance = () => {
		ppsService.getDepositBalance(window.address)
			.then((data) => {
				this.setState({
					ppsDeposited: data.data.balance
				});
			});

		ppsService.getBalance(window.address).then((data) => {
			this.setState({
				ppsBalance: data
			});
		});

		guestService.getGuesterInfo(window.address).then((data) => {
			this.setState({
				user: data.user,
				phone: data.phone,
				email: data.email
			});
			if(data.profile) {
				this.setState({
					userPictures: data.profile.profile
				});
				this.setState({
					userPicturesing: false
				});
			}
		});
	}

	fileChangedHandler(files) {
		console.log(files)
		var Picturesfiles = this.state.userPictures;

		var imgtype = files.type.split("/")[1];

		if((imgtype == 'jpg' || imgtype == 'jpeg' || imgtype == 'png' || imgtype == 'PNG' || imgtype == 'JPG')) {
			this.setState({
				userPicturesing: true
			});
			guestService.profile(this.state.email, files.base64).then((data) => {
				if(data.data.state == "success") {
					this.onGetDepositBalance();
				} else {
					console.log("bug")
				}
			});
		} else {
			this.setState({
				imgprompt: true
			});
		}
	}

	render() {
		const language = this.state.languagelist;
		return(

			<div className="info">

      {this.state.imgprompt &&
        <Overlay>
          <div className="imgprompt">
            <p className="hostbook">{window.languagelist.Image_format_is} jpg、jpeg、png</p>
            <button className="balance" onClick={(e)=>this.setState({imgprompt:false})}>{window.languagelist.Close}</button>
          </div>  
        </Overlay>
      }

      {this.state.userqrurl &&
        <Overlay>
          <div className="userqrurl">
            <h4>{language.Use_the_token_wallet_to_scan_and_recharge}</h4>
            <img className="photo" src={this.state.qrurl}  />
            <p className="address">{window.address}</p>
            <button onClick={(e)=>this.setState({userqrurl:false})}>{language.Cancel}</button>
          </div>
        </Overlay>
      }

      <div className="UserBox row">
        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
          {!this.state.userPicturesing &&
            <div className="userPhoto" style={{background:"url("+this.state.userPictures+")"}}>
                <FileBase64 onDone={ this.fileChangedHandler.bind(this) } />
                <p><span>{language.Revise_the_head_image}</span></p>
            </div>
          }
          {this.state.userPicturesing &&
            <div className="userPhoto">
                <span className="glyphicon glyphicon-refresh"></span>
            </div>
          }
        </div>
        <div className="userinformation col-sm-12 col-md-10 col-lg-10">
          <div className="userlist row">
            <h2 className="col-sm-12 col-md-12 col-lg-4"><span className="col-sm-12 col-md-12 col-lg-12">{language.Hello}!</span><span className="col-sm-12 col-md-12 col-lg-12"> {this.state.user}</span></h2>
            <div className="col-sm-12 col-md-12 col-lg-8">
              <ul className="PPSinformation">
                <li><span>{language.PPS_balance}:</span>{this.state.ppsBalance}</li>
                <li><span>{language.ETH_balance}:</span>{this.state.ethBalance/this.CONST.weiToEther-0 == 0 ? '0' : (this.state.ethBalance/this.CONST.weiToEther-0).toFixed(5)}</li>
                <li className="hide"><span>{language.PPS_deposited_in_Populstay}:</span>{this.state.ppsDeposited}</li>
              </ul>
              <ul className="information">
                <li><span>{language.User_name}:</span>{this.state.user}</li>
                <li></li>
                <li><span>{language.Phone}:</span>{this.state.phone}</li>
                <li></li>
                <li><span>{language.Email}:</span>{this.state.email}</li>
              </ul>
              <span className="round"></span>
            </div>
          </div>
          <div className="userbtn">
            <ul>
              <li><button onClick={(e)=>this.setState({userqrurl:true})}>{language.Recharge}</button></li>
              <li className="hide"><WalletWithdraw  onGetDepositBalance={this.onGetDepositBalance}/></li>
              <li className="hide"><WalletDeposit  onGetDepositBalance={this.onGetDepositBalance}/></li>
              <li><WalletGas/></li>
              <li><WalletManage/></li>
            </ul>
          </div>
        </div>
      </div>

          

      <h1>{language.Guest_Managment_Panel} <span onClick={(e) => {if(this.state.Guest_Managment_Panel)this.setState({Guest_Managment_Panel:false});else this.setState({Guest_Managment_Panel:true});}} className={!this.state.Guest_Managment_Panel ? "glyphicon glyphicon-eye-open" : "glyphicon glyphicon-eye-close" }></span></h1>
      <p className={(this.state.orderlist.length == 0 && this.state.usdOrderList.length == 0 )? 'show No_order' : 'hide No_order'}>{language.No_order}</p>
      {!this.state.Guest_Managment_Panel &&
	      <div className={(this.state.orderlist.length == 0  && this.state.usdOrderList.length == 0 ) ? 'hide GuestManagment' : 'show GuestManagment'}>
	        {this.state.orderlist.map(item => (
	            <GuestOrderRow item={item}/>
	        ))}
	      </div>
      }

      <h1>{language.Host_Managment_Panel} <span onClick={(e) => {if(this.state.Host_Managment_Panel)this.setState({Host_Managment_Panel:false});else this.setState({Host_Managment_Panel:true});}} className={!this.state.Host_Managment_Panel ? "glyphicon glyphicon-eye-open" : "glyphicon glyphicon-eye-close" }></span></h1>
      
      <Link to='/create'>
        <button className={this.state.roomInfoList.length == 0 ? 'show Add_a_house' : 'hide Add_a_house'}>{language.Add_a_house}</button>
      </Link>

      {!this.state.Host_Managment_Panel &&
	      <div  className={this.state.roomInfoList.length == 0 ? 'hide HostManagment' : 'show HostManagment'}>
	         {this.state.roomInfoList.map(row => (
	                <HostRoomList row={row}/>
	          ))}
	      </div>
      }


      </div>
		);
	}
}
export default GuestInfo