import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import guestService from '../../services/guest-service';
import Wallet from '../Wallet/wallet';
import Web3 from 'web3';
import web3service from '../../services/web3-service';
import languageService from '../../services/language-service';
import { reactLocalStorage } from 'reactjs-localstorage';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import aesService from '../../services/aes-service';
import Overlay from '../overlay';

var CryptoJS = require("crypto-js");

class GuestRegister extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			modalIsOpen: false,
			id: "",
			user: "",
			account: "",
			phone: "",
			email: "",
			Verification: 60,
			VerificationCode: 0,
			registered: false,
			emailactive: 0,
			emailCode: '',
			Prompt: "",
			languagelist: {},
			subject: "",
			password: "",
			repeatPassword: "",
			codeimg: "",
			wrong_password: false,
			privateKey: "",
			Loginemail: "",
			Loginpassword: "",
			codeimgtype: "",
		};

		this.openModal = this.openModal.bind(this);
		this.afterOpenModal = this.afterOpenModal.bind(this);
		this.register = this.register.bind(this);
		web3service.loadWallet();
		languageService.language();

		// guestService.sendEmail(   "admin@populstay.com",
		//                           "13753068898@163.com",
		//                           "populstay demo test",
		//                           "populstay demo test"
		//                         );

	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
			Verification: window.languagelist.Get_verification_code,
			emailrandom: parseInt(10000000 * Math.random())
		});
		this.setState({});
		this.loadUserData();
	}

	register() {
		var register = {};
		register.id = window.address;
		register.user = this.state.user;
		register.account = window.address;
		register.phone = this.state.phone;
		register.email = this.state.email;
		register.password = this.state.password;
		register.repeatPassword = this.state.repeatPassword;
		register.encryptedPK = aesService.encrypt(window.aesprivateKey, this.state.password),
			guestService.guestRegister(register).then((data) => {
				this.import(this.state.email, this.state.password, 2)
				guestService.setWebToken(data.data.token);
				this.setState({
					registered: true,
					modalIsOpen: false
				});
			});
	}

	openModal() {
		this.setState({
			modalIsOpen: true
		});
	}

	afterOpenModal() {
		// references are now sync'd and can be accessed.
		this.subtitle.style.color = '#f00';
	}

	loadUserData = () => {
		this.setState({
			account: window.address,
			id: window.address
		});
		guestService.getGuesterInfo(window.address).then((data) => {
			this.setState({
				registered: true
			});
			this.setState({
				user: data.user
			});
		});
	}

	onAccountChange = (address, status) => {
		this.getVerificationCode()
		if(status == "import") {
			this.setState({
				account: "",
				modalIsOpen: false,
				modalinOpen: true,
				importstats: true
			})
		} else {
			this.setState({
				account: address
			})
		}
		guestService.getGuesterInfo(window.address).then((data) => {
			if(data && data.user) {
				this.setState({
					registered: true
				});
				this.setState({
					user: data.user
				});
				this.closeModal();
			}
		});
	}

	name(e) {
		this.namePrompt();
		this.setState({
			user: e
		});
		var rephone = /^[0-9a-zA-Z_]{3,16}$/;
		if(e.length != "" && rephone.test(e)) {
			guestService.checkname(e).then((data) => {
				if(data.status == "success") {
					this.setState({
						nameactive: 1
					})
				} else {
					this.setState({
						nameactive: 0
					})
				}
			})
		}
	}

	namePrompt() {
		var rephone = /^[0-9a-zA-Z_]{3,16}$/;
		if(this.state.user == "") {
			return
		} else if(!rephone.test(this.state.user)) {
			return this.state.languagelist.Chinese_and_English_length_316characters
		} else if(this.state.nameactive == 1) {
			return this.state.languagelist.Name_is_already_registered
		} else if(this.state.nameactive == 0) {
			return "ok"
		}
	}

	email(e) {
		this.emailPrompt();
		this.setState({
			emailCode: ""
		})
		this.setState({
			Verification: this.state.languagelist.Get_verification_code
		});
		clearTimeout(this.clearCode);
		this.setState({
			email: e
		});
		var rephone = /^[A-Za-z0-9\u4e00-\u9fa5-_.]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
		if(e.length != "" && rephone.test(e)) {
			guestService.checkemail(e).then((data) => {
				if(data.data.length >= 1) {
					this.setState({
						state: this.state.emailactive = 1
					});
				} else {
					this.setState({
						state: this.state.emailactive = 0
					});
				}
			});
		}
	}

	emailPrompt() {
		var rephone = /^[A-Za-z0-9\u4e00-\u9fa5-_.]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
		if(this.state.email == "") {
			return
		} else if(!rephone.test(this.state.email)) {
			return this.state.languagelist.Incorrect_mailbox_format
		} else if(this.state.emailactive == 1) {
			return this.state.languagelist.Email_is_already_registered
		} else if(this.state.emailactive == 0) {
			return "ok"
		}
	}

	Password(e) {
		this.PasswordPrompt();
		this.setState({
			state: this.state.password = e
		});
	}

	PasswordPrompt() {
		var rephone = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d~!@#$%^&*.]{8,16}$/;
		if(this.state.password == "") {
			return
		} else if(!rephone.test(this.state.password)) {
			return this.state.languagelist.Password_format_is_not_standardized
		} else if(this.state.password.length < 8 || this.state.password.length > 16) {
			return this.state.languagelist.Password_length_is_816characters
		} else {
			return "ok"
		}
	}

	repeatPassword() {
		if(this.state.repeatPassword == "") {
			return
		} else if(this.state.repeatPassword != this.state.password) {
			return this.state.languagelist.Two_passwords_are_different
		} else {
			return "ok"
		}
	}

	VerificationCode() {
		this.setState({
			VerificationCodeTS: true
		});
		setInterval(
			() => {
				this.setState({
					VerificationCodeTS: false
				});
			},
			3000
		);
		var num = 60;
		this.clearCode = setInterval(
			() => {
				if(this.state.Verification == 1) {
					this.setState({
						Verification: this.state.languagelist.Get_verification_code
					});
					clearTimeout(this.clearCode);
				} else {
					this.setState({
						Verification: num--
					});
				}
			},
			1000
		);
		guestService.getGuesterCode(this.state.email, window.languagelist.subject)
	}

	emailCode(e) {
		this.setState({
			emailCode: e
		})
		if(e.length == 4) {
			guestService.VerificationCode(this.state.email, e).then((data) => {
				if(data.data.success == e) {
					clearTimeout(this.clearCode);
					this.setState({
						Verification: this.state.languagelist.Successful_verification
					});
				} else {
					clearTimeout(this.clearCode);
					this.setState({
						Verification: this.state.languagelist.Verification_code_error
					});
				}
			})
		}
		if(e.length == 0) {
			this.setState({
				Verification: this.state.languagelist.Get_verification_code
			});
		}
	}
	user_status() {
		this.setState({
			modalIsOpen: false
		});
	}

	ClosemodalIsOpen() {
		clearTimeout(this.clearCode);
		this.setState({
			Verification: this.state.languagelist.Get_verification_code
		});
		this.setState({
			modalIsOpen: false,
			account: "",
			user: "",
			email: "",
			password: "",
			repeatPassword: "",
			phone: "",
			emailCode: ""
		})
		window.address = null;
		window.addressShow = null;
		window.aesprivateKey = null;
		reactLocalStorage.setObject('wallet', null);
	}

	/*登录*/

	clear() {

		window.address = null;
		window.addressShow = null;
		window.aesprivateKey = null;
		sessionStorage.removeItem('step');
		sessionStorage.removeItem('step1');
		sessionStorage.removeItem('step2');
		sessionStorage.removeItem('step3');
		sessionStorage.removeItem('step4');
		sessionStorage.removeItem('step6');
		sessionStorage.removeItem('step7');
		sessionStorage.removeItem('step8');
		sessionStorage.removeItem('step10');
		sessionStorage.removeItem('step12');
		sessionStorage.removeItem('step13');
		sessionStorage.removeItem('step15');
		sessionStorage.removeItem('step16');
		sessionStorage.removeItem('step17');
		sessionStorage.removeItem('step18');
		reactLocalStorage.setObject('wallet', null);
		window.location.href = '/';
	}

	import(Loginemail, Loginpassword, num) {
		var that = this;

		if(num == 2) {
			guestService.login(Loginemail, Loginpassword).then((data) => {
					if(data) {
						guestService.setWebToken(data.token);
						var obj = window.web3.eth.accounts.wallet.add(aesService.decrypt(data.user.encryptedPK, Loginpassword));
						console.log(obj)
						window.address = obj.address;
						window.addressShow = obj.address.substring(0, 10) + "...";
						window.aesprivateKey = data.user.encryptedPK;
						reactLocalStorage.setObject('wallet', {
							'address': window.address,
							'aesprivateKey': window.aesprivateKey,
							'addressshow': window.addressShow
						});
						this.setState({
							registered: true,
							modalinOpen: false
						});
						window.location.reload()
					}
				})
				.catch(function(error) {
					that.getVerificationCode()
					that.setState({
						wrong_password: true
					})
				});
		} else {
			//登录验证码验证
			guestService.loginVerificationCode(this.state.emailrandom, this.state.loginemailCode).then((data) => {
				if(data.data.success == this.state.loginemailCode) {
					//验证码正确登录
					this.setState({
						loginCode: false
					});
					guestService.login(Loginemail, Loginpassword).then((data) => {
							if(data) {
								guestService.setWebToken(data.token);
								var obj = window.web3.eth.accounts.wallet.add(aesService.decrypt(data.user.encryptedPK, Loginpassword));
								window.address = obj.address;
								window.addressShow = obj.address.substring(0, 10) + "...";
								window.aesprivateKey = data.user.encryptedPK;
								reactLocalStorage.setObject('wallet', {
									'address': window.address,
									'aesprivateKey': window.aesprivateKey,
									'addressshow': window.addressShow
								});
								this.setState({
									registered: true,
									modalinOpen: false
								});
								window.location.reload()
							}
						})
						.catch(function(error) {
							that.getVerificationCode()
							that.setState({
								wrong_password: true
							})
						});
				} else {
					this.getVerificationCode()
					this.setState({
						loginCode: true
					});
				}
			})
		}

	}

	substring0x = (str) => {
		str = str + "";
		return str.substring(2, str.length);
	}

	getVerificationCode() {
		guestService.getVerificationCode(this.state.emailrandom).then((data) => {
			this.setState({
				loginemailCode: ""
			});
			this.setState({
				codeimg: data.data.image
			})
		})
	}

	Loginemailcode(e) {
		this.setState({
			loginemailCode: e
		})
	}

	LoginemailPrompt(e) {
		this.setState({
			Loginemail: e,
			Loginpassword: ""
		})
	}

	Getkey() {
		if(aesService.decrypt(window.aesprivateKey, this.state.password)) {
			this.setState({
				privateKey: aesService.decrypt(window.aesprivateKey, this.state.password)
			})
		} else {
			this.setState({
				privateKey: ""
			})
		}
	}

	Loginpassword(e) {
		this.setState({
			Loginpassword: e,
			wrong_password: false,
			codeimgtype: true
		})
		this.setState({
			loginCode: false
		});
	}

	keykeypress(e) {
		if(e.which !== 13) return
		this.Getkey();
	}

	render() {
		const language = this.state.languagelist;
		return(

			<div>

        {
          this.state.registered === true && this.props.type == 'Signup'  && 
          <button onClick={(e) => window.location.href="/Info"} className="logoutButton float-right">{language.Welcome} &nbsp;{this.state.user.length>10 ? this.state.user.substr(0,9)+"..." : this.state.user}<span></span></button>
        }

        { 
          this.state.registered === false && this.props.type == 'Signup'  && 
          <button className="button__outline" onClick={this.openModal}>{language.Sign_up}</button>
        }

        {
          this.state.registered === true && this.props.type == 'login'  &&
          <a onClick={(e) => this.setState({modaloutOpen:true})}>{language.Log_out}</a>
        }

        {
          this.state.registered === false && this.props.type == 'login'  && 
          <a onClick={(e) => {this.setState({modalinOpen:true});this.getVerificationCode()}}>{language.Log_in}</a>
        }

        {
          this.state.registered === true && this.props.type == 'create'  && 
          <Link to="/create">{language.Become_a_Host}</Link>
        }

        { 
          this.state.registered === false && this.props.type == 'create'  &&
          <a onClick={(e) => {this.setState({modalinOpen:true});this.getVerificationCode()}}>{language.Become_a_Host}</ a>
        }

        {
          this.state.registered === true && this.props.type == 'footer_create'  && 
          <Link to="/create"><li>{language.Become_a_Host}</li></Link>
        }

        { 
          this.state.registered === false && this.props.type == 'footer_create'  &&
          <a onClick={(e) => {this.setState({modalinOpen:true});this.getVerificationCode()}}><li>{language.Become_a_Host}</li></ a>
        }

        {
          this.state.registered === false && this.props.type == 'Book'  && 
          <button className="bg-pink color-blue btn-lg btn-block text-bold text-center" onClick={(e) => {this.setState({modalinOpen:true});this.getVerificationCode()}}>{language.Log_in}</button>
        }




         <div className="registermodal">
            <Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={(e)=>this.ClosemodalIsOpen(e)} 
            contentLabel="Example Modal">
            <div className="Signup">
                <img className="close" onClick={(e)=>this.ClosemodalIsOpen()} src="../images/closezi.png" />
                <h2 ref={subtitle => this.subtitle = subtitle}>{language.Guest_Register}</h2>
                <br/>
                <div className="content">
                  <Wallet onAccountChange={this.onAccountChange}/>  

                  <div className="Prompt">
                    <img src="../images/Prompt.png" />
                    <p><span></span>{language.Please_remember_the_private_key_and_login_password}</p>
                  </div>
                <div className="overflow"></div>  
                <br/>

                <div className="form-group">
                  <label>{language.Wallet_Account} <span>*</span></label>
                  <input type="text"  className="form-control" placeholder={language.Wallet_Account} 
                  value={this.props.clicklogout == true ? this.setState({account:""}) : this.state.account} disabled/>
                </div>

                <div className="form-group">
                  <label>{language.User} <span>*</span></label>
                  <input type="text" className="form-control" placeholder={language.Please_enter_User_name} value={this.state.user} onChange={(e) => this.name(e.target.value)}/>
                  <small className={this.namePrompt() == "ok" ? "emailHelp hide" : "emailHelp"} >{this.namePrompt()}</small>
                  <b className={this.namePrompt() == "ok" ? "Active" : ""}></b>
                </div>

                <div className="form-group">
                  <label >{language.Email} <span>*</span></label>
                  <input type="email" className="form-control" id="exampleInputEmail1" value={this.state.email} aria-describedby="emailHelp" placeholder={language.Please_Enter_your_email}  onChange={(e) => this.email(e.target.value)}/>
                  <small className={this.emailPrompt() == "ok" ? "emailHelp hide" : "emailHelp"} >{this.emailPrompt()}</small>
                  <b className={this.emailPrompt() == "ok" ? "Active" : ""}></b>
                </div>


                <div className="form-group">
                  <label>{language.User_Password} <span>*</span></label>
                  <input type="password" className="form-control" placeholder={language.Please_Enter_your_password} onChange={(e) => this.Password(e.target.value)}/>
                  <small className={this.PasswordPrompt() == "ok" ? "emailHelp hide" : "emailHelp"} >{this.PasswordPrompt()}</small>
                  <b className={this.PasswordPrompt() == "ok" ? "Active" : ""}></b>
                </div>

                <div className="form-group">
                  <label>{language.Please_enter_password_again} <span>*</span></label>
                  <input type="password" className="form-control" placeholder={language.Please_enter_password_again} onChange={(e) => this.setState({repeatPassword: e.target.value})}/>
                  <small className={this.repeatPassword() == "ok" ? "emailHelp hide" : "emailHelp"} >{this.repeatPassword()}</small>
                  <b className={this.repeatPassword() == "ok" ? "Active" : ""}></b>
                </div>

                <div className="form-group">
                  <label>{language.Phone}<span>({language.Optional})</span></label>
                  <input type="number" className="form-control" placeholder={language.Please_Enter_your_phone} value={this.state.phone} onChange={(e) => this.setState({phone:e.target.value})}/>
                </div>

                <div className="VerificationCode">
                  <button className={this.state.account != "" && this.emailPrompt() == "ok" && this.repeatPassword() == "ok" ? "" : "active"} disabled={this.state.account != "" && this.emailPrompt() == "ok" && this.repeatPassword() == "ok" && this.state.Verification == this.state.languagelist.Get_verification_code ? "" : "true"} onClick={(e)=>this.VerificationCode(e)}>{this.state.Verification}</button>
                  <input type="number" placeholder={language.Code}  value={this.state.emailCode} onChange={(e) => this.emailCode(e.target.value)} />
                </div>

                <br/>
                <button className="btn closecancel" onClick={(e)=>this.ClosemodalIsOpen()}>{language.Cancel}</button>
                <button className={this.state.account == "" || this.namePrompt() != "ok" || this.repeatPassword() != "ok" || this.state.Verification != this.state.languagelist.Successful_verification ? 'closeok closeactive' : 'closeok'} disabled={this.state.account == "" || this.namePrompt() != "ok" || this.repeatPassword() != "ok" || this.state.Verification != this.state.languagelist.Successful_verification ? 'true' : ''} onClick={this.register}>{language.OK}</button>
                <p onClick={(e)=>{this.setState({modalIsOpen:false,modalinOpen:true,loginCode:"",codeimgtype:false,loginemailCode:""}); this.ClosemodalIsOpen();this.getVerificationCode()}} className="user_status">{language.Have_Account}<a>{language.Log_in}</a></p>
                </div>
                {this.state.VerificationCodeTS &&
                    <p className="VerificationCodeTS" >{language.Mail_has_been_sent_to_the_mailbox_please_check}</p>
                }
              </div>
            </Modal>
        </div>


        <Modal isOpen={this.state.modaloutOpen} onAfterOpen={this.afterOpenModal} onRequestClose={(e)=>this.setState({modaloutOpen:false})}  contentLabel="Wallet Message">
          <div className="clear">
            <img className="redlogo" src="../images/pps.png" />
            <h2 ref={subtitle => this.subtitle = subtitle}>{language.Please_Remember_Your_Pirvate_Key}</h2>
            <div>
              <h3>{language.Address}: &nbsp;&nbsp;</h3>
              <CopyToClipboard text={window.address}
                onCopy={() => this.setState({copied: true})}>
                <button className="copy">{this.state.copied ? language.Successful_copy : language.Copy_address}</button>
              </CopyToClipboard>
              <p className="text1">{window.address}</p>
              <h3>{language.Private_Key}: &nbsp;&nbsp;</h3>
              {this.state.privateKey != "" &&
                <CopyToClipboard text={this.substring0x(this.state.privateKey)}
                  onCopy={() => this.setState({copied1: true})}>
                  <button className="copy">{this.state.copied1 ? language.Successful_copy : language.Copy_Private_Key}</button>
                </CopyToClipboard>
              }
              {this.state.privateKey == "" &&
                <div className="Getkey">
                  <input type="password" placeholder={language.Enter_your_login_password_to_get_the_private_key} onChange={(e)=>this.setState({password:e.target.value})} onKeyPress={this.keykeypress.bind(this)} />
                  <button onClick={(e)=>this.Getkey()}>{language.Get}</button>
                </div>
              }
              <p className={this.state.privateKey == "" ? "hide text1" : "show text1"}>{this.substring0x(this.state.privateKey)}</p>
              
            </div>  
            <button className="btn btn-danger Left" onClick={this.clear}>{language.Log_out}</button>
            <button className="btn btn-primary Right"  onClick={(e) => this.setState({modaloutOpen:false,copied: false,copied1: false})}>{language.Cancel}</button>
          </div>
        </Modal>

        <Modal isOpen={this.state.modalinOpen} onAfterOpen={this.afterOpenModal} onRequestClose={(e)=>this.setState({modalinOpen:false})}  contentLabel="Wallet Message">
          <div className="Import">
          <img className="redlogo" src="../images/pps.png" />
          <h2 className={this.state.importstats ? "hide" : "show"}  ref={subtitle => this.subtitle = subtitle}>{language.Log_in}</h2>
          <h2 className={this.state.importstats ? "show" : "hide"} >{language.Have_Account}{language.Log_in}</h2>
              <div className="form-group">
              <label>{language.Email}</label>
              <input type="email"  className="form-control" placeholder={language.Please_Enter_your_email} onChange={(e) => this.LoginemailPrompt(e.target.value)} />
              <small className="emailHelp" >{this.state.LoginemailPrompt}</small>
              <br/>
              <label>{language.User_Password}</label>
              <input type="password" value={this.state.Loginpassword}  className="form-control" placeholder={language.Please_enter_your_login_password}  onChange={(e) => this.Loginpassword(e.target.value)} />

              <div className={this.state.codeimgtype ? "codeimg show" : "codeimg hide"}>
                <input type="text" placeholder={language.Please_Enter_code} value={this.state.loginemailCode} disabled={this.state.loginemailCode == language.Successful_verification || this.state.loginemailCode == language.verification_failed ? "true" : ""} onChange={(e) => this.Loginemailcode(e.target.value)}/>
                <img onClick={(e)=>this.getVerificationCode()} src={this.state.codeimg} />
              </div>
              <br/>
              {!this.state.loginCode &&
                <p className={this.state.wrong_password ? "show wrong_password" : "hide wrong_password"}>{language.Email_address_or_password_is_incorrect}</p>
              }
              {this.state.loginCode &&
                <p className="wrong_password">{language.Verification_code_error}</p>
              }
            </div>
            <div className="overflow">
              <button className="btn btn-danger Left" disabled={this.state.Loginemail == "" || this.state.Loginpassword == "" || this.state.loginemailCode == "" ? "true" : "" } onClick={(e)=>this.import(this.state.Loginemail,this.state.Loginpassword,1)}>{language.Log_in}</button>
              <button className="btn btn-primary Right " onClick={(e)=>this.setState({modalinOpen:false,wrong_password:false,Loginemail:"",Loginpassword:""})}>{language.Cancel}</button>
            </div>
          <p onClick={(e)=>this.setState({modalIsOpen:true,modalinOpen:false,Loginemail:"",Loginpassword:""})} className="user_status">{language.No_account}<a>{language.Sign_up}</a></p>
          </div>
        </Modal>


      </div>
		);
	}
}
export default GuestRegister