import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';
import ppsService from '../../services/pps-service';
import { Link } from 'react-router-dom';
import Overlay from './../overlay';
import web3Service from '../../services/web3-service';
import EthereumQRPlugin from 'ethereum-qr-code';
import Modal from 'react-modal';
import aesService from '../../services/aes-service';

var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

const qr = new EthereumQRPlugin();

class Payments extends Component {
	constructor(props) {
		super(props)

		this.CONST = {
			weiToEther: 1000000000000000000,
			weiToGwei: 1000000000,
			GweiToEther: 1000000000,
			weiToUSD: 1000000,
			CNY_USD: 0.1464

		}

		this.STEP = {
			VIEW: 1,
			SUBMIT: 2,
			PROCESSING: 3,
			PURCHASED: 4,
			Insufficient: 5,
			EnterPassword: 6,
			BOOKED: 404,
		}

		this.state = {
			step: this.STEP.VIEW,
			modalIsOpen: false,
			qrurl: "",
			languagelist: {},
			tx: "",
			Service_fees: 3,
		};

		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		web3Service.loadWallet();
		languageService.language();
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			book: nextProps.book
		})
		if(nextProps.bookStep == "Payments") {
			this.setState({
				step: this.STEP.EnterPassword
			})
		}
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist
		});

		if(window.address) {
			web3Service.getETHBalance(window.address).then((data) => {
				this.setState({
					ethBalance: data / this.CONST.weiToEther
				});
			});

			ppsService.getBalance(window.address).then((data) => {
				this.setState({
					ppsBalance: data
				});
			});
		}
		houselistingService.getHouseInfoById(this.props.book.listingId).then((data) => {
			this.setState({
				lister: data.hostAddress
			})
			var PPSprice = data.description.price_perday
			if(this.props.book.priceActive == 0) {
				this.setState({
					price: PPSprice
				})
			} else {
				ppsService.getethprice().then((data) => {
					var ethprice = Math.round((PPSprice * this.CONST.CNY_USD * (1 / data.ethprice)) * 100000) / 100000;

					this.setState({
						price: ethprice
					})
				})

			}
		})

		this.loadQrCode();

	}

	calcTotalPrice() {
		var TotalPrice = Math.round((this.DateDays() * this.state.price + this.Service_fees()) * 100000) / 100000;
		return TotalPrice == 0 ? "0.00" : TotalPrice;
	}

	TotalPrice() {
		return(this.state.price * this.DateDays()) == 0 ? "0.00" : (this.state.price * this.DateDays());
	}

	DateDays() {
		var checkInDate = moment(new Date(Number(this.state.book.checkInDate)));
		var checkOutDate = moment(new Date(Number(this.state.book.checkOutDate)));
		if(checkInDate && checkOutDate) {
			let days = checkOutDate.diff(checkInDate, 'days');
			return days
		}
		return 0
	}

	Service_fees() {
		var Service_fees = this.state.Service_fees * 0.01;

		if(this.state.book.priceActive == 0) {
			return parseInt(this.TotalPrice() * Service_fees);
		} else {
			return Math.round((this.TotalPrice() * Service_fees) * 100000) / 100000;
		}

	}

	EnterPasswordBook() {
		var that = this;
		var promise;
		if(aesService.decrypt(window.aesprivateKey, this.state.PasswordBook)) {
			this.setState({
				step: this.STEP.VIEW
			});
			if(this.state.book.priceActive == 0) {
				if(this.state.ppsBalance - 0 < this.calcTotalPrice() - 0) {
					this.openModal();
				} else {
					promise = ppsService.setPreOrder(
						this.state.lister,
						this.calcTotalPrice(),
						this.state.book.listingId,
						this.state.book.checkInDate,
						this.state.book.checkOutDate,
						this.DateDays(),
						this.state.PasswordBook,
						this.Service_fees(),
						this.state.book.guest
					);
				}

			} else if(this.state.priceActive == 2) {

				promise = ppsService.setOrderByUSD(
					this.state.lister,
					this.calcTotalPrice(),
					this.state.book.listingId,
					this.state.book.checkInDate,
					this.state.book.checkOutDate,
					this.DateDays(),
					this.state.PasswordBook,
					this.state.book.guest
				);
				this.setState({
					step: this.STEP.PURCHASED
				})
				setTimeout(
					() => window.location.href = "/Info",
					2000
				);
				return;

			} else {
				if(this.calcTotalPrice() > this.state.ethBalance) {
					var to = window.address;
					var value = Math.round(this.calcTotalPrice());

					// qr.toDataUrl({
					//     to    : window.address,
					//     value : value,
					//     gas   : window.gas
					// }).then((qrCodeDataUri)=>{
					// this.setState({qrurl:qrCodeDataUri.dataURL}); //'data:image/png;base64,iVBORw0KGgoA....'
					// })

					this.openModal();
					web3Service.getBalanceForCharge(to, value).then((balance) => {
						promise = houselistingService.setPreOrderByETH(
							this.state.lister,
							this.calcTotalPrice(),
							this.calcTotalPrice() * this.CONST.GweiToEther,
							this.state.book.listingId,
							this.state.book.checkInDate,
							this.state.book.checkOutDate,
							this.DateDays(),
							this.state.PasswordBook
						);
					});
					return;
				} else {
					promise = houselistingService.setPreOrderByETH(
						this.state.lister,
						this.calcTotalPrice(),
						this.calcTotalPrice() * this.CONST.GweiToEther,
						this.state.book.listingId,
						this.state.book.checkInDate,
						this.state.book.checkOutDate,
						this.DateDays(),
						this.state.PasswordBook
					);
				}
			}
			promise.then((data) => {
					console.log(123)
					if(data.error == "booked" || data.error == "house_in_booking") {
						this.setState({
							step: this.STEP.BOOKED
						});
					} else {
						this.setState({
							step: this.STEP.PROCESSING,
							tx: data.txhash
						})
						return ppsService.waitTransactionFinished(data.txhash)
					}
				})
				.then((data) => {
					console.log(data)
					if(data) {
						this.setState({
							step: this.STEP.PURCHASED
						})
						setTimeout(
							() => window.location.href = "/Info",
							2000
						);
					}
				})
				.catch((error) => {
					console.log(error)
					this.setState({
						step: this.STEP.VIEW
					})
				})

		} else {
			that.setState({
				wrong_password: true
			})
		}
	}

	loadQrCode = () => {
		qr.toDataUrl({
			to: window.address,
			gas: window.gas
		}).then((qrCodeDataUri) => {
			this.setState({
				qrurl: qrCodeDataUri.dataURL
			}); //'data:image/png;base64,iVBORw0KGgoA....'
		})
	}

	openModal() {
		this.setState({
			modalIsOpen: true
		});
		console.log(123)
	}

	closeModal() {
		this.setState({
			modalIsOpen: false
		});
	}

	keypress(e) {
		if(e.which !== 13) return
		this.EnterPasswordBook()
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div>

              <Modal isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal} contentLabel="Wallet Message">
                <div className="modalcode">
                  <h3 ref={subtitle => this.subtitle = subtitle}>{language.Your_balance_is_not_enough_SCAN_QR_to_pay}</h3>
                  <br/>
                    <div className="listing-card">
                    <img className="photo" src={this.state.qrurl}  />
                    </div>
                  <br/>
                  <button onClick={(e) => {this.closeModal(e)}} >{language.Cancel}</button>
                </div>  
              </Modal>

              {this.state.step===this.STEP.METAMASK &&
                  <Overlay>
                    <p>{language.Confirm_transaction}</p>
                    <p>{language.Press_Submit_in_MetaMask_window}</p>
                  </Overlay>
                }


                <Modal isOpen={this.state.step===this.STEP.PROCESSING} contentLabel="Wallet Message">
                  <div className="ModalALL">
                    <h3>{language.Processing_your_booking}</h3>
                    <p>{language.Please_stand_by}...</p>
                  </div>
                </Modal>

                <Modal isOpen={this.state.step===this.STEP.PURCHASED} contentLabel="Wallet Message">
                  <div className="ModalALL">
                    <h3>{language.Booking_was_successful}</h3>
                  </div>
                </Modal>

                <Modal isOpen={this.state.step===this.STEP.BOOKED} contentLabel="Wallet Message">
                  <div className="ModalALL">
                    <h3>{language.The_order_has_been_pre_ordered}</h3>
                    <a href={`/listing/${this.props.book.listingId}`}  ><button>{language.back}</button></a>
                  </div>
                </Modal>
                

                {this.state.step===this.STEP.EnterPassword &&
                  <Overlay>
                    <div className="EnterPassword">
                        <h2>{language.Enter_your_login_password_to_book}</h2><br/>
                        <input type="password" placeholder={language.User_Password} onChange={(e)=>this.setState({PasswordBook:e.target.value,wrong_password:false})} onKeyPress={this.keypress.bind(this)} />
                        <p className={this.state.wrong_password ? "show wrong_password" : "hide wrong_password"}>{language.wrong_password}</p>
                        <button className={this.state.PasswordBook == "" ? "PasswordNull Left" : "Left"} disabled={this.state.PasswordBook == "" ? "true" : ""} onClick={(e)=>this.EnterPasswordBook()} >{language.Book}</button>
                        <button className="Right" onClick={(e)=>this.setState({step:this.STEP.VIEW,PasswordBook:""})}>{language.Cancel}</button>
                    </div>
                  </Overlay>
                }
            
          </div>
		)
	}
}

export default Payments