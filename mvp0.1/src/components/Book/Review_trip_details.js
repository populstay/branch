import React, { Component } from 'react';
import languageService from '../../services/language-service';
import houselistingService from '../../services/houseinfolist-service';
import Amenities from './../Detail/Amenities';
import Rules from './../Detail/Rules';
import Manual from './../Detail/Manual';
import guestService from '../../services/guest-service';
var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

class Review_trip_details extends Component {
	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			Description: {},
			hostTime: "",
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
			info: this.props.book
		});

		houselistingService.getHouseInfoById(this.props.book.listingId)
			.then((data) => {
				this.setState({
					Description: data.description,
					hostTime: data.createdAt
				})
				if(data.hostAddress) {
					guestService.getGuesterInfo(data.hostAddress).then((data) => {
						this.setState({
							hostName: data.user
						});
					});
				}

				//获取房源是否被预定
				if(data.bookedDate != undefined && data.bookedDate.data != undefined) {
					var DateLists = data.bookedDate.data;
  				
          for(var i = 0; i < DateLists.length; i++) {
  					if(this.props.book.checkInDate == DateLists[i].start && this.props.book.checkOutDate == DateLists[i].end) {
  						if(DateLists[i]) {
  							this.setState({
  								booked: true
  							})
  						} else {
  							this.setState({
  								booked: false
  							})
  						}
  					}

  				}
        }


			})
	}

	PaymentsStep = () => {
		this.props.PaymentsStep("Whos_coming");
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="col-sm-12 col-md-7 col-lg-7 Left_Part">
                    <div className="About_the_Space" >
                        <ul>
                            <li>
                                <img src="../images/BookCorrect.png" />
                                <div>
                                <h3>{language.ABOUT_THE_SPACE}</h3>
                                <p>{this.state.Description.roombasics_guestbedroom}{language.zhang}{language.bedrooms}、{this.state.Description.roombasics_guestbathroom}{language.jian}{language.bathrooms}、{this.state.Description.roombasics_guestsnumber} {language.wei}{language.guests}.</p>
                                </div>
                            </li>
                            <li>
                                <img src="../images/BookCorrect.png" />
                                <div>
                                <h3>{language.INCLUDED_AMENITIES}</h3>
                                  <Amenities listingId={this.props.book.listingId} type="book"  />
                                </div>
                            </li>
                            <li>
                                <img src="../images/BookCorrect.png" />
                                <div>
                                <h3>{language.ABOUT_YOUR_HOST}</h3>
                                {language.language == "zh" &&
                                  <p>{this.state.hostName}是超级房东，于{this.state.hostTime.substr(0,4)}年成为房东.</p>
                                }
                                {language.language != "zh" &&
                                  <p>{this.state.hostName} is a Superhost and started hosting in {this.state.hostTime.substr(0,4)}.</p>
                                }
                                </div>
                            </li>
                            <li>
                                <img src="../images/BookCorrect.png" />
                                <div>
                                <h3>{language.RECENT_REVIEWS}</h3>
                                <p>{language.This_home_has} 0 {language.five_star_reviews}</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div className="Nights_in" >
                    {language.language == "zh" &&
                      <h4>{this.state.Description.roomstuff_City} {language.Check_in} {this.state.DateDays} {language.nights}  </h4>
                    }

                    {language.language != "zh" &&
                      <h4>{this.props.book.DateDays} {language.nights} in {this.state.Description.roomstuff_City}</h4>
                    }
                      <ul>
                          <li>
                              <div className="imgdiv" >
                                <img src="../images/step3_8img2.png" />
                                <p>{moment(this.props.book.checkInDate).format('ll').split(" ")[1]}<br/>{moment(this.props.book.checkInDate).format('ll').split(" ")[0]}</p>
                              </div>
                              <div>
                                  <p>{language.language == "zh" ? moment(this.props.book.checkInDate).locale("zh-cn").format('dddd') : moment(this.props.book.checkInDate).format('dddd')} {language.Check_in}</p>
                                  <h5 className="hide" >{language.After12PM}</h5>
                              </div>
                          </li>
                          <li>
                              <div className="imgdiv" >
                                <img src="../images/step3_8img2.png" />
                                <p>{moment(this.props.book.checkOutDate).format('ll').split(" ")[1]}<br/>{moment(this.props.book.checkOutDate).format('ll').split(" ")[0]}</p>
                              </div>
                              <div>
                                  <p>{language.language == "zh" ? moment(this.props.book.checkOutDate).locale("zh-cn").format('dddd') : moment(this.props.book.checkOutDate).format('dddd')} {language.Check_out}</p>
                                  <h5 className="hide" >{language.Before12PM}</h5>
                              </div>
                          </li>
                      </ul>
                      {this.state.Description.roomstuff_smartpincode == 1 &&
                        <p>{language.Self_checkin_with_lockbox}</p>
                      }
                    </div>

                    <div className="Nights_in" >
                      <h4>{language.Things_to_keep_in_mind}</h4>
                      <Rules type="confrim"  listingId={this.props.book.listingId} />
                    </div>

                    <div className="Nights_in" >
                      <h4>{language.You_must_acknowledge}</h4>
                      <Manual type="book"  listingId={this.props.book.listingId} />
                      <p className="color-pink">{language.Enjoy_Your_Stay}</p>
                    </div>

                     {!this.state.booked &&
                          <button  onClick={(e)=>this.PaymentsStep()} >{language.Agree_and_continue}</button>
                      }

                      {this.state.booked &&
                        <button disabled className="buttonActive" >{language.The_order_has_been_pre_ordered}</button>
                      }
                </div>
		)
	}
}

export default Review_trip_details