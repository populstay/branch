import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';

class Whos_coming extends Component {
	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			GuestsARR: [],
			guest: 1,
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
			guest: this.props.book.guest
		});

		houselistingService.getHouseInfoById(this.props.book.listingId)
			.then((data) => {
				this.setState({
					Description: data.description
				})
				if(data.hostAddress) {
					guestService.getGuesterInfo(data.hostAddress).then((data) => {
						this.setState({
							hostName: data.user
						});
					});
				}

				//获取房源可住人数
				var Guest = [];
				for(var i = 1; i <= data.description.roombasics_guestsnumber; i++) {
					Guest.push(i);
				}
				this.setState({
					GuestsARR: Guest
				});

			})
	}

	urlgaibian(Guests) {
		var url = "/Book?checkInDate=" + this.props.book.checkInDate + //入住时间  
			"&checkOutDate=" + this.props.book.checkOutDate + //退房时间         
			"&guest=" + Guests + //客人人数  
			"&currency=" + this.props.book.currency + //货币  
			"&priceActive=" + this.props.book.priceActive + //支付类型
			"&listingId=" + this.props.book.listingId //房屋id

		history.pushState(this.state, "My Profile", url)
		this.Guest(Guests)
	}

	Guest = (Guests) => {
		this.props.Guest(Guests);
	}

	PaymentsStep = () => {
		this.props.PaymentsStep("Confirm_and_Pay");
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="col-sm-12 col-md-7 col-lg-7 Left_Part">
                <div className="Guests" >
                  <h4>{language.Guests}</h4>
                  <div className="form-group">    
                    <div className="btn-group">
                      <button type="button" data-toggle="dropdown">{this.state.guest} {language.Guests}<span>▼</span></button>
                      <ul className="dropdown-menu" role="menu"> 
                        {this.state.GuestsARR.map((item,index) => (
                            <li><a onClick={(e)=>{this.setState({guest:item});this.urlgaibian(item)}} >{item} {language.Guests}</a></li>
                          ))
                        }
                      </ul>
                    </div>
                  </div>
                </div>

                <div className="ContactHost" >
                  <h4>{language.Say_hello_to_your_host}</h4>
                  <p>{language.Let+" "+this.state.hostName+" "+language.know_a_little_about_yourself_and_why_youre_coming}</p>
                  <textarea placeholder={language.Hello+this.state.hostName+language.Cant_wait_to_go_to_your_house}  onChange={(e) => this.setState({roomstuff_City: this.Trim(e.target.value)})} >
                    {this.state.roomstuff_City}
                  </textarea>
                </div>

                  <button onClick={(e)=>this.PaymentsStep()} >{language.Continue}</button>
 
            </div>
		)
	}
}

export default Whos_coming