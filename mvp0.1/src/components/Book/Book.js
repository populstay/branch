import React, { Component } from 'react';
import languageService from '../../services/language-service';
import Review_trip_details from './Review_trip_details';
import Whos_coming from './Whos_coming';
import Confirm_and_Pay from './Confirm_and_Pay';
import Book_Detail from './Book_Detail';
import Payments from './Payments';
import houselistingService from '../../services/houseinfolist-service';
import guestService from '../../services/guest-service';
var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

class Book extends Component {

	constructor(props) {
		super(props)
		this.state = {
			languagelist: {},
			Description: {},
			hostTime: "",
			PaymentsStep: "Review_trip_details",
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
		});

		//获取url订单信息
		var href = window.location.href;
		var params = href.split("?")[1];
		var paramArr = params.split('&');
		var res = {};
		for(var i = 0; i < paramArr.length; i++) {
			var str = paramArr[i].split('=');
			res[str[0]] = decodeURI(str[1]);
		}

		this.setState({
			checkInDate: Number(res.checkInDate),
			checkOutDate: Number(res.checkOutDate),
			guest: res.guest,
			currency: res.currency,
			priceActive: res.priceActive,
			listingId: res.listingId,
		})

		houselistingService.getHouseInfoById(res.listingId)
			.then((data) => {
				this.setState({
					Description: data.description,
					hostTime: data.createdAt
				})

				if(data.hostAddress) {
					guestService.getGuesterInfo(data.hostAddress).then((data) => {
						this.setState({
							hostName: data.user
						});
					});
				}

			})

		if(window.address) {
			guestService.getGuesterInfo(window.address).then((data) => {
				this.setState({
					guestName: data.user
				});
			});
		}

	}

	PaymentsStep = (value) => {
		this.setState({
			PaymentsStep: value
		})
	}

	Guest = (value) => {
		this.setState({
			guest: value
		})
	}

	Confirm = (index,Currency) => {
		this.setState({
			priceActive: index,
			currency:Currency,
		})
	}

	bookStep = (value) => {
		this.setState({
			bookStep: value
		})
		console.log(value)
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="Payments"  >
            <div className="BookNav" >
                <a href="../" ><img className="logo" src="../images/logo.png" alt=""/></a>
                <ul>
                  <li className="color-pink cursor" onClick={(e)=>this.setState({PaymentsStep:"Review_trip_details"})} >1. {language.Review_trip_details}   
                  &nbsp;&nbsp;
                  <span className="bj-pink"></span>
                  <span className="bj-pink"></span>
                  <span className="bj-pink"></span>
                  </li>
                  <li onClick={(e)=>{if(this.state.PaymentsStep == "Review_trip_details")this.setState({PaymentsStep:"Review_trip_details"});else this.setState({PaymentsStep:"Whos_coming"}) }} className={this.state.PaymentsStep == "Whos_coming" || this.state.PaymentsStep == "Confirm_and_Pay" ? "color-pink cursor" : ""}>2. {language.Whos_coming} 
                  &nbsp;&nbsp;
                  <span className={this.state.PaymentsStep == "Whos_coming" || this.state.PaymentsStep == "Confirm_and_Pay" ? "bj-pink" : ""}></span>
                  <span className={this.state.PaymentsStep == "Whos_coming" || this.state.PaymentsStep == "Confirm_and_Pay" ? "bj-pink" : ""}></span>
                  <span className={this.state.PaymentsStep == "Whos_coming" || this.state.PaymentsStep == "Confirm_and_Pay" ? "bj-pink" : ""}></span>
                  </li>
                  <li className={this.state.PaymentsStep == "Confirm_and_Pay" ? "color-pink" : ""}>3. {language.Confirm_and_Pay}</li>
                </ul>
            </div>

            {this.state.PaymentsStep == "Review_trip_details" &&
              <h2>{language.Review_trip_details}</h2>
            }

            {this.state.PaymentsStep == "Whos_coming" &&
              <h2>{language.Whos_coming}</h2>
            }

            {this.state.PaymentsStep == "Confirm_and_Pay" &&
              <h2>{language.Confirm_and_Pay}</h2>
            }
            

            <div className="row" >
              {this.state.PaymentsStep == "Review_trip_details" &&
                <Review_trip_details PaymentsStep={this.PaymentsStep} book={this.state} />
              }

              {this.state.PaymentsStep == "Whos_coming" &&
                <Whos_coming PaymentsStep={this.PaymentsStep} Guest={this.Guest} book={this.state} />
              }

              {this.state.PaymentsStep == "Confirm_and_Pay" &&
                <Confirm_and_Pay Confirm={this.Confirm} bookStep={this.bookStep} book={this.state} />
              }

              <div className="col-sm-12 col-md-5 col-lg-5 Right_Part">
              	<Book_Detail book={this.state} />
              </div>

            </div>

            <Payments Payments={this.Payments} bookStep={this.state.bookStep} book={this.state} />
        </div>

		)
	}
}

export default Book