import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';
import Booking from './../Detail/Booking';

class Confirm_and_Pay extends Component {
	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			CurrencyArr: ["PPS","ETH"],
			priceActive: 1,
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
			priceActive:this.props.book.priceActive
		});

		houselistingService.getHouseInfoById(this.props.book.listingId)
			.then((data) => {
				this.setState({
					Description: data.description,
					Country:data.description.roomstuff_Country
				})
				if(data.hostAddress) {
					guestService.getGuesterInfo(data.hostAddress).then((data) => {
						this.setState({
							hostName: data.user
						});
					});
				}

				//获取房源可住人数
				
			})
	}

	urlgaibian(index,Currency) {
		var url = "/Book?checkInDate=" + this.props.book.checkInDate + //入住时间  
			"&checkOutDate=" + this.props.book.checkOutDate + //退房时间         
			"&guest=" + this.props.book.guest + //客人人数  
			"&currency=" + Currency + //货币  
			"&priceActive=" + index + //支付类型
			"&listingId=" + this.props.book.listingId //房屋id

		history.pushState(this.state, "My Profile", url)
		this.Confirm(index,Currency)
	}

	Confirm = (index,Currency) => {
		this.props.Confirm(index,Currency);
	}

	bookStep = () => {
		this.props.bookStep("Payments");
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="col-sm-12 col-md-7 col-lg-7 Left_Part">
                <div className="Pay_with" >
                  	<h4>{language.Pay_with}</h4>
                  	<div className="form-group">    
                    	<div className="btn-group">
	                      	<button type="button" data-toggle="dropdown">{this.state.CurrencyArr[this.state.priceActive]}<span>▼</span></button>
	                      	<ul className="dropdown-menu" role="menu"> 
	                        	{this.state.CurrencyArr.map((Currency,index) => (
	                            	<li><a onClick={(e)=>{this.urlgaibian(index,Currency);this.setState({priceActive:index})}} >{Currency}</a></li>
	                          	))
	                       		}
	                      	</ul>
	                    </div>

	                    <div className="GuestsDetail" >
	                    	<div className="Left">
	                    		<label>{language.First_name}</label>
	                    		<input type="text" placeholder={language.First_name} />
		                    </div>

		                    <div className="Right">
	                    		<label>{language.Last_name}</label>
	                    		<input type="text" placeholder={language.Last_name} />
		                    </div>

		                    <div className="Left">
	                    		<label>{language.Billing_info}</label>
	                    		<input type="text" placeholder={language.ZIP_Code} />
		                    </div>
							
							<div className="Right btn-group ">
	                    		<label>{language.Billing_country}</label>
		                      	<button type="button" data-toggle="dropdown">{this.state.Country}<span>▼</span></button>
		                      	<ul className="dropdown-menu" role="menu"> 
		                        	{language.Countrys.map((Country,index) => (
		                            	<li><a onClick={(e)=>this.setState({Country:Country})} >{Country}</a></li>
		                          	))
		                       		}
		                      	</ul>
		                    </div>
							<p className="clearFloat"></p>
	                    </div>
                  	</div>
                </div>

                <p className="color-pink" >{language.RedeemAcoupon}</p>

                 <Booking listingId={this.props.book.listingId} type="Confirm_and_Pay" />

                 <button onClick={(e)=>this.bookStep()} >{language.Continue}</button>
 
            </div>
		)
	}
}

export default Confirm_and_Pay