import React, { Component } from 'react';
import ppsService from '../../services/pps-service';
import languageService from '../../services/language-service';
import Fees from './../Detail/Fees';
import Booking from './../Detail/Booking';
import houselistingService from '../../services/houseinfolist-service';

var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

class Rules extends Component {
	constructor(props) {
		super(props)

		this.CONST = {
			CNY_USD: 0.1464
		}

		this.state = {
			languagelist: {},
			Description: {},
			Service_fees: 3
		};

		languageService.language();
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			info: nextProps.book
		});

		this.getHouseInfoById()
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
			info: this.props.book
		});
	}

	getHouseInfoById() {
		houselistingService.getHouseInfoById(this.props.book.listingId)
		.then((data) => {
			var PPSprice = data.description.price_perday
			if(this.props.book.priceActive == 0) {
				this.setState({
					price: PPSprice
				})
			} else {
				ppsService.getethprice().then((data) => {
					var ethprice = Math.round((PPSprice * this.CONST.CNY_USD * (1 / data.ethprice)) * 100000) / 100000;
					this.setState({
						price: ethprice
					})
				})

			}
			this.setState({
				Description: data.description,
				previewImage: data.profile.previewImage
			})
		})
	}

	calcTotalPrice() {
		var TotalPrice = Math.round((this.Service_fees() + this.TotalPrice()) * 100000) / 100000;
		return TotalPrice == 0 ? "0.00" : TotalPrice;
	}

	TotalPrice() {
		return(this.state.price * this.DateDays()) == 0 ? "0.00" : (this.state.price * this.DateDays());
	}

	DateDays() {
		var checkInDate = moment(new Date(Number(this.props.book.checkInDate)));
		var checkOutDate = moment(new Date(Number(this.props.book.checkOutDate)));
		if(checkInDate && checkOutDate) {
			let days = checkOutDate.diff(checkInDate, 'days');
			return days
		}
		return 0
	}

	Service_fees() {
		var Service_fees = this.state.Service_fees * 0.01;

		if(this.props.book.priceActive == 0) {
			return parseInt(this.TotalPrice() * Service_fees);
		} else {
			return Math.round((this.TotalPrice() * Service_fees) * 100000) / 100000;
		}

	}

	render() {

		const language = this.state.languagelist;

		return(
			<div>
          		<div className="title row" >
	          		<div className="Room col-sm-8 col-md-8 col-lg-8">
	          			<h2>{this.state.Description.roombasics_guestbedroom}{language.zhang}{language.bedrooms} + {this.state.Description.roombasics_guestbathroom}{language.jian}{language.bathrooms} {this.state.Description.roomstuff_wifi ==  1 ? "+"+language.Free_Pocket_WiFi : ""}</h2>
	          			<p>{language.Categorys[this.state.Description.roomdescription_guests_have]}/{this.state.Description.roomstuff_City}</p>
	          			<div className="divxx">
				            <img src="../images/detail-xx01.png" alt="" />
				            <img src="../images/detail-xx01.png" alt="" />
				            <img src="../images/detail-xx01.png" alt="" />
				            <img src="../images/detail-xx01.png" alt="" />
				            <img src="../images/detail-xx01.png" alt="" />
				            <span>200 {language.Reviews}</span> 
				        </div>
	          		</div>
	          		<div className="col-sm-4 col-md-4 col-lg-4">
	          			<img src={this.state.previewImage} />
	          		</div>
          		</div>

          		<div className="time" >
          			<div>
          				<img src="../images/userIcon.png" />
          				<p>{this.state.info.guest} {language.guests}</p>
          			</div>
          			<div>
          				<img src="../images/step3_8img2.png" />
          				<p>{language.language == "zh" ? moment(this.props.book.checkInDate).locale("zh-cn").format('ll') : moment(this.props.book.checkInDate).format('ll')}  →   {language.language == "zh" ? moment(this.props.book.checkOutDate).locale("zh-cn").format('ll') : moment(this.props.book.checkOutDate).format('ll')}</p>
          			</div>
          		</div>

          		<div className="Price" >
					<div>
						<p className="Left" >{this.props.book.currency}:{this.state.price} x {this.DateDays()} {language.nights}</p>
						<p className="Right" >{this.props.book.currency}:{this.TotalPrice()}</p>
						<span></span>
					</div>
					<div>
						<Fees type="Cleaning_fees"  />
						<p className="Right" >0</p>
						<span></span>
					</div>
					<div>
						<Fees type="Service_fees"  />
						<p className="Right" >{this.Service_fees()}</p>
						<span></span>   
					</div>
					<div>
						<p className="Left" >{language.Total}</p>
						<p className="Right" >{this.props.book.currency}:{this.calcTotalPrice()}</p>
						<span></span>
					</div>
          		</div>

          		<div className="Price hide" >
          			<div>
						<p className="Left" >Due now (PPS)</p>
						<p className="Right" >$220.68</p>
						<span></span>
					</div>
					<div>
						<p className="Left" >Due on Oct 13, 2018</p>
						<p className="Right" >$220.67</p>
						<span></span>
					</div>
          		</div>

          		<div className="Cancellation row" >
                <Booking listingId={this.props.book.listingId} type="BOOK" />
          		</div>

            </div>
		)
	}
}


export default Rules