import React, { Component } from 'react';
import languageService from '../../services/language-service';
import { Map, Marker } from 'react-amap';
import houselistingService from '../../services/houseinfolist-service';

class Detail_Map extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			neighbourhoodurl: '../images/detail-content-map.png',
			position: {},
		};

		languageService.language();
	}

	componentWillMount() {

		this.setState({
			languagelist: window.languagelist,
		});

		houselistingService.getHouseInfoById(this.props.listingId)
			.then((data) => {
				if(data.description.lng || data.description.lat) {
					this.setState({
						position: {
							longitude: data.description.lng,
							latitude: data.description.lat
						}
					})
				} else {
					this.setState({
						position: {
							longitude: data.description.position.lng,
							latitude: data.description.position.lat
						}
					})
				}
			})

	}

	neighbourhood(e) {
		var DataIndex = e.currentTarget.getAttribute('data-index');
		if(DataIndex == 1) {
			this.setState({
				state: this.state.neighbourhoodurl = "../images/detail-transport.jpg"
			})
		} else if(DataIndex == 2) {
			this.setState({
				state: this.state.neighbourhoodurl = "../images/detail-res.jpg"
			})
		} else if(DataIndex == 3) {
			this.setState({
				state: this.state.neighbourhoodurl = "../images/detail-pps.jpg"
			})
		} else if(DataIndex == 4) {
			this.setState({
				state: this.state.neighbourhoodurl = "../images/detail-shop.jpg"
			})
		} else if(DataIndex == 5) {
			this.setState({
				state: this.state.neighbourhoodurl = "../images/detail-museum.jpg"
			})
		} else if(DataIndex == 6) {
			this.setState({
				state: this.state.neighbourhoodurl = "../images/detail-guide.jpg"
			})
		} else {
			this.setState({
				state: this.state.neighbourhoodurl = "../images/detail-content-map.png"
			})
		}
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="neighbourhood">
            <p>{language.See_the_neighbourhood}</p>
            <div className="DetailMap">
            	<Map 
                  center={this.state.position} 
                  zoom={6}
                >
                    <Marker 
                        position={this.state.position}
                    >
                        <div className="Mapicon" >
                            <img src="../images/Map.png" />
                        </div> 
                    </Marker>
                </Map>
            </div>
            <ul>
                <li onClick={(e)=>this.neighbourhood(e)} data-index='1'><img src="../images/transport.png" alt="" /> {language.Public_Transit}</li>
                <li onClick={(e)=>this.neighbourhood(e)} data-index='2'><img src="../images/res.png" alt="" /> {language.Restaurant}</li>
                <li onClick={(e)=>this.neighbourhood(e)} data-index='3'><img src="../images/pps.png" alt="" /> {language.PPS_Enabled}</li>
                <li onClick={(e)=>this.neighbourhood(e)} data-index='4'><img src="../images/shop.png" alt="" /> {language.Shopping_Center}</li>
                <li onClick={(e)=>this.neighbourhood(e)} data-index='5'><img src="../images/museum.png" alt="" /> {language.Souvenir_Shop}</li>
                <li onClick={(e)=>this.neighbourhood(e)} data-index='6'><img src="../images/guide.png" alt="" />{language.Guide}</li>
            </ul>
        </div>
		)
	}
}

export default Detail_Map