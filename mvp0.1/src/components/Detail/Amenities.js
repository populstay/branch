import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';
import Modal from 'react-modal';

class Amenities extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			descriptioninfo: {},
			Amenities: [],
			modalOpen: false,
		};

		languageService.language();
	}

	componentWillMount() {

		this.setState({
			languagelist: window.languagelist,
		});
		houselistingService.getHouseInfoById(this.props.listingId)
			.then((data) => {
				console.log(data.description)

				var Amenities = [];
				var AmenitiesARR = [];

				Amenities.push(data.description.roomstuff_Shampoo)
				Amenities.push(data.description.roomstuff_Closet_drwers)
				Amenities.push(data.description.roomstuff_TV)
				Amenities.push(data.description.roomstuff_Heat)
				Amenities.push(data.description.roomstuff_aircondition)
				Amenities.push(data.description.roomstuff_breakfastcoffetea)
				Amenities.push(data.description.roomstuff_desk_workspace)
				Amenities.push(data.description.roomstuff_fireplace)
				Amenities.push(data.description.roomstuff_iron)
				Amenities.push(data.description.roomstuff_hairdryer)
				Amenities.push(data.description.roomstuff_private_entrance)
				Amenities.push(data.description.roomstuff_Essentials)
				Amenities.push(data.description.roomstuff_wifi)
				Amenities.push(data.description.roomstuff_smoke_detector)
				Amenities.push(data.description.roomstuff_Pool)
				Amenities.push(data.description.roomstuff_kitchen)
				Amenities.push(data.description.roomstuff_washer)
				Amenities.push(data.description.roomstuff_dryer)
				Amenities.push(data.description.roomstuff_Park)
				Amenities.push(data.description.roomstuff_Lift)
				Amenities.push(data.description.roomstuff_HotTub)
				Amenities.push(data.description.roomstuff_Gym)
				Amenities.push(data.description.roomstuff_petsinhouse)

				for(var i = 0; i < Amenities.length; i++) {
					if(Amenities[i] == 1) {
						AmenitiesARR.push(Amenities[i])
					}
				}
				this.setState({
					Amenities: AmenitiesARR,
					descriptioninfo: data.description
				})
			})

	}

	render() {

		const language = this.state.languagelist;

		return(
			<div>
            {this.props.type=="book" &&
              <div>
              { 
                this.state.descriptioninfo.roomstuff_Essentials==1 &&
                <p>{language.Essentials}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_Shampoo==1 &&
                <p>{language.Shampoo}</p>
              }  

              { 
                this.state.descriptioninfo.roomstuff_Closet_drwers==1 &&
                <p>{language.Closet_drawers}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_TV==1 &&
                <p>{language.TV}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_Heat==1 &&
                <p>{language.Heat}</p>
              }

              { 
                this.state.descriptioninfo.roomstuff_aircondition==1 &&
                <p>{language.Air_conditioning}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_breakfastcoffetea==1 &&
                <p>{language.Breakfast_coffe_tea}</p>
              }  

              { 
                this.state.descriptioninfo.roomstuff_desk_workspace==1 &&
                <p>{language.Desk_workspace}</p>
              }  

              { 
                this.state.descriptioninfo.roomstuff_fireplace==1 &&
                <p>{language.Fireplace}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_iron==1 &&
                <p>{language.Iron}</p>
              }  

              { 
                this.state.descriptioninfo.roomstuff_hairdryer==1 &&
                <p>{language.Hair_dryer}</p>
              }  

              { 
                this.state.descriptioninfo.roomstuff_private_entrance==1 &&
                <p>{language.Private_entrance}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_wifi==1 &&
                <p>{language.WIFI}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_smoke_detector==1 &&
                <p>{language.Smoke_detector}</p>
              }  

              { 
                this.state.descriptioninfo.roomstuff_Pool==1 &&
                <p>{language.Pool}</p>
              }  

              { 
                this.state.descriptioninfo.roomstuff_kitchen==1 &&
                <p>{language.Kitchen}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_washer==1 &&
                <p>{language.Laundry_washer}</p>
              }  

              { 
                this.state.descriptioninfo.roomstuff_dryer==1 &&
                <p>{language.Laundry_dryer}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_Park==1 &&
                <p>{language.Park}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_Lift==1 &&
                <p>{language.Lift}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_HotTub==1 &&
                <p>{language.Hot_tub}</p>
              }  
              
              { 
                this.state.descriptioninfo.roomstuff_Gym==1 &&
                <p>{language.Gym}</p>
              } 

              { 
                this.state.descriptioninfo.roomstuff_petsinhouse==1 &&
                <p>{language.Pets_in_the_house}</p>
              }
            
              </div>
            }
            {this.props.type=="detail" &&
              <div>
                {this.state.Amenities.length > 0 &&
                  <div className="L_box3">
                    {this.state.Amenities.length > 0 &&
                      <h5>{language.Amenities}</h5>
                    }

                    <div  style={this.state.More_amenities ? {height:"auto"} : {height:"130px"}}>
                    { 
                      this.state.descriptioninfo.roomstuff_Essentials==1 &&
                      <p><img src="../images/Amenities/towel.png" alt="" />{language.Essentials}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_Shampoo==1 &&
                      <p><img src="../images/Amenities/shampoo.png" alt="" />{language.Shampoo}</p>
                    }  

                    { 
                      this.state.descriptioninfo.roomstuff_Closet_drwers==1 &&
                      <p><img src="../images/Amenities/drawer.png" alt="" />{language.Closet_drawers}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_TV==1 &&
                      <p><img src="../images/Amenities/TV.png" alt="" />{language.TV}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_Heat==1 &&
                      <p><img src="../images/Amenities/heat.png" alt="" />{language.Heat}</p>
                    }

                    { 
                      this.state.descriptioninfo.roomstuff_aircondition==1 &&
                      <p><img src="../images/Amenities/air_conditioner.png" alt="" />{language.Air_conditioning}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_breakfastcoffetea==1 &&
                      <p><img src="../images/Amenities/coffee_tea.png" alt="" />{language.Breakfast_coffe_tea}</p>
                    }  

                    { 
                      this.state.descriptioninfo.roomstuff_desk_workspace==1 &&
                      <p><img src="../images/Amenities/desk.png" alt="" />{language.Desk_workspace}</p>
                    }  

                    { 
                      this.state.descriptioninfo.roomstuff_fireplace==1 &&
                      <p><img src="../images/Amenities/fireplace.png" alt="" />{language.Fireplace}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_iron==1 &&
                      <p><img src="../images/Amenities/iron.png" alt="" />{language.Iron}</p>
                    }  

                    { 
                      this.state.descriptioninfo.roomstuff_hairdryer==1 &&
                      <p><img src="../images/Amenities/hair_dryer.png" alt="" />{language.Hair_dryer}</p>
                    }  

                    { 
                      this.state.descriptioninfo.roomstuff_private_entrance==1 &&
                      <p><img src="../images/Amenities/private_entrance.png" alt="" />{language.Private_entrance}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_wifi==1 &&
                      <p><img src="../images/Amenities/wifi.png" alt="" />{language.WIFI}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_smoke_detector==1 &&
                      <p><img src="../images/Amenities/smoke_detector.png" alt="" />{language.Smoke_detector}</p>
                    }  

                    { 
                      this.state.descriptioninfo.roomstuff_Pool==1 &&
                      <p><img src="../images/Amenities/pool.png" alt="" />{language.Pool}</p>
                    }  

                    { 
                      this.state.descriptioninfo.roomstuff_kitchen==1 &&
                      <p><img src="../images/Amenities/kitchen.png" alt="" />{language.Kitchen}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_washer==1 &&
                      <p><img src="../images/Amenities/laundry_washer.png" alt="" />{language.Laundry_washer}</p>
                    }  

                    { 
                      this.state.descriptioninfo.roomstuff_dryer==1 &&
                      <p><img src="../images/Amenities/laundry_dryer.png" alt="" />{language.Laundry_dryer}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_Park==1 &&
                      <p><img src="../images/Amenities/park.png" alt="" />{language.Park}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_Lift==1 &&
                      <p><img src="../images/Amenities/lift.png" alt="" />{language.Lift}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_HotTub==1 &&
                      <p><img src="../images/Amenities/hot tub.png" alt="" />{language.Hot_tub}</p>
                    }  
                    
                    { 
                      this.state.descriptioninfo.roomstuff_Gym==1 &&
                      <p><img src="../images/Amenities/gym.png" alt="" />{language.Gym}</p>
                    } 

                    { 
                      this.state.descriptioninfo.roomstuff_petsinhouse==1 &&
                      <p><img src="../images/Amenities/gym.png" alt="" />{language.Pets_in_the_house}</p>
                    }  
                    </div>
                  </div>
                }
                <p className={this.state.Amenities.length<6 ? "hide More" : "show More"} onClick={(e)=>this.setState({modalOpen:true})}>{language.More_amenities}</p>
                <Modal isOpen={this.state.modalOpen} onAfterOpen={this.afterOpenModal} onRequestClose={(e)=>this.setState({modalOpen:false})}  contentLabel="Wallet Message">
                  <div className="Amenities">
                  <img  onClick={(e)=>this.setState({modalOpen:false})} className="close" src="../images/closezi.png" />
                      <h2>{language.Amenities}</h2>

                      { 
                        this.state.descriptioninfo.roomstuff_Essentials==1 &&
                        <p><img src="../images/Amenities/towel.png" alt="" />{language.Essentials}</p>
                      }

                      { 
                        this.state.descriptioninfo.roomstuff_Shampoo==1 &&
                        <p><img src="../images/Amenities/shampoo.png" alt="" />{language.Shampoo}</p>
                      }  

                      { 
                        this.state.descriptioninfo.roomstuff_Closet_drwers==1 &&
                        <p><img src="../images/Amenities/drawer.png" alt="" />{language.Closet_drawers}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_TV==1 &&
                        <p><img src="../images/Amenities/TV.png" alt="" />{language.TV}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_Heat==1 &&
                        <p><img src="../images/Amenities/heat.png" alt="" />{language.Heat}</p>
                      }

                      { 
                        this.state.descriptioninfo.roomstuff_aircondition==1 &&
                        <p><img src="../images/Amenities/air_conditioner.png" alt="" />{language.Air_conditioning}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_breakfastcoffetea==1 &&
                        <p><img src="../images/Amenities/coffee_tea.png" alt="" />{language.Breakfast_coffe_tea}</p>
                      }  

                      { 
                        this.state.descriptioninfo.roomstuff_desk_workspace==1 &&
                        <p><img src="../images/Amenities/desk.png" alt="" />{language.Desk_workspace}</p>
                      }  

                      { 
                        this.state.descriptioninfo.roomstuff_fireplace==1 &&
                        <p><img src="../images/Amenities/fireplace.png" alt="" />{language.Fireplace}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_iron==1 &&
                        <p><img src="../images/Amenities/iron.png" alt="" />{language.Iron}</p>
                      }  

                      { 
                        this.state.descriptioninfo.roomstuff_hairdryer==1 &&
                        <p><img src="../images/Amenities/hair_dryer.png" alt="" />{language.Hair_dryer}</p>
                      }  

                      { 
                        this.state.descriptioninfo.roomstuff_private_entrance==1 &&
                        <p><img src="../images/Amenities/private_entrance.png" alt="" />{language.Private_entrance}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_wifi==1 &&
                        <p><img src="../images/Amenities/wifi.png" alt="" />{language.WIFI}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_smoke_detector==1 &&
                        <p><img src="../images/Amenities/smoke_detector.png" alt="" />{language.Smoke_detector}</p>
                      }  

                      { 
                        this.state.descriptioninfo.roomstuff_Pool==1 &&
                        <p><img src="../images/Amenities/pool.png" alt="" />{language.Pool}</p>
                      }  

                      { 
                        this.state.descriptioninfo.roomstuff_kitchen==1 &&
                        <p><img src="../images/Amenities/kitchen.png" alt="" />{language.Kitchen}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_washer==1 &&
                        <p><img src="../images/Amenities/laundry_washer.png" alt="" />{language.Laundry_washer}</p>
                      }  

                      { 
                        this.state.descriptioninfo.roomstuff_dryer==1 &&
                        <p><img src="../images/Amenities/laundry_dryer.png" alt="" />{language.Laundry_dryer}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_Park==1 &&
                        <p><img src="../images/Amenities/park.png" alt="" />{language.Park}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_Lift==1 &&
                        <p><img src="../images/Amenities/lift.png" alt="" />{language.Lift}</p>
                      } 

                      { 
                        this.state.descriptioninfo.roomstuff_HotTub==1 &&
                        <p><img src="../images/Amenities/hot tub.png" alt="" />{language.Hot_tub}</p>
                      }  
                      
                      { 
                        this.state.descriptioninfo.roomstuff_Gym==1 &&
                        <p><img src="../images/Amenities/gym.png" alt="" />{language.Gym}</p>
                      }

                      { 
                        this.state.descriptioninfo.roomstuff_petsinhouse==1 &&
                        <p><img src="../images/Amenities/pet.png" alt="" />{language.Pets_in_the_house}</p>
                      }
                  </div>
                </Modal>
              </div>
            }
        </div>
		)
	}
}

export default Amenities