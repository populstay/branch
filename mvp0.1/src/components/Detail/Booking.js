import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';
import Modal from 'react-modal';

class Booking extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			Cancellations: 0,
			modalOpen: false,
		};

		languageService.language();
	}

	componentWillMount() {

    this.setState({
      languagelist: window.languagelist,
    });
    
    houselistingService.getHouseInfoById(this.props.listingId)
      .then((data) => {
        console.log(data)
				this.setState({
					Cancellations: data.description.Cancellations
				})
			})

	}

	render() {

		const language = this.state.languagelist;

		return(
			<div>
            {this.props.type == "detail" && 
              <div className="L_box6">
                <h5>{language.Cancellations}</h5>
                {this.state.Cancellations == 0 &&
                  <p>
                    {language.Flexible} <br/><br/>
                    {language.Full_refund_within_limited_period}  <a target="_blank" href="/Cancellations?Cancellations=0"><span>{language.Read_more}</span></a>
                  </p>
                }

                {this.state.Cancellations == 1 &&
                  <p>
                    {language.Moderate} <br/><br/>
                    {language.Full_refund_within_limited_period}  <a target="_blank" href="/Cancellations?Cancellations=1"><span>{language.Read_more}</span></a>
                  </p>
                }

                {this.state.Cancellations == 2 &&
                  <p>
                    {language.Strict} <br/><br/>
                    {language.Full_refund_if_cancellation_is_within_48_hours_of_booking}  <a target="_blank" href="/Cancellations?Cancellations=2"><span>{language.Read_more}</span></a>
                  </p>
                }
              </div>
            }

            {this.props.type == "BOOK" && 
              <div>
                {this.state.Cancellations == 0 &&
                  <div className="Room col-sm-8 col-md-8 col-lg-8">
                    <h2>{language.Flexible}</h2>
                    <p>{language.Full_refund_within_limited_period}<a target="_blank" href="/Cancellations?Cancellations=0"><span  className="color-pink"> {language.More_details} </span></a></p>
                  </div>
                }

                {this.state.Cancellations == 1 &&
                  <div className="Room col-sm-8 col-md-8 col-lg-8">
                    <h2>{language.Moderate}</h2>
                    <p>{language.Full_refund_within_limited_period}<a target="_blank" href="/Cancellations?Cancellations=1"><span  className="color-pink"> {language.More_details} </span></a></p>
                  </div>
                }

                {this.state.Cancellations == 2 &&
                  <div className="Room col-sm-8 col-md-8 col-lg-8">
                    <h2>{language.Strict}</h2>
                    <p>{language.Full_refund_if_cancellation_is_within_48_hours_of_booking}<a target="_blank" href="/Cancellations?Cancellations=2"><span  className="color-pink"> {language.More_details} </span></a></p>
                  </div>
                }
                
                <div className="col-sm-4 col-md-4 col-lg-4">
                  <img src="../images/step3_4img4.png" />
                </div>
              </div>
            }

            {this.props.type == "Confirm_and_Pay" && 

              <div>
                {this.state.Cancellations == 0 &&
                  <div>
                      <h3>{language.Cancellation_policy}: {language.Flexible} - {language.Full_refund_within_limited_period}</h3>
                      <p>{language.Confirm_Flexible}</p>

                      <p>{language.I_agree_to_the} <a className="color-pink"  target="_blank"  href={`/listing/${this.props.listingId}`} >{language.House_Rules}</a>, <a className="color-pink" target="_blank" href="/Cancellations" >{language.Cancellation_policy}</a>{language.and_to_the} <span  >{language.Guest_Refund_Policy}</span>, {language.Ialso_agree_to_pay_the_total_amount_shown_which_includes_Service_Fees} </p>
                  </div>
                }

                {this.state.Cancellations == 1 &&
                  <div>
                      <h3>{language.Cancellation_policy}: {language.Moderate} - {language.Full_refund_within_limited_period}</h3>
                      <p>{language.Confirm_Moderate}</p>

                      <p>{language.I_agree_to_the} <a className="color-pink"  target="_blank"  href={`/listing/${this.props.listingId}`} >{language.House_Rules}</a>, <a className="color-pink" target="_blank" href="/Cancellations" >{language.Cancellation_policy}</a>{language.and_to_the} <span  >{language.Guest_Refund_Policy}</span>, {language.Ialso_agree_to_pay_the_total_amount_shown_which_includes_Service_Fees} </p>
                  </div>
                }

                {this.state.Cancellations == 2 &&
                  <div>
                      <h3>{language.Cancellation_policy}: {language.Strict} - {language.Full_refund_if_cancellation_is_within_48_hours_of_booking}</h3>
                      <p>{language.Confirm_Strict}</p>

                      <p>{language.I_agree_to_the} <a className="color-pink"  target="_blank"  href={`/listing/${this.props.listingId}`} >{language.House_Rules}</a>, <a className="color-pink" target="_blank" href="/Cancellations" >{language.Cancellation_policy}</a>{language.and_to_the} <span  >{language.Guest_Refund_Policy}</span>, {language.Ialso_agree_to_pay_the_total_amount_shown_which_includes_Service_Fees} </p>
                  </div>
                }
              </div>
            }

       </div>
		)
	}
}

export default Booking