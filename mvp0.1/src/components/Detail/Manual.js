import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';
import Modal from 'react-modal';

class Manual extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			descriptioninfo: {},
			Manual: [],
			modalOpen: false,
		};

		languageService.language();
	}

	componentWillMount() {

		this.setState({
			languagelist: window.languagelist,
		});

		houselistingService.getHouseInfoById(this.props.listingId)
			.then((data) => {

				var Manual = [];
				var ManualARR = [];

				Manual.push(data.description.climb_stairs)
				Manual.push(data.description.Potential_noise)
				Manual.push(data.description.property_Pet)
				Manual.push(data.description.property_parking)
				Manual.push(data.description.shared_spaces)
				Manual.push(data.description.Amenity_limitations)
				Manual.push(data.description.property_recording)
				Manual.push(data.description.property_Weapons)
				Manual.push(data.description.property_animals)

				for(var i = 0; i < Manual.length; i++) {
					if(Manual[i] == 1) {
						ManualARR.push(Manual[i])
					}
				}
				this.setState({
					Manual: ManualARR,
					descriptioninfo: data.description
				})
			})

	}

	render() {

		const language = this.state.languagelist;

		return(
			<div>
          {this.props.type == "confrim" &&
            <div>
                <div className="Show_all_Manual" style={this.state.Show_all_Manual ? {height:"auto"} : {height:"135px"}} >
                    <p className="fontWeight">{language.You_also_acknowledge}:</p>

                    { 
                      this.state.descriptioninfo.climb_stairs==1 &&
                      <p>{language.Must_climb_stairs}</p>
                    } 
                    { 
                      this.state.descriptioninfo.Potential_noise==1 &&
                      <p>{language.Potential_for_noise}</p>
                    } 
                    { 
                      this.state.descriptioninfo.property_Pet==1 &&
                      <p>{language.Pets_live_on_property}</p>
                    } 
                    { 
                      this.state.descriptioninfo.property_parking==1 &&
                      <p>{language.No_parking_on_property}</p>
                    } 
                    { 
                      this.state.descriptioninfo.shared_spaces==1 &&
                      <p>{language.Some_spaces_are_shared}</p>
                    } 
                    { 
                      this.state.descriptioninfo.Amenity_limitations==1 &&
                      <p>{language.Amenity_limitations}</p>
                    } 
                    { 
                      this.state.descriptioninfo.property_recording==1 &&
                      <p>{language.D_Survellance_or_recording_devices_on_property}</p>
                    } 
                    { 
                      this.state.descriptioninfo.property_Weapons==1 &&
                      <p>{language.Weapons_on_property}</p>
                    } 
                    { 
                      this.state.descriptioninfo.property_animals==1 &&
                      <p>{language.Dangerous_animals_on_property}</p>
                    }  

                </div>
                {this.state.Manual.length>3 &&
                  <span className="color-pink" onClick={(e)=>{if(this.state.Show_all_Manual)this.setState({Show_all_Manual:false}); else this.setState({Show_all_Manual:true});}}>+{this.state.Show_all_Manual ? language.back : language.more }</span>
                }
            </div>
          }
          {this.props.type == "book" &&
            <div className="Show_all_Manual">
                { 
                  this.state.descriptioninfo.climb_stairs==1 &&
                  <p>{language.Must_climb_stairs}</p>
                } 
                { 
                  this.state.descriptioninfo.Potential_noise==1 &&
                  <p>{language.Potential_for_noise}</p>
                } 
                { 
                  this.state.descriptioninfo.property_Pet==1 &&
                  <p>{language.Pets_live_on_property}</p>
                } 
                { 
                  this.state.descriptioninfo.property_parking==1 &&
                  <p>{language.No_parking_on_property}</p>
                } 
                { 
                  this.state.descriptioninfo.shared_spaces==1 &&
                  <p>{language.Some_spaces_are_shared}</p>
                } 
                { 
                  this.state.descriptioninfo.Amenity_limitations==1 &&
                  <p>{language.Amenity_limitations}</p>
                } 
                { 
                  this.state.descriptioninfo.property_recording==1 &&
                  <p>{language.D_Survellance_or_recording_devices_on_property}</p>
                } 
                { 
                  this.state.descriptioninfo.property_Weapons==1 &&
                  <p>{language.Weapons_on_property}</p>
                } 
                { 
                  this.state.descriptioninfo.property_animals==1 &&
                  <p>{language.Dangerous_animals_on_property}</p>
                }  

            </div>
          }
        </div>
		)
	}
}

export default Manual