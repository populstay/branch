import 'react-dates/initialize';
import '../../css/react_dates.css';
import { Link } from 'react-router-dom';
import { DateRangePicker } from 'react-dates';
import React, { Component } from 'react';
import houselistingService from '../../services/houseinfolist-service';
import ppsService from '../../services/pps-service';
import ipfsService from '../../services/ipfs-service';
import Carousel from 'nuka-carousel';
import Overlay from '../overlay';
import web3Service from '../../services/web3-service';
import guestService from '../../services/guest-service';
import EthereumQRPlugin from 'ethereum-qr-code';
import Video from './video';
import languageService from '../../services/language-service';
import GuestRegister from '../RegisterLogin/guest-register';
import Reviews from './Reviews';
import DetailMap from './DetailMap';
import Amenities from './Amenities';
import Rules from './Rules';
import Booking from './Booking';
import SleepingArrangements from './SleepingArrangements';
import Description from './Description';
import Fees from './Fees';
import Modal from 'react-modal';
import Slider from '../Other/Slider';

const qr = new EthereumQRPlugin();

class ListingsDetail extends Component {

	constructor(props) {
		super(props)

		this.CONST = {
			weiToEther: 1000000000000000000,
			weiToGwei: 1000000000,
			GweiToEther: 1000000000,
			weiToUSD: 1000000,
			CNY_USD: 0.1464
		}

		this.STEP = {
			VIEW: 1,
			SUBMIT: 2,
			PROCESSING: 3,
			PURCHASED: 4,
			Insufficient: 5,
		}

		this.state = {
			user: "Loading...",
			ppsPrice: 0,
			ethPrice: 0,
			usdPrice: 0,
			Total_price: 0,
			ipfsHash: null,
			pictures: [],
			step: this.STEP.VIEW,
			totalPrice: 0,
			slides: [],
			currentActive: 0,
			descriptioninfo: {},
			guests: [1, 2, 3, 4, 5, 6],
			guest: 1,
			price: 0,
			priceActive: 0,
			priceCurrency: "",
			neighbourhood: 0,
			neighbourhoodlist: [],
			ethBalance: 0,
			ppsBalance: 0,
			usddatalist: [],
			modalIsOpen: false,
			qrurl: "",
			DateLists: [],
			Progress: 0,
			Progresshide: 0,
			languagelist: {},
			clicklogout: false,
			detail: '',
			Reviews: 5,
			Service_fees: 3,
			CurrencyArr: ['PPS', 'ETH', 'USD'],
			bedroom: []
		}
		this.handleBooking = this.handleBooking.bind(this);
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.isStartDayBlocked = this.isStartDayBlocked.bind(this);
		this.isEndDayBlocked = this.isEndDayBlocked.bind(this);

		web3Service.loadWallet();
		languageService.language();
	}

	onLogOut = (value) => {
		this.setState({
			clicklogout: value
		});
	}

	componentWillUnmount() {
		window.location.reload();
	}

	openModal() {
		this.setState({
			modalIsOpen: true
		});
	}

	closeModal() {
		this.setState({
			modalIsOpen: false
		});
	}

	loadListing() {
		var ipfsHash = houselistingService.getIpfsHashFromBytes32(this.props.listingId);
		var slideArray = this.state.slides;

		houselistingService.getHouseInfoDetailFromDB(this.props.listingId).then((data) => {
			this.processLoadHouseInfo(ipfsHash, slideArray, data);
		})
	}

	processLoadHouseInfo = (ipfsHash, slideArray, data) => {

		var CurrencyArr = [];

		var houseInfoDetailPromise;

		if(data.description) {
			this.setState({
				descriptioninfo: data.description,
				bedroom: data.description.bedroom
			});
			this.setState({
				Progress: this.state.Progress + 100
			})
			if(this.state.Progress >= 100) {
				this.timerID = setTimeout(
					() => this.setState({
						Progresshide: 1
					}),
					1000
				);
			}
		}

		if(data.generateSmartContract == 0 || data.generateSmartContract == "0") {

			houseInfoDetailPromise = ipfsService.getListing(ipfsHash);
		} else if(data.generateSmartContract == 1 || data.generateSmartContract == "1") {

			//智能合约价格显示

			var PPSprice = data.price
			if(data.price != 0) {
				CurrencyArr.push({
					Currency: "PPS",
					Currencyprice: data.price,
					CurrencyActive: 0
				})
			}

			ppsService.getethprice().then((data) => {
				var ethprice = Math.round((PPSprice * this.CONST.CNY_USD * (1 / data.ethprice)) * 100000) / 100000;
				CurrencyArr.push({
					Currency: "ETH",
					Currencyprice: ethprice,
					CurrencyActive: 1
				})
			})

			this.setState({
				CurrencyArr: CurrencyArr
			})

		} else {
			console.log("error");
			return;
		}

		this.setState({
			detail: data.profile.previewImage,
			ppsPrice: data.price,
			ethPrice: data.ethprice,
			usdPrice: data.usdprice,
			priceCurrency: CurrencyArr[0].Currency,
			price: CurrencyArr[0].Currencyprice,
			priceActive: CurrencyArr[0].CurrencyActive,
			beds: data.description.roombasics_guestbeds,
			guestbedrooms: data.description.roombasics_guestbedrooms,
			bathroom_private: data.description.bathroom_private,
			advance_book: data.description.advance_book,
		});

		//客人数
		var Guest = [];
		for(var i = 1; i <= data.description.roombasics_guestsnumber; i++) {
			Guest.push(i);
		}
		this.setState({
			guests: Guest
		});

		ipfsService.getListing(ipfsHash).then((result) => {

			for(var i = 0; i < result.selectedPictures.length; i++) {
				var slide = {};
				// slide.imgageUrl = result.selectedPictures[i].imagePreviewUrl;
				slide.original = result.selectedPictures[i].imagePreviewUrl;
				slide.thumbnail = result.selectedPictures[i].imagePreviewUrl;
				slideArray.push(slide);
			}
			this.setState({
				slides: slideArray
			});

		}).catch((error) => {
			console.error(error);
		});
	}

	componentWillMount() {

		this.setState({
			languagelist: window.languagelist
		});

		if(this.props.listingId) {
			this.loadOrdered(this.props.listingId);
			this.loadListing();

		}

		if(window.address) {
			web3Service.getETHBalance(window.address).then((data) => {
				this.setState({
					ethBalance: data / this.CONST.weiToEther
				});
			});
			ppsService.getBalance(window.address).then((data) => {
				this.setState({
					ppsBalance: data
				});
			});
			guestService.getGuesterInfo(window.address).then((data) => {
				this.setState({
					login: true
				});
			});
		}
	}

	isStartDayBlocked(day) {
		var DateLists = this.state.DateLists;
		var currentDate = new Date(this.state.checkInDate).getTime();
		var dayS = new Date(day).getTime();
		var today = new Date().getTime();

		if(DateLists.length == 0) {
			if(this.state.advance_book == 1) {
				//三个月
				if(dayS > today + 7948800000) {
					return dayS;
				}
			} else if(this.state.advance_book == 2) {
				//六个月
				if(dayS > today + 15811200000) {
					return dayS;
				}
			} else if(this.state.advance_book == 3) {
				//九个月
				if(dayS > today + 23673600000) {
					return dayS;
				}
			} else if(this.state.advance_book == 4) {
				//十二个月
				if(dayS > today + 31536000000) {
					return dayS;
				}
			} else if(this.state.advance_book == 5) {
				return dayS
			}
		} else {
			if(this.state.advance_book == 1) {
				//三个月
				for(var i = 0; i < DateLists.length; i++) {
					console.log(DateLists[i])
					if(dayS > DateLists[i].start - 86400000 && dayS < DateLists[i].end || dayS > today + 7948800000) {
						return new Date(dayS);
					}
				}
			} else if(this.state.advance_book == 2) {
				//六个月
				for(var i = 0; i < DateLists.length; i++) {
					if(dayS > DateLists[i].start - 86400000 && dayS < DateLists[i].end || dayS > today + 15811200000) {
						return new Date(dayS);
					}
				}
			} else if(this.state.advance_book == 3) {
				//九个月
				for(var i = 0; i < DateLists.length; i++) {
					if(dayS > DateLists[i].start - 86400000 && dayS < DateLists[i].end || dayS > today + 23673600000) {
						return new Date(dayS);
					}
				}
			} else if(this.state.advance_book == 4) {
				//十二个月
				for(var i = 0; i < DateLists.length; i++) {
					if(dayS > DateLists[i].start - 86400000 && dayS < DateLists[i].end || dayS > today + 31536000000) {
						return new Date(dayS);
					}
				}
			} else if(this.state.advance_book == 5) {
				return dayS
			} else {
				for(var i = 0; i < DateLists.length; i++) {
					if(dayS > DateLists[i].start - 86400000 && dayS < DateLists[i].end) {
						return new Date(dayS);
					}
				}
			}

		}
	}

	isEndDayBlocked(day) {
		var DateLists = this.state.DateLists;
		var currentDate = new Date(this.state.checkInDate).getTime();
		var dayS = new Date(day).getTime();
		var today = new Date().getTime();
		var startdateArr = [];
		var enddateArr = [];
		for(var i = 0; i < DateLists.length; i++) {
			if(dayS > DateLists[i].start && dayS < DateLists[i].end) {
				return new Date(dayS);
			}

			if(currentDate <= DateLists[i].start) {
				startdateArr.push(DateLists[i].start)
			}

			if(currentDate >= DateLists[i].end) {
				enddateArr.push(DateLists[i].end)
			}
		}

		if(this.state.advance_book == 1) {
			//三个月
			startdateArr.push(today + 8035200000)
		} else if(this.state.advance_book == 2) {
			//六个月
			startdateArr.push(today + 15897600000)
		} else if(this.state.advance_book == 3) {
			//九个月
			startdateArr.push(today + 23760000000)
		} else if(this.state.advance_book == 4) {
			//十二个月
			startdateArr.push(today + 31622400000)
		}

		if(dayS > Math.min(...startdateArr)) {
			return true;
		}
		if(dayS < currentDate - 86400000) {
			return true;
		}
	}

	loadOrdered = (id) => {
		houselistingService.getHouseInfoById(id).then((data) => {
			this.setState({
				hostAddress: data.hostAddress
			})
			guestService.getGuesterInfo(data.hostAddress).then((data) => {
				this.setState({
					user: data.user
				});
				if(data.profile) {
					this.setState({
						userPictures: data.profile.profile
					});
				} else {
					this.setState({
						userPictures: "../images/uesr.png"
					});
				}
			});

			if(data.bookedDate != undefined && data.bookedDate.data != undefined) {
				console.log(data.bookedDate)
				this.setState({
					DateLists: data.bookedDate.data
				});
			}
		});
	}

	handleBooking() {

		var url = "/Book?checkInDate=" + this.state.checkInDate + //入住时间  
			"&checkOutDate=" + this.state.checkOutDate + //退房时间         
			"&guest=" + this.state.guest + //客人人数  
			"&currency=" + this.state.priceCurrency + //货币  
			"&priceActive=" + this.state.priceActive + //支付类型
			"&listingId=" + this.props.listingId //房屋id

		if(window.address == this.state.hostAddress) {
			this.setState({
				hostbook: true
			});

			setTimeout(
				() => this.setState({
					hostbook: false
				}),
				2000
			);
		} else {
			window.location.href = url;
		}
	}

	DateDays() {
		if(this.state.checkInDate && this.state.checkOutDate) {
			let days = this.state.checkOutDate.diff(this.state.checkInDate, 'days');
			return days
		}
		return 0
	}

	calcTotalPrice() {
		var TotalPrice = Math.round((this.Service_fees() + this.TotalPrice()) * 100000) / 100000;
		return TotalPrice == 0 ? "0.00" : TotalPrice;
	}

	TotalPrice() {
		return(this.state.price * this.DateDays()) == 0 ? "0.00" : (this.state.price * this.DateDays());
	}

	Service_fees() {
		var Service_fees = this.state.Service_fees * 0.01;

		if(this.state.priceActive == 0) {
			return parseInt(this.TotalPrice() * Service_fees);
		} else {
			return Math.round((this.TotalPrice() * Service_fees) * 100000) / 100000;
		}

	}

	Guests(guest) {
		this.setState({
			guest: guest
		})
	}

	onReviews = (value) => {
		if(value) {
			this.setState({
				Reviews: value
			});
		} else {
			this.setState({
				Reviews: 5
			});
		}
	}

	CurrencyArr = (Currency, CurrencyActive, Currencyprice) => {
		this.setState({
			priceCurrency: Currency,
			priceActive: CurrencyActive,
			price: Currencyprice
		})
	}

	bedroomNUM() {
		var bedroom = this.state.bedroom;
		var bedroomARR = [];
		var bedroomnum = 0;

		for(var i = 0; i < bedroom.length; i++) {
			console.log(bedroom[i])
			for(var j in bedroom[i]) {
				bedroomARR.push(bedroom[i][j])
			}
		}
		for(var i = 0; i < bedroomARR.length; i++) {
			bedroomnum += bedroomARR[i];
		}
		return bedroomnum
	}

	render() {
		const language = this.state.languagelist;
		const price = typeof this.state.ppsPrice === 'string' ? 0 : this.state.ppsPrice;
		const isDayBlocked = this.state.focusedInput === "startDate" ? this.isStartDayBlocked : this.isEndDayBlocked;
		return(

			<div> 

      <div className="banner" onClick={(e)=>this.setState({modalslider:true})}> 
        <img src={this.state.detail} />
      </div>  

      {this.state.hostbook &&
        <Overlay>
          <p className="hostbook" >{language.Prohibit_booking_your_own_house}</p>
        </Overlay>
      }


      <Modal isOpen={this.state.modalslider} onRequestClose={(e)=>this.setState({modalslider:false})} 
        contentLabel="Wallet Message">
        <div className="carousel-slider">
          <span className="slidesClose"  onClick={(e)=>this.setState({modalslider:false})}>×</span>
          {this.state.slides.length != 0 &&
      		<Slider slidesList={this.state.slides} />
          }
          {this.state.slides.length == 0 &&
            <div className="Loading" >
              <img src="../images/logogif.gif" />
              <p>{language.slidesLoading}</p>
            </div>
          }
        </div>
      </Modal>

      <div className="detail-content container">
        <div className={this.state.Progresshide == 1 ? "Progress hide" : "Progress"}><p style={{width:this.state.Progress+"%"}}></p></div>
      <div >
      <div className="col-sm-12 col-md-12 col-lg-7">
        <div className="L_box1 col-sm-8 col-md-9">
          <p className="text1">{language.homeorhotels[this.state.descriptioninfo.roomdescription_homeorhotel]}<span></span>{language.types[this.state.descriptioninfo.roomdescription_type]}</p>
          <p className="text2">{this.state.descriptioninfo.roomdescription_title}</p>
          <div className="box1_list col-lg-9">
            <p><img src="../images/detail-img02.png" alt="" />{this.state.descriptioninfo.roombasics_guestsnumber} {language.wei}{language.guests}</p>
            <p><img src="../images/detail-img01.png" alt="" />{this.state.descriptioninfo.roombasics_guestbedroom} {language.jian}{language.bedrooms}</p>
            <p><img src="../images/detail-img05.png" alt="" />{this.bedroomNUM()} {language.zhang}{language.bed}</p>
            <p><img src="../images/detail-img03.png" alt="" />{this.state.descriptioninfo.roombasics_guestbathroom} {language.ge}{this.state.bathroom_private == 1 ? language.shared_bath : language.private_bath} </p>
          </div>
        </div>

        <div className="L_box2 col-sm-3 col-md-3">
          <div className="BOX__logo">
            <img src={this.state.userPictures} alt="" />
          </div>
          <div className="BOX__user">
            <h4>{this.state.user}</h4>
            <img className="BOX2img hide" src="../images/detail-list.png" alt="" />
          </div>
        </div>

        <Description listingId={this.props.listingId}  />

        <Amenities listingId={this.props.listingId} type="detail"  />

        <SleepingArrangements listingId={this.props.listingId} />

        <Rules listingId={this.props.listingId} type="detail" />

        <Booking listingId={this.props.listingId} type="detail" />

        <Reviews onReviews={this.onReviews} listingId={this.props.listingId} />
        
        <DetailMap listingId={this.props.listingId} />
      </div>
      <div className=" col-sm-12 col-md-12 col-lg-5">
      <div className="detail-summary">
          
          <div className="detail-price-div">
            <div className="priceCurrency">  
              <div className="btn-group col-xs-4 col-sm-4 col-md-4">
                <button type="button" disabled={this.state.CurrencyArr.length == 1 ? "true" : ""} data-toggle="dropdown">{this.state.priceCurrency}<span className={this.state.CurrencyArr.length == 1 ? "hide" : "show"}>▼</span></button>
                <ul className="dropdown-menu" role="menu"> 
                  {this.state.CurrencyArr.map((item,index) => (
                     <li><a onClick={(e)=>this.CurrencyArr(item.Currency,item.CurrencyActive,item.Currencyprice)} >{item.Currency}</a></li>
                    ))
                  }
                </ul>
              </div>

              <div className="col-xs-8 col-sm-8 col-md-8 detailPrice">
                <span className = "detail-price">
                  <b>{this.state.priceCurrency}:</b> {this.state.price == 0 ? this.state.ppsPrice : this.state.price}
                </span>
                <span className = "detail-price-font">{language.Daily_Price}</span>
                <p className="detail-price-xx">
                  {this.state.Reviews > 0 && this.state.Reviews <= 0.5 &&
                    <div className="divxx">
                        <img src="../images/reviews1_5.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 0.5 && this.state.Reviews <= 1 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 1 && this.state.Reviews <= 1.5 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews1_5.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 1.5 && this.state.Reviews <= 2 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 2 && this.state.Reviews <= 2.5 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews1_5.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 2.5 && this.state.Reviews <= 3 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 3 && this.state.Reviews <= 3.5 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews1_5.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 3.5 && this.state.Reviews <= 4 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews1.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 4 && this.state.Reviews <= 4.5 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews1_5.png" alt="" />
                    </div>
                  }
                  {this.state.Reviews > 4.5 && this.state.Reviews <= 5 &&
                    <div className="divxx">
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                        <img src="../images/reviews2.png" alt="" />
                    </div>
                  }
                </p>
              </div>  
              <p className="overflow"></p>
            </div>  
              <div className="details-daterange-div">
              {
                  this.props.listingId &&
                  <DateRangePicker
                    startDate={this.state.checkInDate}
                    startDateId="start_date"
                    endDate={this.state.checkOutDate}
                    startDatePlaceholderText={language.Check_In}
                    endDatePlaceholderText={language.Check_Out}
                    endDateId="end_date"
                    onDatesChange={({ startDate, endDate }) => {this.setState({checkInDate: startDate, checkOutDate: endDate })}}
                    focusedInput={this.state.focusedInput}
                    isDayBlocked={isDayBlocked}
                    onFocusChange={focusedInput => this.setState({ focusedInput })}
                    readOnly
                    numberOfMonths
                    hideKeyboardShortcutsPanel
                    showClearDates 
                  />
              }
              </div>

              <div className="detail-guest-div">
                <p>{language.Guest}</p>
                <div className="btn-group">
                  <button type="button" data-toggle="dropdown" >{this.state.guest} {language.guests}<span>▼</span></button>
                  <ul className="dropdown-menu" role="menu">
                    {this.state.guests.map(guest => (
                      <li><a onClick={this.Guests.bind(this,guest)} >{guest} {language.guests}</a></li>
                    ))}
                  </ul>
                </div>
              </div>

              <div className ="details-totalprice-div">
                <ul>
                    <li className="blueColor">
                      <span className = "LeftSpan"><b className="pricesize">{this.state.priceCurrency} : </b>{this.state.price == 0 ? this.state.ppsPrice : this.state.price}×{this.DateDays()}{language.nights}
                      </span>
                      <span className = "RightSpan">{this.state.priceCurrency}: {this.TotalPrice()}</span>
                      <p className="clearFloat"></p>
                    </li>
                    <li>
                      <Fees type="Service_fees"  />
                      <span className = "RightSpan">0</span>
                      <p className="clearFloat"></p>
                    </li>
                    <li>
                      <Fees type="Cleaning_fees"  />
                      <span className = "RightSpan">{this.Service_fees()}</span>
                      <p className="clearFloat"></p>
                    </li>
                    <li className="blueColor">
                      <span className = "LeftSpan">{language.Total_Price}</span>
                      <span className = "RightSpan">
                        {this.state.priceCurrency}: { this.calcTotalPrice()}
                      </span>
                      <p className="clearFloat"></p>
                    </li>
                </ul>
               
             </div>

             <div className="detail-summary__action">
                 {
                    this.props.listingId && this.state.login == true &&
                    <button
                      className="bg-pink color-blue btn-lg btn-block text-bold text-center"
                      onClick={this.handleBooking}
                      disabled={!this.props.listingId || !this.state.checkInDate || !this.state.checkOutDate}
                      onMouseDown={e => e.preventDefault()}
                      >
                        {language.Book}
                    </button>
                }  
                {
                  !this.state.login &&
                  <GuestRegister type='Book' />
                }  

            <br/>
             <h4 className="text-center">{language.You_wont_be_charged_yet}</h4>
             </div>

        </div>

      

      </div>
      </div>
      </div>
      </div>
 </div>
		)
	}
}

export default ListingsDetail