import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';
import Modal from 'react-modal';

class Rules extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			descriptioninfo: {},
			modalOpen: false,
			AdditionalRules: []
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
		});

		houselistingService.getHouseInfoById(this.props.listingId)
			.then((data) => {
				this.setState({
					descriptioninfo: data.description,
					AdditionalRules: data.description.AdditionalRules
				})
			})

	}

	render() {

		const language = this.state.languagelist;

		return(
			<div>
          {this.props.type=="detail" &&
              <div>
                <div className="L_box5" >
                    <h5>{language.House_Rules}</h5>
                    {this.state.descriptioninfo.anytime_Checkin == 1 &&
                      <p>{language.Check_in_is_anytime_after_12PM}</p>
                    }  
                    <p>{this.state.descriptioninfo.rules_children == 0 ? language.Not_safe_or_suitable_for_children : language.Safe_or_suitable_for_children}</p>
                    <p>{this.state.descriptioninfo.rules_infants == 0 ? language.Not_safe_or_suitable_for_infants : language.Safe_or_suitable_for_infants}</p>
                    <p>{this.state.descriptioninfo.rules_pets == 0 ? language.Not_suitable_for_pets : language.Suitable_for_pets}</p>
                    <p>{this.state.descriptioninfo.rules_Smoking == 0 ? language.No_smoking_allowed : language.Smoking_allowed}</p>
                    <p>{this.state.descriptioninfo.rules_parties == 0 ? language.No_parties_or_events : language.Parties_or_events}</p>
                </div> 
                <p className="More" onClick={(e)=>this.setState({modalOpen:true})}>{language.More_rules}</p>

                <Modal isOpen={this.state.modalOpen} onAfterOpen={this.afterOpenModal} onRequestClose={(e)=>this.setState({modalOpen:false})}  contentLabel="Wallet Message">
                <div className="Rules">
                    <img  onClick={(e)=>this.setState({modalOpen:false})} className="close" src="../images/closezi.png" />
                    <h2>{language.House_Rules}</h2>
                    
                    {this.state.descriptioninfo.anytime_Checkin == 1 &&
                      <p>{language.Check_in_is_anytime_after_12PM}</p>
                    }  
                    <p>{this.state.descriptioninfo.rules_children == 0 ? language.Not_safe_or_suitable_for_children : language.Safe_or_suitable_for_children}</p>
                    <p>{this.state.descriptioninfo.rules_infants == 0 ? language.Not_safe_or_suitable_for_infants : language.Safe_or_suitable_for_infants}</p>
                    <p>{this.state.descriptioninfo.rules_pets == 0 ? language.Not_suitable_for_pets : language.Suitable_for_pets}</p>
                    <p>{this.state.descriptioninfo.rules_Smoking == 0 ? language.No_smoking_allowed : language.Smoking_allowed}</p>
                    <p>{this.state.descriptioninfo.rules_parties == 0 ? language.No_parties_or_events : language.Parties_or_events}</p>
                    <br/>
                    {this.state.AdditionalRules.length != 0 &&
                      <h2>{language.Additional_rules}</h2>
                    }
                    <div className="AdditionalRules">
                    {this.state.AdditionalRules.map((Rules,index) => (
                        <p>{Rules}</p>
                    ))}
                    </div>
                </div>
              </Modal>
              </div>
          }

          {this.props.type=="confrim" &&
            <div>
              {this.state.descriptioninfo.anytime_Checkin == 1 &&
                <p>{language.Check_in_is_anytime_after_12PM}</p>
              }  
              <p>{this.state.descriptioninfo.rules_children == 0 ? language.Not_safe_or_suitable_for_children : language.Safe_or_suitable_for_children}</p>
              <p>{this.state.descriptioninfo.rules_infants == 0 ? language.Not_safe_or_suitable_for_infants : language.Safe_or_suitable_for_infants}</p>
              <p>{this.state.descriptioninfo.rules_pets == 0 ? language.Not_suitable_for_pets : language.Suitable_for_pets}</p>
              <p>{this.state.descriptioninfo.rules_Smoking == 0 ? language.No_smoking_allowed : language.Smoking_allowed}</p>
              <p>{this.state.descriptioninfo.rules_parties == 0 ? language.No_parties_or_events : language.Parties_or_events}</p>
            </div>  
          }
       </div>
		)
	}
}

export default Rules