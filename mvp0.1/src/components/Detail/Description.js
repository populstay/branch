import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';
import Modal from 'react-modal';

class Description extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			Description: "",
			Descriptionlen: 200,
			modalOpen: false,
		};

		languageService.language();
	}

	componentWillMount() {

		this.setState({
			languagelist: window.languagelist,
		});

		houselistingService.getHouseInfoById(this.props.listingId)
			.then((data) => {
				this.setState({
					Description: data.description.roomdescription_description
				})
			})

	}

	render() {

		const language = this.state.languagelist;

		return(
			<div>
            <pre className="L_TEXT1">{this.state.Description} {this.state.Description.length > this.state.Descriptionlen ? "......" : ""}</pre>

            {this.state.Description.length > this.state.Descriptionlen  &&
              <p onClick={(e)=>this.setState({modalOpen:true})} className="More">{language.More_details}</p>
            }

            {this.state.Description.length <= this.state.Descriptionlen  &&
              <p className="borderbottom"></p>
            }

            <Modal isOpen={this.state.modalOpen} onAfterOpen={this.afterOpenModal} onRequestClose={(e)=>this.setState({modalOpen:false})}  contentLabel="Wallet Message">
            <div className="Description">
                <img  onClick={(e)=>this.setState({modalOpen:false})} className="close" src="../images/closezi.png" />
                <h2>{language.More_details}</h2>
                <pre>{this.state.Description}</pre>
            </div>
          </Modal>
       </div>
		)
	}
}

export default Description