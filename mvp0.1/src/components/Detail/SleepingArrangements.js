import React, { Component } from 'react';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import houselistingService from '../../services/houseinfolist-service';
import Modal from 'react-modal';

class SleepingArrangements extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			descriptioninfo: {},
			Amenities: [],
			modalOpen: false,
			bedroom: []
		};

		languageService.language();
	}

	componentWillMount() {

		this.setState({
			languagelist: window.languagelist,
			modalOpen: 100000,
		});

		houselistingService.getHouseInfoById(this.props.listingId)
			.then((data) => {
				this.setState({
					bedroom: data.description.bedroom
				})
			})
	}

	render() {

		const language = this.state.languagelist;

		var list = (length, type) => {
			var res = [];
			for(var i = 0; i < length; i++) {
				if(type == "Add_beds") {
					res.push(
						<img src="../images/SleepingArrangements/single_bed.png" />
					)
				} else if(type == "Air_mattress") {
					res.push(
						<img src="../images/SleepingArrangements/air_bed.png" />
					)
				} else if(type == "Baby_cot") {
					res.push(
						<img src="../images/SleepingArrangements/baby_bed.png" />
					)
				} else if(type == "Childrens_bed") {
					res.push(
						<img src="../images/SleepingArrangements/children_bed.png" />
					)
				} else if(type == "Double_bed") {
					res.push(
						<img src="../images/SleepingArrangements/queen_bed.png" />
					)
				} else if(type == "Hammock") {
					res.push(
						<img src="../images/SleepingArrangements/hammock.png" />
					)
				} else if(type == "Sofa_bed") {
					res.push(
						<img src="../images/SleepingArrangements/sofa_bed.png" />
					)
				} else if(type == "Water_bed") {
					res.push(
						<img src="../images/SleepingArrangements/water_bed.png" />
					)
				}
			}
			return res
		}

		return(
			<div className="L_box4">
            {this.state.bedroom.length == 1 && (this.state.bedroom[0].roombasics_Add_beds != 0 || this.state.bedroom[0].roombasics_Add_beds != 0 || this.state.bedroom[0].roombasics_Air_mattress != 0 || this.state.bedroom[0].roombasics_Baby_cot != 0 || this.state.bedroom[0].roombasics_Childrens_bed != 0 || this.state.bedroom[0].roombasics_Double_bed != 0 || this.state.bedroom[0].roombasics_Hammock != 0 || this.state.bedroom[0].roombasics_Sofa_bed != 0 || this.state.bedroom[0].roombasics_Water_bed != 0) &&
              <h5>{language.Sleeping_arrangements}</h5>
            }

            {this.state.bedroom.length > 1 &&
              <h5>{language.Sleeping_arrangements}</h5>
            }

            <div className="overflow" >
              {this.state.bedroom.map((item,index) => (
                  <div  onClick={(e)=>this.setState({modalOpen:index})} className={item.roombasics_Add_beds != 0 || item.roombasics_Air_mattress != 0 || item.roombasics_Baby_cot != 0 || item.roombasics_Childrens_bed != 0 || item.roombasics_Double_bed != 0 || item.roombasics_Hammock != 0 || item.roombasics_Sofa_bed != 0 || item.roombasics_Water_bed != 0  ? "listbedroom modalshow" : "listbedroom hide" } >
                    <div className="images" >
                      {list(item.roombasics_Add_beds,"Add_beds")}
                      {list(item.roombasics_Air_mattress,"Air_mattress")}
                      {list(item.roombasics_Baby_cot,"Baby_cot")}
                      {list(item.roombasics_Childrens_bed,"Childrens_bed")}
                      {list(item.roombasics_Double_bed,"Double_bed")}
                      {list(item.roombasics_Hammock,"Hammock")}
                      {list(item.roombasics_Sofa_bed,"Sofa_bed")}
                      {list(item.roombasics_Water_bed,"Water_bed")}
                    </div>

                    <h1>{window.languagelist.bedrooms} {index+1}</h1>
                    <div className="pdiv" >
                      {item.roombasics_Add_beds != 0 &&
                        <p>{item.roombasics_Add_beds}&nbsp;{language.zhang}{language.Single_bed}</p>
                      }
                      {item.roombasics_Air_mattress != 0 &&
                        <p>{item.roombasics_Air_mattress}&nbsp;{language.zhang}{language.Air_mattress}</p>
                      }  
                      {item.roombasics_Baby_cot != 0 &&
                        <p>{item.roombasics_Baby_cot}&nbsp;{language.zhang}{language.Baby_cot}</p>
                      }  
                      {item.roombasics_Childrens_bed != 0 &&
                        <p>{item.roombasics_Childrens_bed}&nbsp;{language.zhang}{language.Childrens_bed}</p>
                      }  
                      {item.roombasics_Double_bed != 0 &&
                        <p>{item.roombasics_Double_bed}&nbsp;{language.zhang}{language.Double_bed}</p>
                      }  
                      {item.roombasics_Hammock != 0 &&
                        <p>{item.roombasics_Hammock}&nbsp;{language.zhang}{language.Hammock}</p>
                      }  
                      {item.roombasics_Sofa_bed != 0 &&
                        <p>{item.roombasics_Sofa_bed}&nbsp;{language.zhang}{language.Sofa_bed}</p>
                      }  
                      {item.roombasics_Water_bed != 0 &&
                        <p>{item.roombasics_Water_bed}&nbsp;{language.zhang}{language.Water_bed}</p>
                      }
                    </div>
                    <Modal isOpen={this.state.modalOpen == index} onAfterOpen={this.afterOpenModal} onRequestClose={(e)=>this.setState({modalOpen:100000})}  contentLabel="Wallet Message">
                      <div className="SleepingArrangements">
                        <img  onClick={(e)=>this.setState({modalOpen:100000})} className="close" src="../images/closezi.png" />
                          <h1>{window.languagelist.bedroom} {index+1}</h1>

                         <div className="images" >
                            {list(item.roombasics_Add_beds,"Add_beds")}
                            {list(item.roombasics_Air_mattress,"Air_mattress")}
                            {list(item.roombasics_Baby_cot,"Baby_cot")}
                            {list(item.roombasics_Childrens_bed,"Childrens_bed")}
                            {list(item.roombasics_Double_bed,"Double_bed")}
                            {list(item.roombasics_Hammock,"Hammock")}
                            {list(item.roombasics_Sofa_bed,"Sofa_bed")}
                            {list(item.roombasics_Water_bed,"Water_bed")}
                          </div>

                          
                          <div className="pdiv" >
                            {item.roombasics_Add_beds != 0 &&
                              <p className="col-sm-6">{item.roombasics_Add_beds}&nbsp;{language.zhang}{language.Single_bed}</p>
                            }
                            {item.roombasics_Air_mattress != 0 &&
                              <p className="col-sm-6">{item.roombasics_Air_mattress}&nbsp;{language.zhang}{language.Air_mattress}</p>
                            }  
                            {item.roombasics_Baby_cot != 0 &&
                              <p className="col-sm-6">{item.roombasics_Baby_cot}&nbsp;{language.zhang}{language.Baby_cot}</p>
                            }  
                            {item.roombasics_Childrens_bed != 0 &&
                              <p className="col-sm-6">{item.roombasics_Childrens_bed}&nbsp;{language.zhang}{language.Childrens_bed}</p>
                            }  
                            {item.roombasics_Double_bed != 0 &&
                              <p className="col-sm-6">{item.roombasics_Double_bed}&nbsp;{language.zhang}{language.Double_bed}</p>
                            }  
                            {item.roombasics_Hammock != 0 &&
                              <p className="col-sm-6">{item.roombasics_Hammock}&nbsp;{language.zhang}{language.Hammock}</p>
                            }  
                            {item.roombasics_Sofa_bed != 0 &&
                              <p className="col-sm-6">{item.roombasics_Sofa_bed}&nbsp;{language.zhang}{language.Sofa_bed}</p>
                            }  
                            {item.roombasics_Water_bed != 0 &&
                              <p className="col-sm-6">{item.roombasics_Water_bed}&nbsp;{language.zhang}{language.Water_bed}</p>
                            }
                          </div>
                      </div>
                    </Modal>
                  </div>
              ))}
            </div>
            
            {this.state.bedroom.length == 1 && (this.state.bedroom[0].roombasics_Add_beds != 0 || this.state.bedroom[0].roombasics_Add_beds != 0 || this.state.bedroom[0].roombasics_Air_mattress != 0 || this.state.bedroom[0].roombasics_Baby_cot != 0 || this.state.bedroom[0].roombasics_Childrens_bed != 0 || this.state.bedroom[0].roombasics_Double_bed != 0 || this.state.bedroom[0].roombasics_Hammock != 0 || this.state.bedroom[0].roombasics_Sofa_bed != 0 || this.state.bedroom[0].roombasics_Water_bed != 0) &&
              <p className="More"></p>
            }
            {this.state.bedroom.length > 1 &&
              <p className="More"></p>
            }
        </div>
		)
	}
}

export default SleepingArrangements