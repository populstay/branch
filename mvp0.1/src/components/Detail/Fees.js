import React, { Component } from 'react';
import languageService from '../../services/language-service';

class Fees extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist
		});
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="Fees" >
          {this.props.type == "Service_fees" &&
            <div className = "LeftSpan">{language.Service_fees}
                <div className="Leftprompt">
                  <img src="../images/detail-img13.png" />
                  <div>
                      <h6>{language.Service_fee_Details}</h6>
                      <p></p>
                  </div>
                </div>
            </div>
          }

          {this.props.type == "Cleaning_fees" &&
            <div className = "LeftSpan">{language.Cleaning_fees}
                <div className="Leftprompt">
                  <img src="../images/detail-img13.png" />
                  <div >
                      <h6>{language.Cleaning_fee_Details}</h6>
                      <p></p>
                  </div>
                </div>
            </div>
          }
        </div>
		)
	}
}

export default Fees