import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { DateRangePicker } from 'react-dates';
import houselistingService from '../../services/houseinfolist-service';
import { withRouter } from 'react-router';
import ListingCard from '../Other/listing-card';
import languageService from '../../services/language-service';

class Search extends Component {

	constructor(props) {
		super(props);
		this.state = {
			checkInDate: 0,
			checkOutDate: 0,
			guests: 1,
			location: "Tokyo",
			locationName: '',
			listingRows: [],
			listingsPerPage: 6,
			districtCodes: [],
			curDistrictCodeIndex: 0,
			Progress: 0,
			Progresshide: 0,
			url: "",
			languagelist: {},
			locationNamearr: []
		};
		languageService.language();
	}

	locationName(e) {
		var DataName = e.currentTarget.getAttribute('data-name');
		this.setState({
			state: this.state.locationName = DataName
		});
	}

	componentDidMount() {

		this.setState({
			languagelist: window.languagelist,
			locationName: window.languagelist.CHENGDU,
			locationNamearr: window.languagelist.locationNamearr,
		});

		houselistingService.getDistrictCodes().then((codes) => {

			this.setListingRows(codes);
			this.setState({
				Progress: this.state.Progress + 100
			})
			if(this.state.Progress >= 100) {
				this.timerID = setTimeout(
					() => this.setState({
						Progresshide: 1
					}),
					1000
				);
			}
		});
	}

	setURL = () => {
		var checkOutDate = this.state.checkOutDate;
		var checkInDate = this.state.checkInDate;

		// if(this.state.checkInDate && this.state.checkOutDate )
		// {
		//   checkOutDate = checkOutDate.toDate().getTime();
		//   checkInDate = checkInDate.toDate().getTime();
		// }
		var url = "/home/search?checkInDate=" + checkInDate + "&checkOutDate=" + checkInDate + "&guests=" + this.state.guests + "&location=" + this.state.locationName + "&skip=" + 0 + "&limit=" + 9
		this.setState({
			url: url
		});
		window.location.href = url;
	}

	setListingRows = (codes) => {
		this.setState({
			districtCodes: codes.data
		});
		if(sessionStorage.getItem('houselist')) {
			this.setState({
				listingRows: JSON.parse(sessionStorage.getItem('houselist'))
			});
		} else {
			var uuids = houselistingService.getRecommand(codes.data[0].id,6).then((data) => {
				console.log(data)
				this.setState({
					listingRows: data
				});
				sessionStorage.setItem('houselist', JSON.stringify(data));
			});
		}

	}

	MouseOver = (id) => {
		this.setState({
			maphomeid: id
		})
	}

	render() {
		const language = this.state.languagelist;
		const activePage = this.props.match.params.activePage || 1;
		const showListingsRows = this.state.listingRows.slice(
			this.state.listingsPerPage * (activePage - 1),
			this.state.listingsPerPage * (activePage))

		return(
			<div className="form SearchBox">
        <div className={this.state.Progresshide == 1 ? "Progress hide" : "Progress"}><p style={{width:this.state.Progress+"%"}}></p></div>
        <div className="container index_content">
            <h1>{language.Find_dream_homes_and_experiences_on_PopulStay}</h1>
            <p className="text-bold">{language.PopulStay_Superior_Guest_Experience_Maximized_Owner_Profit}</p>
            <div className="SearchBody" >
              <div>
                <h4>{language.Choose_your_city}</h4>
                <div className="form__location form__location1">
                    <div className="col-xs-6 col-sm-4 col-md-4 col-lg-2 cursor" data-name={language.CHENGDU} onClick={(e)=>this.locationName(e)}>
                        <img src={ this.state.locationName == language.CHENGDU ? "../images/icon/chengdu_red.png" : "../images/icon/chengdu_grey.png"} alt="chengdu" />
                        <p className={this.state.locationName == language.CHENGDU ? "activeiconp" : ""}>
                          <span className={this.state.locationName == language.CHENGDU ? "left activeiconspan" : "left"}></span>
                          {language.CHENGDU}
                          <span className={this.state.locationName == language.CHENGDU ? "right activeiconspan" : "right"}></span>
                        </p>
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-4 col-lg-2 cursor" data-name={language.TOKYO} onClick={(e)=>this.locationName(e)}>
                        <img src={ this.state.locationName == language.TOKYO ? "../images/icon/tokyo_red.png" : "../images/icon/tokyo_grey.png"} alt="tokyo" />
                        <p className={this.state.locationName == language.TOKYO ? "activeiconp" : ""}>
                          <span className={this.state.locationName == language.TOKYO ? "left activeiconspan" : "left"}></span>
                          {language.TOKYO}
                          <span className={this.state.locationName == language.TOKYO ? "right activeiconspan" : "right"}></span>
                        </p>
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-4 col-lg-2 cursor" data-name={language.SINGAPORE} onClick={(e)=>this.locationName(e)}>
                        <img src={ this.state.locationName == language.SINGAPORE ? "../images/icon/singapore_red.png" : "../images/icon/singapore_grey.png"} alt="singapore" />
                        <p className={this.state.locationName == language.SINGAPORE ? "activeiconp" : ""}>
                          <span className={this.state.locationName == language.SINGAPORE ? "left activeiconspan" : "left"}></span>
                          {language.SINGAPORE}
                          <span className={this.state.locationName == language.SINGAPORE ? "right activeiconspan" : "right"}></span>
                        </p>
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-4 col-lg-2" data-name={language.NEW_YORK}>
                        <img src="../images/icon/new_york_disable.png" alt="" />
                        <p className="disable" >
                          <span className="left disablespan"></span>
                          {language.NEW_YORK}
                          <span className="right disablespan"></span>
                        </p>
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-4 col-lg-2" data-name={language.SHANGHAI}>
                        <img src="../images/icon/shanghai_disable.png" alt="" />
                        <p className="disable" >
                          <span className="left disablespan"></span>
                          {language.SHANGHAI}
                          <span className="right disablespan"></span>
                        </p>
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-4 col-lg-2" data-name={language.LONDON}>
                        <img src="../images/icon/london_disable.png"alt="" />
                        <p className="disable" >
                          <span className="left disablespan"></span>
                          {language.LONDON}
                          <span className="right disablespan"></span>
                        </p>
                    </div>
                </div>

                <div className="form__location hide">
                    <div className="col-xs-6 col-sm-4 col-md-2 col-lg-2 cursor" data-name={language.CHENGDU} onClick={(e)=>this.locationName(e)}>
                        <img src={ this.state.locationName == language.CHENGDU ? language.chengdu_red : language.chengdu_grey} alt="" />
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-2 col-lg-2 cursor" data-name={language.TOKYO} onClick={(e)=>this.locationName(e)}>
                        <img src={ this.state.locationName == language.TOKYO ? language.tokyo_red : language.tokyo_grey} alt="" />
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-2 col-lg-2 cursor" data-name={language.SINGAPORE} onClick={(e)=>this.locationName(e)}>
                        <img src={ this.state.locationName == language.SINGAPORE ? language.singapore_red : language.singapore_grey} alt="" />
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-name={language.NEW_YORK}>
                        <img src={language.new_york_disable} alt="" />
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-name={language.SHANGHAI}>
                        <img src={language.shanghai_disable} alt="" />
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-name={language.LONDON}>
                        <img src={language.london_disable} alt="" />
                    </div>
                    <div className="col-xs-6 col-sm-4 col-md-2 col-lg-2 hide" data-name={language.PARIS}>
                        <img src={language.paris_disable} alt="" />
                    </div>
                </div>
                <form action="">
                    <div className="row">
                        <div className="col-md-12 col-lg-12 col-sm-12 ">
                            <div className=" col-sm-12 col-md-8 col-lg-8 index_box">
                                <div className="col-md-12 col-lg-12 col-sm-12 guestsleft">
                                    <div className="form-group">
                                        <label className="col-sm-6 col-md-6 col-lg-6 ">{language.Check_in}</label>
                                        <label className="col-sm-6 col-md-6 col-lg-6 Right">{language.Check_out}</label>
                                        <DateRangePicker 
                                          startDatePlaceholderText={language.start_date}
                                          endDatePlaceholderText={language.end_date}
                                          startDate={this.state.checkInDate} 
                                          startDateId='start date' 
                                          endDate={this.state.checkOutDate} 
                                          endDateId='end date' 
                                          onDatesChange={({ startDate, endDate })=> {this.setState({checkInDate: startDate, checkOutDate: endDate });
                                          window.searchCondition.checkOutDate = endDate;window.searchCondition.checkInDate = startDate;}} 
                                          focusedInput={this.state.focusedInput} 
                                          onFocusChange={focusedInput => this.setState({ focusedInput })} 
                                          readOnly 
                                          numberOfMonths
                                          hideKeyboardShortcutsPanel
                                          showClearDates
                                        />
                                    </div>
                                </div>

                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 guestsnum Left">
                                    <div className="form-group">
                                        <label>{language.Guests}</label>
                                        <div className="guestBtn" >
                                          <span className={this.state.guests == 1 ? "btnjian spanActive  glyphicon glyphicon-chevron-left" : "btnjian  glyphicon glyphicon-chevron-left"} onClick={(e)=>{if(this.state.guests == 1 )this.setState({guests:1});else this.setState({guests:this.state.guests-1})}} data-name="jian"></span>
                                          <button type="button">{this.state.guests}</button>
                                          <span className={this.state.guests == 50 ? "btnjia spanActive glyphicon glyphicon-chevron-right" : "btnjia glyphicon glyphicon glyphicon-chevron-right"} onClick={(e)=>{if(this.state.guests == 50 )this.setState({guests:50});else this.setState({guests:this.state.guests+1})}} data-name="jia"></span>
                                        </div>  
                                        <input type="number" className="form-control input-lg hide" value={this.state.guests} onChange={(e) => {if(e.target.value <= 0 )this.setState({guests:1});else this.setState({guests:e.target.value});}}/>
                                    </div>
                                </div>
                          

                                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 guestsnum Right">
                                    <div className="form-group">
                                        <label>{language.location}</label>
                                        <div className="btn-group">
                                          <button type="button" data-toggle="dropdown">{this.state.locationName}<span>▼</span></button>
                                          <ul className="dropdown-menu" role="menu">
                                            {this.state.locationNamearr.map((item,index) => (
                                                <li><a onClick={(e)=>this.setState({locationName:item})} >{item}</a></li>
                                              ))
                                            }
                                          </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="search  col-sm-12  col-md-3  col-lg-3">
                                <a onClick={this.setURL} href="#" className="btn button__fill btn-lg form__search">
                                    <img src={language.search_home} />
                                </a>
                            </div>
                        </div>
                        <div className="col-lg-12 index_foot">
                            <ul>
                                <li className="liimg"><img src="../images/img_home.png" /></li>
                                <li className="litext">{language.Find_dream}</li>
                                <li className="liicon">•</li>
                                <a href="/all"><li className="litext1">{language.HOMES}</li></a>
                                <li className="liicon">•</li>
                                <a><li className="litext1">{language.EXPERIENCE}</li></a>
                            </ul>
                        </div>
                    </div>
                </form>
              </div>  
              <p className="SearchBottom" ></p>
            </div>  
        </div>

        <div className="container index_home">
            <h2>{language.You_may_also_like}</h2>
            <div className="overflow">
                  {showListingsRows.map(row => (
                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 listing-card">
                    <ListingCard row={row} MouseOver={this.MouseOver} />
                    </div>
                  ))}
            </div>
            <div className={showListingsRows.length == 0 ? "show divLoad" : 'hide divLoad'}>
              <img src="images/loader.gif" />
            </div>
            <Link to="/all">
            <h4>{language.Show_all}</h4>
            </Link>
        </div>
        <div className="container index_home">
            <h2>{language.Stay_tuned}</h2>
        </div>
    </div>

		)
	}
}

export default withRouter(Search)