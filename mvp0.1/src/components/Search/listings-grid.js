import React, { Component } from 'react';
import houselistingService from '../../services/houseinfolist-service';
import Pagination from 'react-js-pagination';
import { withRouter } from 'react-router';
import ListingCard from '../Other/listing-card';
import languageService from '../../services/language-service';
import AMap from '../Other/AMap';
import Filter from '../Other/Filter';
import moment from 'moment';

class ListingsGrid extends Component {

	constructor(props, context) {
		super(props);
		this.state = {
			listingRows: [],
			listingsPerPage: 9,
			listingRowsLen: 0,
			districtCodes: [],
			curDistrictCodeIndex: 0,
			Progress: 0,
			Progresshide: 0,
			languagelist: {},
			limit: 9,
			url: "",
			filterhide: false,
			Citys_type: "",
		};

		languageService.language();
	}
	componentWillMount() {
		this.setState({
			languagelist: window.languagelist
		});

		this.handlePageChange = this.handlePageChange.bind(this);

		var href = window.location.href;

		var params = href.split("?")[1];
		var paramArr = params.split('&');
		var res = {};
		for(var i = 0; i < paramArr.length; i++) {
			var str = paramArr[i].split('=');
			res[str[0]] = decodeURI(str[1]);
		}

		this.setState({
			checkInDate: res.checkInDate ? res.checkInDate : 0,
			checkOutDate: res.checkInDate ? res.checkOutDate : 0,
			guests: res.guests ? res.guests : 1,
			Home_Type: res.Home_Type ? res.Home_Type : "",
			Pricemin: res.Pricemin ? res.Pricemin : 0,
			Pricemax: res.Pricemax ? res.Pricemax : 5000,
			place: res.location ? res.location : "",
			token: res.token ? res.token : 0,
			skip: res.skip ? res.skip : 0,
		});

		houselistingService.getDistrictCodes().then((codes) => {
			this.setListingRows(this.state.checkInDate, this.state.checkOutDate, this.state.guests, this.state.place, this.state.token, this.state.Home_Type, this.state.Pricemin, this.state.Pricemax, this.state.skip, this.state.limit);
		});
	}

	setListingRows = (checkInDate, checkOutDate, guests, place, token, Home_Type, Pricemin, Pricemax, skip, limit) => {

		houselistingService.getHouseId(checkInDate, checkOutDate, guests, place, token, Home_Type, Pricemin, Pricemax, skip, limit).then((data) => {
				if(data.length != 0) {
					this.setState({
						listingRows: data,
						alllength: false
					});
				} else {
					this.setState({
						listingRows: data,
						alllength: true
					});
				}
			})
			.catch((error) => {})
	}

	handlePageChange(pageNumber) {
		this.setState({
			pageNumber: pageNumber
		})
		pageNumber = (pageNumber - 1) * 9;
		var url = "/home/search?checkInDate=" + this.state.checkInDate + "&checkOutDate=" + this.state.checkOutDate + "&guests=" + this.state.guests + "&location=" + this.state.place + "&skip=" + pageNumber + "&limit=" + this.state.limit
		window.location.reload();
	}

	//鼠标移入地图高亮
	MouseOver = (id) => {
		this.setState({
			maphomeid: id
		})
	}

	filterbj = (value) => {
		this.setState({
			filterbj: value
		})
	}

	render() {
		const activePage = this.state.skip / 9 + 1;
		const showListingsRows = this.state.listingRows

		const position = {
			longitude: 120,
			latitude: 32
		}
		const language = this.state.languagelist;

		return(

			<div className="listings-grid">
        {!this.state.FilterFilter &&
          <Filter filterbj={this.filterbj} filterhide={this.state.filterbj} />
        }
        <div className={this.state.Progresshide == 1 ? "Progress hide" : "Progress"}><p style={{width:this.state.Progress+"%"}}></p></div>
            <div className="grid">
              <div className={this.state.filterbj ? "filterbj show" : "filterbj hide"} onClick={(e)=>this.setState({filterbj:false})} ></div>
              <h1>{language.Homes_around_the_world}</h1>
              {this.state.listingRows.length != 0 &&
                <div className="row">
                  <div className="col-md-8 col-lg-8">
                    <div className="row">          
                      {showListingsRows.map(row => (
                        <div className="col-sm-6 col-md-6 col-lg-4 listing-card">
                        <ListingCard row={row} MouseOver={this.MouseOver} />
                        </div>
                      ))}
                     </div>
                     <Pagination
                      activePage={activePage}
                      itemsCountPerPage={this.state.listingsPerPage}
                      totalItemsCount={this.state.listingRowsLen}
                      onChange={this.handlePageChange}
                      itemClass="page-item"
                      linkClass="page-link"
                      hideDisabled="true"
                    />
                  </div>
                  <div className="col-md-4 col-lg-4">
                    <AMap listingRows={this.state.listingRows} Maphomeid={this.state.maphomeid} />
                  </div>
                </div>
              }
              {
                this.state.listingRows.length == 0 && !this.state.alllength &&
                <div className="No_result">
                  <img src="/images/loader.gif" />
                </div>
              }

              {
                this.state.listingRows.length == 0 && this.state.alllength &&
                <div className="No_result">
                  <h1 className="text-center color-pink">{language.No_result}</h1>
                </div>
              }
            </div>
      </div>

		)
	}
}

export default withRouter(ListingsGrid)