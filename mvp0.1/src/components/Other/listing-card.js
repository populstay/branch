import React, { Component } from 'react';
import houselistingService from '../../services/houseinfolist-service'
import ipfsService from '../../services/ipfs-service'
import { Link } from 'react-router-dom'
import languageService from '../../services/language-service';

class ListingCard extends Component {

	constructor(props) {
		super(props)
		this.state = {
			category: "Loading...",
			name: "Loading...",
			price: "Loading...",
			lister: null,
			descriptioninfo: {},
			previewurl: "",
			Progress: 0,
			Progresshide: 0,
			languagelist: {},
		}
		languageService.language();
	}

	componentDidMount() {
		this.setState({
			languagelist: window.languagelist
		});

		var descriptioninfo;

		// houselistingService.getHouseInfoDetail(this.props.row)
		// .then((result) => {
		//     var roominfo = JSON.parse(result[4]);
		//     this.setState({price:result[0],category:roominfo.category,location:roominfo.location,beds:roominfo.beds});
		//     return 

		//var ipfsHash = houselistingService.getIpfsHashFromBytes32(this.props.row.id);

		var roominfo = this.props.row.houseinfo;
		this.setState({
			price: this.props.row.price,
			ethprice: this.props.row.ethprice,
			usdprice: this.props.row.usdprice,
			category: roominfo.category,
			location: this.props.row.place,
			beds: roominfo.beds,
			category: roominfo.category,
			title: this.props.row.description.roomdescription_title,
			roomdescription_guests_have: window.languagelist.Categorys[this.props.row.description.roomdescription_guests_have]
		});

		if(this.props.row.id) {
			houselistingService
				.getHouseInfoById(this.props.row.id)
				.then((res) => {
					this.setState({
						previewurl: res.profile.previewImage
					});
				});
		}

	}

	MouseOver(id) {
		this.props.MouseOver(id)
	}

	render() {
		const language = this.state.languagelist;
		return(
			<a target="_blank" href={`/listing/${this.props.row.id}`} onMouseOver={(e)=>this.MouseOver(this.props.row.id)} onMouseOut={(e)=>this.MouseOver(0)}>
          <div className="photo" style={this.state.previewurl == '' ? {background:"#fafafa"}:{backgroundImage:"url("+this.state.previewurl+")"}}>
          <img className={this.state.previewurl == '' ? 'show' : 'hide'} src="/images/loader.gif" />
          </div>
          <div className="title">{this.state.location}-{this.state.roomdescription_guests_have}</div>
          <div className="category">{this.state.title ? this.state.title : "-"}</div>
          <div className="price">
              <p>{this.state.price} PPS {language.per_night}</p>
          </div>
          <div className="divxx">
            <img src="../images/detail-xx01.png" alt="" />
            <img src="../images/detail-xx01.png" alt="" />
            <img src="../images/detail-xx01.png" alt="" />
            <img src="../images/detail-xx01.png" alt="" />
            <img src="../images/detail-xx01.png" alt="" />
            <span>200</span> 
          </div>
        </a>
		)
	}
}
export default ListingCard