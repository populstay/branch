import React, { Component } from 'react';
import languageService from '../../services/language-service';
import houselistingService from '../../services/houseinfolist-service';

class PPSRecharge extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checkInDate: 0,
			checkOutDate: 0,
			Adult: 1,
			children: 0,
			Baby: 0,
			Citys: ["TOKYO", "CHENGDU", "NEW YORK", "SHANGHAI", "LONDON", "SINGAPORE"],
			language: ['English', 'Français', 'Deutsch', '日本語', 'Italiano', 'Русский', 'Español', '中文', 'العربية', 'Hindi', 'Português', 'Türkçe', 'Bahasa Indonesia', 'Nederlands', '한국어', 'Bengali', 'ภาษาไทย', 'Punjabi', 'Ελληνικά', 'Sign Language', 'עברית', 'Polski', 'Bahasa Malaysia', 'Tagalog', 'Dansk', 'Svenska', 'Norsk', 'Suomi', 'Čeština', 'Magyar', 'українська'],
			Facilities: ['Kitchen', 'Shampoo', 'Heating', 'Air conditioner', 'Washing machine', 'Dryer', 'Wireless network', 'breakfast', 'Indoor fireplace', 'Doorbell / building interphone', 'Guard', 'Coat hanger', 'Iron', 'Hair drier', 'Desk / work area', 'Television', 'Baby bed', 'High foot chair for children', 'Check in', 'smoke detector', 'Carbon Monoxide Alarm'],
			Facilities1: ['Free parking space', 'Gym', 'Massage bathtub', 'Swimming Pool'],
			Source_type: ['A complete set of single house', 'Apartment type residence', 'Breakfast and Breakfast', 'The Inn Boutique', 'Loft', 'Village hut', 'Villa', 'Guest Room', 'Guest suite', 'Log cabin', 'Bungalow', 'Holiday wooden house', 'Resort', 'Hostel', 'Villas', 'Hotel'],
			Characteristic: ['Agritainment', 'Primary residence acupoint', 'Cuban family hotel', 'Castle', 'Tent', 'Miniature house', 'Tree House', 'Train', 'Natural Hostel', 'Ship', 'A ship’s house', 'Thatched cottage', 'Camping area', 'Camping car / RV'],
			Rules: ['Suitable for hosting activities', 'Allowed to carry a pet', 'Allow smoking'],
			Code_house: ['Suitable for hosting activities', 'Allowed to carry a pet', 'Allow smoking'],
			Citys_type: 'City',
			Home_Type: '',
			PriceAdd: 0,
			PriceDele: 0,
			Pricemin: 0,
			Pricemax: 10000,
			Price: 'Price',
			listingRows: [],
			listingsPerPage: 20,
			districtCodes: [],
			curDistrictCodeIndex: 0,
			experienceList: 1,
			listingtype: 0,
			languagelist: {},

		};
		languageService.language();

	}

	componentDidMount() {

	}

	render() {

		const language = this.state.languagelist;

		return(
			<Overlay>
              <div className="userqrurl">
                <h4>{language.Use_the_token_wallet_to_scan_and_recharge}</h4>
                <img className="photo" src={this.state.qrurl}  />
                <p className="address">{window.address}</p>
                <button onClick={(e)=>this.setState({userqrurl:false})}>{language.Cancel}</button>
              </div>
            </Overlay>
		)
	}
}

export default PPSRecharge