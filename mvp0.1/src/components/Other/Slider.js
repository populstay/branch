import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ImageGallery from 'react-image-gallery';
import "../../css/image-gallery.css";

class slidesList extends Component {

	constructor(props) {
		super(props)
		this.state = {
			images:{}
		}
	}

	componentWillMount(){
		this.onPause()
	}

	render() {
	
		return(

			<div>
		       <ImageGallery items={this.props.slidesList} showIndex />
			</div>
			
		)
	}
}







export default slidesList