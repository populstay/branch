import React, { Component } from 'react';
import languageService from '../../services/language-service';
import InputRange from 'react-input-range';
import { DateRangePicker } from 'react-dates';
import houselistingService from '../../services/houseinfolist-service';
import moment from 'moment';

class Filter extends Component {
	constructor(props) {
		super(props);
		this.state = {
			checkInDate: 0,
			checkOutDate: 0,
			focusedInput: true,
			guests: 0,
			language: ['English', 'Français', 'Deutsch', '日本語', 'Italiano', 'Русский', 'Español', '中文', 'العربية', 'Hindi', 'Português', 'Türkçe', 'Bahasa Indonesia', 'Nederlands', '한국어', 'Bengali', 'ภาษาไทย', 'Punjabi', 'Ελληνικά', 'Sign Language', 'עברית', 'Polski', 'Bahasa Malaysia', 'Tagalog', 'Dansk', 'Svenska', 'Norsk', 'Suomi', 'Čeština', 'Magyar', 'українська'],
			Facilities: ['Kitchen', 'Shampoo', 'Heating', 'Air conditioner', 'Washing machine', 'Dryer', 'Wireless network', 'breakfast', 'Indoor fireplace', 'Doorbell / building interphone', 'Guard', 'Coat hanger', 'Iron', 'Hair drier', 'Desk / work area', 'Television', 'Baby bed', 'High foot chair for children', 'Check in', 'smoke detector', 'Carbon Monoxide Alarm'],
			Facilities1: ['Free parking space', 'Gym', 'Massage bathtub', 'Swimming Pool'],
			Source_type: ['A complete set of single house', 'Apartment type residence', 'Breakfast and Breakfast', 'The Inn Boutique', 'Loft', 'Village hut', 'Villa', 'Guest Room', 'Guest suite', 'Log cabin', 'Bungalow', 'Holiday wooden house', 'Resort', 'Hostel', 'Villas', 'Hotel'],
			Characteristic: ['Agritainment', 'Primary residence acupoint', 'Cuban family hotel', 'Castle', 'Tent', 'Miniature house', 'Tree House', 'Train', 'Natural Hostel', 'Ship', 'A ship’s house', 'Thatched cottage', 'Camping area', 'Camping car / RV'],
			Rules: ['Suitable for hosting activities', 'Allowed to carry a pet', 'Allow smoking'],
			Code_house: ['Suitable for hosting activities', 'Allowed to carry a pet', 'Allow smoking'],
			Home_Type: '',
			PriceAdd: 0,
			PriceDele: 0,
			Pricemin: 0,
			Pricemax: 5000,
			Price: 'Price',
			listingRows: [],
			listingsPerPage: 20,
			districtCodes: [],
			curDistrictCodeIndex: 0,
			experienceList: 1,
			listingtype: 0,
			languagelist: {},
		};
		languageService.language();
		this.isStartDayBlocked = this.isStartDayBlocked.bind(this);
		this.isEndDayBlocked = this.isEndDayBlocked.bind(this);

	}

	componentDidMount() {
		var href = window.location.href;

		var params = href.split("?")[1];
		var paramArr = params.split('&');
		var res = {};
		for(var i = 0; i < paramArr.length; i++) {
			var str = paramArr[i].split('=');
			res[str[0]] = decodeURI(str[1]);
		}

		this.setState({
			res: res
		})

		this.setState({
			checkInDate: res.checkInDate != 0 && res.checkInDate ? moment(new Date(Number(res.checkInDate))) : 0,
			checkOutDate: res.checkOutDate != 0 && res.checkOutDate ? moment(new Date(Number(res.checkOutDate))) : 0,
			guests: res.guests ? res.guests : 1,
			Citys_type: res.location ? res.location : window.languagelist.City,
			Strainer_token: res.token ? 1 : 0,
			Home_Type: res.Home_Type ? res.Home_Type : window.languagelist.Home_Type,
			Pricemin: res.Pricemin ? res.Pricemin : 0,
			Pricemax: res.Pricemax ? res.Pricemax : 5000,
			Price: res.Pricemin && res.Pricemin != 0 || res.Pricemax && res.Pricemax != 5000 ? "PPS " + res.Pricemin + " - PPS " + res.Pricemax : window.languagelist.Price,
			languagelist: window.languagelist,
		});
	}

	componentWillReceiveProps(nextProps) {
		if(!nextProps.filterhide) {
			this.setState({
				Strainer_City: false,
				Strainer_Time: false,
				Strainer_Guests: false,
				Strainer_Home_Type: false,
				Strainer_Price: false,
				Strainer_More: false
			})
		}
	}

	Strainer(e) {
		var Strainer = e.currentTarget.getAttribute('data-Strainer');
		if(Strainer == "Strainer_City") {
			this.setState({
				Strainer_City: true,
				Strainer_Time: false,
				Strainer_Guests: false,
				Strainer_Home_Type: false,
				Strainer_Price: false,
				Strainer_More: false
			})
			this.props.filterbj(true)
		}

		if(Strainer == "Strainer_Time") {
			this.setState({
				Strainer_City: false,
				Strainer_Time: true,
				Strainer_Guests: false,
				Strainer_Home_Type: false,
				Strainer_Price: false,
				Strainer_More: false
			})
			this.setState({
				focusedInput: true
			})
		}

		if(Strainer == "Strainer_Guests") {
			this.setState({
				Strainer_City: false,
				Strainer_Time: false,
				Strainer_Guests: true,
				Strainer_Home_Type: false,
				Strainer_Price: false,
				Strainer_More: false
			})
			this.props.filterbj(true)
		}

		if(Strainer == "Strainer_Home_Type") {
			this.setState({
				Strainer_City: false,
				Strainer_Time: false,
				Strainer_Guests: false,
				Strainer_Home_Type: true,
				Strainer_Price: false,
				Strainer_More: false
			})
			this.props.filterbj(true)
		}
		if(Strainer == "Strainer_Price") {
			this.setState({
				Strainer_City: false,
				Strainer_Time: false,
				Strainer_Guests: false,
				Strainer_Home_Type: false,
				Strainer_Price: true,
				Strainer_More: false
			})
			this.props.filterbj(true)
		}
		if(Strainer == "Strainer_More") {
			this.setState({
				Strainer_City: false,
				Strainer_Time: false,
				Strainer_Guests: false,
				Strainer_Home_Type: false,
				Strainer_Price: false,
				Strainer_More: true
			})
		}

	}

	TagSelect(e) {
		if(e) {
			var Strainer = e.currentTarget.getAttribute('data-Strainer');
		}

		this.taghide();

		if(this.state.Citys_type == this.state.languagelist.City || this.state.Citys_type == undefined) {
			var location = ""
		} else {
			var location = "&location=" + this.state.Citys_type
		}

		if(this.state.checkInDate == 0 && this.state.checkOutDate == 0) {
			var checkInDate = ""
			var checkOutDate = ""
		} else {
			var checkInDate = "&checkInDate=" + this.state.checkInDate
			var checkOutDate = "&checkOutDate=" + this.state.checkOutDate
		}

		if(Strainer == "Strainer_token") {
			if(this.state.res.token) {
				var token = ""
			} else {
				var token = "&token=1"
			}
		} else {
			if(this.state.res.token) {
				var token = "&token=1"
			} else {
				var token = ""
			}
		}

		if(this.state.Home_Type == this.state.languagelist.Home_Type) {
			var Home_Type = ""
		} else {
			var Home_Type = "&Home_Type=" + this.state.Home_Type
		}

		if(this.state.Pricemin == 0 && this.state.Pricemax == 5000) {
			var Pricemin = "",
				Pricemax = "";
		} else {
			var Pricemin = "&Pricemin=" + this.state.Pricemin;
			var Pricemax = "&Pricemax=" + this.state.Pricemax;
		}

		var url = "/home/search?" + checkInDate + checkOutDate + "&guests=" + this.state.guests + location + token + Home_Type + Pricemin + Pricemax + "&skip=0" + "&limit=9"
		window.location.href = url;

	}

	TagRemove(e) {
		this.taghide();
		var Strainer = e.currentTarget.getAttribute('data-Strainer');

		if(Strainer == "Strainer_City") {
			var locationtype = ""
		} else {
			if(this.state.Citys_type == this.state.City) {
				var locationtype = ""
			} else {
				var locationtype = "&location=" + this.state.Citys_type
			}
		}

		if(Strainer == "Strainer_Time") {
			var checkInDate = ""
			var checkOutDate = ""
		} else {
			var checkInDate = "checkInDate=" + this.state.checkInDate
			var checkOutDate = "&checkOutDate=" + this.state.checkOutDate
		}

		if(Strainer == "Strainer_Guests") {
			var gueststype = ""
		} else {
			var gueststype = "&guests=" + this.state.guests
		}

		if(this.state.res.token) {
			var token = "&token=1"
		} else {
			var token = ""
		}

		if(Strainer == "Strainer_Home_Type") {
			var Home_Type = ""
		} else {
			var Home_Type = "&Home_Type=" + this.state.Home_Type
		}

		if(Strainer == "Strainer_Price") {
			var Pricemin = "",
				Pricemax = "";
		} else {
			var Pricemin = "&Pricemin=" + this.state.Pricemin;
			var Pricemax = "&Pricemax=" + this.state.Pricemax;
		}

		var url = "/home/search?" + checkInDate + checkOutDate + gueststype + locationtype + Home_Type + Pricemin + Pricemax + token + "&skip=0" + "&limit=9"
		window.location.href = url;

	}

	taghide = () => {
		this.setState({
			Strainer_City: false,
			Strainer_Time: false,
			Strainer_Guests: false,
			Strainer_token: false,
			Strainer_Home_Type: false,
			Strainer_Price: false,
			Strainer_More: false
		})
	}

	/*城市搜索*/
	keykeypress(e) {
		if(e.which !== 13) return
		this.TagSelect(e);
	}

	isStartDayBlocked(day) {
		this.setState({
			checkOutDate: 0
		});
	}

	isEndDayBlocked(day) {
		if(this.state.checkInDate != 0 && this.state.checkOutDate != 0) {
			// this.TagSelect();
		}
	}

	render() {

		const isDayBlocked = this.state.focusedInput === "startDate" ? this.isStartDayBlocked : this.isEndDayBlocked;
		const language = this.state.languagelist;

		return(
			<div className="Filter">
              <ul className="tag container">
              <li className="tag__item hide"><a><img src="../../images/Experience.png" alt=""/><span>{language.Experience}</span></a></li>

              <li className={this.state.Citys_type != language.City ? "tag__item active" : "tag__item"}><span className="location-tag Strainerspan" data-Strainer="Strainer_City" onClick={(e)=>this.Strainer(e)}>{this.state.Citys_type}</span>
                  <div className={this.state.Strainer_City ? "Strainer_City show" : "Strainer_City hide"}>
                      <div className="Strainer_Home_Type">
                          <input data-Strainer="Strainer_City" onKeyPress={this.keykeypress.bind(this)} type="text" placeholder={language.Enter_the_city_you_want_to_go_to} onChange={(e) => this.setState({Citys_type:e.target.value=="" ? language.City:e.target.value})} value={this.state.Citys_type == language.City ? "" : this.state.Citys_type} />
                          <h6><span>{language.eg} &nbsp;</span> {language.chengdu_jingdu_shanghai}...</h6>
                      </div>
                      <div className="operation">
                          <p className="confirm Left" data-Strainer="Strainer_City" onClick={(e)=>this.TagRemove(e)}>{language.Clear}</p>
                          <p className="Reset Right" data-Strainer="Strainer_City" onClick={(e)=>this.TagSelect(e)}>{language.Confirm}</p>
                      </div>
                  </div>
              </li>

              <li className={this.state.checkInDate != 0 || this.state.checkOutDate != 0 ? "tag__item active" : "tag__item"}>

                <span className="Strainerspan Strainer_Time" data-Strainer="Strainer_Time" >
                    {this.state.checkInDate != 0 && this.state.checkOutDate != 0 &&
                      <b className="glyphicon glyphicon-search" data-Strainer="Strainer_Time" onClick={(e)=>this.TagSelect(e)}></b>
                    }
                    <DateRangePicker 
                      startDatePlaceholderText={language.start_date}
                      endDatePlaceholderText={language.end_date}
                      startDate={this.state.checkInDate} 
                      startDateId='start date' 
                      endDate={this.state.checkOutDate} 
                      endDateId='end date' 
                      onDatesChange={({ startDate, endDate })=> {this.setState({checkInDate: startDate, checkOutDate: endDate });}} 
                      focusedInput={this.state.focusedInput} 
                      isDayBlocked={isDayBlocked}
                      onFocusChange={focusedInput => {this.setState({ focusedInput })}} 
                      readOnly 
                      numberOfMonths
                      hideKeyboardShortcutsPanel
                    />
                    {this.state.checkInDate != 0 && this.state.checkOutDate != 0 &&
                      <b className="glyphicon glyphicon-remove" data-Strainer="Strainer_Time" onClick={(e)=>{if(this.state.res.checkInDate)this.TagRemove(e); else this.setState({checkInDate:0,checkOutDate:0});}}></b>
                    }
                </span>
              </li>

              <li className={this.state.guests > 1 ? "tag__item active" : "tag__item"}><span className="Strainerspan" data-Strainer="Strainer_Guests" onClick={(e)=>this.Strainer(e)}>{this.state.guests} {language.Guests}</span>
                  <div className={this.state.Strainer_Guests ? "Strainer_Guests show" : "Strainer_Guests hide"}>
                      <ul>
                          <li>
                            <p className="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-left">{language.Guests}</p>
                            <p className="col-xs-7 col-sm-7 col-md-7 col-lg-7 text-right">
                            <span className="Left" onClick={(e)=>this.setState({guests:this.state.guests > 1 ? this.state.guests-1 : this.state.guests})}>◀</span>
                            <span className="text">{this.state.guests}</span><span className="Right" onClick={(e)=>this.setState({guests:this.state.guests >= 16 ? 16 : this.state.guests-0+1})}>▶</span>
                            </p>
                          </li>
                      </ul>
                      <div className="operation">
                          <p className="confirm Left" data-Strainer="Strainer_Guests" onClick={(e)=>this.TagRemove(e)}>{language.Clear}</p>
                          <p className="Reset Right" data-Strainer="Strainer_Guests" onClick={(e)=>this.TagSelect(e)}>{language.Confirm}</p>
                      </div>
                  </div>
              </li>

              <li className={this.state.Strainer_token == 1 ? "tag__item active" : "tag__item"}><img src="../../images/pps_white.png" alt=""/> <span data-Strainer="Strainer_token" onClick={(e)=>{this.TagSelect(e); }}>{language.Support_PPS_token}</span>
              </li>

              <li className={this.state.Home_Type != language.Home_Type ? "tag__item active" : "tag__item"}><span className="Strainerspan" data-Strainer="Strainer_Home_Type" onClick={(e)=>this.Strainer(e)}>{this.state.Home_Type == language.Home_Type ? language.Home_Type : window.languagelist.Categorys[this.state.Home_Type]}</span>
                  <div className={this.state.Strainer_Home_Type ? "Strainer_Home_Type show" : "Strainer_Home_Type hide"}>
                      <div  className="checkbox" onClick={(e) => this.setState({Home_Type:0})}>
                        <p className="Pinput" >
                          <img className={this.state.Home_Type == 0 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                        </p>
                        <div className="divinput">
                          <p>{language.Whole_house}</p>
                          <p className="text" >{language.Enjoy_the_entire_listing}</p>
                        </div>
                      </div>
                      <div  className="checkbox" onClick={(e) => this.setState({Home_Type:1})}>
                        <p className="Pinput" >
                          <img className={this.state.Home_Type == 1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                        </p>
                        <div className="divinput">
                          <p>{language.Private_Room}</p>
                          <p className="text">{language.Have_your_own_separate_room_and_share_some_public_space}</p>
                        </div>
                      </div>
                      <div  className="checkbox" onClick={(e) => this.setState({Home_Type:2})}>
                        <p className="Pinput" >
                          <img className={this.state.Home_Type == 2 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                        </p>
                        <div className="divinput">
                          <p>{language.Share_Room}</p>
                          <p className="text">{language.A_joint_space_such_as_a_public_Lounge}</p>
                        </div>
                      </div>
                      <div className="operation">
                          <p className="confirm Left" data-Strainer="Strainer_Home_Type" onClick={(e)=>this.TagRemove(e)}>{language.Clear}</p>
                          <p className="Reset Right" data-Strainer="Strainer_Home_Type" onClick={(e)=>this.TagSelect(e)}>{language.Confirm}</p>
                      </div>
                  </div>
              </li>

              <li className={this.state.Pricemin != 0 || this.state.Pricemax != 5000 ? "tag__item active" : "tag__item"}><span className="Strainerspan" data-Strainer="Strainer_Price" onClick={(e)=>this.Strainer(e)}>{this.state.Price}</span>
                  <div className={this.state.Strainer_Price ? "Strainer_Price show" : "Strainer_Price hide"}>
                      <p className="text1"><span>PPS</span>{this.state.Pricemin} &nbsp;&nbsp;-&nbsp;&nbsp;<span>PPS</span>{this.state.Pricemax}</p>
                      <InputRange maxValue={5000} minValue={0} value={{min: this.state.Pricemin, max: this.state.Pricemax}} onChange={value=>this.setState({ Pricemin : value.min,Pricemax : value.max })} />
                      <div className="operation">
                          <p className="confirm Left" data-Strainer="Strainer_Price" onClick={(e)=>this.TagRemove(e)}>{language.Clear}</p>
                          <p className="Reset Right" data-Strainer="Strainer_Price" onClick={(e)=>this.TagSelect(e)}>{language.Confirm}</p>
                      </div>
                  </div>
              </li>

              <li className={this.state.Strainer_More ? "tag__item active hide" : "tag__item hide"}><span className="Strainerspan" data-Strainer="Strainer_More" onClick={(e)=>this.Strainer(e)}>{language.More_Fiters}</span>
                  <div className={this.state.Strainer_More ? "Strainer_More  show" : "Strainer_More hide"}>
                  <p>{language.Stay_tuned}</p>
                      <div className="Bedroom">
                          <h6>Bedroom and bed</h6>
                          <ul>
                              <li><p className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left">Number of beds</p><p className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"><span className="Left" onClick={(e)=>this.setState({guests:this.state.guests > 1 ? this.state.guests-1 : this.state.guests})}>◀</span><span className="text">{this.state.guests}</span><span className="Right" onClick={(e)=>this.setState({guests:this.state.guests >= 16 ? 16 : this.state.guests+1})}>▶</span></p></li>
                              <li><p className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left text1">Bedroom</p><p className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"><span className="Left" onClick={(e)=>this.setState({children:this.state.children > 0 ? this.state.children-1 : this.state.children})}>◀</span><span className="text">{this.state.children}</span><span className="Right" onClick={(e)=>this.setState({children:this.state.children >= 5 ? 5 : this.state.children+1})}>▶</span></p></li>
                              <li><p className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left text1">TOILET</p><p className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"><span className="Left" onClick={(e)=>this.setState({Baby:this.state.Baby > 0 ? this.state.Baby-1 : this.state.Baby})}>◀</span><span className="text">{this.state.Baby}</span><span className="Right" onClick={(e)=>this.setState({Baby:this.state.Baby >= 5 ? 5 : this.state.Baby+1})}>▶</span></p></li>
                          </ul>
                      </div>
                      <div className="Flashover options">
                          <div>
                            <h6>Flashover</h6>
                            <p>No need to wait for a reply from the landlord to make a reservation</p>
                          </div>
                          <p className="optionsclose optionsActive"><span className="optionsActivespan"><img src="../images/registerlist_dui.png" /></span></p>
                      </div>
                      <div className="options">
                          <h6>More options</h6>
                          <div>
                              <p>Great landlord</p>
                              <p>Renting a landlord to an approved landlord</p>
                              <p className="Understand textpink">Understand more</p>
                              <p>Wheelchair Accessible</p>
                              <p>Find the room that meets your flexible needs.</p>
                              <p className="Understand textpink">Choice of barrier free demand</p>
                              <p>Free cancellation of reservations</p>
                              <p>Show the free cancellation of the reservation within 48 hours after the reservation</p>
                              <p className="textpink">Understand more</p>
                          </div>
                          <p className="optionsclose optionsActive"><span className="optionsActivespan"><img src="../images/registerlist_dui.png" /></span></p>
                          <p className="optionsclose optionsclose1 optionsActive"><span className="optionsActivespan"><img src="../images/registerlist_dui.png" /></span></p>
                      </div>

                      <div className="Strainer_Home_Type language">
                          <h6>Facilities</h6>
                          <div>
                            {this.state.Facilities.map((item,index) => (
                                <div  className="checkbox col-lg-6" onClick={(e) => {if(this.state.roomstuff_Essentials ==0 )this.setState({roomstuff_Essentials:1});else this.setState({roomstuff_Essentials:0});}}>
                                  <p className="Pinput" >
                                    <img className={this.state.roomstuff_Essentials ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                                  </p>
                                  <div className="divinput">
                                    <p>{item}</p>
                                  </div>
                                </div>
                              ))
                            }
                          </div>
                          <p className="textpink">Display all the facilities</p>
                      </div>

                      <div className="Strainer_Home_Type language">
                          <h6>Facilities</h6>
                          <div>
                            {this.state.Facilities1.map((item,index) => (
                                <div  className="checkbox col-lg-6" onClick={(e) => {if(this.state.roomstuff_Essentials ==0 )this.setState({roomstuff_Essentials:1});else this.setState({roomstuff_Essentials:0});}}>
                                  <p className="Pinput" >
                                    <img className={this.state.roomstuff_Essentials ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                                  </p>
                                  <div className="divinput">
                                    <p>{item}</p>
                                  </div>
                                </div>
                              ))
                            }
                          </div>
                          <p className="textpink">Display all the facilities</p>
                      </div>

                      <div className="Strainer_Home_Type language">
                          <h6>Source type</h6>
                          <div>
                            {this.state.Source_type.map((item,index) => (
                                <div  className="checkbox col-lg-6" onClick={(e) => {if(this.state.roomstuff_Essentials ==0 )this.setState({roomstuff_Essentials:1});else this.setState({roomstuff_Essentials:0});}}>
                                  <p className="Pinput" >
                                    <img className={this.state.roomstuff_Essentials ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                                  </p>
                                  <div className="divinput">
                                    <p>{item}</p>
                                  </div>
                                </div>
                              ))
                            }
                          </div>
                          <p className="textpink">Display all the source types</p>
                      </div>

                      <div className="Strainer_Home_Type language">
                          <h6>Characteristic house</h6>
                          <div>
                            {this.state.Characteristic.map((item,index) => (
                                <div  className="checkbox col-lg-6" onClick={(e) => {if(this.state.roomstuff_Essentials ==0 )this.setState({roomstuff_Essentials:1});else this.setState({roomstuff_Essentials:0});}}>
                                  <p className="Pinput" >
                                    <img className={this.state.roomstuff_Essentials ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                                  </p>
                                  <div className="divinput">
                                    <p>{item}</p>
                                  </div>
                                </div>
                              ))
                            }
                          </div>
                          <p className="textpink">Display all the features of the house</p>
                      </div>

                      <div className="Strainer_Home_Type language">
                          <h6>Code of the house</h6>
                          <div>
                            {this.state.Code_house.map((item,index) => (
                                <div  className="checkbox col-lg-6" onClick={(e) => {if(this.state.roomstuff_Essentials ==0 )this.setState({roomstuff_Essentials:1});else this.setState({roomstuff_Essentials:0});}}>
                                  <p className="Pinput" >
                                    <img className={this.state.roomstuff_Essentials ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                                  </p>
                                  <div className="divinput">
                                    <p>{item}</p>
                                  </div>
                                </div>
                              ))
                            }
                          </div>
                          <p className="textpink">Show all the rules of the house</p>
                      </div>

                      <div className="Strainer_Home_Type language">
                          <h6>Landlord language</h6>
                          <div>
                            {this.state.language.map((item,index) => (
                                <div  className="checkbox col-lg-6" onClick={(e) => {if(this.state.roomstuff_Essentials ==0 )this.setState({roomstuff_Essentials:1});else this.setState({roomstuff_Essentials:0});}}>
                                  <p className="Pinput" >
                                    <img className={this.state.roomstuff_Essentials ==1 ? 'show' : 'hide'} src="../images/checkdui.png" alt=""/>
                                  </p>
                                  <div className="divinput">
                                    <p>{item}</p>
                                  </div>
                                </div>
                              ))
                            }
                          </div>
                          <p className="textpink">Show all the languages that the landlord can use</p>
                      </div>

                      <div className="foot">
                          <span onClick={(e)=>this.setState({Strainer_More:false})}>Cancel</span>
                          <button onClick={(e)=>this.setState({Strainer_More:false})}>House</button>
                      </div>
                  </div>
              </li>
              </ul>
            </div>
		)
	}
}

export default Filter