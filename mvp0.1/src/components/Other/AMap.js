import React, { Component } from 'react';
import houselistingService from '../../services/houseinfolist-service';
import GoogleMapReact from 'google-map-react';
import { Map, Marker } from 'react-amap';
import ipfsService from '../../services/ipfs-service'
import { Link } from 'react-router-dom'
import ListingCard from './listing-card'

class AMap extends Component {

	constructor(props) {
		super(props);
		const _this = this;
		this.state = {
			amapkey: "5bfcb82509472c1e6486a350217cdeae",
			listingRows: [],
			longitude: '',
			latitude: '',
			Maphomeid: 1,
			zIndex: 100
		};

		this.mapEvents = {
			click(e) {
				setTimeout(
					() => _this.setState({
						ListingCard: 0
					}),
					10
				);
			}
		};

	}

	componentWillMount() {
		this.setState({
			listingRows: this.props.listingRows,
			longitude: this.props.listingRows[0].description.position.lng,
			latitude: this.props.listingRows[0].description.position.lat,
		})

	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			Maphomeid: nextProps.Maphomeid
		})
	}

	render() {

		return(
			<div id="Map">
        <Map 
          center={{longitude: this.state.longitude,latitude: this.state.latitude}} 
          zoom={6}
          events={this.mapEvents}
        >

        {this.state.listingRows.map(row => (
          <Marker position={{longitude: row.description.position.lng, latitude: row.description.position.lat}}  >
            {
              row.id == this.state.ListingCard &&
              <a target="_blank" href={`/listing/${row.id}`} > <ListingCard row={row} /> </a>
            }          
            {
              row.generateSmartContract == 1 &&
              <button className={this.state.Maphomeid == row.id ? "position positionActive" : "position"} onClick={(e)=>this.setState({ListingCard:row.id,longitude:row.description.position.lng,latitude: row.description.position.lat})}>{row.price} PPS</button>
            }

            {
              row.generateSmartContract == 0 &&
              <button className={this.state.Maphomeid == row.id ? "position positionActive" : "position"} onClick={(e)=>this.setState({ListingCard:row.id,longitude:row.description.position.lng,latitude: row.description.position.lat})}>{row.usdprice} USD</button>
            }
          </Marker>
        ))}
        </Map>
      </div>

		)
	}
}

export default AMap