import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Overlay from '../overlay';
import PropTypes from 'prop-types';
import GuestRegister from '../RegisterLogin/guest-register';
import { DateRangePicker } from 'react-dates';
import WalletClear from '../Wallet/walletClear';
import tagService from '../../services/tag-service';
import languageService from '../../services/language-service';
import guestService from '../../services/guest-service';
import web3service from '../../services/web3-service';
import { reactLocalStorage } from 'reactjs-localstorage';

class NavBar extends Component {

	constructor(props) {
		super(props);
		this.state = {
			checkInDate: null,
			checkOutDate: null,
			guests: null,
			place: null,
			locationName: "Tokyo",
			clicklogout: false,
			languagelist: {},
			userlogin: "hide",
			registered: false,
			userPictures: ""
		};
		window.searchCondition = this.state;
		web3service.loadWallet();
		languageService.language();
	}

	componentDidMount() {
		console.log(this.props)
		this.setState({
			languagelist: window.languagelist
		});
		guestService.getGuesterInfo(window.address).then((data) => {
			if(data.profile) {
				this.setState({
					userPictures: data.profile.profile
				});
			}
			this.setState({
				registered: true
			});
		});

	}

	locationName(e) {
		var DataName = e.currentTarget.getAttribute('data-name');
		this.setState({
			state: this.state.locationName = DataName
		});
	}

	onLogOut = (value) => {
		this.setState({
			clicklogout: value
		});
	}

	Onuserlogin = (value) => {
		this.setState({
			userlogin: value
		});
	}

	render() {
		const language = this.state.languagelist;

		return(

			<div className="headerbox">
    {this.props.hideTagHeader !="NO" && !this.props.renderChild &&
      <header className="header header__white">
      <nav className="nav navbar-nav navbar-right">
        <div className="navbar-header">
          <butoon  className="glyphicon glyphicon-align-justify navBtn" data-toggle="collapse" data-target="#example-navbar-collapse"></butoon>
          <a className="navbar-brand" href="../">
          {
            this.props.type == "experience" &&
            <img className="header__logo" src="../images/header_icon-09.png" alt=""/>
          }

          {
            this.props.type != "experience" &&
            <img className="header__logo" src="../images/logo.png" alt=""/>
          }
          </a>
        </div>
        <div className="collapse navbar-collapse" id="example-navbar-collapse">  
          <a className="navbar-brand" href="../">
            {
              this.props.type == "experience" &&
              <img className="header__logo" src="../images/header_icon-09.png" alt=""/>
            }

            {
              this.props.type != "experience" &&
              <img className="header__logo" src="../images/logo.png" alt=""/>
            }
          </a>
          <ul>
            <li className="Li1">
              <GuestRegister type='create' />
            </li>
            <li className="Li2">
              <Link to="/Intro">
                    {language.Become_an_organiser}
              </Link>
            </li>
            <li className="Li4 hide">
              <Link to="/Intro">
                    {language.Help} 
              </Link>
            </li>
            <li className="Li4">
              <GuestRegister type="login" />
            </li>
            <li className="Li5">
              <GuestRegister type='Signup' />
            </li>
            {!this.props.userimg &&
              <li className={this.state.registered ? "modalshow Li6" : "hide"}>
                <div className="userPhoto" onClick={(e) => window.location.href="/Info"} style={this.state.userPictures == '' ? {backgroundImage:"url(/images/uesrimg.png)"}:{backgroundImage:"url("+this.state.userPictures+")"}}>
                </div>
              </li>
            }
          </ul>
        </div>
      </nav>
      </header>
    }
    </div>

		)
	}
}

export default NavBar