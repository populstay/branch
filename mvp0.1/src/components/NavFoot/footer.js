import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { reactLocalStorage } from 'reactjs-localstorage';
import languageService from '../../services/language-service';
import GuestRegister from '../RegisterLogin/guest-register';

class Footer extends Component {

	constructor(props) {
		super(props);
		this.state = {
			CountryList: [],
			languagelist: {},
			languageIndex: 0,
		};
		window.searchCondition = this.state;
		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist,
			CountryList: window.languagelist.CountryList,
		});

		if(localStorage.getItem('languageIndex')) {
			this.setState({
				languageIndex: localStorage.getItem('languageIndex')
			});
		}
	}

	languageCookie(e, index) {
		var language = e.currentTarget.getAttribute('data-language');
		localStorage.setItem('language', language);
		localStorage.setItem('languageIndex', index);
		// languageService.language(language);
		window.location.reload()
	}
	render() {

		const language = this.state.languagelist;

		return(

			<div>
    {this.props.hideTagFooter !="NO" &&
    <footer className="footer">
        <div className="footer__brand-info container">
            <div className="footer_ul">
                <a href="/"><img className="footer__logo" src="../images/logo-2.png" alt="" /></a>
                <ul>
                    <GuestRegister type='footer_create' />
                    <a target="__blank" href="https://www.populstay.com" ><li>{language.About_Populstay}</li></a>
                    <li>{language.Stay_tuned}</li>
                </ul>
            </div>
            <div className="footer__dropdown pull-right">
                <div className="btn-group col-md-12">
                  <button type="button" data-toggle="dropdown"><img src={language.CountryList[this.state.languageIndex].img} />{language.CountryList[this.state.languageIndex].Country}<span>▼</span></button>
                  <ul className="dropdown-menu" role="menu">
                    {this.state.CountryList.map((item,index) => (
                        <li data-currency={item.currency} data-language={item.language}  onClick={(e)=>this.languageCookie(e,index)}><a  onClick={(e)=>this.setState({languageIndex:index})} ><img src={item.img} />{item.Country}</a></li>
                      ))
                    }
                  </ul>
                </div>
            </div>
        </div>
        <div className="footer__social-info bg-blue">
            <div className="container text-right"><span className="footer__copyright color-white pull-left">2018@copyright</span>
                <ul className="social">
                    <li><a className="social__facebook social__icon" target="__blank" href="https://fb.me/populstay"></a></li>
                    <li><a className="social__youtube social__icon" target="__blank" href="https://www.youtube.com/channel/UCAOiiN8HmppZ-qTeCW2pcwg"></a></li>
                    <li><a className="social__instagram social__icon" target="__blank" href="https://www.instagram.com/populstay/"></a></li>
                    <li><a className="social__telegram social__icon" target="__blank" href="https://t.me/PopulStayEn"></a></li>
                    <li><a className="social__twitter social__icon" target="__blank" href="https://twitter.com/populstay"></a></li>
                    <li><a className="social__medium social__icon" target="__blank" href="https://medium.com/populstay"></a></li>
                    <li><a className="social__naver social__icon" target="__blank" href="https://blog.naver.com/populstay"></a></li>
                    <li><a className="social__github social__icon" target="__blank" href="https://github.com/populstay"></a></li>
                    <li><a className="social__reddit social__icon" target="__blank" href="https://www.reddit.com/user/PopulStay"></a></li>
                    <li><a className="social__wechat social__icon" href="#wechatmodal" data-toggle="modal" data-target="#wechatModal"></a></li>
                    <li><a className="social__weibo social__icon" target="__blank" href="http://weibo.com/populstay"></a></li>
                </ul>
            </div>
        </div>
        <div className="modal fade qrcod-modal" id="wechatModal" tabindex="-1" role="dialog" aria-labelledby="wechatModalLabel">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header"><button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                    <div className="modal-body"><img src="../images/qrcode.jpg" alt=""/></div>
                </div>
            </div>
        </div>
    </footer>
    }
    </div>
		)
	}
}

export default Footer