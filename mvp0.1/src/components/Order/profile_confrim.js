import React from 'react';
import ReactDOM from 'react-dom';
import ppsService from '../../services/pps-service';
import web3Service from '../../services/web3-service';
import EthereumQRPlugin from 'ethereum-qr-code';
import Modal from 'react-modal';
import houselistingService from '../../services/houseinfolist-service';
import languageService from '../../services/language-service';
import aesService from '../../services/aes-service';
import guestService from '../../services/guest-service';
import Rules from '../Detail/Rules';
import Manual from '../Detail/Manual';
var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

class Confrim extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			checkInDate: "",
			checkOutDate: "",
			guest: 0,
			Totalprice: 0,
			DateDays: 0,
			price: 0,
			currency: 0,
			ethBalance: 0,
			modalIsOpen: false,
			languagelist: {},
			list: {},
			manual: [],
			Directions: [],
			Service_fees: 0
		};

		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		web3Service.loadWallet();
		languageService.language();
	}

	componentWillMount() {
		var that = this;

		//获取订单信息
		var OrderList = JSON.parse(sessionStorage.getItem('Order'));

		if(OrderList.price == 0) {
			this.setState({
				Totalprice: OrderList.ethprice,
				currency: "ETH",
			})
		} else if(OrderList.ethprice == 0) {
			this.setState({
				Totalprice: OrderList.price,
				currency: "PPS",
			})
		}

		this.setState({
			checkInDate: Number(OrderList.from),
			checkOutDate: Number(OrderList.to),
			guest: OrderList.guest,
			DateDays: Number(OrderList.days),
			listingId: OrderList.houseinfoid,
		})

		houselistingService.getHouseInfoDetailFromDB(OrderList.houseinfoid).then((data) => {
			console.log(data)

			this.setState({
				previewImage: data.profile.previewImage,
				place: data.place,
				guestsnumber: data.description.roombasics_guestsnumber,
				Directions: data.description.roomdescription_around,
				manual: data.description.roomdescription_Otherthings,
				Country: data.description.roomstuff_Country,
				City: data.description.roomstuff_City,
				Street: data.description.roomstuff_Street,
				Apt: data.description.roomstuff_Apt,
			})

			if(this.state.currency == "PPS") {
				this.setState({
					price: data.description.price_perday,
				})
			} else if(this.state.currency == "ETH") {
				this.setState({
					price: data.description.ETHprice_perday,
				})
			}

			if(data.hostAddress) {
				guestService.getGuesterInfo(data.hostAddress).then((data) => {
					this.setState({
						hostName: data.user
					});
				});
			}

			if(window.address) {
				guestService.getGuesterInfo(window.address).then((data) => {
					this.setState({
						guestName: data.user
					});
				});
			}

			if(data.bookedDate != undefined && data.bookedDate.data != undefined) {
				var DateLists = data.bookedDate.data;
			}

			for(var i = 0; i < DateLists.length; i++) {
				if(this.state.checkInDate == DateLists[i].start && this.state.checkOutDate == DateLists[i].end) {
					if(DateLists[i]) {
						this.setState({
							booked: true
						})
					} else {
						this.setState({
							booked: false
						})
					}
				}
			}

		})

	}

	Service_fees() {
		return this.state.Totalprice - this.state.price * this.state.DateDays
	}

	openModal() {
		this.setState({
			modalIsOpen: true
		});
	}

	closeModal() {
		this.setState({
			modalIsOpen: false
		});
	}

	componentDidMount() {
		this.setState({
			languagelist: window.languagelist
		});
	}

	render() {
		const language = this.state.languagelist;
		return(
			<div className="Confrim">

          <h3>{language.Youre_going_to} {this.state.place}!<p className="Right cursor" onClick={(e)=>window.print()}>{language.Print}</p></h3>
          <div className="box1 hide"><p className="Left"><a href="/Receipt" className="color-pink">{language.View_receipt}</a> or <span className="color-pink">{language.make_a_change_to_the_reservation}.</span></p><p className="Right cursor" onClick={(e)=>window.print()}>{language.Print}</p></div>

          <div className="box2">
            <div className="box3 col-sm-12 col-md-7 col-lg-7">
              <div className="box3_1">
                <p className="Left"><b>{language.Guests}</b> <p> {this.state.guest} {language.of} {this.state.guestsnumber} {language.accepted}</p></p>
                <button className="Right hide">{language.Manage_Guests}</button>
              </div>
  
              <div className="box3_2 hide">
                <div className="userlist">
                  <div><img src="/images/uesrimg.png" /></div>
                  <p className="text1">{this.state.guestName}</p>
                  <p className="text2">{language.Accepted}</p>
                </div>
              </div>
 
              <div className="box3_3 row">
                <div className="divleft col-sm-6 col-md-6 col-lg-6"><b>{language.Check_In}</b> <p className="Right">{language.language == "zh" ? moment(this.state.checkInDate).locale("zh-cn").format('LL') : moment(this.state.checkInDate).format('LL')}<br/>{language.After12PM}</p></div>
                <div className="divright col-sm-6 col-md-6 col-lg-6"><b>{language.Check_Out}</b> <p className="Right">{language.language == "zh" ? moment(this.state.checkOutDate).locale("zh-cn").format('LL') : moment(this.state.checkOutDate).format('LL')} <br/>{language.Before12PM}</p></div>
              </div> 

              <div className="box3_4 row">
                <div className="col-sm-4 col-md-4 col-lg-4"><b>{language.Address}</b></div>
                <div className="col-sm-8 col-md-8 col-lg-8">
                <p>{this.state.Country}</p>
                <p>{this.state.City}</p>
                <p>{this.state.Street}</p>
                <p>{this.state.Apt}</p>
                <p className="hide"><span className="color-pink">{language.Get_directions}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span className="color-pink">{language.View_listing}</span></p>
                </div>
              </div>

              <div className="box3_4 hide">
                <div className="col-sm-4 col-md-4 col-lg-4"><b>{language.Guidebook}</b></div>
                <div className="col-sm-8 col-md-8 col-lg-8">
                <p>Mo {language.recommended1place_to_visit_near_your_listing} </p>
                <p><span className="color-pink">{language.See} Mo’s {language.recommendations}</span></p>
                </div>
              </div>

              <div className="box3_4 row">
                <div className="col-sm-4 col-md-4 col-lg-4"><b>{language.House_Rules}</b></div>
                <div className="col-sm-8 col-md-8 col-lg-8">
                  <Rules listingId={this.state.listingId} type="confrim" />
                  <p className="fdx"></p>
                  <span className="color-pink hide">+ {language.more}</span>
                  <Manual type="confrim" listingId={this.state.listingId} />
                </div>
              </div>

              <div className="box3_5 row">
                <div className="col-sm-4 col-md-4 col-lg-4"><b>{language.Billing}</b></div>
                <div className="col-sm-8 col-md-8 col-lg-8">
                  <div className="overflow"><p className="Left">{this.state.DateDays} {language.nights} {language.total}</p>
                    <p className="Right">{this.state.Totalprice} &nbsp;/&nbsp; {this.state.currency}</p>
                  </div> 

                  <div className="overflow"><p className="Left">{language.Service_fees}</p>
                    <p className="Right">{this.Service_fees()} &nbsp;/&nbsp; {this.state.currency}</p>
                  </div>

                  <div className="overflow"><p className="Left">{language.Per_guest}</p>
                    <p className="Right">{this.state.price} &nbsp;/&nbsp; {this.state.currency}</p>
                  </div> 
                  <p><span className="color-pink">{language.Detailed_receipt}</span></p>
                </div>
              </div>

              <div className="box3_5 hide">
                <div className="col-sm-4 col-md-4 col-lg-4"><b>{language.Need_help}</b></div>
                <div className="col-sm-8 col-md-8 col-lg-8">
                  <p>{language.Visit_the} <span className="color-pink">{language.Help_Center}</span></p>
                </div>
              </div>


            </div>
            <div className="box4 col-sm-12 col-md-5 col-lg-5">
              <div className="box5 hide">
                  <div><div className="hostimg"><img src="/images/uesrimg.png" /></div></div>
                  <p><b>{language.Your_host} {this.state.hostName}</b></p> 
                  <p>{language.Request_a_landlord}</p>
                  {!this.state.booked &&
                    <button onClick={(e)=>this.booking(e)}>{language.Send_or_request_money}</button>
                  }

                  {this.state.booked &&
                    <button disabled className="buttonActive" >该订单已预定</button>
                  }
              </div>

              <div className="box6">
                <img src={this.state.previewImage} />
              </div>
  
              <div className="box7">
                  	<p><b>{language.How_to_get_around}</b></p>
                   	<pre>{this.state.Directions == "" ? language.The_landlord_did_not_add_information : this.state.Directions}</pre>

                  	<span className="color-pink hide">+ {language.more}</span>
              </div>

              <div className="box8"></div>
            
              <div className="box7">
                  	<p><b>{language.Other_things_to_note}</b></p>
                   	<pre>{this.state.manual == "" ? language.The_landlord_did_not_add_information : this.state.manual}</pre>

                  <span className="color-pink hide">+ {language.more}</span>
              </div>


            </div>
          </div>
      </div>
		);
	}
}
export default Confrim