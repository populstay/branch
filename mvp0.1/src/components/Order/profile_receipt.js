import React from 'react';
import ReactDOM from 'react-dom';
import Timestamp from 'react-timestamp';
import ppsService from '../../services/pps-service';
import languageService from '../../services/language-service';
import houselistingService from '../../services/houseinfolist-service';
import guestService from '../../services/guest-service';
var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

class Receipt extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			languagelist: {},
			checkInDate: '',
			checkOutDate: '',
			description: {}
		};

		languageService.language();
	}

	componentWillMount() {

		this.setState({
			languagelist: window.languagelist
		});

    //获取订单信息
    var OrderList = JSON.parse(sessionStorage.getItem('Order'));

    if(OrderList.price == 0) {
      this.setState({
        Totalprice: OrderList.ethprice,
        currency: "ETH",
      })
    } else if(OrderList.ethprice == 0) {
      this.setState({
        Totalprice: OrderList.price,
        currency: "PPS",
      })
    }

		this.setState({
			checkInDate: Number(OrderList.from),
			checkOutDate: Number(OrderList.to),
			guest: OrderList.guest,
			DateDays: Number(OrderList.days),
			listingId: OrderList.houseinfoid,
		})

		houselistingService.getHouseInfoDetailFromDB(OrderList.houseinfoid).then((data) => {

      this.setState({
        description: data.description,
        previewImage: data.profile.previewImage,
        place: data.place,
      })

      if(this.state.currency == "PPS") {
        this.setState({
          price: data.description.price_perday,
        })
      } else if(this.state.currency == "ETH") {
        this.setState({
          price: data.description.ETHprice_perday,
        })
      }

			if(data.hostAddress) {
				guestService.getGuesterInfo(data.hostAddress).then((data) => {
					this.setState({
						hostName: data.user
					});
				});
			}

			if(window.address) {
				guestService.getGuesterInfo(window.address).then((data) => {
					this.setState({
						guestName: data.user
					});
				});
			}

		})
	}

  Service_fees() {
    return this.state.Totalprice - this.state.price * this.state.DateDays
  }

	render() {
		const language = this.state.languagelist;
		return(

			<div className="Receipt">
          <div className="box1"><p className="Left"><span></span>{language.To_Itinerary}</p><p className="Right cursor"  onClick={(e)=>window.print()}>{language.Print}</p></div>

          {language.language == "zh" &&
            <h3>{language.Confirmed}: {this.state.place} {language.Check_in} {this.state.DateDays} {language.nights}  </h3>
          }

          {language.language != "zh" &&
            <h3>{language.Confirmed}: {this.state.DateDays} {language.nights} in {this.state.place}</h3>
          }

          <div className="box2">
              <p className="Left">{language.Booked_by} <b>{this.state.guestName}</b><br/> {language.language == "zh" ? moment(this.state.checkInDate).locale("zh-cn").format('dddd') : moment(this.state.checkInDate).format('dddd')}, {language.language == "zh" ? moment(this.state.checkInDate).locale("zh-cn").format('LL') : moment(this.state.checkInDate).format('LL')}</p>
              <p className="Right"><b>{language.order_number}</b><br/> HMFTDP9Q48</p>
          </div>
          <div className="box3">
            <div className="box4 col-sm-12 col-md-12 col-lg-5">
              <div className="box4_1">
                <p className="Left">{language.Check_In}</p>
                <p className="Right">{language.Check_Out}</p>
              </div>

              <div className="box4_2 row">
                <p className="col-sm-4 col-md-5 col-lg-4">{language.language == "zh" ? moment(this.state.checkInDate).locale("zh-cn").format('LL') : moment(this.state.checkInDate).format('LL')} </p>
                <p className="col-sm-4 col-md-2 col-lg-4 text-center"><span></span><span></span><span></span></p>
                <p className="col-sm-4 col-md-5 col-lg-4 text-right">{language.language == "zh" ? moment(this.state.checkOutDate).locale("zh-cn").format('LL') : moment(this.state.checkOutDate).format('LL')}</p>
              </div>

              <div className="box4_3 row">
                <h4>{language.Categorys[this.state.description.roomdescription_guests_have]}/{language.types[this.state.description.roomdescription_type]}</h4>
                <p>{this.state.description.roomstuff_Country}</p>
                <p>{this.state.description.roomstuff_City}</p>
                <p>{this.state.description.roomstuff_Apt}</p>
                <br/><br/>
                <p>{this.state.hostName}</p>
                <p>{language.Phone}: {this.state.description.roomdescription_phone}</p>
              </div>
              <div className="box4_4 row">
                <h4>{this.state.guest} {language.Travellers_on_this_trip}</h4>
                <div className="userlist">
                  <div><img src="/images/uesrimg.png" /></div>
                  <p>{this.state.guestName}</p>
                </div>
              </div>
            </div>
            <div className="box5 col-sm-12 col-md-12 col-lg-7">
              <div className="box5_top">
                  <h3>{language.Charges}</h3>
                  <ul>
                    <li>
                      <p className="Left">{language.Daily_Price}</p>
                      <p className="Right">{this.state.price}</p>
                    </li>
                    <li>
                      <p className="Left">{language.Cleaning_fees}</p>
                      <p className="Right">0</p>
                    </li>
                    <li>
                      <p className="Left">{language.Service_fees}</p>
                      <p className="Right">{this.Service_fees()}</p>
                    </li>
                  </ul>
                  <button><span className="Left">{language.Total}</span>
                    <span className="Right">{this.state.Totalprice}&nbsp;/&nbsp; {this.state.currency}</span>
                  </button>
              </div>
              <div className="box5_bottom">
                  <h3>{language.Payment}</h3>
                  <ul>
                    <li>
                      <p className="Left">{language.language == "zh" ? moment(new Date()).locale("zh-cn").format('LLLL') : moment(new Date()).format('LLLL')}</p>
                      <p className="Right"> {this.state.currency}</p>
                    </li>
                    <li>
                      <h4 className="Left">{language.Total_Remaining}</h4>
                      <h4 className="Right"> {this.state.currency}</h4>
                    </li>
                  </ul>
                  <span className="Adddetails">{language.Add_billing_details}</span>
              </div>
            </div>
          </div>

          <div className="box6">
              <h4>{language.Cost_per_traveler}</h4>
                <p>
                {language.language != "zh" &&
                  <p>{language.This_trip_was} <b>{this.state.price} {this.state.currency}</b> {language.per_person_per_night},</p>
                }
                {language.language == "zh" &&
                  <p>{language.This_trip_was} {language.per_person_per_night} <b>{this.state.price} {this.state.currency}</b> ,</p>
                }
                </p>
              <p>{language.including_taxes_and_other_fees}.</p>
          </div>

          <div className="box7">
            <ul> 
              <li>
                <h4 className="Left">{language.Need_help}?</h4>
                <p className="Right">HMFTDP9Q48</p>
              </li>
              <li>
                <p className="Left">{language.Visit_the} <span>{language.Help_Center}</span> {language.for_any_questions}.</p>
                <p className="Right">{language.Booked_by} <b>{this.state.guestName}</b></p>
              </li>
              <li>
                <p className="Right">{language.language == "zh" ? moment(new Date()).locale("zh-cn").format('LL') : moment(new Date()).format('LL')}</p>
              </li>
            </ul>
          </div>
          
          <div className="box8">
            <p>{language.Cancellation_policy}:{language.Certain_fees_and_taxes_may_be_nonrefundable}.<span><a href="/Cancellations"  target="_blank">{language.See_here_for_more_details}</a></span></p>
            <p>{language.conversion_fee_was_applied_to_this_booking}.</p>
            <p>{language.PopulStay_Payments_UK_Ltd_is_a_limited_collection_agent_of_your_Host} <span> <a href="https://www.PopulStay.com"> https://www.PopulStay.com </a>。</span> {language.Questions_or_complaints_contact_Airbnb_Payments}</p>
            <br/>
            <p>{language.Payment_processed_by}:</p>
            <p>{language.PopulStay_Payments_UK_Ltd}.</p>
            <p>{language.ComptonSt}.</p>
            <p>{language.London}</p>
            <p>{language.EC1V_0AP}</p>
            <p>{language.United_Kingdom}</p>
            <br/>
            <p>{language.PopulStay_Ireland_UC}</p>
            <p>{language.The_Watermarque_Building}</p>
            <p>{language.South_Lotts_Road}</p>
            <p>{language.Ringsend_Dublin4}</p>
            <p>{language.Ireland}</p>
            <p>{language.VAT_NumberIE9827384L}</p>
            <img src="/images/logo_grey.png" />
          </div>
      </div>
		);
	}
}
export default Receipt