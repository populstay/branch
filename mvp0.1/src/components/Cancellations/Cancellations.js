import React, { Component } from 'react';
import languageService from '../../services/language-service';
import Flexible from './Flexible';
import Moderate from './Moderate';
import Strict from './Strict';

class Cancellations extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
			Cancellations: 0,
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist
		});

		var href = window.location.href;
		if(href.split("?")[1]){
			var params = href.split("?")[1];
			var paramArr = params.split('&');
			var res = {};
			for(var i = 0; i < paramArr.length; i++) {
				var str = paramArr[i].split('=');
				res[str[0]] = decodeURI(str[1]);
			}
			if(res.Cancellations){
				this.setState({
					Cancellations:res.Cancellations
				})
			}
		}


	}
	render() {

		const language = this.state.languagelist;

		return(
			<div className="Cancellations">
          <h2>{language.Cancellation_Policies}</h2>
          <p>{language.Populstay_Cancellation_Policies}</p>

          <div className="CancellationsList" > 
              <ul className="navList">
                  <li className={this.state.Cancellations == 0 ? "active" : ""} onClick={(e)=>this.setState({Cancellations:0})} >{language.Flexible}</li>
                  <li className={this.state.Cancellations == 1 ? "active" : ""} onClick={(e)=>this.setState({Cancellations:1})}>{language.Moderate}</li>
                  <li className={this.state.Cancellations == 2 ? "active" : ""} onClick={(e)=>this.setState({Cancellations:2})}>{language.Strict}</li>
              </ul>

              {this.state.Cancellations == 0 &&
                <Flexible />
              }

              {this.state.Cancellations == 1 &&
                <Moderate />
              }

              {this.state.Cancellations == 2 &&
                <Strict />
              }
          </div>
        </div>
		)
	}
}

export default Cancellations