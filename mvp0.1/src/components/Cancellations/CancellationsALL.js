import React, { Component } from 'react';
import languageService from '../../services/language-service';

class CancellationsALL extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist
		});
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div>
              <ul>
                <li>{language.Cancellations1}</li>
                <li>{language.Cancellations2}</li>
                <li>{language.Cancellations3}</li>
                <li>{language.Cancellations4}</li>
                <li>{language.Cancellations5}</li>
                <li>{language.Cancellations6}</li>
                <li>{language.Cancellations7}</li>
              </ul>
              <p>{language.Example}</p>
          </div>
		)
	}
}

export default CancellationsALL