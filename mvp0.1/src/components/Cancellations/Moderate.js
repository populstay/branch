import React, { Component } from 'react';
import languageService from '../../services/language-service';
import CancellationsALL from './CancellationsALL';
var moment = require('moment');
require('moment/locale/zh-cn');
require('moment/locale/en-au');

class Moderate extends Component {

	constructor(props) {
		super(props)

		this.state = {
			languagelist: {},
		};

		languageService.language();
	}

	componentWillMount() {
		this.setState({
			languagelist: window.languagelist
		});
	}

	render() {

		const language = this.state.languagelist;

		return(
			<div className="List" >
          <h3>{language.Moderate}: {language.Full_refund_within_limited_period}</h3>
          <CancellationsALL />

          <div className="row">
            <div className="col-sm-4 col-md-4 col-lg-4 box1">
                <p className="textDAY" >{language.days5_prior}</p>
                <p className="textTIME">{language.language == "zh" ? moment(new Date().getTime()-120*60*60*1000).locale("zh-cn").format('ll')+"下午3点" : moment(new Date().getTime()-120*60*60*1000).format('ll')+", 3:00 PM" }</p>
                <p className="textDETAIL">{language.Moderate_Cancellations1}</p>
            </div>

            <div className="col-sm-4 col-md-4 col-lg-4 box2">
                <p className="textDAY" >{language.Check_in}</p>
                <p className="textTIME">{language.language == "zh" ? moment(new Date()).locale("zh-cn").format('ll')+"下午3点" : moment(new Date()).format('ll')+", 3:00 PM" }</p>
                <p className="textDETAIL">{language.Moderate_Cancellations2}</p>
            </div>

            <div className="col-sm-4 col-md-4 col-lg-4 box3">
                <p className="textDAY" >{language.Check_out}</p>
                <p className="textTIME">{language.language == "zh" ? moment(new Date().getTime()+72*60*60*1000).locale("zh-cn").format('ll')+"上午11点" : moment(new Date().getTime()+72*60*60*1000).format('ll')+", 11:00 AM" }</p>
                <p className="textDETAIL">{language.Moderate_Cancellations3}</p>
            </div>
          </div>
        </div>
		)
	}
}

export default Moderate